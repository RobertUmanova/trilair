﻿using ImageMagick;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace TrailAir.Utility
{
    /// <summary>
    /// Classed used to Resize and Compress Images
    /// </summary>
    public class ImageFileUtil
    {

        /// <summary>
        /// Resize Image to MaxSizePixels, set MaxSizePixels to default 1920 <br/><br/>
        /// Example ( if MaxSizePixels = 1280): Original 3000x2000  resized to => 1280*853, <br/> 
        /// Example ( if MaxSizePixels = 1280): Original 2000x3000  resized to => 853*1280, <br/>
        /// </summary>
        /// <param name="image"></param>
        /// <returns>Resized image</returns>
        public static Image ResizeImage(Image image, int MaxSizePixels)
        {
            if (image.Width > MaxSizePixels || image.Height > MaxSizePixels)
            {
                float sourceWidth = image.Width;
                float sourceHeight = image.Height;
                float resizedImageHeight = MaxSizePixels;
                float resizedImageWidth = MaxSizePixels;

                // 5000*2000 => 1280*512
                if (image.Width >= image.Height)
                {
                    resizedImageHeight = MaxSizePixels / (sourceWidth / sourceHeight);
                }
                // 2000*5000 => 512*1280
                else if (image.Width < image.Height)
                {
                    resizedImageWidth = MaxSizePixels / (sourceHeight / sourceWidth);
                }

                //The Bitmap object is declared to get the image in pixel data
                Bitmap bitmap = new((int)resizedImageWidth, (int)resizedImageHeight);

                Graphics g = Graphics.FromImage((Image)bitmap);
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                // Draw image with new width and height  
                g.DrawImage(image, 0, 0, resizedImageWidth, resizedImageHeight);
                g.Dispose();
                return bitmap;
            }
            else
            {
                return image;
            }

        }

        /// <summary>
        /// Compress image by QualityCompresspercentage, set qualityCompressPercentage to default 70L <br/><br/>
        /// </summary>
        /// <param name="image"></param>
        /// <param name="QualityCompressPercentage"></param>
        /// <returns>byte array of compressed image</returns>
        public static byte[] CompressImage(Image image, long qualityCompressPercentage)
        {
            byte[] outputBytes;
            ImageCodecInfo myImageCodecInfo = ImageCodecInfo.GetImageEncoders().First(_ => _.FormatID == ImageFormat.Jpeg.Guid);
            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;

            EncoderParameters myEncoderParameters = new(1);
            EncoderParameter myEncoderParameter = new(myEncoder, qualityCompressPercentage);
            myEncoderParameters.Param[0] = myEncoderParameter;

            using (var outputStream = new MemoryStream())
            {
                try
                {
                    image.Save(outputStream, myImageCodecInfo, myEncoderParameters);
                }
                catch (Exception ex)
                {
                    throw;
                }
                outputBytes = outputStream.ToArray();
            }
            return outputBytes;
        }

        public static byte[] ConvertImageToJpg(byte[] data)
        {
            try
            {
                using (var image = new MagickImage(data))
                {
                    using (var stream = new MemoryStream())
                    {
                        image.Write(stream, MagickFormat.Jpg);
                        return stream.ToArray();
                    }
                }
            }
            catch (MagickException) // Catch Magick ExceptionErrors
            {
                throw;
            }
            catch (Exception) // Catch Exception Errors
            {
                throw;
            }
        }

        public static byte[] MakeThumbnail(Image image, int thumbnailWidth, int thumbnailHeight)
        {
            byte[] thumbArray = null;

            Image thumb = image.GetThumbnailImage(thumbnailWidth, thumbnailHeight, () => false, IntPtr.Zero);
            using (MemoryStream thumbStream = new MemoryStream())
            {
                thumb.Save(thumbStream, ImageFormat.Jpeg);
                thumbArray = thumbStream.ToArray();
            }

            return thumbArray;
        }

        /// <summary>
        /// Resizes image using Magick lib <br/><br/>
        /// </summary>
        public static byte[] ResizeImage(byte[] data, int MaxSizePixels)
        {
            try
            {
                using (var image = new MagickImage(data))
                {
                    float sourceWidth = image.Width;
                    float sourceHeight = image.Height;
                    float resizedImageHeight = MaxSizePixels;
                    float resizedImageWidth = MaxSizePixels;

                    // 5000*2000 => 1280*512
                    if (image.Width >= image.Height)
                    {
                        resizedImageHeight = MaxSizePixels / (sourceWidth / sourceHeight);
                    }
                    // 2000*5000 => 512*1280
                    else if (image.Width < image.Height)
                    {
                        resizedImageWidth = MaxSizePixels / (sourceHeight / sourceWidth);
                    }

                    var geometry = new MagickGeometry
                    {
                        Height = (int)resizedImageHeight,
                        Width = (int)resizedImageWidth,
                        IgnoreAspectRatio = false,
                        Greater = true
                    };
                    image.Resize(geometry);

                    using (var stream = new MemoryStream())
                    {
                        image.Write(stream, MagickFormat.Jpg);
                        return stream.ToArray();
                    }
                }
            }
            catch (MagickException) // Catch Magick ExceptionErrors
            {
                throw;
            }
            catch (Exception) // Catch Exception Errors
            {
                throw;
            }
        }
    }
}
