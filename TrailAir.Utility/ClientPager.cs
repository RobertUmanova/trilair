﻿
namespace TrailAir.Utility
{
    public class ClientPager<T>
    {
        public List<T> PageItems
        {
            get;
            private set;
        }

        public int PageCount
        {
            get;
            private set;
        }

        public int CurrentPage
        {
            get;
            private set;
        }

        public ClientPager(List<T> clientList, PageInfo pageInfo)
        {
            PageCount = pageInfo.PageCount;
            CurrentPage = pageInfo.CurrentPage;
            PageItems = clientList;
        }


    }
}
