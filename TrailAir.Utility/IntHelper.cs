﻿
namespace TrailAir.Utility
{
    public static class IntHelper
    {
        public static int? StringToInt(string value)
        {
            if (string.IsNullOrEmpty(value))
                return 0;

            try
            {
                return Convert.ToInt32(value);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static List<int> StringToIntList(string value, char delimiter)
        {
            if (string.IsNullOrEmpty(value))
                return null;

            try
            {
                return value.Split(delimiter).Select(int.Parse).ToList();
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
