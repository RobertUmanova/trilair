﻿using System.Text;

namespace TrailAir.Utility
{
    public static class StringHelper
    {
        public static void Concatenate(StringBuilder result, object stringToAdd, string separator = ", ")
        {
            if (stringToAdd == null)
                return;

            if (stringToAdd.ToString().Length == 0)
                return;

            if (result.Length > 0)
                result.Append(separator);

            result.Append(stringToAdd.ToString());
        }

        public static bool IsNullOrEmptyTrim(string value)
        {
            if (value == null)
                return true;

            return string.IsNullOrEmpty(value.Trim());
        }



        public static int? ToIntNull(string value)
        {
            if (string.IsNullOrEmpty(value))
                return null;

            try
            {
                return int.Parse(value);
            }
            catch
            {
                return null;
            }

        }

        public static int ToInt(string value, int onNull)
        {
            var result = ToIntNull(value);

            if (result == null)
                return onNull;

            return result.Value;
        }

        public static bool DoesNIRContainsOnlyNumbersAndAorB(string nir)
        {
            var nirArray = nir.ToCharArray();

            nirArray = nirArray.Where(c => !char.IsDigit(c)).ToArray();
            if (nirArray.Length > 0)
            {
                foreach (var ch in nirArray)
                {
                    if (!ch.ToString().Equals("a", StringComparison.OrdinalIgnoreCase) && !ch.ToString().Equals("b", StringComparison.OrdinalIgnoreCase)) return false;
                }
            }
            return true;
        }
        public static bool ContainsInsensitive(this string source, string toCheck)
        {
            bool result = source.IndexOf(toCheck, StringComparison.OrdinalIgnoreCase) >= 0;

            return result;
        }

        /// <summary>
        /// Trims the long string to a N letters string + "..."
        /// </summary>
        /// <param name="numberOfLetters">Default is 30</param>
        /// <returns></returns>
        public static string TrimString(string input, int numberOfLetters = 30)
        {
            if (string.IsNullOrEmpty(input) || input.Length < numberOfLetters)
            {
                return input;
            }
            string trimmedInput = input.Substring(0, numberOfLetters) + "...";

            return trimmedInput;
        }
    }
}
