﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace TrailAir.Utility
{
    public class CloneUtility
    {
        /// <summary>
        /// Object need to be [Serializable]
        /// </summary>
        /// <typeparam name="T">[Serializable]</typeparam>
        /// <param name="obj">[Serializable]</param>
        /// <returns></returns>
        public static T DeepClone<T>(T obj)
        {
            try
            {
                using (var ms = new MemoryStream())
                {
                    var formatter = new BinaryFormatter();
                    formatter.Serialize(ms, obj);
                    ms.Position = 0;

                    return (T)formatter.Deserialize(ms);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}