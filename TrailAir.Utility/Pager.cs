﻿
namespace TrailAir.Utility
{
    public class PageInfo
    {
        public int PageCount
        {
            get;
            set;
        }

        public int CurrentPage
        {
            get;
            set;
        }

        public int PageSize
        {
            get;
            set;
        }
    }

    public class Pager<T>
    {
        public const int DefaultPageSize = 5;

        public List<T> Page
        {
            get;
            private set;
        }

        public PageInfo Info
        {
            get;
            private set;
        }

        public Pager(IEnumerable<T> items, int? page, int? pageSize = DefaultPageSize)
        {
            Info = new PageInfo();
            Info.CurrentPage = (page == null) ? 1 : page.Value;
            Info.PageSize = (pageSize == null) ? DefaultPageSize : pageSize.Value;
            Info.PageCount = (int)Math.Ceiling((decimal)items.Count() / (decimal)Info.PageSize);
            Page = items.Skip((Info.CurrentPage - 1) * Info.PageSize).Take(Info.PageSize).ToList();
        }
    }
}
