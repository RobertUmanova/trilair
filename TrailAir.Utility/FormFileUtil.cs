﻿using Microsoft.AspNetCore.Http;
using System.Drawing;

namespace TrailAir.Utility
{
    public static class FormFileUtil
    { 
        public static async Task<byte[]> GetBytes(IFormFile formFile)
        {
            using var memoryStream = new MemoryStream();
            await formFile.CopyToAsync(memoryStream);
           
            if (formFile.ContentType.Contains("image")) 
            {
                Image image = Image.FromStream(memoryStream);
                image = ImageFileUtil.ResizeImage(image, 1920);
                return ImageFileUtil.CompressImage(image, 70L);
            }
            return memoryStream.ToArray();
        }
    }
}
