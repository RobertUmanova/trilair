﻿
namespace TrailAir.Utility
{
    /// <summary>
    /// Time in string
    /// 13 = 1PM
    /// 12:23 = you know :) 
    /// Time in Int
    /// 13 = 00:13
    /// 1300 = 1PM
    /// 230 = 2am:30min
    /// </summary>
    public class TimeOb
    {

        public bool IsTimeValid { get; private set; }
        public int TimeInt { get; private set; }
        public string TimeStr { get; private set; }
        public int TimeHour { get; private set; }
        public int TimeMinutes { get; private set; }


        public TimeOb(string strVal)
        {
            IsTimeValid = IsTimeValidAndGetValues(strVal, out int? hourInt, out int? hourHoursInt, out int? hourMinutesInt);
            if (IsTimeValid)
            {
                TimeInt = hourInt.Value;
                TimeStr = strVal;
                TimeHour = hourHoursInt.Value;
                TimeMinutes = hourMinutesInt.Value;
            }
        }

        public TimeOb(int intVal)
        {
            if (intVal < 0 && intVal > 2359)
                throw new Exception($"Invalid time found in the Database: {intVal}");

            IsTimeValid = true;
            TimeInt = intVal;

            TimeHour = (int)(intVal / 100);
            TimeMinutes = intVal - (TimeHour * 100);

            TimeStr = $"{TimeHour}:{TimeMinutes.ToString().PadLeft(2, '0')}";
        }

        public TimeOb(DateTime dateTime) : this(dateTime.Hour * 100 + dateTime.Minute)
        {
        }

        public static bool IsTimeValidAndGetValues(string strVal, out int? intTimeVal)
        {
            int? intTimeHourVal; int? intTimeMinutesVal;
            return IsTimeValidAndGetValues(strVal, out intTimeVal, out intTimeHourVal, out intTimeMinutesVal);
        }

        public DateTime AddTimeToDate(DateTime dateToAddTime)
        {
            return new DateTime(dateToAddTime.Year, dateToAddTime.Month, dateToAddTime.Day, TimeHour, TimeMinutes, 0);
        }

        public static bool IsTimeValidAndGetValues(string strVal, out int? intTimeVal, out int? intTimeHourVal, out int? intTimeMinutesVal)
        {
            bool IsTimeValid = false;
            intTimeVal = null;
            intTimeHourVal = null;
            intTimeMinutesVal = null;

            if (string.IsNullOrEmpty(strVal))
                return false;

            var splited = strVal.Split(':');

            if (splited.Length != 1 && splited.Length != 2)
                return false;


            if (splited.Length == 1 && strVal.Length == 4)
            {
                var hourStr = strVal.Substring(0, 2);
                if (!int.TryParse(hourStr, out int hourInt))
                    return false;

                if (hourInt < 0 || hourInt > 23)
                    return false;


                var minutesStr = strVal.Substring(2, 2);

                if (!int.TryParse(minutesStr, out int minutesInt))
                    return false;

                if (minutesInt < 0 || minutesInt > 59)
                    return false;


                intTimeHourVal = hourInt;
                intTimeMinutesVal = minutesInt;
                intTimeVal = hourInt * 100 + minutesInt;
                return true;
            }
            else
            {
                var hourStr = splited[0];
                if (!int.TryParse(hourStr, out int hourInt))
                    return false;

                if (hourInt < 0 || hourInt > 23)
                    return false;

                var minutesStr = "0";
                if (splited.Length == 2)
                    minutesStr = splited[1];

                if (!int.TryParse(minutesStr, out int minutesInt))
                    return false;

                if (minutesInt < 0 || minutesInt > 59)
                    return false;


                intTimeHourVal = hourInt;
                intTimeMinutesVal = minutesInt;
                intTimeVal = hourInt * 100 + minutesInt;
                return true;
            }

        }

        public static string GetTimeStringValue(int? intTime)
        {
            if (intTime == null)
                return string.Empty;

            var timeOb = new TimeOb(intTime.Value);
            return timeOb.TimeStr;
        }
        public static int? GetTimeIntValue(string strTime)
        {
            if (string.IsNullOrEmpty(strTime))
                return null;

            var timeOb = new TimeOb(strTime);
            return timeOb.TimeInt;
        }
    }
}
