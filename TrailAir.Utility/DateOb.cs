﻿using System.Globalization;

namespace TrailAir.Utility
{
    public class DateOb
    {
        public static string DateFormat = "dd/MM/yyyy";
        public static string DateTimeFormat = "dd/MM/yyyy H:mm";
        public static string TimeFormat = "H:mm";
        private string CurrentDateFormat
        {
            get
            {
                if (IgnoreTime == null || IgnoreTime.Value == true)
                    return DateFormat;
                return DateTimeFormat;
            }
        }
        private DateTime? DateVal=null;
        public bool? IgnoreTime = null;
        private DateTime? startTreatmentDate;

        public DateTime? DateValue
        {
            get
            {
                return DateVal;
            }
        }

        /// <summary>
        /// Date time from two different date time objects 
        /// </summary>
        /// <param name="date"></param>
        /// <param name="time"></param>
        public DateOb(DateTime date, DateTime time) : this(date: UpdateDateWithTime(date, time).Value, ignoreTime: false)
        {
        }

        /// <summary>
        /// Date time from date time ob and int time 
        /// </summary>
        /// <param name="date"></param>
        /// <param name="time">hhmm?</param>
        public DateOb(DateTime date, int time) : this(date: UpdateDateWithTime(date, time).Value, ignoreTime: false)
        {
        }

        public DateOb(DateTime? date) : this(date, true)
        {

        }

        public DateOb(DateTime? date, bool ignoreTime = true)
        {
            if (IgnoreTime == null)
                IgnoreTime = ignoreTime;

            if (ignoreTime)
                DateVal = date.Value.Date;
            else
                DateVal = date;

        }

        public static string PrintDate(DateTime? date, bool ignoreTime = true) 
        {
            if (date == null || date.Value.Year < 1900)
                return string.Empty;

            return new DateOb(date, ignoreTime).ToString();
        }

        public DateOb(string date, bool ignoreTime = true) :
            this(ParseStringToDate(date), ShouldIgnoreTime(date, ignoreTime))
        {
        }


        public static DateTime? UpdateDateWithTime(DateTime? date, DateTime? time)
        {
            if (date == null || time == null)
                return null;

            DateTime result = new DateTime(
                date.Value.Year, date.Value.Month, date.Value.Day,
                time.Value.Hour, time.Value.Minute,
                0
                );

            return result;
        }

      
        public static int GetDiffInDays(DateOb start, DateOb end)
        {
            if (start.DateVal == null || end.DateVal == null)
                return 0;

            return (int)(end.DateVal.Value - start.DateVal.Value).TotalDays; 
        }

        public static DateTime? UpdateDateWithTime(DateTime? date, int? time)
        {
            if (date == null || time == null)
                return null;
            var timeVal = new TimeOb(time.Value);
            DateTime result = new DateTime(
                date.Value.Year, date.Value.Month, date.Value.Day,
                timeVal.TimeHour, timeVal.TimeMinutes,
                0
                );

            return result;
        }


        public static string DateToSting(DateTime? date, bool ignoreTime = true)
        {
            if (date == null)
                return string.Empty;
            return new DateOb(date.Value).ToString();
        }

        public static bool ShouldIgnoreTime(string date, bool ignoreTime)
        {
            int dtLength = date.Length;
            if (dtLength == 8)
            {
                return true;
            }
            else if (dtLength == 14)
            {
                return false;
            }

            return ignoreTime;
        }

        public static string ToFromToStr(DateTime? startDateTime, DateTime? endDateTime)
        {
            string result = string.Empty;

            if (startDateTime == null)
                return result;

            result = DateOb.DateToString(startDateTime.Value, false);
            if (endDateTime == null)
                return result;

            string endTime = ToTimeString(endDateTime.Value, " - ");

            return $"{result}{endTime}";
        }

        public static DateTime ParseStringToDate(string date)
        {
            int dtLength = date.Length;

            if (date.Contains("/") && dtLength <= 10)
            {
                ///ex 31/12/20 

                var dates = date.Split('/');
                var day = Convert.ToInt32(dates[0]);
                var month = Convert.ToInt32(dates[1]);
                var year = Convert.ToInt32(dates[2]);

                if (year < 100)
                    year = 2000 + year;

                return new DateTime(year, month, day);
            }
            if (dtLength == 8 || dtLength == 14)
            {
                var year = Convert.ToInt32(date.Substring(0, 4));
                var month = Convert.ToInt32(date.Substring(4, 2));
                var day = Convert.ToInt32(date.Substring(6, 2));

                if (dtLength == 8)
                {
                    return new DateTime(year, month, day);
                }
                else
                {
                }

                var hour = Convert.ToInt32(date.Substring(8, 2));
                var minute = Convert.ToInt32(date.Substring(10, 2));
                var second = Convert.ToInt32(date.Substring(12, 2));

                return new DateTime(year, month, day, hour, minute, second);
            }
            if (date.Trim().Contains("-") && dtLength == 19)
            //31-07-2020 05:32:00
            {
                var dateTime = date.Trim().Split(' ');

                var dateVals = dateTime[0].Split('-');

                var year = Convert.ToInt32(dateVals[2]);
                var month = Convert.ToInt32(dateVals[1]);
                var day = Convert.ToInt32(dateVals[0]);

                var timeVals = dateTime[1].Split(':');

                var hour = Convert.ToInt32(timeVals[0]);
                var minute = Convert.ToInt32(timeVals[1]);
                var second = Convert.ToInt32(timeVals[2]);

                return new DateTime(year, month, day, hour, minute, second);
            }

            // TODO: Maybe here put some error if you cannot parse DateTime and return handled error instead of 500 error
            return DateTime.ParseExact(date, DateFormat, CultureInfo.InvariantCulture);
        }

        public static DateTime? ParseStringToNullableDate(string date)
        {
            if (string.IsNullOrEmpty(date))
                return null;

            return ParseStringToDate(date);
        }

        public string ToDateTimeString()
        {
            return this.ToString();
        }
        public override string ToString()
        {
            if (DateValue == null)
                return string.Empty;

            var result = DateVal.Value.ToString(CurrentDateFormat);
            if (DateFormat.Contains("/") && result.Contains("."))
                result = result.Replace(".", "/");

            return result;
        }
        public string ToJson()
        {
            return DateOb.ToJson(this.DateVal);
        }

        public static string ToJson(DateTime? date)
        {
            if (date == null)
                return string.Empty;

            var result = $"{date.Value.Year}-{date.Value.Month}-{date.Value.Day}";
            return result;
        }

        public string ToTimeString()
        {

            if (this.IgnoreTime.Value)
                return "";

            return DateOb.ToTimeString(this.DateVal);


        }

        public static string ToTimeString(DateTime? time, string prefix = null)
        {
            if (time == null)
                return "";

            //string.Format("{0}:{1}", 
            //               this.DateVal.Hour.ToString().PadLeft(2, '0'), 
            //               this.DateVal.Minute.ToString().PadLeft(2, '0'));
            return $"{prefix}{time.Value.ToString(TimeFormat)}";
        }

        public static string DateToString(DateTime date, bool ignoreTime = true)
        {
            var dateOb = new DateOb(date, ignoreTime);
            return dateOb.ToString();
        }



        public static DateTime? StringToDate(string date, bool ignoreTime = true)
        {
            var dateOb = new DateOb(date, ignoreTime);
            return dateOb.DateVal;
        }

        public static implicit operator string(DateOb d)
        {
            return d.ToString();
        }

        public static implicit operator DateTime(DateOb d)
        {
            if (d.DateVal == null)
                return DateTime.MinValue;
            return d.DateVal.Value;
        }

        public static implicit operator DateOb(string d)
        {
            return new DateOb(d);
        }

        public static implicit operator DateOb(DateTime d)
        {
            return new DateOb(d);
        }


        public static implicit operator DateOb(DateTime? d)
        {
            return new DateOb(d);
        }


        public static implicit operator DateTime?(DateOb d)
        {           
            return d.DateVal;
        }

    }
}
