﻿using TrailAir.Utility.ExceptionHelpers;
namespace TrailAir.Utility
{
    public class JsonResponseOb
    {

        public bool success { get; set; }
        public object data { get; set; }
        public string errorMessage { get; set; }
        public string warningMessage { get; set; }

        public static object GetError(Exception ex)
        {
            return new JsonResponseOb
            {
                success = false,
                errorMessage = ExceptionObject.GetErrorMessage(ex)
            };
        }


        public static object GetSuccess(object response, string warningMessage = null)
        {
            return new JsonResponseOb
            {
                success = true,
                data = response,
                warningMessage = warningMessage
            };
        }


        public static object GetCustomError(string message)
        {
            return new JsonResponseOb
            {
                success = false,
                errorMessage = message
            };
        }
    }
}
