﻿using MimeKit;
using TrailAir.Utility.Configuration;

namespace TrailAir.Utility
{
    public class MailHelper
    {

        //PORT
        private int txtClientPort;

        private string txtFrom;
        //TO
        private string txtTo;
        //USERNAME
        private string txtUserName;
        //PASSWORD
        private string txtPassword;
        //SMTP
        private string txtClientHost;

        private MailKit.Net.Smtp.SmtpClient client;

        private string _txtSubject;
        private string _txtMessage;


        public MailHelper(string txtSubject, string txtMessage, string mailFrom, string mailTo)
        {
            txtClientPort = EmailSetup.ClientPort;
            txtClientHost = EmailSetup.ClientHost;
            client = new MailKit.Net.Smtp.SmtpClient();
            txtFrom = mailFrom;
            txtTo = mailTo;

            txtUserName = EmailSetup.USerName;
            txtPassword = EmailSetup.Password;

            _txtSubject = txtSubject;
            _txtMessage = txtMessage;
        }





        public void Send(bool isHtml = false)
        {
            var builder = new BodyBuilder();
            var mail = new MimeMessage();

            mail.From.Add(new MailboxAddress("", txtFrom));
            mail.To.Add(new MailboxAddress("", txtTo));
            mail.Subject = _txtSubject;
            if (isHtml)
                builder.HtmlBody = _txtMessage;
            else
                builder.TextBody = _txtMessage;

            mail.Body = builder.ToMessageBody();

            client.Connect(txtClientHost, txtClientPort, true);
            client.AuthenticationMechanisms.Remove("XOAUTH2");
            client.Authenticate(txtUserName, txtPassword);
            client.Send(mail);
            client.Disconnect(true);

        }

        public void SendToMultiple(bool isHtml = false)
        {
            var builder = new BodyBuilder();
            var mail = new MimeMessage();

            mail.From.Add(new MailboxAddress("", txtFrom));
            foreach (var address in txtTo.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
            {
                mail.To.Add(new MailboxAddress("", address));
            }
            //mail.To.Add(new MailboxAddress("", txtTo));
            mail.Subject = _txtSubject;
            if (isHtml)
                builder.HtmlBody = _txtMessage;
            else
                builder.TextBody = _txtMessage;

            mail.Body = builder.ToMessageBody();

            client.Connect(txtClientHost, txtClientPort, true);
            client.AuthenticationMechanisms.Remove("XOAUTH2");
            client.Authenticate(txtUserName, txtPassword);
            client.Send(mail);
            client.Disconnect(true);

        }
    }
}
