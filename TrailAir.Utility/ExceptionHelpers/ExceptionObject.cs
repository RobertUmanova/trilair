﻿
namespace TrailAir.Utility.ExceptionHelpers
{
    public class ExceptionObject
    {
        public static string GetErrorMessage(Exception ex)
        {
            if (ex.InnerException != null)
                return ex.InnerException.Message;

            return ex.Message;
        }
    }
}
