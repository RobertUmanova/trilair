﻿
namespace TrailAir.Utility
{
    public class KeyValName
    {
        public int Id { get; set; }
        public string Text { get; set; }

        public static KeyValName AddValue(int valId, string valText) {
            var result = new KeyValName
            {
                Id = valId,
                Text = valText
            };

            return result;

        }
    }

    public class KeyGUIDValName
    {
        public string Id { get; set; }
        public string Text { get; set; }

        public static KeyGUIDValName AddValue(string valId, string valText)
        {
            var result = new KeyGUIDValName
            {
                Id = valId,
                Text = valText
            };

            return result;

        }
    }
}
