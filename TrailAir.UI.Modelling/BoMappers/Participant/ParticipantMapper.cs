﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.BoLoaders;
using TrailAir.UI.Modelling.UIModels.Participant;

namespace TrailAir.UI.Modelling.BoMappers.Participant
{
    public class ParticipantMapper
    {

        #region MAP UI TO DB

        #region ExperimentsParticipants

        internal static List<DB.Models.ExperimentsParticipant> MapExpParticipantsUIToDB(List<ExperimentsParticipantListIM> sourceListDb,
            List<DB.Models.ExperimentsParticipant> destinationList = null)
        {
            if (sourceListDb == null || sourceListDb.Count < 1)
                return new List<DB.Models.ExperimentsParticipant>();

            if (destinationList == null)
                destinationList = new List<DB.Models.ExperimentsParticipant>();

            if (destinationList.Count == 0)
            {
                destinationList = sourceListDb.Select(_ => MapExpParticipantsUIToDB(_)).ToList();
            }
            else
            {
                for (int i = 0; i < sourceListDb.Count; i++)
                {
                    if (i < destinationList.Count)
                    {
                        MapExpParticipantsUIToDB(sourceListDb[i], destinationList[i]);
                    }
                    else
                    {
                        destinationList.Add(MapExpParticipantsUIToDB(sourceListDb[i]));
                    }
                }
            }

            return destinationList;
        }

        internal static DB.Models.ExperimentsParticipant MapExpParticipantsUIToDB(ExperimentsParticipantListIM source, 
            DB.Models.ExperimentsParticipant destination = null)
        {
            if (source == null)
                return null;

            if(destination == null) destination = new DB.Models.ExperimentsParticipant();

            destination.Id = source.Id;
            destination.ExperimentIndex = source.ExperimentIndex;
            destination.ParticipantId = source.ParticipantId;

            return destination;
        }

        #endregion

        #region Participants
        internal static void MapUIToDb(ParticipantListIM source, DB.Models.Participant destination)
        {
            /// This is commented out just because we will not use Participants table anymore and 
            /// this is breking the code
            //destination.Id = source.Id;
        }

        internal static void MapUIToDbDet(ParticipantListIM source, DB.Models.Participant destination)
        {
            MapUIToDb(source, destination);


        }
        #endregion

        #endregion


        #region MAP DB TO UI

        #region ExperimentsParticipants

        internal static List<ExperimentsParticipantListIM> MapExpParticipantsDBToUI(List<DB.Models.ExperimentsParticipant> sourceListDb)
        {
            if (sourceListDb == null)
                return null;
            var result = sourceListDb.Select(_ => MapExpParticipantsDBToUI(_)).ToList();
            return result;
        }

        internal static ExperimentsParticipantListIM MapExpParticipantsDBToUI(DB.Models.ExperimentsParticipant source)
        {
            if (source == null)
                return null;

            ExperimentsParticipantListIM destination = new ExperimentsParticipantListIM();
            destination.Id = source.Id;
            destination.ParticipantId = source.ParticipantId;
            destination.ExperimentIndex = source.ExperimentIndex;

            MapDBToUI(source.Participant, destination.Participant = new ParticipantListIM());

            return destination;
        }

        #endregion

        #region Participants

        internal static List<ParticipantListIM> MapDBToUI(List<ApplicationUser> sourceListDb)
        {
            if (sourceListDb == null)
                return null;
            var result = sourceListDb.Select(_ => MapDBToUI(_)).ToList();
            return result;
        }
        internal static ParticipantListIM MapDBToUI(ApplicationUser source)
        {
            if (source == null)
                return null;

            ParticipantListIM destination = new ParticipantListIM();
            MapDBToUI(source, destination);

            return destination;
        }

        internal static void MapDBToUI(ApplicationUser source, ParticipantListIM destination)
        {
            destination.Id = source.Id;
            destination.Fullname = source.Fullname;
        }

        internal static ParticipantListIM MapDBToUIDet(ApplicationUser source)
        {
            ParticipantListIM destination = new();

            MapDBToUI(source, destination);
            return destination;
        }
        #endregion

        #endregion
    }
}
