﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.UI.Modelling.BoLoaders;
using TrailAir.UI.Modelling.UIModels.ExpParticipantResult;
using TrailAir.UI.Modelling.UIModels.Participant;
using TrailAir.DB.Models;
using TrailAir.UI.Modelling.UIModels.Experiment;
using TrailAir.UI.Modelling.BoMappers.ExperimentResult;
using TrailAir.DB.Models.Enumerations;

namespace TrailAir.UI.Modelling.BoMappers.BenchmarkResult
{
    public class BenchmarkResultMapper
    {

        #region MAP UI TO DB

        internal static List<DB.Models.BenchmarkResult> MapBenchmarkResultsUIToDB(List<ExperimentResultIM> sourceListDb, List<DB.Models.BenchmarkResult> destinationList = null)
        {
            if (sourceListDb == null || sourceListDb.Count < 1)
                return new List<DB.Models.BenchmarkResult>();

            if (destinationList == null)
                destinationList = new List<DB.Models.BenchmarkResult>();

            if (destinationList.Count == 0)
            {
                destinationList = sourceListDb.Select(_ => MapBenchmarkResultsUIToDB(_)).ToList();
            }
            else
            {
                for (int i = 0; i < sourceListDb.Count; i++)
                {
                    int sourceId = sourceListDb[i].Id;
                    var objectToUpdate = destinationList.FirstOrDefault(_ => _.Id == sourceId);

                    if (objectToUpdate != null)
                    {
                        MapBenchmarkResultsUIToDB(sourceListDb[i], null, objectToUpdate);
                    }
                    else
                    {
                        var destinationObject = new DB.Models.BenchmarkResult();
                        MapBenchmarkResultsUIToDB(sourceListDb[i], null, destinationObject);
                        destinationList.Add(destinationObject);
                    }
                }
            }

            return destinationList;
        }

        internal static DB.Models.BenchmarkResult MapBenchmarkResultsUIToDB(ExperimentResultIM source, int? experimentBenchmarkId = null, 
            DB.Models.BenchmarkResult destination = null)
        {
            if (source == null)
                return null;

            if (destination == null) 
                destination = new DB.Models.BenchmarkResult();

            destination.Id = source.Id;
            if (source.ExperimentBencTestSampId != 0)
                destination.BenchmarkId = source.ExperimentBencTestSampId;
            else if (experimentBenchmarkId != null)
                destination.BenchmarkId = (int)experimentBenchmarkId;
            else destination.BenchmarkId = 0;

            ExperimentResultMapper.MapUIToDb(source, destination);
            
            return destination;
        }

        #endregion

        #region MAP DB TO UI

        internal static List<ExperimentResultIM> MapBenchmarkResultsDBToUI(List<DB.Models.BenchmarkResult> sourceListDb)
        {
            if (sourceListDb == null)
                return null;
            var result = sourceListDb.Select(_ => MapBenchmarkResultsDBToUI(_)).ToList();
            return result;
        }

        internal static ExperimentResultIM MapBenchmarkResultsDBToUI(DB.Models.BenchmarkResult source)
        {
            if (source == null)
                return null;

            ExperimentResultIM destination = new ExperimentResultIM();

            destination.Id = source.Id;
            destination.ExperimentBencTestSampId = source.BenchmarkId;
            destination.SampleCode = source.Benchmark.SampleCode;
            destination.SampleName = source.Benchmark.SampleName;

            ExperimentResultMapper.MapBaseResDBToUI(source, destination, BaseExperimentResultType.BenchmarkResult);

            return destination;
        }

        #endregion
    }
}
