﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.UI.Modelling.BoMappers.ExperimentResult;
using TrailAir.UI.Modelling.UIModels.ExpParticipantResult;

namespace TrailAir.UI.Modelling.BoMappers.ControlSampleResult
{
    public class ControlSampleResultMapper
    {
        #region MAP UI TO DB

        internal static DB.Models.ControlSampleResult MapControlSampleResultsUIToDB(ExperimentResultIM source, int? experimentControlSampleId = null, DB.Models.ControlSampleResult destination = null)
        {
            if (source == null)
                return null;

            if (destination == null)
                destination = new DB.Models.ControlSampleResult();

            destination.Id = source.Id;

            if (experimentControlSampleId != null && experimentControlSampleId != 0)
                destination.ControlSampleId = experimentControlSampleId.Value;
            else if (source.ExperimentBencTestSampId > 0)
                destination.ControlSampleId = source.ExperimentBencTestSampId;


            ExperimentResultMapper.MapUIToDb(source, destination);

            return destination;
        }

        #endregion


        #region MAP DB TO UI

        #endregion
    }
}
