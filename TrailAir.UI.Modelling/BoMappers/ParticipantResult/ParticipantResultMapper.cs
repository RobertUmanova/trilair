﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB;
using TrailAir.UI.Modelling.BoMappers.BenchmarkResult;
using TrailAir.UI.Modelling.BoMappers.ControlSampleResult;
using TrailAir.UI.Modelling.BoMappers.TestSampleResult;
using TrailAir.UI.Modelling.UIModels.ExpParticipantResult;
using TrailAir.Utility;

namespace TrailAir.UI.Modelling.BoMappers.ParticipantResult
{
    internal class ParticipantResultMapper
    {
        internal static void MapUIToDb(ParticipantResultIM source, DB.Models.ParticipantResult destination)
        {
            destination.Id = source.Id;
            destination.ExperimentId = source.ExperimentId;

            if (!string.IsNullOrEmpty(source.CompletionDate))
            {
                destination.CompletionDate = DateOb.ParseStringToDate(source.CompletionDate);
            }
            destination.ParticipantId = source.ParticipantId;
            destination.IsSubmitted = source.IsSubmitted;

            #region TestSampleResults
            var resultTestSampleResults = new List<DB.Models.TestSampleResult>();
            foreach (var testSampleResult in source.TestSampleResults)
            {
                var tsr = destination.TestSampleResults?.SingleOrDefault(tsr => tsr.Id == testSampleResult.Id);
                if (tsr == null)
                {
                    resultTestSampleResults.Add(TestSampleResultMapper.MapTestSampleResultsUIToDB(testSampleResult, testSampleResult.ExperimentBencTestSampId));
                }
                else
                {
                    tsr = TestSampleResultMapper.MapTestSampleResultsUIToDB(testSampleResult, testSampleResult.ExperimentBencTestSampId, tsr);
                    resultTestSampleResults.Add(tsr);
                }
            }
            destination.TestSampleResults = resultTestSampleResults;
            #endregion

            #region ControlSampleResult
            var csr = destination.ControlSampleResult;
            destination.ControlSampleResult = ControlSampleResultMapper.MapControlSampleResultsUIToDB(source.ControlSampleResult, source.ControlSampleResult.Id);
            #endregion
        }

        internal static void MapDBToUI(DB.Models.ParticipantResult source, ParticipantResultIM destination)
        {
            destination.Id = source.Id;
            destination.ExperimentId = source.ExperimentId;

            if (source.CompletionDate != null)
            {
                destination.CompletionDate = DateOb.DateToString((DateTime)source.CompletionDate);
            }
            else destination.CompletionDate = String.Empty;

            destination.ParticipantId = source.ParticipantId;
        }

    }
}
