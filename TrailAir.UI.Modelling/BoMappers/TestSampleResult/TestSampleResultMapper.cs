﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.UI.Modelling.BoLoaders;
using TrailAir.UI.Modelling.UIModels.ExpParticipantResult;
using TrailAir.UI.Modelling.UIModels.Participant;
using TrailAir.DB.Models;
using TrailAir.UI.Modelling.BoMappers.ExperimentResult;
using TrailAir.DB.Models.Enumerations;

namespace TrailAir.UI.Modelling.BoMappers.TestSampleResult
{
    public class TestSampleResultMapper
    {

        #region MAP UI TO DB

        internal static List<DB.Models.TestSampleResult> MapTestSampleResultsUIToDB(List<ExperimentResultIM> sourceListDb, List<DB.Models.TestSampleResult> destinationList = null)
        {
            if (sourceListDb == null || sourceListDb.Count < 1)
                return new List<DB.Models.TestSampleResult>();

            if (destinationList == null)
                destinationList = new List<DB.Models.TestSampleResult>();

            if (destinationList.Count == 0)
            {
                destinationList = sourceListDb.Select(_ => MapTestSampleResultsUIToDB(_)).ToList();
            }
            else
            {
                for (int i = 0; i < sourceListDb.Count; i++)
                {
                    int sourceId = sourceListDb[i].Id;
                    var objectToUpdate = destinationList.FirstOrDefault(_ => _.Id == sourceId);

                    if (objectToUpdate != null)
                    {
                        MapTestSampleResultsUIToDB(sourceListDb[i], null, objectToUpdate);
                    }
                    else
                    {
                        var destinationObject = new DB.Models.TestSampleResult();
                        MapTestSampleResultsUIToDB(sourceListDb[i], null, destinationObject);
                        destinationList.Add(destinationObject);
                    }
                }
            }

            return destinationList;
        }

        internal static DB.Models.TestSampleResult MapTestSampleResultsUIToDB(ExperimentResultIM source, int? experimentTestSampleId = null, DB.Models.TestSampleResult destination = null)
        {
            if (source == null)
                return null;

            if (destination == null) 
                destination = new DB.Models.TestSampleResult();

            destination.Id = source.Id;
            
            if(experimentTestSampleId != null && experimentTestSampleId!= 0)
                destination.TestSampleId = experimentTestSampleId.Value;
            else if(source.ExperimentBencTestSampId > 0)
                destination.TestSampleId = source.ExperimentBencTestSampId;


            ExperimentResultMapper.MapUIToDb(source, destination);
            
            return destination;
        }

        #endregion

        #region MAP DB TO UI

        internal static List<ExperimentResultIM> MapTestSampleResultsDBToUI(List<DB.Models.TestSampleResult> sourceListDb)
        {
            if (sourceListDb == null)
                return null;
            var result = sourceListDb.Select(_ => MapTestSampleResultsDBToUI(_)).ToList();
            return result;
        }

        internal static ExperimentResultIM MapTestSampleResultsDBToUI(DB.Models.TestSampleResult source)
        {
            if (source == null)
                return null;

            ExperimentResultIM destination = new ExperimentResultIM();

            destination.Id = source.Id;
            destination.ExperimentBencTestSampId = source.TestSampleId;
            destination.SampleName = source.TestSample.SampleName;
            destination.SampleCode = source.TestSample.SampleCode;

            ExperimentResultMapper.MapBaseResDBToUI(source, destination, BaseExperimentResultType.TestSampleResult);

            destination.SampleIndex = source.TestSample.SampleIndex;

            return destination;
        }

        #endregion
    }
}
