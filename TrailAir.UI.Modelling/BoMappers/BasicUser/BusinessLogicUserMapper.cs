﻿using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.UIModels.User;

namespace TrailAir.UI.Modelling.BoMappers.BasicUser
{
    public class BusinessLogicUserMapper
    {
        public static void MapUIToDb(BusinessLogicUserDetailsIM source, BusinessLogicUser destination)
        {
            destination.PermissionId = source.PermissionId;
            destination.TeamId = source.TeamId;
        }

        //public static void MapUIImageToDb(BusinessLogicUserDetailsIM sourceUI, BusinessLogicUser destinationDb)
        //{
        //    destinationDb.DocumentId = sourceUI.DocumentId;
        //}

        internal static List<BusinessLogicUserListIM> MapDBToUI(List<BusinessLogicUser> sourceListDb)
        {
            if (sourceListDb == null)
                return null;

            var result = sourceListDb.Select(_ => MapDBToUI(_)).ToList();
            return result;
        }
        internal static List<BusinessLogicUserListIM> MapDBToUIList(List<BusinessLogicUser> sourceListDb)
        {
            if (sourceListDb == null)
                return null;

            var result = sourceListDb.Select(_ => MapDBToUI(_)).ToList();
            return result;
        }

        internal static BusinessLogicUserListIM MapDBToUI(BusinessLogicUser source)
        {
            if (source == null)
                return null;
            BusinessLogicUserDetailsIM destination = new BusinessLogicUserDetailsIM();

            MapDBToUI(source, destination);

            return destination;
        }

        internal static void MapDBToUI(BusinessLogicUser source, BusinessLogicUserDetailsIM destination)
        {
            destination.Id = source.Id;

            if (source.Permission != null)
            {
                destination.PermissionName = source.Permission.ActiveLangName;
            }
            if (source.Team != null)
            {
                destination.TeamName = source.Team.Name;
            }

            destination.PermissionId = source.PermissionId;
            destination.TeamId = source.TeamId;
        }
        internal static void MapDBToUIDet(BusinessLogicUser source, BusinessLogicUserDetailsIM destination)
        {
            MapDBToUI(source, destination);

        }
        internal static BusinessLogicUserDetailsIM MapDBToUIDet(BusinessLogicUser source)
        {
            BusinessLogicUserDetailsIM result = new BusinessLogicUserDetailsIM();

            MapDBToUIDet(source, result);

            return result;
        }


    }
}
