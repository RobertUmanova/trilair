﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.UI.Modelling.BoMappers.BenchmarkResult;
using TrailAir.UI.Modelling.BoMappers.ExperimentResult;
using TrailAir.UI.Modelling.BoMappers.TestSampleResult;
using TrailAir.UI.Modelling.UIModels.ExpParticipantResult;
using TrailAir.Utility;

namespace TrailAir.UI.Modelling.BoMappers.ExpParticipantResult
{
    public class ExpParticipantResultMapper
    {
        #region MAP UI TO DB

        #endregion


        #region MAP DB TO UI
        /// <summary>
        /// WIll map an existing object or created one based on the existing eperiment if is the first time you insert experiment result 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        internal static ParticipantResultIM MapDBToUI(DB.Models.ParticipantResult source, bool includeControleSample)
        {
            var destination = new ParticipantResultIM();

            destination.Id = source.Id;
            destination.ParticipantId = source.ParticipantId;
            destination.ExperimentId = source.ExperimentId;
            if (source.CompletionDate != null)
            {
                destination.CompletionDate = DateOb.DateToString((DateTime)source.CompletionDate);
            }
            else destination.CompletionDate = String.Empty;

            destination.IsSubmitted = source.IsSubmitted;

            if (destination.BenchmarkResults == null)

                destination.BenchmarkResults = new List<ExperimentResultIM>();

            if (includeControleSample)
            {
                var ctrSamlpRes = new ExperimentResultIM();
                ExperimentResultMapper.MapBaseResDBToUI(source.ControlSampleResult, ctrSamlpRes, TrailAir.DB.Models.Enumerations.BaseExperimentResultType.ControlSample);
                ctrSamlpRes.ComputedName = source.ControlSampleResult.ControlSample.ControlSampleType.Name;
                destination.ControlSampleResult = ctrSamlpRes;

            }

            //GET benchmarks and test samples

            if (source.BenchmarkResults != null)//brijem ova linija je visak 
                foreach (var item in source.BenchmarkResults)
                {
                    destination.BenchmarkResults.Add(BenchmarkResultMapper.MapBenchmarkResultsDBToUI(item));
                }

            
            
            if (destination.TestSampleResults == null)
                destination.TestSampleResults = new List<ExperimentResultIM>();

            if (source.TestSampleResults != null)//brijem ova linija je visak
                foreach (var item in source.TestSampleResults)
                {
                    destination.TestSampleResults.Add(TestSampleResultMapper.MapTestSampleResultsDBToUI(item));
                }

            return destination;
        }
        internal static ParticipantResultIM MapNewFromExperimentToUI(TrailAir.DB.Models.Experiment experimentForNew)
        {
            var destination = new ParticipantResultIM();

            destination.TestSampleResults = new List<ExperimentResultIM>();
            foreach (var experimentTestSample in experimentForNew.ExperimentTestSamples)
            {
                var ExperimentResultIM = MapExperimentTestSampleToResultIM(experimentTestSample);
                destination.TestSampleResults.Add(ExperimentResultIM);
            }

            var expResIMForControlSample = MapExperimentControlSampleToResultIM(experimentForNew.ControlSample);
            destination.ControlSampleResult = expResIMForControlSample;

            return destination;
        }

        private static ExperimentResultIM MapExperimentTestSampleToResultIM(DB.Models.TestSample experimentTestSample)
        {
            var testSampleResult = new ExperimentResultIM()
            {
                ExperimentBencTestSampId = experimentTestSample.Id,
                ComputedName = experimentTestSample.ComputedName,
                SampleIndex = experimentTestSample.SampleIndex
            };
            CreateEmptyAttributes(testSampleResult);
            return testSampleResult;
        }

        private static ExperimentResultIM MapExperimentControlSampleToResultIM(DB.Models.ControlSample experimentControlSample)
        {
            var controlSampleResult = new ExperimentResultIM()
            {
                ExperimentBencTestSampId = experimentControlSample.Id,
                SampleName = experimentControlSample.SampleName,
                SampleCode = experimentControlSample.SampleCode,
                ComputedName = experimentControlSample.FullText,
                Intensity = experimentControlSample.ControlSampleType.IntensityScore
            };
            CreateEmptyAttributes(controlSampleResult);
            return controlSampleResult;
        }

        private static void CreateEmptyAttributes(ExperimentResultIM destination)
        {
            destination.AttributeTexts = new List<string>();
            for (int i = 0; i < 5; i++)
            {
                destination.AttributeTexts.Add(String.Empty);
            }
        }

        #endregion
    }
}
