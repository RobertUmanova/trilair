﻿using TrailAir.UI.Modelling.BoMappers.MultiLanguage;
using TrailAir.DB.Models.Base;
using TrailAir.DB.Models;
using TrailAir.UI.Modelling.UIModels.ExpParticipantResult;
using TrailAir.Utility;
using TrailAir.DB.Models.Enumerations;

namespace TrailAir.UI.Modelling.BoMappers.ExperimentResult
{
    public class ExperimentResultMapper
    {
        #region MAP UI TO DB
        internal static void MapUIToDb(ExperimentResultIM source, BaseExperimentResult destination)
        {
            destination.FirstDetectionTime = source.FirstDetectionTime;
            destination.BuildUpTime = source.BuildUpTime;
            destination.FullDetectionTime = source.FullDetectionTime;
            destination.Intensity = source.Intensity;
            destination.IsSubmitted = source.IsSubmitted;
            destination.Order = source.Order;

            if (!string.IsNullOrEmpty(source.Date))
            {
                destination.Date = DateOb.ParseStringToDate(source.Date);
            }
            destination.Sensation = source.Sensation;
            destination.ParticipantResultId = source.ParticipantResultId;
            destination.Attributes = MapAttributesUIToDb(source.AttributeTexts, destination.Attributes);
        }


        #endregion MAP UI TO DB


        #region MAP DB TO UI

        internal static void MapBenchResultDBToUI(DB.Models.BenchmarkResult source, ExperimentResultIM destination)
        {
            MapBaseResDBToUI(source, destination, BaseExperimentResultType.BenchmarkResult);
        }

        internal static void MapTestSamResultDBToUI(DB.Models.TestSampleResult source, ExperimentResultIM destination)
        {
            MapBaseResDBToUI(source, destination, BaseExperimentResultType.TestSampleResult);
            destination.ComputedName = "Sample S" + source.TestSample?.SampleIndex ?? "";
        }

        internal static void MapControlSamResultDBToUI(DB.Models.ControlSampleResult source, ExperimentResultIM destination)
        {
            MapBaseResDBToUI(source, destination, BaseExperimentResultType.ControlSample);
            destination.ComputedName = source.ControlSample?.FullText ?? "";
        }

        internal static void MapBaseResDBToUI(BaseExperimentResult source, ExperimentResultIM destination, BaseExperimentResultType type)
        {
            destination.Id = source.Id;
            destination.FirstDetectionTime = source.FirstDetectionTime;
            destination.BuildUpTime = source.BuildUpTime;
            destination.FullDetectionTime = source.FullDetectionTime;
            destination.Intensity = source.Intensity;
            destination.IsSubmitted = source.IsSubmitted;
            destination.Order = source.Order;
            destination.ExpResultType = type;

            if (source.Date != null)
            {
                destination.Date = DateOb.DateToString((DateTime)source.Date);
            }
            else destination.Date = String.Empty;
            destination.Sensation = source.Sensation;
            destination.ParticipantResultId = source.ParticipantResultId;
            destination.AttributeTexts = MapAttributesDbToUI(source.Attributes);
        }

        private static List<string> MapAttributesDbToUI(List<ExperimentResultAttribute> attributes)
        {
            var result = new List<string>();
            if (attributes == null || attributes.Count == 0)
            {
                for (int i = 0; i < 5; i++)
                {
                    result.Add(string.Empty);
                }
            }
            else
            {
                result = attributes.Select(a => a.AttributeText).ToList();
            }
            return result;
        }
        #endregion MAP DB TO UI

        public static List<ExperimentResultAttribute> MapAttributesUIToDb(List<string> attributesUi, List<ExperimentResultAttribute> attributesDb)
        {
            if (attributesDb == null || attributesDb.Count == 0)
            {
                List<ExperimentResultAttribute> result = new List<ExperimentResultAttribute>();
                result.AddRange(attributesUi.Select(a => new ExperimentResultAttribute() { AttributeText = a }));
                return result;
            }
            else
            {
                for (int i = 0; i < attributesDb.Count; i++)
                {
                    attributesDb[i].AttributeText = attributesUi[i];
                }
                return attributesDb;
            }
        }
    }
}
