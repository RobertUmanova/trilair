﻿using TrailAir.UI.Modelling.BoMappers.MultiLanguage;
using TrailAir.UI.Modelling.UIModels.StaticText;

namespace TrailAir.UI.Modelling.BoMappers.StaticText
{
    public class StaticTextMapper
    {
        #region MAP UI TO DB
        internal static void MapUIToDb(StaticTextDetailsIM source, DB.Models.StaticText destination)
        {
            destination.Key = source.Key;
            destination.Description = source.Description;
            destination.Id = source.Id;

            if (destination.Text == null)
                destination.Text = new DB.Models.Base.MultyLanguageText();
            LangTextMapper.MapToBO(source.MlText, destination.Text);
            //multilang todo
        }


        #endregion MAP UI TO DB


        #region MAP DB TO UI


        internal static List<StaticTextListIM> MapDBToUIs(List<DB.Models.StaticText> sourceListDb)
        {
            if (sourceListDb == null)
                return null;

            var result = sourceListDb.Select(_ => MapDBToUIList(_)).ToList();
            return result;
        }


        internal static StaticTextListIM MapDBToUIList(DB.Models.StaticText source)
        {
            if (source == null)
                return null;
            StaticTextListIM destination = new StaticTextListIM();

            MapDBToUIList(source, destination);

            return destination;
        }

        internal static void MapDBToUIList(DB.Models.StaticText source, StaticTextListIM destination)
        {
            destination.Id = source.Id;
            destination.Key = source.Key;
            destination.Text = source.ActiveLangText;
        }

        internal static StaticTextDetailsIM MapDBToUIDet(DB.Models.StaticText source)
        {
            StaticTextDetailsIM result = new StaticTextDetailsIM();

            MapDBToUIDet(source, result);

            return result;

        }

        internal static void MapDBToUIDet(DB.Models.StaticText source, StaticTextDetailsIM destination)
        {
            MapDBToUIList(source, destination);

            destination.MlText = LangTextMapper.Instance.MapToUI(source.Text);
        }
        #endregion MAP DB TO UI
    }
}
