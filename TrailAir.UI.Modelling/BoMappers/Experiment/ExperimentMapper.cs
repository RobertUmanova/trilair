﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.Models;
using TrailAir.DB.Models.Enumerations;
using TrailAir.UI.Modelling.BoMappers.MultiLanguage;
using TrailAir.UI.Modelling.BoMappers.Participant;
using TrailAir.UI.Modelling.UIModels.Experiment;
using TrailAir.Utility;

namespace TrailAir.UI.Modelling.BoMappers.Experiment
{
    public class ExperimentMapper
    {
        #region MAP UI TO DB
        internal static void MapListUIToDb(ExperimentListIM source, DB.Models.Experiment destination)
        {
            destination.Id = source.Id;
            destination.Name = source.Name;
            destination.NumberOfSamples = source.NumberOfSamples;
            destination.ExperimentStatusId = source.ExperimentStatusId;
            destination.Instruction = source.Instruction;
            destination.Comment = source.Comment;
            //destination.DatePublished = new DateOb(source.DatePublished); // TODO: Fix this !!
            destination.TeamId = source.TeamId;
            if (string.IsNullOrEmpty(source.RequesterId)) source.RequesterId = null;
            destination.RequesterId = source.RequesterId;
            destination.Consumer = source.Consumer;

            if (source.ExperimentStatusId == null)
            {
                destination.ExperimentStatusId = (int)ExperimentStatusEnum.Draft;
            }
        }

        internal static void MapUIToDbDet(DB.TrailAirDbContext db, ExperimentDetailsIM source, DB.Models.Experiment destination)
        {
            MapListUIToDb(source, destination);

            destination.Participants = source.Participants.Select(p => new ExperimentsParticipant()
            {
                ParticipantId = p.ParticipantId,
            }).ToList();
            destination.NumberOfParticipants = destination.Participants.Count;


            if (destination.SamplePreparation == null)
            {
                destination.SamplePreparation = new DB.Models.SamplePreparation
                {
                    EvaporingTemperature = source.SamplePreparation.EvaporingTemperature,
                    EvaporingTime = source.SamplePreparation.EvaporingTime,
                    PerfumeConcentration = source.SamplePreparation.PerfumeConcentration,
                };
            }
            else
            {
                destination.SamplePreparation.EvaporingTemperature = source.SamplePreparation.EvaporingTemperature;
                destination.SamplePreparation.EvaporingTime = source.SamplePreparation.EvaporingTime;
                destination.SamplePreparation.PerfumeConcentration = source.SamplePreparation.PerfumeConcentration;
            }

            destination.ControlSample = MapControlSampleUIToDB(source.ControlSample);
            //destination.ExperimentTestSamples = MapExperimentTestSampleUIToDB(destination.ExperimentTestSamples, source.ExperimentTestSamples);
        }

        public static ControlSample MapControlSampleUIToDB(ControlSampleListIM source)
        {
            if (source.ControlSampleTypeId == 0)
                return null;

            var destination = new ControlSample();

            destination.Id = source.Id;
            destination.ControlSampleTypeId = source.ControlSampleTypeId;
            destination.SampleName = source.SampleName;
            destination.SampleCode = source.SampleCode;
            destination.LOTCode = source.LOTCode;
            destination.ManufacturedDate = !string.IsNullOrEmpty(source.ManufacturedDate) ? DateOb.StringToDate(source.ManufacturedDate) : null;
            destination.Base = source.Base;

            return destination;
        }

        /// <summary>
        /// CONTROL SAMPLE TYPE Mapping one object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static void MapControlSampleTypeUIToDB(ControlSampleTypeListIM source, ControlSampleType destination)
        {
            destination.Id = source.Id;
            destination.Name = source.Name;
            destination.ControlSampleTypeCode = source.ControlSampleTypeCode;
            destination.IntensityScore = source.IntensityScore;
        }

        public static List<Benchmark> MapExperimentBenchmarkUIToDB(List<Benchmark> destination,
            List<ExperimentBenchmarkIM> source)
        {
            if (source == null || source.Count < 1)
            {
                return new List<Benchmark>();
            }

            if (destination == null)
                destination = new List<Benchmark>();


            if (destination.Count == 0)
            {
                foreach (var item in source)
                {
                    var destExpBench = new Benchmark();

                    destination.Add(destExpBench);
                    destination.Add(MapExperimentBenchmarkUIToDB(destExpBench, item));
                }
            }
            else
            {
                for (int i = 0; i < source.Count; i++)
                {
                    int sourceId = source[i].Id;
                    var objectToUpdate = destination.FirstOrDefault(_ => _.Id == sourceId);

                    if (objectToUpdate != null)
                    {
                        MapExperimentBenchmarkUIToDB(objectToUpdate, source[i]);
                    }
                    else
                    {
                        var destExpBench = new Benchmark();
                        MapExperimentBenchmarkUIToDB(destExpBench, source[i]);
                        destination.Add(destExpBench);
                    }

                    /*
                    if (i < destination.Count)
                    {
                        destination[i] = MapExperimentBenchmarkUIToDB(destination[i], source[i]);

                    }
                    else
                    {
                        var destExpBench = new ExperimentBenchmark();

                        destination.Add(destExpBench);

                        destination[i] = MapExperimentBenchmarkUIToDB(destExpBench, source[i]);
                    }*/
                }
            }

            return destination;
        }

        #region TEST SAMPLE
        public static List<TestSample> MapExperimentTestSampleUIToDB(List<TestSample> destination,
        List<ExperimentTestSampleIM> source) //this method is kind of obsolete and should be refactored now that test samples work in a different way, "mark" is important
        {
            if (source == null || source.Count < 1)
            {
                return new List<TestSample>();
            }

            if (destination == null)
                destination = new List<TestSample>();


            if (destination.Count == 0)
            {
                foreach (var item in source)
                {
                    var destExpTestSample = new TestSample();

                    destination.Add(destExpTestSample);
                    destination.Add(MapExperimentTestSampleUIToDB(destExpTestSample, item));
                }
            }
            else
            {
                if (destination.Count >= source.Count) //mark
                {
                    int index = 1;
                    foreach (var ts in destination)
                    {
                        ts.SampleIndex = index++;
                    }
                }
                else
                {
                    for (int i = 0; i < source.Count; i++)
                    {
                        int sourceId = source[i].Id;
                        var objectToUpdate = destination.FirstOrDefault(_ => _.Id == sourceId);

                        if (objectToUpdate != null)
                        {
                            MapExperimentTestSampleUIToDB(objectToUpdate, source[i]);
                        }
                        else
                        {
                            var destExpBench = new TestSample();
                            MapExperimentTestSampleUIToDB(destExpBench, source[i]);
                            destination.Add(destExpBench);
                        }
                    }
                }
            }

            return destination;
        }

        public static TestSample MapExperimentTestSampleUIToDB(TestSample destination, ExperimentTestSampleIM source)
        {
            destination.ExperimentId = source.ExperimentId;
            destination.SampleName = source.TestSample.SampleName;
            destination.SampleCode = source.TestSample.SampleCode;

            if (source.TestSample.ApplicationDate != null)
            {
                destination.ApplicationDate = DateOb.ParseStringToDate(source.TestSample.ApplicationDate);
            }

            destination.Base = source.TestSample.Base;
            destination.Concentration = source.TestSample.Concentration;
            destination.FantasyName = source.TestSample.FantasyName;
            destination.GroupNumber = source.TestSample.GroupNumber;
            destination.InternalName = source.TestSample.InternalName;
            destination.IsBenchmark = source.TestSample.IsBenchmark;
            destination.IsInternal = source.TestSample.IsInternal;
            destination.LOTCode = source.TestSample.LOTCode;
            if (source.TestSample.ManufacturedDate != null)
            {
                destination.ManufacturedDate = DateOb.ParseStringToDate(source.TestSample.ManufacturedDate);
            }
            destination.ProductName = source.TestSample.ProductName;
            destination.Technology = source.TestSample.Technology;
            destination.TrialNumber = source.TestSample.TrialNumber;
            destination.Comment = source.TestSample.Comment;

            return destination;
        }

        #endregion

        public static Benchmark MapExperimentBenchmarkUIToDB(Benchmark destination, ExperimentBenchmarkIM source)
        {
            destination.ExperimentId = source.ExperimentId;
            destination.SampleName = source.Benchmark.SampleName;
            destination.SampleCode = source.Benchmark.SampleCode;

            return destination;
        }

        #endregion

        #region MAP DB TO UI
        internal static List<ExperimentListIM> MapDBToUI(List<DB.Models.Experiment> sourceListDb)
        {
            if (sourceListDb == null)
                return null;
            var result = sourceListDb.Select(_ => MapDBToUI(_)).ToList();
            return result;
        }
        internal static ExperimentListIM MapDBToUI(DB.Models.Experiment source)
        {
            if (source == null)
                return null;

            ExperimentListIM destination = new ExperimentListIM();
            MapDBToUI(source, destination);

            return destination;
        }

        internal static string MapStatusCodeToText(ExperimentStatusEnum expStatus)
        {
            switch (expStatus)
            {
                case ExperimentStatusEnum.Draft:
                    return "Draft";
                case ExperimentStatusEnum.InProgress:
                    return "In Progress";
                case ExperimentStatusEnum.Finished:
                    return "Finished";
                default:
                    return String.Empty;
            }
        }

        internal static void MapDBToUI(DB.Models.Experiment source, ExperimentListIM destination)
        {
            destination.Id = source.Id;
            destination.Name = source.Name;
            //destinationUI.IsArchived = sourceDb.IsArchived;

            destination.Instruction = source.Instruction;
            destination.Comment = source.Comment;
            destination.NumberOfSamples = source.NumberOfSamples;
            destination.RequesterId = source.RequesterId;
            destination.Consumer = source.Consumer;
            destination.DateFinished = source.DateFinished.HasValue ? new DateOb(source.DateFinished).ToString() : null;

            if (source.ExperimentStatusId != null)
            {
                destination.ExperimentStatusId = source.ExperimentStatusId;
                destination.ExperimentStatusName = MapStatusCodeToText(source.ExperimentStatus.ExperimentStatusCode);
                switch (source.ExperimentStatus.ExperimentStatusCode)
                {
                    case ExperimentStatusEnum.Draft:
                        destination.HasAllResultsSubmitted = false;
                        destination.IsFinished = false;
                        destination.IsPublished = false;
                        break;
                    case ExperimentStatusEnum.InProgress:
                        destination.HasAllResultsSubmitted = false;
                        destination.IsFinished = false;
                        destination.IsPublished = true;
                        break;
                    case ExperimentStatusEnum.Finished:
                        destination.HasAllResultsSubmitted = true;
                        destination.IsPublished = false;
                        destination.IsFinished = true;
                        break;
                    default:
                        destination.ExperimentStatusName = String.Empty;
                        break;
                }

            }
            if (source.DatePublished != null)
            {
                destination.DatePublished = new DateOb(source.DatePublished);
            }
            if (destination.TeamId != null)
            {
                destination.TeamId = source.TeamId;
                destination.TeamName = source.Team?.Name ?? string.Empty;
            }
            destination.ControlSampleId = source.ControlSampleId;


            if (source.Participants != null)
            {
                destination.Participants = new List<UIModels.Participant.ExperimentsParticipantListIM>();
                foreach (var item in source.Participants)
                {
                    destination.Participants.Add(new UIModels.Participant.ExperimentsParticipantListIM
                    {

                        ExperimentIndex = item.ExperimentIndex,
                        Id = item.Id,
                        ParticipantId = item.ParticipantId,
                        Participant = new UIModels.Participant.ParticipantListIM
                        {

                            Id = item.Participant.Id,
                            Fullname = item.Participant.Fullname
                        }

                    });
                }
            }

        }

        internal static ExperimentDetailsIM MapDBToUIDet(DB.Models.Experiment source)
        {
            ExperimentDetailsIM result = new ExperimentDetailsIM();

            MapDBToUIDet(source, result);

            return result;
        }

        internal static void MapDBToUIDet(DB.Models.Experiment source, ExperimentDetailsIM destination)
        {
            MapDBToUI(source, destination);

            destination.Instruction = source.Instruction;
            destination.Comment = source.Comment;

            if (source.Requester != null)
            {
                destination.RequesterFullName = source.Requester.Fullname;
            }

            if (source.ControlSample != null)
            {
                destination.ControlSample = MapControlSampleDBToUI(source.ControlSample);
            }
            else destination.ControlSample = new ControlSampleListIM();

            destination.Consumer = source.Consumer;
            if (source.SamplePreparation != null)
            {
                destination.SamplePreparation = new SamplePreparationIM
                {
                    EvaporingTemperature = source.SamplePreparation.EvaporingTemperature,
                    EvaporingTime = source.SamplePreparation.EvaporingTime,
                    PerfumeConcentration = source.SamplePreparation.PerfumeConcentration
                };
            }

            if (source.ExperimentTestSamples == null || source.ExperimentTestSamples.Count < 1)
            {
                destination.ExperimentTestSamples = new List<ExperimentTestSampleIM>
                {
                    new ExperimentTestSampleIM
                    {
                        Id = 0,
                        TestSampleId = 0,
                        ExperimentIndex = 0,
                        TestSample = new TestSampleIM
                        {
                            Id = 0,
                            SampleName = "",
                            SampleCode = ""
                        }
                    }
                };
            }
            else
            {
                destination.ExperimentTestSamples = MapExperimentTestSampleDBToUI(source.ExperimentTestSamples);
            }

            destination.Participants = ParticipantMapper.MapExpParticipantsDBToUI(source.Participants);
        }


        /// <summary>
        /// CONTROL SAMPLE Mapping one object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private static ControlSampleListIM MapControlSampleDBToUI(ControlSample source)
        {
            var destination = new ControlSampleListIM();

            destination.Id = source.Id;
            destination.ControlSampleTypeId = source.ControlSampleTypeId;
            destination.ControlSampleTypeName = source.ControlSampleType?.Name ?? "";
            destination.ControlSampleTypeIntensityScore = source.ControlSampleType?.IntensityScore ?? 0;
            destination.SampleName = source.SampleName;
            destination.SampleCode = source.SampleCode;
            destination.LOTCode = source.LOTCode;

            if (source.ManufacturedDate != null)
            {
                destination.ManufacturedDate = DateOb.DateToString((DateTime)source.ManufacturedDate);
            }
            destination.Base = source.Base;

            return destination;
        }

        /// <summary>
        /// CONTROL SAMPLE TYPE Mapping List
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static List<ControlSampleTypeListIM> MapControlSampleTypeDBToUI(List<ControlSampleType> source)
        {
            if (source == null || source.Count < 1)
            {
                return new List<ControlSampleTypeListIM>();
            }

            var destination = new List<ControlSampleTypeListIM>();

            foreach (var item in source)
            {
                destination.Add(MapControlSampleTypeDBToUI(item));
            }

            return destination;
        }

        /// <summary>
        /// CONTROL SAMPLE TYPE Mapping one object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static ControlSampleTypeListIM MapControlSampleTypeDBToUI(ControlSampleType source)
        {
            var destination = new ControlSampleTypeListIM();

            destination.Id = source.Id;
            destination.Name = source.Name;
            destination.ControlSampleTypeCode = source.ControlSampleTypeCode;
            destination.IntensityScore = source.IntensityScore;
            destination.FullText = source.FullText;

            return destination;
        }


        /// <summary>
        /// EXPERIMENT STATUS Mapping list
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static List<ExperimentStatusIM> MapExperimentStatusDBToUI(List<ExperimentStatus> source)
        {
            if (source == null || source.Count < 1)
            {
                return new List<ExperimentStatusIM>();
            }

            var destination = new List<ExperimentStatusIM>();

            foreach (var item in source)
            {
                destination.Add(MapExperimentStatusDBToUI(item));
            }

            return destination;
        }

        /// <summary>
        /// EXPERIMENT STATUS Mapping one object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static ExperimentStatusIM MapExperimentStatusDBToUI(ExperimentStatus source)
        {
            var destination = new ExperimentStatusIM();

            destination.Id = source.Id;
            destination.Name = source.ActiveLangName;
            destination.ExperimentStatusCode = source.ExperimentStatusCode;

            return destination;
        }

        public static List<ExperimentBenchmarkIM> MapExperimentBenchmarkDBToUI(List<Benchmark> source)
        {
            if (source == null || source.Count < 1)
            {
                return new List<ExperimentBenchmarkIM>();
            }

            var destination = new List<ExperimentBenchmarkIM>();

            foreach (var item in source)
            {
                destination.Add(MapExperimentBenchmarkDBToUI(item));
            }

            return destination;
        }

        public static ExperimentBenchmarkIM MapExperimentBenchmarkDBToUI(Benchmark source)
        {
            /// TODO: This pattern of instantiating and immediatly mapping class object 
            /// is much faster then the regular one where you always write destination.x = source.y
            /// If you have time, refactor it on all the application

            var destination = new ExperimentBenchmarkIM
            {
                Id = source.Id,
                ExperimentId = source.ExperimentId,
                Benchmark = new BenchmarkIM()
                {
                    SampleCode = source.SampleCode,
                    SampleName = source.SampleName,
                    Id = source.Id
                }
            };

            return destination;
        }

        #region TEST SAMPLE
        public static List<ExperimentTestSampleIM> MapExperimentTestSampleDBToUI(List<TestSample> source)
        {
            if (source == null || source.Count < 1)
            {
                return new List<ExperimentTestSampleIM>();
            }

            var destination = new List<ExperimentTestSampleIM>();

            foreach (var item in source)
            {
                destination.Add(MapExperimentTestSampleDBToUI(item));
            }

            return destination;
        }

        public static ExperimentTestSampleIM MapExperimentTestSampleDBToUI(TestSample source)
        {
            /// TODO: This pattern of instantiating and immediatly mapping class object 
            /// is much faster then the regular one where you always write destination.x = source.y
            /// If you have time, refactor it on all the application

            var destination = new ExperimentTestSampleIM
            {
                Id = source.Id,
                ExperimentId = source.ExperimentId,
                TestSample = new TestSampleIM()
                {
                    SampleCode = source.SampleCode,
                    SampleName = source.SampleName,
                    Id = source.Id,
                    ApplicationDate = source.ApplicationDate != null ? DateOb.DateToString((DateTime)source.ApplicationDate) : null,
                    Concentration = source.Concentration,
                    FantasyName = source.FantasyName,
                    Base = source.Base,
                    GroupNumber = source.GroupNumber,
                    InternalName = source.InternalName,
                    IsBenchmark = source.IsBenchmark,
                    IsInternal = source.IsInternal,
                    LOTCode = source.LOTCode,
                    ManufacturedDate = source.ManufacturedDate != null ? DateOb.DateToString((DateTime)source.ManufacturedDate) : null,
                    ProductName = source.ProductName,
                    Technology = source.Technology,
                    TrialNumber = source.TrialNumber,
                    ComputedName = source.ComputedName,
                    SampleIndex = source.SampleIndex,
                    Comment = source.Comment,
                    ConcentrationStr = source.ConcentrationStr,
                }
            };

            return destination;
        }
        #endregion

        #endregion
    }
}
