﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.Models;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.BoMappers.BasicUser;
using TrailAir.UI.Modelling.UIModels.Team;
using TrailAir.UI.Modelling.UIModels.User;

namespace TrailAir.UI.Modelling.BoMappers.Team
{
    public class TeamMapper
    {

        #region MAP UI TO DB

        internal static void MapUIToDb(TeamListIM source, DB.Models.Team destination)
        {
            destination.Name = source.Name;
            destination.Id = source.Id;
        }

        internal static void MapUIToDbDet(TeamDetailsIM source, DB.Models.Team destination)
        {
            MapUIToDb(source, destination);
            destination.Description = source.Description;

            //List<BusinessLogicUser> assignedUsers = new();

            //if (source.AssignedUsers?.Count > 0)
            //{
            //    foreach (var assignedUser in source.AssignedUsers)
            //    {
            //        assignedUsers.Add(new BusinessLogicUser
            //        {
            //            Id = assignedUser.Id
            //        });
            //    }
            //}
            //destination.Users = assignedUsers;

        }

        #endregion


        #region MAP DB TO UI
        internal static List<TeamListIM> MapDBToUI(List<DB.Models.Team> sourceListDb)
        {
            if (sourceListDb == null)
                return null;
            var result = sourceListDb.Select(_ => MapDBToUI(_)).ToList();
            return result;
        }
        internal static TeamListIM MapDBToUI(DB.Models.Team source)
        {
            if (source == null)
                return null;

            TeamListIM destination = new TeamListIM();
            MapDBToUI(source, destination);

            return destination;
        }

        internal static void MapDBToUI(DB.Models.Team source, TeamListIM destination)
        {
            destination.Id = source.Id;
            destination.Name = source.Name;
            destination.IsArchived = source.IsArchived;
            destination.AssignedUsers = BusinessLogicUserMapper.MapDBToUIList(source.Users.ToList());
        }

        internal static TeamDetailsIM MapDBToUIDet(DB.Models.Team source)
        {
            TeamDetailsIM destination = new();
            List<BusinessLogicUserListIM> users = new();

            MapDBToUI(source, destination);
            if (source.Users != null)
            {
                foreach (var item in source.Users)
                {
                    //var user = dbContext.Users.Where(_ => _.BusinessLogicDataId == item.BusinessLogicUserId).FirstOrDefault();
                    //CompanyListIM company = new();
                    //if (item.BusinessLogicUser.Company != null)
                    //    CompanyMapper.MapDBToUI(item.BusinessLogicUser.Company, company);

                    //users.Add(new BusinessLogicUserListIM
                    //{
                    //    Id = item.BusinessLogicUserId,
                    //    UserName = user != null ? user.UserName : "",
                    //    Email = user != null ? user.Email : "",
                    //    PhoneNumber = user != null ? user.PhoneNumber : "",
                    //    FirstName = user != null ? user.FirstName : "",
                    //    LastName = user != null ? user.LastName : ""

                    //});
                }
            }
            destination.AssignedUsers = users;
            destination.Description = source.Description;
            return destination;
        }


        #endregion
    }
}
