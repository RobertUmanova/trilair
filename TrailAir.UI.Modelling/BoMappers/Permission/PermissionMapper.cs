﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.BoMappers.MultiLanguage;
using TrailAir.UI.Modelling.UIModels.Permission;

namespace TrailAir.UI.Modelling.BoMappers.Permission
{
    public class PermissionMapper
    {
        internal static void MapUIToDb(PermissionDetailsIM source, DB.Models.Permission destination)
        {

            if (destination.Name == null)
                destination.Name = new MultyLanguageText();
            LangTextMapper.MapToBO(source.MlName, destination.Name);
            destination.PermissionCode = source.PermissionCode;
            //destination.IsOwner = source.IsOwner;
            // destination.Name = new MultyLanguageText { Texts = source.Name};

        }

        internal static List<PermissionListIM> MapDBToUI(List<DB.Models.Permission> sourceListDb)
        {
            if (sourceListDb == null)
                return null;

            var result = sourceListDb.Select(_ => MapDBToUI(_)).ToList();
            return result;
        }

        internal static PermissionListIM MapDBToUI(DB.Models.Permission source)
        {
            if (source == null)
                return null;
            PermissionListIM destination = new PermissionListIM();

            MapDBToUI(source, destination);

            return destination;
        }
        internal static void MapDBToUI(DB.Models.Permission source, PermissionListIM destination)
        {
            destination.Name = source.ActiveLangName;
            destination.Id = source.Id;
            destination.PermissionCode = source.PermissionCode;
        }
        internal static void MapDBToUIDet(DB.Models.Permission source, PermissionDetailsIM destination)
        {
            MapDBToUI(source, destination);
            destination.MlName = LangTextMapper.Instance.MapToUI(source.Name);

            //destinationUI.IsOwner = sourceDb.IsOwner;
            //destinationUI.MlName = LangTextMapper.Instance.MapToUI(sourceDb.NameId);

        }
        internal static PermissionDetailsIM MapDBToUIDet(DB.Models.Permission source)
        {
            PermissionDetailsIM result = new PermissionDetailsIM();

            MapDBToUIDet(source, result);

            return result;
        }
    }
}
