﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.Managers.Language;
using TrailAir.UI.Modelling.UIModels.MultiLanguage;

namespace TrailAir.UI.Modelling.BoMappers.MultiLanguage
{
    public class LangTextMapper
    {

        private static LangTextMapper SingletonInstance = null;
        public static void InitializeSingleton(Dictionary<string, LangSettingsModel> langIn)
        {
            SingletonInstance = new LangTextMapper(langIn);
        }

        public static LangTextMapper Instance
        {
            get
            {
                if (SingletonInstance == null)
                    throw new Exception("IT Controlled exception. Language Text Mapper not Initialized at start up.");
                return SingletonInstance;
            }
        }


        Dictionary<string, LangSettingsModel> LangInSettings;
        public LangTextMapper(Dictionary<string, LangSettingsModel> langInSettings)
        {
            LangInSettings = langInSettings;
        }

        public MultyLangTextModel MapToUI(MultyLanguageText multyLanguageText)
        {
            return LangTextMapper.MapToUI(multyLanguageText, LangInSettings);
        }
        public void MapToUI(MultyLanguageText multyLanguageText, MultyLangTextModel result)
        {
            LangTextMapper.MapToUI(multyLanguageText, result, LangInSettings);
        }

        public static void MapToUI(MultyLanguageText multyLanguageText, MultyLangTextModel result, Dictionary<string, LangSettingsModel> langInSettings)
        {
            if (multyLanguageText == null)
                multyLanguageText = new MultyLanguageText();


            result.LanguageTexts = new List<LangTextModel>();
            result.Id = multyLanguageText.Id;

            if (multyLanguageText.Texts == null || multyLanguageText.Texts.Count == 0)
                CreateNewLangTextModel(result, langInSettings);
            else
                MapFromExistingObject(multyLanguageText, langInSettings, result);

        }
        public static MultyLangTextModel MapToUI(MultyLanguageText multyLanguageText, Dictionary<string, LangSettingsModel> langInSettings)
        {

            MultyLangTextModel result = new MultyLangTextModel();
            MapToUI(multyLanguageText, result, langInSettings);

            return result;
        }

        private static void CreateNewLangTextModel(MultyLangTextModel result, Dictionary<string, LangSettingsModel> langInSettings)
        {
            foreach (var langSet in langInSettings)
            {
                var langSetVal = langSet.Value;

                LangTextModel singleTextRes = new LangTextModel
                {
                    Text = string.Empty,
                    Id = 0,
                    Code = langSetVal.Code,
                    ShortCode = langSetVal.ShortCode
                };

                result.LanguageTexts.Add(singleTextRes);

            }
        }

        internal static void MapToBO(MultyLangTextModel name, MultyLanguageText result)
        {
            if (result.Texts == null)
                result.Texts = new List<LanguageText>();


            List<LanguageText> deletedLanguageTexts = new List<LanguageText>();

            foreach (var singleText in result.Texts)
            {
                var relativeModel = name.LanguageTexts.FirstOrDefault(_ => _.Id == singleText.Id);
                if (relativeModel == null)
                    deletedLanguageTexts.Add(singleText);
                else
                    MapSingleTextTOBo(relativeModel, singleText);
            }
            //TODO we are not removing unused(not configured anymore in webconfig) Languages 

            foreach (var newObject in name.LanguageTexts.Where(_ => _.Id == 0))
            {
                LanguageText bo = new LanguageText();
                MapSingleTextTOBo(newObject, bo);
                result.Texts.Add(bo);
            }
        }

        private static void MapSingleTextTOBo(LangTextModel uiModel, LanguageText result)
        {
            result.Key = uiModel.Code;
            result.Name = uiModel.Text;
        }

        private static void MapFromExistingObject(MultyLanguageText multyLanguageText, Dictionary<string, LangSettingsModel> langInSettings, MultyLangTextModel result)
        {

            foreach (var singleText in multyLanguageText.Texts)
            {
                LangSettingsModel langSettings;
                if (!(langInSettings.TryGetValue(singleText.Key, out langSettings)))
                    langSettings = new LangSettingsModel();

                LangTextModel singleTextRes = new LangTextModel
                {
                    Text = singleText.Name,
                    Id = singleText.Id,
                    Code = singleText.Key,
                    ShortCode = langSettings.ShortCode
                };

                result.LanguageTexts.Add(singleTextRes);
            }
        }
    }
}
