﻿using Microsoft.EntityFrameworkCore;
using TrailAir.DB;
using TrailAir.DB.Models;
using TrailAir.UI.Modelling.BoMappers.Participant;
using TrailAir.UI.Modelling.UIModels.GenericApp;
using TrailAir.UI.Modelling.UIModels.Participant;
using TrailAir.Utility;
using TrailAir.Utility.ExceptionHelpers;

namespace TrailAir.UI.Modelling.BoLoaders
{
    /// <summary>
    /// DEPRECATED !
    /// </summary>
    public class ParticipantLoader : BaseBoLoader<ParticipantListIM>
    { 
        /// <summary>
        /// DEPRECATED
        /// This is a list used for example to fill a drop down list
        /// </summary>
        /// <returns> Tasks per project</returns>
        //public static List<KeyValName> GetDDList(TrailAirDbContext db, Managers.ApplicationUserManager appUserManager)
        //{
        //    var elementList = GetItemList(db, appUserManager);

        //    return elementList.Select(_ => KeyValName.AddValue(_.Id, _.Fullname)).ToList();
        //}

        public static List<ParticipantListIM> GetItemList(TrailAirDbContext db, Managers.ApplicationUserManager appUserManager)
        {
            var tmpElementsDB = appUserManager.Users.ToList();

            var elementsDB = ParticipantMapper.MapDBToUI(tmpElementsDB);

            return elementsDB;
        }

        /// <summary>
        /// This is used for example when you saving the Experiment. After you map UItoDB experiment, 
        /// you can load the DBExperimentParticipants with the UIExperiment ones
        /// </summary>
        /// <param name="db"></param>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        public static List<ExperimentsParticipant> GetExpParticDbListFromUIList(TrailAirDbContext db, List<ExperimentsParticipantListIM> source, 
            List<ExperimentsParticipant> destination)
        {
            if (source == null)
                source = new List<ExperimentsParticipantListIM>();

            if (destination == null)
                destination = new List<ExperimentsParticipant>();
            else
            {
                // For now, the logic is that you first remove all the ExperimentParticipants and then just create a new ones!
                return null;
            }

            destination = ParticipantMapper.MapExpParticipantsUIToDB(source);
            return destination;
        }

        public static SingleBOActionResponse Update(TrailAirDbContext db, ParticipantListIM objectToUpdate, string userEmail)
        {
            var response = new SingleBOActionResponse()
            {
                Success = true
            };
            try
            {
                Participant elementDB;
                /// This is commented out just because we will not use Participants table anymore and 
                /// this is breking the code
                //if (string.IsNullOrEmpty(objectToUpdate.Id))
                //{
                //    elementDB = new Participant();
                //    db.Participants.Add(elementDB);
                //}
                //else
                //{
                 
                //    elementDB = db.Participants
                //        .FirstOrDefault(x => x.Id == objectToUpdate.Id);



                //    //RemoveAssignedUsers(db, elementDB);
                //}
                /// This is commented out just because we will not use Participants table anymore and 
                /// this is breking the code
                //ParticipantMapper.MapUIToDbDet(objectToUpdate, elementDB);


                //AddAssignedUsers(db, elementDB);
                db.SaveChanges(userEmail);

                /// This is commented out just because we will not use Participants table anymore and 
                /// this is breking the code
                //response.ObjectId = elementDB.Id;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessages.Add(ExceptionObject.GetErrorMessage(ex));
            }
            return response;
        }
    }
}
