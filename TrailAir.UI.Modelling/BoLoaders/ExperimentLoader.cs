﻿using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB;
using TrailAir.DB.Models;
using TrailAir.DB.Models.Base;
using TrailAir.DB.Models.Enumerations;
using TrailAir.UI.Modelling.BoMappers.Experiment;
using TrailAir.UI.Modelling.BoMappers.Participant;
using TrailAir.UI.Modelling.BoMappers.ParticipantResult;
using TrailAir.UI.Modelling.UIModels.Experiment;
using TrailAir.UI.Modelling.UIModels.GenericApp;
using TrailAir.UI.Modelling.UIModels.Participant;
using TrailAir.Utility;
using TrailAir.Utility.ExceptionHelpers;


namespace TrailAir.UI.Modelling.BoLoaders
{
    public class ExperimentLoader : BaseBoLoader<ExperimentDetailsIM>
    {



        public static Experiment GetItemDet(TrailAirDbContext db, int objectId)
        {

            Experiment experimentDb;
            if (objectId == 0)
            {
                experimentDb = new Experiment();
            }
            else
            {
                experimentDb = db.Experiments.AsNoTracking()
                    //.Include(_ => _.TestType)
                    //.Include(_ => _.ScoringScaleDefinition)
                    .Include(_ => _.SamplePreparation)
                    .Include(_ => _.Team)
                    .Include(_ => _.ExperimentStatus)
                    .Include(_ => _.Participants).ThenInclude(x => x.Participant)
                    .Include(_ => _.ControlSample).ThenInclude(x => x.ControlSampleType)
                    .Include(_ => _.ExperimentBenchmarks)
                    .Include(_ => _.ExperimentTestSamples)
                    .Include(_ => _.Requester)
                    .Where(_ => _.Id == objectId).FirstOrDefault();

                experimentDb.ExperimentTestSamples.Sort((x, y) => x.SampleIndex.CompareTo(y.SampleIndex));

                if (experimentDb == null)
                    throw new Exception($"IT controlled error requested Business Logic User doesn't exist in the DB :{objectId}");
            }

            return experimentDb;
        }


        public static ExperimentDetailsIM GetItemDetUI(TrailAirDbContext db, int objectId)
        {

            Experiment experimentDb = GetItemDet(db, objectId);

            ExperimentDetailsIM elementDB = ExperimentMapper.MapDBToUIDet(experimentDb);

            SetParticipantsSubmittedResult(db, elementDB);

            return elementDB;
        }

        /// <summary>
        /// Get Experiments for the list page based on various filters
        /// </summary>
        /// <param name="db"></param>
        /// <param name="teamsIds">Or the Location of the Experiment</param>
        /// <param name="statusIds">For now: Draft, Inprogress, Finished - look into ExperimentStatusEnum</param>
        /// <param name="appUserManager"></param>
        /// <returns></returns>
        public static List<ExperimentListIM> GetItemFilteredList(TrailAirDbContext db, string teamsIds, string statusIds)
        {
            var experimentsIQ = db.Experiments.AsNoTracking();

            experimentsIQ = FilterExperimentsByTeams(teamsIds, experimentsIQ);

            experimentsIQ = FilterExperimentsByStatuses(statusIds, experimentsIQ);

            experimentsIQ = experimentsIQ.Include(_ => _.Team)
                                .Include(_ => _.ExperimentStatus)
                                .Include(_ => _.Participants).ThenInclude(_ => _.Participant)
                                .OrderByDescending(_ => _.UpdatedAt);


            List<ExperimentListIM> elementsDB = new List<ExperimentListIM>();
            var tmpElementsDB = experimentsIQ.ToList();

            elementsDB = ExperimentMapper.MapDBToUI(tmpElementsDB);

            foreach (var experimentList in elementsDB)
            {
                SetParticipantsSubmittedResult(db, experimentList);
            }

            foreach (var experimentList in elementsDB)
            {
                if (experimentList.Participants != null && experimentList.Participants.Count > 0)
                {
                    experimentList.HasAllResultsSubmitted = experimentList.Participants.All(p => p.HasSubmittedResult);
                }
            }

            return elementsDB;
        }

        private static IQueryable<Experiment> FilterExperimentsByTeams(string teamsIds, IQueryable<Experiment> experimentsIQ)
        {
            if (!string.IsNullOrEmpty(teamsIds) && teamsIds != "0")
            {
                var ids = teamsIds.Split(',').Select(_ => Convert.ToInt32(_.Trim()));

                experimentsIQ = experimentsIQ.Where(_ => ids.Contains((int)_.TeamId));
            }

            return experimentsIQ;
        }

        private static IQueryable<Experiment> FilterExperimentsByStatuses(string statusIds, IQueryable<Experiment> experimentsIQ)
        {
            if (!string.IsNullOrEmpty(statusIds) && statusIds != "0" && !statusIds.Contains("0"))
            {
                var ids = statusIds.Split(',').Select(_ => Convert.ToInt32(_.Trim()));

                experimentsIQ = experimentsIQ.Where(_ => ids.Contains((int)_.ExperimentStatusId));
            }

            return experimentsIQ;
        }

        /// <summary>
        /// For every Participant in this Experiment, check did the Participant submitted result
        /// </summary>
        /// <param name="db"></param>
        /// <param name="experimentListOb"></param>
        private static void SetParticipantsSubmittedResult(TrailAirDbContext db, ExperimentListIM experimentListOb)
        {
            if (experimentListOb.Participants != null && experimentListOb.Participants.Count > 0)
            {
                foreach (var participant in experimentListOb.Participants)
                {
                    var participantResult = db.ParticipantResults.SingleOrDefault(pr
                        => pr.ExperimentId == experimentListOb.Id && pr.ParticipantId == participant.ParticipantId);
                    if (participantResult == null)
                    {
                        participant.HasSubmittedResult = false;
                    }
                    else
                    {
                        participant.HasSubmittedResult = participantResult.IsSubmitted;
                    }
                }
            }
        }

        public static SingleBOActionResponse Update(TrailAirDbContext db, ExperimentDetailsIM objectToUpdateFrom, string userEmail)
        {
            var response = new SingleBOActionResponse()
            {
                Success = true
            };

            Experiment elementDB;
            if (objectToUpdateFrom.Id < 1)
            {
                elementDB = new Experiment();

                /// If there is just empty Participants, make empty list, otherwise the newly created Experiment 
                /// will not be saved because there is no Participant with Id 0
                if (objectToUpdateFrom.Participants.All(_ => _.ParticipantId == "0"))
                {
                    objectToUpdateFrom.Participants = new List<ExperimentsParticipantListIM>();
                }

                ExperimentMapper.MapUIToDbDet(db, objectToUpdateFrom, elementDB);
                elementDB.TeamId = objectToUpdateFrom.TeamId;
                db.Experiments.Add(elementDB);

                db.SaveChanges(userEmail);

                List<TestSample> testSamples = new List<TestSample>();

                var lastExperiment = db.Experiments.OrderBy(e => e.Id).Last();
                foreach (var testSampleUi in objectToUpdateFrom.ExperimentTestSamples)
                {
                    testSampleUi.ExperimentId = lastExperiment.Id;
                    testSamples.Add(ExperimentMapper.MapExperimentTestSampleUIToDB(new TestSample(), testSampleUi));
                }

                lastExperiment.ExperimentTestSamples = testSamples;

                db.SaveChanges(userEmail);
            }
            else
            {
                elementDB = db.Experiments
                    .Include(_ => _.SamplePreparation)
                    .Include(_ => _.Participants).ThenInclude(_ => _.Participant)
                    .Include(_ => _.ExperimentStatus)
                    .Include(_ => _.ExperimentBenchmarks)
                    .Include(_ => _.ExperimentTestSamples)
                    .FirstOrDefault(x => x.Id == objectToUpdateFrom.Id);

                db.ExperimentsParticipants.RemoveRange(db.ExperimentsParticipants.Where(ep => ep.ExperimentId == elementDB.Id));
                /// If there is at least on Participant which ParticipantId is not 0 (not an empty one), map all of them
                if (objectToUpdateFrom.Participants.Any(_ => _.ParticipantId != "0"))
                {
                    elementDB.Participants = objectToUpdateFrom.Participants.Select(p => new ExperimentsParticipant()
                    {
                        ParticipantId = p.ParticipantId,
                    }).ToList();
                }
                /// If there is no Participants in UI model, or you have just empty ones, remove all of them 
                /// from the DB object
                else
                {
                    objectToUpdateFrom.Participants = new List<ExperimentsParticipantListIM>();
                    db.ExperimentsParticipants.RemoveRange(elementDB.Participants);
                    elementDB.Participants = new List<ExperimentsParticipant>();
                }

                elementDB.NumberOfParticipants = elementDB.Participants.Count;

                ExperimentMapper.MapUIToDbDet(db, objectToUpdateFrom, elementDB);


                // You don't (and can't) change and delete Test Samples for a published Experiment
                if (elementDB.ExperimentStatus.ExperimentStatusCode != ExperimentStatusEnum.InProgress)
                {
                    List<TestSample> testSamples = new List<TestSample>();

                    db.TestSamples.RemoveRange(db.TestSamples.Where(ep => ep.ExperimentId == elementDB.Id));
                    foreach (var testSampleUi in objectToUpdateFrom.ExperimentTestSamples)
                    {
                        testSampleUi.ExperimentId = elementDB.Id;
                        testSamples.Add(ExperimentMapper.MapExperimentTestSampleUIToDB(new TestSample(), testSampleUi));
                    }

                    elementDB.ExperimentTestSamples = testSamples;
                }

                db.SaveChanges(userEmail);
            }

            response.ObjectId = elementDB.Id;
            return response;
        }

        private static void UpdateOrCreateParticipant(ExperimentsParticipant particpant, List<ExperimentsParticipant> participants)
        {
            var participantForUpdate = participants.SingleOrDefault(x => x.ParticipantId == particpant.ParticipantId);
            if (participantForUpdate == null)
            {
                participants.Add(participantForUpdate);
            }
            else
            {
                participantForUpdate.ParticipantId = particpant.ParticipantId;
            }
        }

        public static SingleBOActionResponse Delete(TrailAirDbContext db, int experimentId, string userEmail)
        {
            var response = new SingleBOActionResponse()
            {
                Success = true
            };
            Experiment experimentForDeletion = db.Experiments
                .Include(e => e.Participants)
                .Include(e => e.ExperimentTestSamples)
                .Include(e => e.ControlSample).ThenInclude(_ => _.ControlSampleType)
                .Include(e => e.SamplePreparation)
                .SingleOrDefault(e => e.Id == experimentId);

            if (experimentForDeletion != null)
            {
                var participantResults = db.ParticipantResults.AsNoTracking()
                    .Include(pr => pr.BenchmarkResults).ThenInclude(br => br.Attributes)
                    .Include(pr => pr.TestSampleResults).ThenInclude(br => br.Attributes)
                    .Where(pr => pr.ExperimentId == experimentId);

                foreach (var participantResult in participantResults)
                {
                    foreach (var benchmarkResult in participantResult.BenchmarkResults)
                    {
                        db.ExperimentResultAttributes.RemoveRange(benchmarkResult.Attributes);
                    }
                    foreach (var testSampleResult in participantResult.TestSampleResults)
                    {
                        db.ExperimentResultAttributes.RemoveRange(testSampleResult.Attributes);
                    }
                }
                db.ExperimentsParticipants.RemoveRange(experimentForDeletion.Participants);
                db.ParticipantResults.RemoveRange(db.ParticipantResults.Where(pr => pr.ExperimentId == experimentId));
                db.TestSamples.RemoveRange(experimentForDeletion.ExperimentTestSamples);
                if (experimentForDeletion.ControlSample != null)
                {
                    db.ControlSamples.Remove(experimentForDeletion.ControlSample);
                }


                db.Experiments.Remove(experimentForDeletion);

                /// You have to delete SamplePreparation after the Experiment because the Experiment has 
                /// not nullable Foreign Key to SamplePreparation
                if (experimentForDeletion.SamplePreparation != null)
                {
                    experimentForDeletion.SamplePreparation.Experiment = null;
                    db.SamplePreparations.Remove(experimentForDeletion.SamplePreparation);
                }
            }
            else
            {
                throw new Exception("Experiment not found");
            }

            db.SaveChanges(userEmail);
            response.ObjectId = experimentId;

            return response;
        }

        public static SingleBOActionResponse Duplicate(TrailAirDbContext db, int experimentId, string newName, string userEmail)
        {
            var response = new SingleBOActionResponse()
            {
                Success = true
            };
            Experiment experimentForDuplication = db.Experiments
                .Include(e => e.ControlSample)
                .Include(e => e.SamplePreparation)
                .Include(e => e.Participants)
                .Include(e => e.ExperimentTestSamples)
                .SingleOrDefault(e => e.Id == experimentId);

            if (experimentForDuplication != null)
            {
                var newExperiment = new Experiment();
                newExperiment.Id = 0;
                newExperiment.Name = newName;

                if (experimentForDuplication.ControlSample != null)
                {
                    var newControlSample = new ControlSample()
                    {
                        SampleName = experimentForDuplication.ControlSample.SampleName,
                        SampleCode = experimentForDuplication.ControlSample.SampleCode,
                        ControlSampleTypeId = experimentForDuplication.ControlSample.ControlSampleTypeId,
                        LOTCode = experimentForDuplication.ControlSample.LOTCode,
                        ManufacturedDate = experimentForDuplication.ControlSample.ManufacturedDate,
                        Base = experimentForDuplication.ControlSample.Base
                    };
                    newExperiment.ControlSample = newControlSample;
                }

                if (experimentForDuplication.SamplePreparation != null)
                {
                    var newSamplePreparation = new SamplePreparation()
                    {
                        EvaporingTemperature = experimentForDuplication.SamplePreparation.EvaporingTemperature,
                        EvaporingTime = experimentForDuplication.SamplePreparation.EvaporingTime,
                        PerfumeConcentration = experimentForDuplication.SamplePreparation.PerfumeConcentration,
                    };
                    newExperiment.SamplePreparation = newSamplePreparation;
                }

                newExperiment.TeamId = experimentForDuplication.TeamId;
                newExperiment.ExperimentStatus = db.ExperimentStatuses.SingleOrDefault(es => es.ExperimentStatusCode == ExperimentStatusEnum.Draft);
                newExperiment.RequesterId = experimentForDuplication.RequesterId;
                newExperiment.ExperimentStatusId = (int)ExperimentStatusEnum.Draft;
                newExperiment.Consumer = experimentForDuplication.Consumer;
                newExperiment.DateFinished = experimentForDuplication.DateFinished;
                newExperiment.Comment = experimentForDuplication.Comment;
                newExperiment.Instruction = experimentForDuplication.Instruction;

                db.Experiments.Add(newExperiment);
                db.SaveChanges(userEmail);


                var lastAddedExperiment = db.Experiments.OrderBy(e => e.Id).Last();

                if (experimentForDuplication.Participants != null && experimentForDuplication.Participants.Count > 0)
                {
                    var newExperimentParticipants = new List<ExperimentsParticipant>();
                    foreach (var participant in experimentForDuplication.Participants)
                    {
                        newExperimentParticipants.Add(new ExperimentsParticipant()
                        {
                            ExperimentId = lastAddedExperiment.Id,
                            ParticipantId = participant.ParticipantId
                        });
                    }
                    lastAddedExperiment.Participants = newExperimentParticipants;
                    lastAddedExperiment.NumberOfParticipants = lastAddedExperiment.Participants.Count;
                }

                if (experimentForDuplication.ExperimentTestSamples != null && experimentForDuplication.ExperimentTestSamples.Count > 0)
                {
                    var newExperimentTestSamples = new List<TestSample>();
                    foreach (var expTestSample in experimentForDuplication.ExperimentTestSamples)
                    {
                        newExperimentTestSamples.Add(new TestSample
                        {
                            ExperimentId = lastAddedExperiment.Id,
                            IsBenchmark = expTestSample.IsBenchmark,
                            IsInternal = expTestSample.IsInternal,
                            ProductName = expTestSample.ProductName,
                            InternalName = expTestSample.InternalName,
                            FantasyName = expTestSample.FantasyName,
                            LOTCode = expTestSample.LOTCode,
                            ManufacturedDate = expTestSample.ManufacturedDate,
                            ApplicationDate = expTestSample.ApplicationDate,
                            GroupNumber = expTestSample.GroupNumber,
                            TrialNumber = expTestSample.TrialNumber,
                            Technology = expTestSample.Technology,
                            Concentration = expTestSample.Concentration,
                            Base = expTestSample.Base,
                            SampleIndex = expTestSample.SampleIndex,
                            Random = expTestSample.Random,
                        });
                    }
                    lastAddedExperiment.ExperimentTestSamples = newExperimentTestSamples;
                }

                db.SaveChanges(userEmail);
            }
            else
            {
                throw new Exception("Experiment not found");
            }

            response.ObjectId = experimentId;

            return response;
        }

        public static SingleBOActionResponse Publish(TrailAirDbContext db, ExperimentDetailsIM experimentDetails, string userEmail)
        {
            var response = new SingleBOActionResponse()
            {
                Success = true
            };
            var updReslt = Update(db, experimentDetails, userEmail);
            if (updReslt.Success == false)
                return updReslt;


            Experiment experimentForPublish = db.Experiments
                .Include(e => e.ExperimentStatus)
                .SingleOrDefault(e => e.Id == updReslt.ObjectId);


            if (experimentForPublish == null)
                throw new Exception("Experiment not found");

            experimentForPublish.ExperimentStatus = db.ExperimentStatuses.SingleOrDefault(es => es.ExperimentStatusCode == ExperimentStatusEnum.InProgress);
            experimentForPublish.DatePublished = DateTime.Now;

            RandomizeAndIndexTestSamples(experimentForPublish.ExperimentTestSamples);

            db.SaveChanges(userEmail);



            response.ObjectId = experimentDetails.Id;

            return response;
        }

        private static void RandomizeAndIndexTestSamples(List<TestSample> experimentTestSamples)
        {
            Random random = new Random();
            foreach (var testSample in experimentTestSamples)
            {
                testSample.Random = random.Next(100);
            }
            experimentTestSamples.Sort((x, y) => x.Random.CompareTo(y.Random));

            for (int i = 0; i < experimentTestSamples.Count; i++)
            {
                experimentTestSamples[i].SampleIndex = i + 1;
                experimentTestSamples[i].SampleCode = $"S{(i + 1).ToString().PadLeft(2, '0')}";
            }
        }

        public static byte[] GetExperimentResultExcelFile(TrailAirDbContext db, int objectId, string webRootPath, out string outputFileName)
        {
            outputFileName = "";
            if (objectId == 0) return null;

            //template directory
            string templateDirectory = webRootPath + "\\templates";
            //file directory
            string fileDirectory = webRootPath + "\\processedFiles";
            //file name
            string fileName = "ExpWizardResultTemplate_V4.xlsx";

            byte[] document;

            var participantResults = db.ParticipantResults
                            .Include(_ => _.TestSampleResults).ThenInclude(x => x.Attributes)
                            .Include(_ => _.TestSampleResults).ThenInclude(x => x.TestSample)
                            .Include(_ => _.ControlSampleResult).ThenInclude(x => x.Attributes)
                            .Include(_ => _.ControlSampleResult).ThenInclude(x => x.ControlSample).ThenInclude(x => x.ControlSampleType)
                            .Include(_ => _.Experiment).ThenInclude(_ => _.ExperimentStatus)
                            .Include(_ => _.Experiment).ThenInclude(_ => _.Requester)
                            .Include(_ => _.Experiment).ThenInclude(_ => _.Team)
                            .Include(_ => _.Experiment).ThenInclude(_ => _.Participants)
                            .Include(_ => _.Experiment).ThenInclude(_ => _.SamplePreparation)
                            .Include(_ => _.Participant)
                            .Where(_ => _.ExperimentId == objectId && _.IsSubmitted == true)
                            .AsNoTracking()
                            .ToList();

            ApplicationUser userCreator;
            if (participantResults.Count > 0)
            {
                userCreator = db.Users.First(_ => _.Email == participantResults.First().CreatedBy);
            }
            else return null; // This shouldn't happen!!

            FileInfo template = new FileInfo(Path.Combine(templateDirectory, fileName));
            FileInfo newFile = new FileInfo(Path.Combine(fileDirectory, fileName));
            var users = db.Users.ToList();
            //generate excel file from template
            MemoryStream streamData = GenerateExcelFile(newFile, template, participantResults, userCreator, users);

            using (Stream stream = streamData)
            {
                using (ExcelPackage excelPackage = new ExcelPackage(stream))
                {
                    document = excelPackage.GetAsByteArray();
                }
            }

            if (participantResults.Count > 0)
            {
                outputFileName = "Exp_" + participantResults.First().Experiment.Name + "_Result_" + DateTime.Now.ToString("dd_MM_yyyy") + ".xlsx";
            }

            return document;
        }

        #region Generate Experiment Excel Result 

        private static void UpdatedCellCollor(ExcelRange excelPageCells, int row, int colSt, int colEnd, ClrCellDef color)
        {
            if (ClrCellDef.whSd != color)
            {
                excelPageCells[row, colSt, row, colEnd].Style.Fill.PatternType = ExcelFillStyle.Solid;
            }
            for (int col = colSt; col <= colEnd; col++)
            {
                excelPageCells[row, col].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            }

            switch (color)
            {
                case ClrCellDef.lhBl:
                    excelPageCells[row, colSt, row, colEnd].Style.Fill.BackgroundColor.SetColor(1, 221, 235, 247);
                    break;
                case ClrCellDef.dkBl:
                    excelPageCells[row, colSt, row, colEnd].Style.Fill.BackgroundColor.SetColor(1, 189, 215, 238);
                    break;
                case ClrCellDef.gyBl:
                    excelPageCells[row, colSt, row, colEnd].Style.Fill.BackgroundColor.SetColor(1, 217, 217, 217);
                    break;
                case ClrCellDef.dkBlTl:
                    excelPageCells[row, colSt, row, colEnd].Style.Fill.BackgroundColor.SetColor(1, 155, 194, 230);
                    break;
                default:
                    //excelPageCells[row, colSt, row, colEnd].Style.Fill.BackgroundColor.SetColor(1, 0, 0, 0);
                    //Do nothig, also used for White
                    break;
            }
        }
        /// <summary>
        /// Excell cells collor used and defined on the Experiment Wizard Resault
        /// </summary>
        private enum ClrCellDef
        {
            /// <summary>
            /// Light Blue data cells 
            /// </summary>
            lhBl = 1,
            /// <summary>
            /// Dark Blue data cells 
            /// </summary>
            dkBl = 3,
            /// <summary>
            /// Light Gray data cells 
            /// </summary>
            gyBl = 6,
            /// <summary>
            /// Wight Standard
            /// </summary>
            whSd = 10,
            /// <summary>
            /// Dark Blue title cells 
            /// </summary>
            dkBlTl = 15,

        }
        internal class TmpResultAvgs
        {
            public int sampleId { get; set; }
            public decimal avgIntensity { get; set; }
            public decimal avgDetectionTime { get; set; }
            public decimal avgFullDetectionTime { get; set; }
        }


        private static string GetUserName(Dictionary<string, string> userDic, string email)
        {
            string userName;
            if (userDic.TryGetValue(email, out userName))
                return userName;

            return email;
        }

        private static MemoryStream GenerateExcelFile(FileInfo newFile, FileInfo template, List<ParticipantResult> participantResults,
            ApplicationUser userCreator, List<ApplicationUser> users)
        {
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            Dictionary<string, string> userDic = new Dictionary<string, string>();
            foreach (var user in users)
                userDic.Add(user.Email, user.Fullname);

            using (ExcelPackage xlPackage = new ExcelPackage(newFile, template))
            {

                var i = 1;
                ///Page with result 
                var worksheetResults = xlPackage.Workbook.Worksheets[1];
                ///page with test information 
                var worksheetTestInformation = xlPackage.Workbook.Worksheets[0];
                ///page with summary 
                var worksheetSummary = xlPackage.Workbook.Worksheets[2];

                var resultData = new List<dynamic>();

                var clrText = ClrCellDef.lhBl;
                var clrNum = ClrCellDef.whSd;

                var partNum = participantResults.Count();



                if (participantResults.Count == 0)
                    throw new Exception("Can't create experiment result excel that has no participan results in it. Umanova controlled error.");
                var experiment = participantResults.FirstOrDefault().Experiment;

                MergeGrpAvg(2, partNum, worksheetResults);


                #region calculate avagrage and group data
                decimal avgCsIntens = (decimal)participantResults.Sum(_ => _.ControlSampleResult.Intensity) / partNum;
                decimal avgCsDetectTime = (decimal)participantResults.Sum(_ => _.ControlSampleResult.FirstDetectionTime) / partNum;
                decimal avgCsFullDetectTime = (decimal)participantResults.Sum(_ => _.ControlSampleResult.FullDetectionTime) / partNum;


                foreach (var parRes in participantResults)
                {
                    i += 1;
                    #region insertcontrol sample 

                    var controlSample = parRes.ControlSampleResult;
                    //Sample code 
                    worksheetResults.Cells[i, 1].Value = controlSample.ControlSample.SampleCode; // This should be S01, S02 etc. Is SampleCode right property ?

                    //Product Name 
                    worksheetResults.Cells[i, 2].Value = controlSample.ControlSample.ControlSampleType.Name;

                    ////Fantasy Name ////Internal Name ////Group Code ////Trail Number

                    //Participant
                    worksheetResults.Cells[i, 8].Value = parRes.Participant.Fullname;
                    UpdatedCellCollor(worksheetResults.Cells, i, 1, 8, clrText);

                    AddToExclRowBaseResult(worksheetResults, parRes.ControlSampleResult, avgCsIntens, avgCsDetectTime, avgCsFullDetectTime, i, userCreator.Fullname, userDic, "Control", string.Empty, clrText, clrNum);

                    #endregion

                    foreach (var testSample in parRes.TestSampleResults)
                        resultData.Add(new { ParticipantResultAll = parRes, TestSampleResultOb = testSample });
                }


                var dataOrderedByTestSample = resultData.OrderBy(_ => _.TestSampleResultOb.TestSampleId).ThenBy(_ => _.ParticipantResultAll.ParticipantId);

                var orderdForAvgIntesity = dataOrderedByTestSample.GroupBy(_ => _.TestSampleResultOb.TestSampleId)
                    .Select(g =>
                    new TmpResultAvgs
                    {
                        sampleId = g.Key,
                        avgIntensity = g.Average(x => x.TestSampleResultOb.Intensity == null ? (decimal)0 : (decimal)x.TestSampleResultOb.Intensity),
                        avgDetectionTime = g.Average(x => x.TestSampleResultOb.FirstDetectionTime == null ? (decimal)0 : (decimal)x.TestSampleResultOb.FirstDetectionTime),
                        avgFullDetectionTime = g.Average(x => x.TestSampleResultOb.FirstDetectionTime == null ? (decimal)0 : (decimal)x.TestSampleResultOb.FullDetectionTime)
                    }).ToList();


                #endregion


                #region test info and summary page

                PrintTestInfoPage(participantResults, worksheetTestInformation, experiment, avgCsIntens, avgCsDetectTime, avgCsFullDetectTime, orderdForAvgIntesity, userDic);
                PrintSummeryPage(participantResults, worksheetSummary, experiment, avgCsIntens, avgCsDetectTime, avgCsFullDetectTime, orderdForAvgIntesity);


                #endregion test info and summary page


                var latestSampleId = 0;
                var isEven = false;


                foreach (var resultOb in dataOrderedByTestSample)
                {
                    var parRes = resultOb.ParticipantResultAll;
                    var testSample = resultOb.TestSampleResultOb;

                    i += 1;
                    if (testSample.TestSampleId != latestSampleId)
                    {
                        MergeGrpAvg(i, partNum, worksheetResults);
                        isEven = !isEven;
                        latestSampleId = testSample.TestSampleId;
                    }

                    clrText = isEven ? ClrCellDef.dkBl : ClrCellDef.lhBl;
                    clrNum = isEven ? ClrCellDef.gyBl : ClrCellDef.whSd;


                    //Sample code 
                    worksheetResults.Cells[i, 1].Value = testSample.TestSample.SampleCode; // This should be S01, S02 etc. Is SampleCode right property ?

                    //Product Name 
                    worksheetResults.Cells[i, 2].Value = testSample.TestSample.ProductName;

                    //Fantasy Name 
                    worksheetResults.Cells[i, 3].Value = testSample.TestSample.FantasyName;

                    //Internal Name 
                    worksheetResults.Cells[i, 4].Value = testSample.TestSample.InternalName;

                    //Group Code 
                    worksheetResults.Cells[i, 5].Value = testSample.TestSample.GroupNumber;

                    //Trail Number
                    worksheetResults.Cells[i, 6].Value = testSample.TestSample.TrialNumber;

                    //Comment
                    worksheetResults.Cells[i, 7].Value = testSample.TestSample.Comment;

                    //Partecipant
                    worksheetResults.Cells[i, 8].Value = parRes.Participant.Fullname;
                    UpdatedCellCollor(worksheetResults.Cells, i, 1, 8, clrText);


                    var avargData = orderdForAvgIntesity.FirstOrDefault(_ => _.sampleId == testSample.TestSampleId);


                    AddToExclRowBaseResult(worksheetResults, testSample, avargData.avgIntensity, avargData.avgDetectionTime, avargData.avgFullDetectionTime, i, userCreator.Fullname, userDic, testSample.TestSample.BenchOrSub, testSample.TestSample.InterExt, clrText, clrNum);

                }


                xlPackage.Save();

                var stream = new MemoryStream(xlPackage.GetAsByteArray());
                return stream;
            }
        }

        private static void MergeGrpAvg(int i, int partNum, ExcelWorksheet worksheetResults)
        {
            var endOfGrp = i + partNum - 1;
            worksheetResults.Cells[i, 10, endOfGrp, 10].Merge = true;
            worksheetResults.Cells[i, 10, endOfGrp, 10].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            worksheetResults.Cells[i, 10, endOfGrp, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheetResults.Cells[i, 14, endOfGrp, 14].Merge = true;

            worksheetResults.Cells[i, 14, endOfGrp, 14].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            worksheetResults.Cells[i, 14, endOfGrp, 14].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            worksheetResults.Cells[i, 15, endOfGrp, 15].Merge = true;
            worksheetResults.Cells[i, 15, endOfGrp, 15].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            worksheetResults.Cells[i, 15, endOfGrp, 15].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        }

        private static void PrintTestInfoPage(List<ParticipantResult> participantResults, ExcelWorksheet worksheetTestInformation, Experiment experiment, decimal avgCsIntens, decimal avgCsDetectTime, decimal avgCsFullDetectTime, List<TmpResultAvgs> testSmplAvgs, Dictionary<string, string> userDic)
        {
            //clrText = isEven ? ClrCellDef.dkBl : ClrCellDef.lhBl;
            //clrNum = isEven ? ClrCellDef.gyBl : ClrCellDef.whSd;
            var anyPartResoult = participantResults[0];
            //Test infromation line 
            int tstInfLn = 2;
            //test name B
            worksheetTestInformation.Cells[tstInfLn, 2].Value = experiment.Name;
            //Status C
            worksheetTestInformation.Cells[tstInfLn, 3].Value = ExperimentMapper.MapStatusCodeToText(experiment.ExperimentStatus.ExperimentStatusCode);
            //Requestor D
            worksheetTestInformation.Cells[tstInfLn, 4].Value = experiment.Requester?.Fullname ?? "";
            ///Creator E
            ///We are going to try to get the user by email. 
            ///If we don't find it ok, we jsut write the email
            worksheetTestInformation.Cells[tstInfLn, 5].Value = GetUserName(userDic, experiment.CreatedBy);
            //Location F
            worksheetTestInformation.Cells[tstInfLn, 6].Value = experiment.Team.Name;
            //Creation Date G
            worksheetTestInformation.Cells[tstInfLn, 7].Value = new DateOb(experiment.DatePublished).ToString();
            // Client H
            worksheetTestInformation.Cells[tstInfLn, 8].Value = experiment.Consumer;
            //Nb finished test I
            worksheetTestInformation.Cells[tstInfLn, 9].Value = participantResults.Count(_ => _.IsSubmitted);
            //Nb of participants J
            worksheetTestInformation.Cells[tstInfLn, 10].Value = experiment.Participants.Count;
            // TEst Instruction K
            worksheetTestInformation.Cells[tstInfLn, 11].Value = experiment.Instruction;
            //Comments O,  12 ili 15
            worksheetTestInformation.Cells[tstInfLn, 15].Value = experiment.Comment;

            ///Sample preparation data
            int smpPrepLn = 5;

            //Evap time  B
            worksheetTestInformation.Cells[smpPrepLn, 2].Value = experiment.SamplePreparation.EvaporingTime;
            //Evap temperature  C
            worksheetTestInformation.Cells[smpPrepLn, 3].Value = experiment.SamplePreparation.EvaporingTemperature;

            var contrSmplRes = participantResults.Select(_ => (BaseExperimentResult)_.ControlSampleResult);
            string descriptors = GetDescriptorFromBaseExpAttr(contrSmplRes);
            string addInfo = GetAddInfoFromBaseExpAttr(contrSmplRes);
            var i = 8;
            var controlSample = anyPartResoult.ControlSampleResult.ControlSample;
            PrintTestInfoLine(i, worksheetTestInformation,
            string.Empty, controlSample.ControlSampleType.Name, string.Empty, string.Empty,
            string.Empty, string.Empty, "Control",
            string.Empty, controlSample.LOTCode, controlSample.ManufacturedDate,
            controlSample.Base, string.Empty, null,
            string.Empty, string.Empty, false);


            var clrText = ClrCellDef.whSd;
            UpdatedCellCollor(worksheetTestInformation.Cells, i, 2, 20, clrText);


            foreach (var testSmplRes in anyPartResoult.TestSampleResults)
            {
                i++;
                clrText = i % 2 != 0 ? ClrCellDef.gyBl : ClrCellDef.whSd;
                var ts = testSmplRes.TestSample;
                var semplAllRes = participantResults.SelectMany(_ => _.TestSampleResults).Where(_ => _.TestSampleId == testSmplRes.TestSampleId);

                string tsDescriptors = GetDescriptorFromBaseExpAttr(semplAllRes);
                string tsAddInfo = GetAddInfoFromBaseExpAttr(semplAllRes);

                var avgTsIntens = testSmplAvgs.First(_ => _.sampleId == testSmplRes.TestSampleId).avgIntensity;
                var avgTsDetectTime = testSmplAvgs.First(_ => _.sampleId == testSmplRes.TestSampleId).avgDetectionTime;
                var avgTsFullDetectTime = testSmplAvgs.First(_ => _.sampleId == testSmplRes.TestSampleId).avgFullDetectionTime;

                PrintTestInfoLine(i, worksheetTestInformation,
                    ts.SampleCode, ts.ProductName, ts.FantasyName, ts.InternalName,
                    ts.GroupNumber, ts.TrialNumber, ts.BenchOrSub,
                    ts.InterExt, ts.LOTCode, ts.ManufacturedDate,
                    ts.Base, ts.Technology, ts.ApplicationDate,
                    ts.ConcentrationStr, ts.Comment, ts.IsInternal);
                UpdatedCellCollor(worksheetTestInformation.Cells, i, 2, 20, clrText);
            }
            worksheetTestInformation.Cells[7, 1, i, 1].Merge = true;

        }

        private static void PrintSummeryPage(List<ParticipantResult> participantResults, ExcelWorksheet worksheetTestInformation, Experiment experiment, decimal avgCsIntens, decimal avgCsDetectTime, decimal avgCsFullDetectTime, List<TmpResultAvgs> testSmplAvgs)
        {
            //clrText = isEven ? ClrCellDef.dkBl : ClrCellDef.lhBl;
            //clrNum = isEven ? ClrCellDef.gyBl : ClrCellDef.whSd;
            var anyPartResoult = participantResults[0];


            var contrSmplRes = participantResults.Select(_ => (BaseExperimentResult)_.ControlSampleResult);
            string descriptors = GetDescriptorFromBaseExpAttr(contrSmplRes);
            string addInfo = GetAddInfoFromBaseExpAttr(contrSmplRes);
            var i = 2;

            var controlSample = anyPartResoult.ControlSampleResult.ControlSample;
            PrintSummeryLine(i, worksheetTestInformation,
            string.Empty, controlSample.ControlSampleType.Name, string.Empty, string.Empty,
            string.Empty, string.Empty, string.Empty,
            avgCsIntens, avgCsDetectTime, avgCsFullDetectTime,
            descriptors, addInfo, "Control");


            foreach (var testSmplRes in anyPartResoult.TestSampleResults)
            {
                i++;
                var ts = testSmplRes.TestSample;

                var semplAllRes = participantResults.SelectMany(_ => _.TestSampleResults).Where(_ => _.TestSampleId == testSmplRes.TestSampleId);

                string tsDescriptors = GetDescriptorFromBaseExpAttr(semplAllRes);
                string tsAddInfo = GetAddInfoFromBaseExpAttr(semplAllRes);

                var avgTsIntens = testSmplAvgs.First(_ => _.sampleId == testSmplRes.TestSampleId).avgIntensity;
                var avgTsDetectTime = testSmplAvgs.First(_ => _.sampleId == testSmplRes.TestSampleId).avgDetectionTime;
                var avgTsFullDetectTime = testSmplAvgs.First(_ => _.sampleId == testSmplRes.TestSampleId).avgFullDetectionTime;


                PrintSummeryLine(i, worksheetTestInformation,
                    ts.SampleCode, ts.ProductName, ts.FantasyName, ts.InternalName,
                    ts.GroupNumber, ts.TrialNumber, ts.Comment,
                    avgTsIntens, avgTsDetectTime, avgTsFullDetectTime,
                    tsDescriptors, tsAddInfo, ts.BenchOrSub);
            }

        }

        private static string GetDescriptorFromBaseExpAttr(IEnumerable<BaseExperimentResult> baseExpResults)
        {
            var descriptors = new StringBuilder();

            foreach (var usResult in baseExpResults)
            {
                if (descriptors.Length > 0)
                    descriptors.Append(" / ");

                var singExp = new StringBuilder();
                foreach (var attVal in usResult.Attributes.Where(_ => !string.IsNullOrEmpty(_.AttributeText)))
                {
                    if (singExp.Length > 0)
                        singExp.Append(", ");
                    singExp.Append(attVal.AttributeText);
                }
                descriptors.Append(singExp);
            }

            return descriptors.ToString();
        }
        private static string GetAddInfoFromBaseExpAttr(IEnumerable<BaseExperimentResult> baseExpResults)
        {
            var addInfo = new StringBuilder();

            foreach (var usResult in baseExpResults)
            {
                if (addInfo.Length > 0)
                    addInfo.Append(" / ");

                var val = string.IsNullOrEmpty(usResult.Sensation) ? "-" : usResult.Sensation;
                addInfo.Append(val);
            }

            return addInfo.ToString();
        }

        private static string WithDefault(object toPrint, string defaultVal = "-")
        {
            if (toPrint == null)
                return defaultVal;

            var trimmed = toPrint.ToString().Trim();
            if (trimmed.Length == 0)
                return defaultVal;

            return trimmed;

        }

        private static void PrintTestInfoLine(int row, ExcelWorksheet worksheetTestInformation,
           string sampleCode, string productName, string fantasyName, string internalName,
           string groupCode, string trialNumber, string ctrlSmpl,
           string internExt, string lotCode, DateTime? manufDate,
           string baseStr, string tehnology, 
           DateTime? applicDate, string concentration, string comment, bool isInterTestSample)
        {
            var clrText = 0 == (row % 2) ? ClrCellDef.dkBl : ClrCellDef.lhBl;
            var clrNum = 0 == (row % 2) ? ClrCellDef.gyBl : ClrCellDef.whSd;


            worksheetTestInformation.Cells[row, 2].Value = ctrlSmpl;
            worksheetTestInformation.Cells[row, 3].Value = internExt;
            worksheetTestInformation.Cells[row, 4].Value = WithDefault(sampleCode);

            if (!isInterTestSample)
            {
                worksheetTestInformation.Cells[row, 5].Value = WithDefault(productName);
                worksheetTestInformation.Cells[row, 6].Value = WithDefault(lotCode);
                worksheetTestInformation.Cells[row, 7].Value = WithDefault(DateOb.PrintDate(manufDate));
                worksheetTestInformation.Cells[row, 8].Value = WithDefault(baseStr);
                worksheetTestInformation.Cells[row, 9].Value = WithDefault(comment);
            }
            else
            {
                worksheetTestInformation.Cells[row, 10].Value = WithDefault(fantasyName);
                worksheetTestInformation.Cells[row, 11].Value = WithDefault(internalName);
                worksheetTestInformation.Cells[row, 12].Value = WithDefault(groupCode);
                worksheetTestInformation.Cells[row, 13].Value = WithDefault(trialNumber);
                worksheetTestInformation.Cells[row, 14].Value = WithDefault(lotCode);
                worksheetTestInformation.Cells[row, 15].Value = WithDefault(tehnology);
                worksheetTestInformation.Cells[row, 16].Value = WithDefault(DateOb.PrintDate(manufDate));
                worksheetTestInformation.Cells[row, 17].Value = WithDefault(DateOb.PrintDate(applicDate));
                worksheetTestInformation.Cells[row, 18].Value = WithDefault(concentration);
                worksheetTestInformation.Cells[row, 19].Value = WithDefault(baseStr);
                worksheetTestInformation.Cells[row, 20].Value = WithDefault(comment);
            }
        }

        private static void PrintSummeryLine(int row, ExcelWorksheet worksheetTestInformation,
            string sampleCode, string productName, string fantasyName, string internalName,
            string groupCode, string trialNumber, string comment,
            decimal avgIten, decimal avgDetTime, decimal avgFullTime,
            string descriptor, string addInfo, string ctrlSmpl)
        {
            var clrText = 0 == (row % 2) ? ClrCellDef.dkBl : ClrCellDef.lhBl;
            var clrNum = 0 == (row % 2) ? ClrCellDef.gyBl : ClrCellDef.whSd;


            worksheetTestInformation.Cells[row, 1].Value = sampleCode;
            worksheetTestInformation.Cells[row, 2].Value = productName;
            worksheetTestInformation.Cells[row, 3].Value = fantasyName;
            worksheetTestInformation.Cells[row, 4].Value = internalName;

            worksheetTestInformation.Cells[row, 5].Value = groupCode;
            worksheetTestInformation.Cells[row, 6].Value = trialNumber;
            worksheetTestInformation.Cells[row, 7].Value = comment;
            UpdatedCellCollor(worksheetTestInformation.Cells, row, 1, 7, clrText);

            worksheetTestInformation.Cells[row, 8].Value = avgIten;
            worksheetTestInformation.Cells[row, 9].Value = avgDetTime;
            worksheetTestInformation.Cells[row, 10].Value = avgFullTime;
            worksheetTestInformation.Cells[row, 11].Value = descriptor;
            worksheetTestInformation.Cells[row, 12].Value = addInfo;
            worksheetTestInformation.Cells[row, 12, row, 16].Merge = true;
            UpdatedCellCollor(worksheetTestInformation.Cells, row, 8, 16, clrNum);

            worksheetTestInformation.Cells[row, 17].Value = ctrlSmpl;
            UpdatedCellCollor(worksheetTestInformation.Cells, row, 17, 17, clrText);


        }

        /// <summary>
        /// Fills all the fields from itensity onward for the page results 
        /// </summary>
        /// <param name="worksheetResults"></param>
        /// <param name="baseResult"></param>
        /// <param name="avgIntens"></param>
        /// <param name="avgDetectTime"></param>
        /// <param name="avgFullDetectTime"></param>
        /// <param name="i"></param>
        /// <param name="creatorFullName"></param>
        /// <param name="usrDic"></param>
        /// <param name="BnchSubCon"></param>
        /// <param name="IntExt"></param>
        private static void AddToExclRowBaseResult(ExcelWorksheet worksheetResults, BaseExperimentResult baseResult, decimal avgIntens, decimal avgDetectTime, decimal avgFullDetectTime, int i, string creatorFullName, Dictionary<string, string> usrDic, string BnchSubCon, string IntExt, ClrCellDef txtFormat, ClrCellDef NumFormat)
        {


            //Intensity
            worksheetResults.Cells[i, 9].Value = baseResult.Intensity;

            //Avg Intensity
            worksheetResults.Cells[i, 10].Value = avgIntens;

            //FirstDetectionTime
            worksheetResults.Cells[i, 11].Value = baseResult.FirstDetectionTime;

            //Build up time
            worksheetResults.Cells[i, 12].Value = baseResult.BuildUpTime;

            //Full detect. Time
            worksheetResults.Cells[i, 13].Value = baseResult.FullDetectionTime;


            //Avg detect. Time
            worksheetResults.Cells[i, 14].Value = avgDetectTime;

            //Avg full detect. Time
            worksheetResults.Cells[i, 15].Value = avgFullDetectTime;

            var j = 16;
            //Avarage
            foreach (var attr in baseResult.Attributes) // 5 attributes for now
            {
                worksheetResults.Cells[i, j].Value = attr.AttributeText == null ? "" : attr.AttributeText;
                j += 1;
            }

            //Additional info
            worksheetResults.Cells[i, 21].Value = baseResult.Sensation;
            worksheetResults.Cells[i, 21, i, 25].Merge = true;


            //Test Date
            worksheetResults.Cells[i, 26].Value = baseResult.Order;
            UpdatedCellCollor(worksheetResults.Cells, i, 9, 26, NumFormat);

            worksheetResults.Cells[i, 27].Value = BnchSubCon;
            worksheetResults.Cells[i, 28].Value = IntExt;

            worksheetResults.Cells[i, 29].Value = DateOb.PrintDate(baseResult.Date);
            ////Data inserted by
            worksheetResults.Cells[i, 30].Value = GetUserName(usrDic, creatorFullName);

            UpdatedCellCollor(worksheetResults.Cells, i, 27, 30, txtFormat);
        }

        #endregion Generate Experiment Excel Result 
        private static void RemoveDeletedExperimentParticipants(TrailAirDbContext db, List<ExperimentsParticipantListIM> source, Experiment destination)
        {
            if (destination.Participants == null)
                destination.Participants = new List<ExperimentsParticipant>();

            if (destination.Id > 0)
            {
                var srcIds = source != null ?
                                    source.Select(_ => _.Id).ToArray() :
                                    new int[] { };

                var objectsToDelete = destination.Participants
                                    .Where(_ => !srcIds.Contains(_.Id));

                if (objectsToDelete.Count() > 0)
                {
                    db.ExperimentsParticipants.RemoveRange(objectsToDelete);

                }
            }

            destination.Participants = ParticipantMapper.MapExpParticipantsUIToDB(source, destination.Participants);
        }


        //private static void RemoveDeletedExperimentBenchmarks(TrailAirDbContext db, List<ExperimentBenchmarkIM> source, Experiment destination)
        //{
        //    if (destination.ExperimentBenchmarks == null)
        //        destination.ExperimentBenchmarks = new List<Benchmark>();

        //    if (destination.Id > 0)
        //    {
        //        var srcIds = source != null ?
        //                            source
        //                                .Where(_ => _.Id != 0)                  //skip new entries (Id = 0)
        //                                .Select(_ => _.Id).ToArray() :
        //                            new int[] { };

        //        var objectsToDelete = destination.ExperimentBenchmarks
        //                            .Where(_ => !srcIds.Contains(_.Id));

        //        if (objectsToDelete.Count() > 0)
        //        {
        //            db.ExperimentBenchmarks.RemoveRange(objectsToDelete);
        //        }
        //    }

        //    destination.ExperimentBenchmarks = ExperimentMapper.MapExperimentBenchmarkUIToDB(destination.ExperimentBenchmarks, source);
        //}

        //private static void RemoveDeletedExperimentTestSamples(TrailAirDbContext db, List<ExperimentTestSampleIM> source, Experiment destination)
        //{
        //    if (destination.ExperimentTestSamples == null)
        //        destination.ExperimentTestSamples = new List<TestSample>();

        //    if (destination.Id > 0)
        //    {
        //        var srcIds = source != null ?
        //                            source
        //                                .Where(_ => _.Id != 0)                  //skip new entries (Id = 0)
        //                                .Select(_ => _.Id).ToArray() :
        //                            new int[] { };

        //        var objectsToDelete = destination.ExperimentTestSamples
        //                            .Where(_ => !srcIds.Contains(_.Id));

        //        if (objectsToDelete.Count() > 0)
        //        {
        //            db.ExperimentTestSamples.RemoveRange(objectsToDelete);
        //        }
        //    }

        //    destination.ExperimentTestSamples = ExperimentMapper.MapExperimentTestSampleUIToDB(destination.ExperimentTestSamples, source);
        //}
    }
}
