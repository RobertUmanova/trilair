﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB;
using TrailAir.DB.Models;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.BoLoaders;
using TrailAir.UI.Modelling.BoMappers.Permission;
using TrailAir.UI.Modelling.UIModels.Permission;
using TrailAir.Utility;

namespace UmaSoft.UI.Modelling.BoLoaders
{
    public class PermissionLoader : BaseBoLoader<PermissionDetailsIM>
    {
        /// <summary>
        /// This is a list used for example to fill a drop down list
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<KeyValName> GetDDList(TrailAirDbContext db)
        {
            var elementList = GetList(db);

            return elementList.Select(_ => KeyValName.AddValue(_.Id, _.Name)).ToList();
        }

        public static List<PermissionListIM> GetList(TrailAirDbContext db)
        {

            List<PermissionListIM> elementsDB = null;
            var tmpElementsDB = db.Permissions.AsNoTracking().Select(_ => new Permission
            {
                Id = _.Id,
                Name = _.Name,
                NameId = _.NameId,
                PermissionCode = _.PermissionCode
            }).ToList();
            elementsDB = PermissionMapper.MapDBToUI(tmpElementsDB);
            return elementsDB;
        }


        //public static SingleBOActionResponse DeleteOb(TrailAirDbContext db, int objectId, string userEmail)
        //{
        //    var response = new SingleBOActionResponse()
        //    {
        //        Success = true
        //    };
        //    try
        //    {
        //        Permission elementDB = db.Permissions
        //                .FirstOrDefault(x => x.Id == objectId);
        //        db.Permissions.Remove(elementDB);
        //        db.SaveChanges(userEmail);
        //        response.ObjectId = elementDB.Id;
        //    }
        //    catch (Exception ex)
        //    {
        //        response.Success = false;
        //        response.ErrorMessages.Add(ExceptionOb.GetErrorMessage(ex));
        //    }
        //    return response;
        //}

        //public static IEnumerable<KeyValName> GetSimpleListPermissionCode(TrailAirDbContext context)
        //{
        //    //var getPermissions = GetList(context);

        //    return Enum.GetValues<PermissionCode>().Select(_ => KeyValName.AddValue((int)_, 
        //        Enum.GetName(typeof(PermissionCode), _))).ToList();
        //}

        //public static SingleBOActionResponse Update(TrailAirDbContext db, PermissionDetailsIM objectToUpdate, string userEmail)
        //{
        //    var response = new SingleBOActionResponse()
        //    {
        //        Success = true
        //    };
        //    try
        //    {
        //        Permission elementDB;
        //        if (objectToUpdate.Id < 1)
        //        {
        //            elementDB = new Permission();
        //            db.Permissions.Add(elementDB);
        //        }
        //        else
        //        {
        //            elementDB = db.Permissions.Include(_ => _.Name.Texts)
        //                .FirstOrDefault(x => x.Id == objectToUpdate.Id);
        //        }

        //        PermissionMapper.MapUIToDb(objectToUpdate, elementDB);
        //        db.SaveChanges(userEmail);
        //        response.ObjectId = elementDB.Id;
        //    }
        //    catch (Exception ex)
        //    {
        //        response.Success = false;
        //        response.ErrorMessages.Add(ExceptionOb.GetErrorMessage(ex));
        //    }
        //    return response;
        //}

        //public static PermissionDetailsIM GetItemDet(TrailAirDbContext db, int objectId)
        //{
        //    Permission permissionBo;
        //    if (objectId == 0)
        //    {
        //        permissionBo = new Permission();
        //    }
        //    else
        //    {
        //        permissionBo = db.Permissions.Include(_ => _.Name.Texts).Where(_ => _.Id == objectId).AsNoTracking().FirstOrDefault();
        //        if (permissionBo == null)
        //            throw new Exception($"IT controlled error requested permission doesn't exist in the DB :{objectId}");
        //    }

        //    PermissionDetailsIM elementDB = PermissionMapper.MapDBToUIDet(permissionBo);

        //    return elementDB;
        //}
    }
}
