﻿using Microsoft.EntityFrameworkCore;
using TrailAir.DB;
using TrailAir.DB.Models;
using TrailAir.UI.Modelling.BoMappers.StaticText;
using TrailAir.UI.Modelling.UIModels.GenericApp;
using TrailAir.UI.Modelling.UIModels.StaticText;
using TrailAir.Utility.ExceptionHelpers;

namespace TrailAir.UI.Modelling.BoLoaders
{
    public class StaticTextLoader
    {
        public static List<StaticTextListIM> StaticTextList(TrailAirDbContext db)
        {

            List<StaticTextListIM> elementsDB = null;
            var tmpElementsDB = db.StaticTexts.AsNoTracking().Select(_ => new StaticText
            {
                Id = _.Id,
                Key = _.Key,
                TextId = _.TextId
            }).OrderBy(_ => _.Key).ToList();
            elementsDB = StaticTextMapper.MapDBToUIs(tmpElementsDB);
            return elementsDB;
        }

        public static SingleBOActionResponse DeleteOb(TrailAirDbContext db, int objectId, string userEmail)
        {
            var response = new SingleBOActionResponse()
            {
                Success = true
            };
            try
            {
                StaticText elementDB = db.StaticTexts
                        .FirstOrDefault(x => x.Id == objectId);
                db.StaticTexts.Remove(elementDB);
                db.SaveChanges(userEmail);
                response.ObjectId = elementDB.Id;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessages.Add(ExceptionObject.GetErrorMessage(ex));
            }
            return response;
        }

        public static SingleBOActionResponse Update(TrailAirDbContext db, StaticTextDetailsIM objectToUpdate, string userEmail)
        {
            var response = new SingleBOActionResponse()
            {
                Success = true
            };
            try
            {
                StaticText elementDB;
                if (objectToUpdate.Id < 1)
                {
                    elementDB = new StaticText();
                    db.StaticTexts.Add(elementDB);
                }
                else
                {
                    elementDB = db.StaticTexts
                        .Include(_ => _.Text.Texts)
                        .FirstOrDefault(x => x.Id == objectToUpdate.Id);
                }

                StaticTextMapper.MapUIToDb(objectToUpdate, elementDB);
                db.SaveChanges(userEmail);
                response.ObjectId = elementDB.Id;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessages.Add(ExceptionObject.GetErrorMessage(ex));
            }
            return response;
        }



        public static StaticTextDetailsIM GetItemDet(TrailAirDbContext db, int objectId)
        {
            StaticText objectBo;
            if (objectId == 0)
            {
                objectBo = new StaticText();
            }
            else
            {
                objectBo = db.StaticTexts.Include(_ => _.Text.Texts).Where(_ => _.Id == objectId).AsNoTracking().FirstOrDefault();
                if (objectBo == null)
                    throw new Exception($"IT controlled error requested static text doesn't exist in the DB :{objectId}");
            }

            StaticTextDetailsIM result = StaticTextMapper.MapDBToUIDet(objectBo);

            return result;
        }

    }
}
