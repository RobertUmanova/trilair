﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.Models.Base;

namespace TrailAir.UI.Modelling.BoLoaders
{
    public class BaseBoLoader<T> where T : new ()
    {
        public T GetEmptyObject()
        {
            //return new T();

            var result = new T();

            FillAdditionalProps(result);

            return result;
        }

        public virtual void FillAdditionalProps(T sentObject)
        {
            
        }

    }
}
