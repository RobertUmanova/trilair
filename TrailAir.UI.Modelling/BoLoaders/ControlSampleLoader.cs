﻿using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB;
using TrailAir.DB.Models;
using TrailAir.DB.Models.Base;
using TrailAir.DB.Models.Enumerations;
using TrailAir.UI.Modelling.BoMappers.Experiment;
using TrailAir.UI.Modelling.BoMappers.Participant;
using TrailAir.UI.Modelling.BoMappers.ParticipantResult;
using TrailAir.UI.Modelling.UIModels.Experiment;
using TrailAir.UI.Modelling.UIModels.GenericApp;
using TrailAir.UI.Modelling.UIModels.Participant;
using TrailAir.Utility;
using TrailAir.Utility.ExceptionHelpers;


namespace TrailAir.UI.Modelling.BoLoaders
{
    public class ControlSampleLoader /*: BaseBoLoader<ExperimentDetailsIM>*/
    {

        



        public static ControlSample ControlSample(TrailAirDbContext db, int experimentId)
        {

            ControlSample controlSample = db.ControlSamples.Include( _ => _.ControlSampleType).Single(_ => _.Experiment.Id == experimentId);


            return controlSample;
        }
    }
}
