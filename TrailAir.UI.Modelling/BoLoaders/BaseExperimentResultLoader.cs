﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB;
using TrailAir.DB.Models;
using TrailAir.DB.Models.Base;
using TrailAir.DB.Models.Enumerations;
using TrailAir.UI.Modelling.BoMappers.BenchmarkResult;
using TrailAir.UI.Modelling.BoMappers.ExperimentResult;
using TrailAir.UI.Modelling.BoMappers.ExpParticipantResult;
using TrailAir.UI.Modelling.BoMappers.ParticipantResult;
using TrailAir.UI.Modelling.BoMappers.TestSampleResult;
using TrailAir.UI.Modelling.UIModels.ExpParticipantResult;
using TrailAir.UI.Modelling.UIModels.GenericApp;

namespace TrailAir.UI.Modelling.BoLoaders
{
    public class BaseExperimentResultLoader : BaseBoLoader<BaseExperimentResult>
    {
        public enum ResType 
        {
            TestSmaple = 1,
            //Benchmark = 3, DEprecated
            ControlSample = 5
        }

        public static ExperimentResultIM GetEmptyUIElementByType(TrailAirDbContext db, int experimentId,
            int? baseExpTestSmplId = null )
        {
            ResType type = baseExpTestSmplId == null ? ResType.ControlSample : ResType.TestSmaple;
            string sampleCodeText=string.Empty;
            int itensity = 0;
            int boId = 0;

            if (type == ResType.TestSmaple)
            {
                sampleCodeText = TestSampleLoader.GetElementNoIcl(db, baseExpTestSmplId.Value).SampleCode;
                boId = baseExpTestSmplId.Value;
            }
            else if (type == ResType.ControlSample)
            {
                var ctrlSmpl = ControlSampleLoader.ControlSample(db, experimentId);
                sampleCodeText = ctrlSmpl.FullText;
                boId = ctrlSmpl.Id;
                itensity = ctrlSmpl.ControlSampleType.IntensityScore;
            }

            return new ExperimentResultIM { AttributeTexts = new List<string>(new string[5]), ExperimentBencTestSampId = boId, SampleCode = sampleCodeText, Intensity = itensity };

        }

        /// <summary>
        /// For now just called from the wizard 
        /// </summary>
        /// <param name="db"></param>
        /// <param name="experimentId"></param>
        /// <param name="participantId"></param>
        /// <param name="objectToUpdateFrom"></param>
        /// <param name="experimentResultTypeId"></param>
        /// <param name="userEmail"></param>
        /// <returns></returns>
        public static SingleBOActionResponse AddTestSampleResult(TrailAirDbContext db, int experimentId, string participantId, ExperimentResultIM objectToUpdateFrom,
            int experimentResultTypeId, string userEmail)
        {
            var response = new SingleBOActionResponse()
            {
                Success = true
            };
            try
            {
                var participantResult = db.ParticipantResults
                        .Include(pr => pr.TestSampleResults).ThenInclude(x => x.TestSample)
                       // .Include(pr => pr.BenchmarkResults).ThenInclude(x => x.Benchmark)
                        .SingleOrDefault(pr => pr.ExperimentId == experimentId && pr.ParticipantId == participantId);

                if (participantResult == null)///This could be if you reuse this code i
                {
                    throw new Exception("Participant result is created in the Control sample phase");
                    //// create new
                    //ParticipantResult newParticipantResult = new ParticipantResult()
                    //{
                    //    ExperimentId = experimentId,
                    //    ParticipantId = participantId,
                    //};
                    //// save particpant 
                    //db.ParticipantResults.Add(newParticipantResult);
                }

                AddExperimentSampleResults(db, participantResult, objectToUpdateFrom, experimentResultTypeId, experimentId);

                db.SaveChanges(userEmail);
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessages.Add(ex.Message);
            }
            return response;
        }

        //private static void UpdateExperimentResultsOrder(TrailAirDbContext db, int experimentId, int participantId, int experimentResultId, Type experimentResultType)
        //{
        //    var participantResult = db.ParticipantResults
        //                   .Include(pr => pr.TestSampleResults)
        //                   .Include(pr => pr.BenchmarkResults)
        //                   .SingleOrDefault(pr => pr.ExperimentId == experimentId && pr.ParticipantId == participantId);

        //    if (experimentResultType.Name.ToLower().Contains("benchmark"))
        //    {
        //        int? newOrder = participantResult.BenchmarkResults.Max(br => br.Order) + 1;
        //        var benchmarkResult = participantResult.BenchmarkResults.SingleOrDefault(br => br.Id == experimentResultId);
        //        benchmarkResult.Order = newOrder;
        //    }
        //    else if (experimentResultType.Name.ToLower().Contains("testsample"))
        //    {
        //        int? newOrder = participantResult.TestSampleResults.Max(br => br.Order) + 1;
        //        var testSampleResult = participantResult.TestSampleResults.SingleOrDefault(tsr => tsr.Id == experimentResultId);
        //        testSampleResult.Order = newOrder;
        //    }
        //}

        private static void AddExperimentSampleResults(TrailAirDbContext db, ParticipantResult participantResultBo,
            ExperimentResultIM objectToUpdateFrom, int experimentResultTypeId, int experimentId)
        {
            //if (experimentResultTypeId == (int)BaseExperimentResultType.BenchmarkResult)
            //{
            //    var benchmarkResultUi = objectToUpdateFrom as ExperimentResultIM;
            //    if (participantResult.BenchmarkResults == null) //create benchmark on first benchmark submit
            //    {
            //        participantResult.BenchmarkResults = new List<BenchmarkResult>();

            //        var workingBenchmarkResult = BenchmarkResultMapper.MapBenchmarkResultsUIToDB(benchmarkResultUi);
            //        workingBenchmarkResult.IsSubmitted = true;
            //        participantResult.BenchmarkResults.Add(workingBenchmarkResult);
            //    }
            //    else
            //    {
            //        var benchmarkResultDb = db.BenchmarkResults.SingleOrDefault(br => br.Id == benchmarkResultUi.Id);
            //        if (benchmarkResultDb == null) //create benchmark on second+ benchmark submit
            //        {
            //            var workingBenchmarkResult = BenchmarkResultMapper.MapBenchmarkResultsUIToDB(benchmarkResultUi);
            //            workingBenchmarkResult.IsSubmitted = true;
            //            participantResult.BenchmarkResults.Add(workingBenchmarkResult);
            //        }
            //        else //update benchmark
            //        {
            //            BenchmarkResultMapper.MapBenchmarkResultsUIToDB(benchmarkResultUi, null, benchmarkResultDb);
            //            benchmarkResultDb.IsSubmitted = true;
            //        }
            //    }
            //}
            //else if (experimentResultTypeId == (int)BaseExperimentResultType.TestSampleResult)
            //{
            var testSampleResultUi = objectToUpdateFrom;
            if (participantResultBo.TestSampleResults == null) //create test sample on first test sample submit
            {
                participantResultBo.TestSampleResults = new List<TestSampleResult>();
            }


            if (testSampleResultUi.Id > 0)
                throw new Exception("For now this is called through the wizard proccess where is always creating");

            //var testSampleResultDb = db.TestSampleResults.SingleOrDefault(tsr => tsr.Id == testSampleResultUi.Id);

            //if (testSampleResultDb == null) //create test sample on second+ test sample submit
            //{


            var workingTestSampleResult = TestSampleResultMapper.MapTestSampleResultsUIToDB(testSampleResultUi);
            workingTestSampleResult.IsSubmitted = true;
            workingTestSampleResult.SampleIndex = testSampleResultUi.SampleIndex;
            workingTestSampleResult.Date = DateTime.Now.Date;

            // Find TestSampleResult's order
            var participantResult = db.ParticipantResults
                .Where(_ => _.ParticipantId == participantResultBo.ParticipantId &&
                _.ExperimentId == experimentId)
                .FirstOrDefault();

            if (participantResult != null)
            {
                var resultsForThisTestSample = db.TestSampleResults
               .Where(_ => _.ParticipantResultId == participantResult.Id)
               .ToList();
                //.Include(_ => _.ParticipantResult)
                //.Where(_ => _.ParticipantResult.ParticipantId == participantResultBo.ParticipantId && 
                //_.ParticipantResult.ExperimentId == experimentId)
                //.ToList();

                workingTestSampleResult.Order = resultsForThisTestSample.Count + 1;
            }
            else workingTestSampleResult.Order = 1;


            participantResultBo.TestSampleResults.Add(workingTestSampleResult);
                //}
                //else //update test sample
                //{
                //    TestSampleResultMapper.MapTestSampleResultsUIToDB(testSampleResultUi, null, testSampleResultDb);
                //    testSampleResultDb.IsSubmitted = true;
                //}
            //}
        }

        public static SingleBOActionResponse UpdateSingle(TrailAirDbContext db, ExperimentResultIM objectToUpdateFrom, string userEmail)
        {
            var response = new SingleBOActionResponse()
            {
                Success = true
            };
            try
            {
                if (objectToUpdateFrom.ExpResultType == BaseExperimentResultType.BenchmarkResult)
                {
                    var experimentResult = db.BenchmarkResults
                        .Include(br => br.Attributes)
                        .SingleOrDefault(br => br.Id == objectToUpdateFrom.Id);

                    ExperimentResultMapper.MapUIToDb(objectToUpdateFrom, experimentResult);
                }
                else if (objectToUpdateFrom.ExpResultType == BaseExperimentResultType.TestSampleResult)
                {
                    var experimentResult = db.TestSampleResults
                        .Include(tsr => tsr.Attributes)
                        .SingleOrDefault(tsr => tsr.Id == objectToUpdateFrom.Id);

                    ExperimentResultMapper.MapUIToDb(objectToUpdateFrom, experimentResult);
                }
                else if (objectToUpdateFrom.ExpResultType == BaseExperimentResultType.ControlSample)
                {
                    var experimentResult = db.ControlSampleResults
                        .Include(tsr => tsr.Attributes)
                        .SingleOrDefault(tsr => tsr.Id == objectToUpdateFrom.Id);

                    ExperimentResultMapper.MapUIToDb(objectToUpdateFrom, experimentResult);
                }

                db.SaveChanges(userEmail);
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessages.Add(ex.Message);
            }
            return response;
        }
    }
}
