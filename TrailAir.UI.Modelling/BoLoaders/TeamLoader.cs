﻿using Microsoft.EntityFrameworkCore;
using TrailAir.DB;
using TrailAir.DB.Models;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.BoMappers.Team;
using TrailAir.UI.Modelling.Managers;
using TrailAir.UI.Modelling.UIModels.GenericApp;
using TrailAir.UI.Modelling.UIModels.Team;
using TrailAir.Utility;
using TrailAir.Utility.ExceptionHelpers;

namespace TrailAir.UI.Modelling.BoLoaders
{
    public class TeamLoader : BaseBoLoader<TeamDetailsIM>
    {

        /// <summary>
        /// This is a list used for example to fill a drop down list
        /// </summary>
        /// <returns> Tasks per project</returns>
        public static List<KeyValName> GetDDList(TrailAirDbContext db)
        {
            var elementList = GetItemList(db);

            return elementList.Select(_ => KeyValName.AddValue(_.Id, _.Name)).ToList();
        }

        public static TeamDetailsIM GetItem(TrailAirDbContext db, int objectId)
        {
            Team teamBo = new();
            if (objectId > 0)
            {
                teamBo = db.Teams.Include(_ => _.Users)
                    .Where(_ => _.Id == objectId).AsNoTracking().FirstOrDefault();    
            }

            TeamDetailsIM element = TeamMapper.MapDBToUIDet(teamBo);
            foreach (var user in teamBo.Users)
            {
                var appUser = db.Users.AsNoTracking().FirstOrDefault(_ => _.BusinessLogicDataId == user.Id);
                element.AssignedUsers.Add(new UIModels.User.BusinessLogicUserListIM
                {
                    Id = user.Id,
                    Email = appUser.Email,
                    UserName = appUser.UserName,
                    PhoneNumber = appUser.PhoneNumber,
                    FirstName = appUser.FirstName,
                    LastName = appUser.LastName
                });
            }
            return element;
        }
        public static List<TeamListIM> GetItemList(TrailAirDbContext db)
        {

            List<TeamListIM> elementsDB = new List<TeamListIM>();
            var tmpElementsDB = db.Teams.AsNoTracking().Include(_ => _.Users).ToList();

            elementsDB = TeamMapper.MapDBToUI(tmpElementsDB);
            foreach (var team in elementsDB)
            {
                foreach (var user in team.AssignedUsers)
                {
                    var appUser = db.Users.AsNoTracking().FirstOrDefault(_ => _.BusinessLogicDataId == user.Id);
                    user.Email = appUser.Email;
                    user.UserName = appUser.UserName;
                    user.FirstName = appUser.FirstName;
                    user.LastName = appUser.LastName;
                }
            }
            return elementsDB;
        }

        public static int GetTeamByLoggedUser(TrailAirDbContext context, string currentUserId)
        {
            int userId = context.Users
                .SingleOrDefault(u => u.Id == currentUserId).BusinessLogicDataId;

            var blu = context.BusinessLogicUsers
                .Include(u => u.Team)
                .SingleOrDefault(u => u.Id == userId);

            var teamId = blu?.TeamId;

            if (teamId.HasValue)
            {
                return teamId.Value;
            }
            else
            {
                return 0;
            }
        }

        public static SingleBOActionResponse Update(TrailAirDbContext db, TeamDetailsIM objectToUpdate, string userEmail)
        {
            var response = new SingleBOActionResponse()
            {
                Success = true
            };
            try
            {
                Team elementDB;
                if (objectToUpdate.Id < 1)
                {
                    elementDB = new Team();
                    db.Teams.Add(elementDB);
                }
                else
                {
                    elementDB = db.Teams
                        .FirstOrDefault(x => x.Id == objectToUpdate.Id);
                    //RemoveAssignedUsers(db, elementDB);
                }

                TeamMapper.MapUIToDbDet(objectToUpdate, elementDB);
                //AddAssignedUsers(db, elementDB);
                db.SaveChanges(userEmail);
                response.ObjectId = elementDB.Id;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessages.Add(ExceptionObject.GetErrorMessage(ex));
            }
            return response;
        }

        //private static void AddAssignedUsers(TrailAirDbContext db, Team elementDB)
        //{
        //    if (elementDB.BusinessLogicUsersTeams.Count() > 0)
        //    {
        //        db.BusinessLogicUsersTeams.AddRange(elementDB.BusinessLogicUsersTeams);
        //    }
        //}

        //private static void RemoveAssignedUsers(TrailAirDbContext db, Team elementDB)
        //{
        //    db.BusinessLogicUsersTeams.RemoveRange(db.BusinessLogicUsersTeams.Where(_ => _.TeamId == elementDB.Id).Select(_ => new DB.Models.BusinessLogicUserTeam
        //    {
        //        Id = _.Id
        //    }));
        //}


        public static SingleBOActionResponse Delete(TrailAirDbContext db, int objectId, string userEmail)
        {
            var response = new SingleBOActionResponse()
            {
                Success = true
            };
            try
            {
                // should we throw an exception like for example:  ?? throw new ArgumentNullException(nameof(Team));
                Team elementDB = db.Teams
                        .FirstOrDefault(x => x.Id == objectId);
                if (elementDB != null) 
                {
                    db.Teams.Remove(elementDB);
                    db.SaveChanges(userEmail);
                    response.ObjectId = elementDB.Id;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessages.Add(ExceptionObject.GetErrorMessage(ex));
            }
            return response;
        }

        public static SingleBOActionResponse Archive(TrailAirDbContext db, int objectId, string userEmail)
        {
            var response = new SingleBOActionResponse()
            {
                Success = true
            };
            try
            {
                // should we throw an exception like for example:  ?? throw new ArgumentNullException(nameof(Team));
                Team elementDB = db.Teams
                        .FirstOrDefault(x => x.Id == objectId);

                if (elementDB != null) 
                {
                    elementDB.IsArchived = !elementDB.IsArchived;
                    elementDB.IsManuallyArchived = elementDB.IsArchived;
                    db.SaveChanges(userEmail);
                }

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessages.Add(ExceptionObject.GetErrorMessage(ex));
            }
            return response;
        }
    }
}
