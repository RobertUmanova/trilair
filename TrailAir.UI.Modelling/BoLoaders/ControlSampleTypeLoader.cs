﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB;
using TrailAir.DB.Models;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.BoMappers.Experiment;
using TrailAir.UI.Modelling.UIModels.Experiment;
using TrailAir.UI.Modelling.UIModels.GenericApp;
using TrailAir.Utility;
using TrailAir.Utility.ExceptionHelpers;

namespace TrailAir.UI.Modelling.BoLoaders
{
    public class ControlSampleTypeLoader : BaseBoLoader<ControlSampleTypeListIM>
    {
        /// <summary>
        /// This is a list used for example to fill a drop down list
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<KeyValName> GetDDList(TrailAirDbContext db)
        {
            var elementList = GetList(db);

            return elementList.Select(_ => KeyValName.AddValue(_.Id, _.FullText)).ToList();
        }

        public static List<ControlSampleTypeListIM> GetList(TrailAirDbContext db)
        {
            List<ControlSampleTypeListIM> elements = new List<ControlSampleTypeListIM>();
            var tmpElementsDB = db.ControlSampleTypes.AsNoTracking().Select(_ => new ControlSampleType
            {
                Id = _.Id,
                Name = _.Name,
                ControlSampleTypeCode = _.ControlSampleTypeCode,
                IntensityScore = _.IntensityScore,
            }).ToList();

            elements = ExperimentMapper.MapControlSampleTypeDBToUI(tmpElementsDB);

            return elements;
        }

        public static ControlSampleTypeListIM GetItem(TrailAirDbContext db, int objectId)
        {
            ControlSampleType elementDB = db.ControlSampleTypes.FirstOrDefault(_ => _.Id == objectId);

            ControlSampleTypeListIM elementUI = ExperimentMapper.MapControlSampleTypeDBToUI(elementDB);

            return elementUI;
        }

        public static SingleBOActionResponse Update(TrailAirDbContext db, ControlSampleTypeListIM objectToUpdate, string userEmail)
        {
            var response = new SingleBOActionResponse()
            {
                Success = true
            };
            try
            {
                ControlSampleType elementDB;
                if (objectToUpdate.Id < 1)
                {
                    elementDB = new ControlSampleType();
                    db.ControlSampleTypes.Add(elementDB);
                }
                else
                {
                    elementDB = db.ControlSampleTypes
                        .FirstOrDefault(x => x.Id == objectToUpdate.Id);
                }

                ExperimentMapper.MapControlSampleTypeUIToDB(objectToUpdate, elementDB);
                //AddAssignedUsers(db, elementDB);
                db.SaveChanges(userEmail);
                response.ObjectId = elementDB.Id;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessages.Add(ExceptionObject.GetErrorMessage(ex));
            }
            return response;
        }
    }
}
