﻿using Microsoft.EntityFrameworkCore;
using TrailAir.DB;
using TrailAir.DB.Models;
using TrailAir.UI.Modelling.BoMappers.Experiment;
using TrailAir.UI.Modelling.UIModels.Experiment;
using TrailAir.UI.Modelling.UIModels.GenericApp;
using TrailAir.Utility.ExceptionHelpers;

namespace TrailAir.UI.Modelling.BoLoaders
{
    public class TestSampleLoader
    {

        public static TestSample GetElementNoIcl(TrailAirDbContext context, int testSampleId)
        {
            var testSample = context.TestSamples
                .SingleOrDefault(e => e.Id == testSampleId);


            return testSample;
        }

        public static List<ExperimentTestSampleIM> GetTestSamplesUiForExperiment(TrailAirDbContext context, int experimentId)
        {
            var experiment = context.Experiments
                .Include(e => e.ExperimentTestSamples)
                .SingleOrDefault(e => e.Id == experimentId);

            if (experiment == null)
                return null;

            var testSamples = new List<ExperimentTestSampleIM>();

            foreach (var testSample in experiment.ExperimentTestSamples)
            {
                testSamples.Add(ExperimentMapper.MapExperimentTestSampleDBToUI(testSample));
            }

            return testSamples;
        }

        public static SingleBOActionResponse SaveTestSample(TrailAirDbContext db, ExperimentTestSampleIM testSample, string userEmail)
        {
            var response = new SingleBOActionResponse()
            {
                Success = true
            };
            try
            {
                if (testSample.Id == 0)
                {
                    TestSample newTestSample = new TestSample();
                    ExperimentMapper.MapExperimentTestSampleUIToDB(newTestSample, testSample);
                    db.TestSamples.Add(newTestSample);
                }
                else
                {
                    var existingTestSample = db.TestSamples.SingleOrDefault(ets => ets.Id == testSample.Id);
                    ExperimentMapper.MapExperimentTestSampleUIToDB(existingTestSample, testSample);
                }
                db.SaveChanges(userEmail);
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessages.Add(ExceptionObject.GetErrorMessage(ex));
            }
            return response;
        }

        public static SingleBOActionResponse DeleteSingleTestSample(TrailAirDbContext db, int testSampleId, string userEmail)
        {
            var response = new SingleBOActionResponse()
            {
                Success = true
            };
            try
            {
                TestSample testSampleForDeletion = db.TestSamples
                    .SingleOrDefault(ts => ts.Id == testSampleId);

                if (testSampleForDeletion != null)
                {
                    db.TestSamples.Remove(testSampleForDeletion);
                }
                else
                {
                    throw new Exception("TestSample not found");
                }

                db.SaveChanges(userEmail);
                response.ObjectId = testSampleId;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessages.Add(ExceptionObject.GetErrorMessage(ex));
            }
            return response;
        }
    }
}
