﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.BoMappers.BasicUser;
using TrailAir.UI.Modelling.UIModels.GenericApp;
using TrailAir.UI.Modelling.UIModels.User;
using TrailAir.Utility.ExceptionHelpers;

namespace TrailAir.UI.Modelling.BoLoaders
{
    public class BasicUserLoader : BaseBoLoader<BusinessLogicUserDetailsIM>
    {
        /// <summary>
        /// This is a list used for example to fill a drop down list
        /// </summary>
        /// <returns> Tasks per project</returns>
        //public static List<KeyValName> GetSimpleList(UmaSoftDbContext db)
        //{
        //    var getUsers = GetList(db);

        //    return getUsers.Select(_ => KeyValName.AddValue(_.Id, $"{_.FirstName} {_.LastName}")).ToList();
        //}

        public static List<BusinessLogicUserListIM> GetList(TrailAirDbContext db)
        {
            List<BusinessLogicUserListIM> elementsDB = null;
            var tmpElementsDB = db.BusinessLogicUsers.AsNoTracking()
                //.Include(_ => _.Permission.Name).ThenInclude(__ => __.Texts.Where(t => t.))
                .Include(_ => _.Permission)
                .Include(_ => _.Team)
                .ToList();

            elementsDB = BusinessLogicUserMapper.MapDBToUIList(tmpElementsDB);
            return elementsDB;
        }

        /// <summary>
        /// This is a list used for example to fill a drop down list
        /// </summary>
        /// <returns> Tasks per project</returns>
        //public static List<KeyValName> GetSimpleListPerCompany(UmaSoftDbContext db, int companyId)
        //{
        //    var getUsers = GetListPerCompany(db, companyId);

        //    return getUsers.Select(_ => KeyValName.AddValue(_.Id, $"{_.Company.Name}")).ToList();
        //}
        /// <summary>
        /// This is a list used for example to fill a drop down list
        /// </summary>
        //public static List<KeyValName> GetSimpleListPerTeam(UmaSoftDbContext db, int teamId)
        //{
        //    var getUsers = GetListPerTeam(db, teamId);

        //    return getUsers.Select(_ => KeyValName.AddValue(_.Id, $"{_.Company.Name}")).ToList();
        //}

        /// <summary>
        /// Get list of all users per company of the project
        /// </summary>
        /// <param name="db"></param>
        /// <param name="projectId"></param>
        /// <returns></returns>
        //public static List<BusinessLogicUserListIM> GetListPerProject(UmaSoftDbContext db, int projectId)
        //{
        //    List<BusinessLogicUserListIM> elementsDB = null;
        //    List<int> companiesIds = db.Projects.AsNoTracking().Where(_ => _.Id == projectId)
        //        .SelectMany(project => project.ProjectsCompanies.Select(_ => _.CompanyId)).ToList();

        //    List<BusinessLogicUser> usersDb = db.BusinessLogicUsers.AsNoTracking()
        //        .Include(_ => _.Company)
        //        .Include(_ => _.Permission)
        //        .Include(_ => _.BusinessLogicUsersWorkPositions).ThenInclude(_ => _.WorkPosition)
        //        .Where(_ => companiesIds.Contains((int)_.CompanyId)).ToList();
        //    elementsDB = BusinessLogicUserMapper.MapDBToUIList(usersDb);
        //    return elementsDB;
        //}
        //public static List<BusinessLogicUserListIM> GetListPerCompany(UmaSoftDbContext db, int companyId)
        //{
        //    List<BusinessLogicUserListIM> elementsDB = null;
        //    var tmpElementsDB = db.BusinessLogicUsers.AsNoTracking()
        //        .Include(_ => _.Company).Where(_ => _.CompanyId == companyId)
        //        .Include(_ => _.Permission)
        //        .Include(_ => _.BusinessLogicUsersWorkPositions).ThenInclude(_ => _.WorkPosition).ToList();
        //    elementsDB = BusinessLogicUserMapper.MapDBToUIList(tmpElementsDB);
        //    return elementsDB;
        //}
        //public static SingleBOActionResponse DeleteOb(UmaSoftDbContext db, int objectId, string userEmail)
        //{
        //    //var response = new SingleBOActionResponse()
        //    //{
        //    //    Success = true
        //    //};
        //    //try
        //    //{
        //    //    BasicUser elementDB = db.BasicUsers
        //    //            .FirstOrDefault(x => x.Id == objectId);
        //    //    db.BasicUsers.Remove(elementDB);
        //    //    db.SaveChanges(userEmail);
        //    //    response.ObjectId = elementDB.Id;
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    response.Success = false;
        //    //    response.ErrorMessages.Add(ExceptionOb.GetErrorMessage(ex));
        //    //}
        //    //return response;
        //    return null;
        //}


        public static SingleBOActionResponse Update(TrailAirDbContext db, BusinessLogicUserDetailsIM objectToUpdate, string userEmail)
        {
            var response = new SingleBOActionResponse()
            {
                Success = true
            };
            try
            {
                using (var dbContext = DB.Helper.ConnectionHelper.GetContextToRelease())
                {
                    BusinessLogicUser elementDB;
                    if (objectToUpdate.Id < 1)
                    {
                        elementDB = new BusinessLogicUser();
                        db.BusinessLogicUsers.Add(elementDB);
                    }
                    else
                    {
                        elementDB = dbContext.BusinessLogicUsers
                            .Include(_ => _.Team)
                            .Include(_ => _.Permission)
                            .FirstOrDefault(x => x.Id == objectToUpdate.Id);
                    }

                    BusinessLogicUserMapper.MapUIToDb(objectToUpdate, elementDB);
                    dbContext.SaveChanges(userEmail);

                    response.ObjectId = elementDB.Id;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessages.Add(ExceptionObject.GetErrorMessage(ex));
            }
            return response;
        }

        //public static List<BusinessLogicUserListIM> GetListPerTeam(UmaSoftDbContext db, int teamId)
        //{
        //    List<BusinessLogicUserListIM> elementsDB = null;
        //    List<BusinessLogicUser> tmpElementsDB = new();

        //    Team teamDB = db.Teams.AsNoTracking()
        //        .Include(_ => _.BusinessLogicUsersTeams).ThenInclude(_ => _.BusinessLogicUser).ThenInclude(_ => _.Company).Where(_ => _.Id == teamId).SingleOrDefault();
        //    if (teamDB.BusinessLogicUsersTeams != null)
        //    {
        //        if (teamDB.BusinessLogicUsersTeams.Count > 0)
        //        {
        //            foreach (var item in teamDB.BusinessLogicUsersTeams)
        //            {
        //                tmpElementsDB.Add(item.BusinessLogicUser);
        //            }
        //        }
        //    }
        //    //var tmpElementsDB = db.BusinessLogicUsers.AsNoTracking()
        //    //    .Include(_ => _.BusinessLogicUsersTeams).ThenInclude(_ => _.Team).Where(_ => _.Id == teamId).ToList();
        //    elementsDB = BusinessLogicUserMapper.MapDBToUIList(tmpElementsDB);
        //    return elementsDB;
        //}

        //public static SingleBOActionResponse UpdateImage(UmaSoftDbContext db, BusinessLogicUserDetailsIM objectToUpdate, string userEmail)
        //{
        //    var response = new SingleBOActionResponse()
        //    {
        //        Success = true
        //    };
        //    try
        //    {
        //        using (var dbContext = DB.Helper.ConnectionHelper.GetContextToRelease())
        //        {
        //            BusinessLogicUser elementDB;
        //            elementDB = dbContext.BusinessLogicUsers
        //                .Include(_ => _.Document)
        //                .FirstOrDefault(x => x.Id == objectToUpdate.Id);

        //            RemoveOldProfileImageFromDB(objectToUpdate, elementDB, dbContext);
        //            BusinessLogicUserMapper.MapUIImageToDb(objectToUpdate, elementDB);
        //            dbContext.SaveChanges(userEmail);

        //            response.ObjectId = elementDB.Id;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        response.Success = false;
        //        response.ErrorMessages.Add(ExceptionOb.GetErrorMessage(ex));
        //    }
        //    return response;
        //}

        //private static void RemoveOldProfileImageFromDB(BusinessLogicUserDetailsIM objectToUpdate, BusinessLogicUser elementDB, UmaSoftDbContext dbContext)
        //{
        //    if (elementDB.DocumentId == null)
        //    {
        //        return;
        //    }
        //    else if (elementDB.DocumentId == objectToUpdate.DocumentId)
        //    {
        //        return;
        //    }
        //    dbContext.Documents.Remove(dbContext.Documents.FirstOrDefault(_ => _.Id == elementDB.DocumentId));
        //}

        public static BusinessLogicUserDetailsIM GetItemDet(TrailAirDbContext db, int objectId)
        {

            BusinessLogicUser businessLogicUserBo;
            if (objectId == 0)
            {
                businessLogicUserBo = new BusinessLogicUser();
            }
            else
            {
                businessLogicUserBo = db.BusinessLogicUsers.AsNoTracking()
                    .Include(_ => _.Permission)
                    .Include(_ => _.Team)
                    .Where(_ => _.Id == objectId).FirstOrDefault();
                if (businessLogicUserBo == null)
                    throw new Exception($"IT controlled error requested Business Logic User doesn't exist in the DB :{objectId}");
            }

            BusinessLogicUserDetailsIM elementDB = BusinessLogicUserMapper.MapDBToUIDet(businessLogicUserBo);
            return elementDB;
        }
    }
}

