﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB;
using TrailAir.DB.Models;
using TrailAir.DB.Models.Enumerations;
using TrailAir.UI.Modelling.BoMappers.Experiment;
using TrailAir.UI.Modelling.UIModels.Experiment;
using TrailAir.Utility;

namespace TrailAir.UI.Modelling.BoLoaders
{
    public class ExperimentStatusLoader : BaseBoLoader<ExperimentStatusIM>
    {


        /// <summary>
        /// This is a list used for example to fill a drop down list
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<KeyValName> GetDDList(TrailAirDbContext db)
        {
            var elementList = GetList(db);

            return elementList.Select(_ => KeyValName.AddValue(_.Id, _.Name)).ToList();
        }


        public static List<ExperimentStatusIM> GetList(TrailAirDbContext db)
        {
            List<ExperimentStatusIM> elements = new List<ExperimentStatusIM>();
            var tmpElementsDB = db.ExperimentStatuses.AsNoTracking().Select(_ => new ExperimentStatus
            {
                Id = _.Id,
                Name = _.Name,
                NameId = _.NameId,
                ExperimentStatusCode = _.ExperimentStatusCode
            }).ToList();

            elements = ExperimentMapper.MapExperimentStatusDBToUI(tmpElementsDB);

            return elements;
        }

        /// <summary>
        /// Fetches default Experiment Statuses. For now: Draft and Inprogress
        /// </summary>
        /// <returns></returns>
        public static int[] GetDefaultStatusIds() // TODO: Put this in some Configuration.cs file
        {
            return new int[] { (int)ExperimentStatusEnum.Draft, (int)ExperimentStatusEnum.InProgress };
        }
    }
}
