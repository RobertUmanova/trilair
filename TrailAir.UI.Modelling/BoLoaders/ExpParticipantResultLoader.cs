﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.UI.Modelling.UIModels.ExpParticipantResult;
using Microsoft.EntityFrameworkCore;
using TrailAir.UI.Modelling.BoMappers.ExpParticipantResult;
using TrailAir.DB;
using TrailAir.UI.Modelling.UIModels.GenericApp;
using TrailAir.DB.Models;
using TrailAir.Utility.ExceptionHelpers;
using TrailAir.UI.Modelling.BoMappers.BenchmarkResult;
using TrailAir.UI.Modelling.BoMappers.TestSampleResult;
using TrailAir.UI.Modelling.BoMappers.ParticipantResult;
using TrailAir.UI.Modelling.UIModels.Experiment;
using TrailAir.DB.Models.Enumerations;
using TrailAir.UI.Modelling.BoMappers.ExperimentResult;
using TrailAir.UI.Modelling.UIModels;
using TrailAir.UI.Modelling.BoMappers.Experiment;
using TrailAir.DB.Models.Base;

namespace TrailAir.UI.Modelling.BoLoaders
{
    public class ParticipantResultLoader : BaseBoLoader<ParticipantResultIM>
    {
        public static ParticipantResultIM GetUIModelByParticAndExp(TrailAirDbContext db, string participantId, int experimentId, bool includeControleSample = false)
        {
            ParticipantResultIM result = new();

            var elementDB = GetDbModelByParticAndExp(db, participantId, experimentId, includeControleSample);

            if (elementDB == null)
            {
                Experiment experiment = ExperimentLoader.GetItemDet(db, experimentId);

                result = ExpParticipantResultMapper.MapNewFromExperimentToUI(experiment);

                result.ParticipantId = participantId;
                result.ExperimentId = experimentId;
            }
            else
            {
                result = ExpParticipantResultMapper.MapDBToUI(elementDB, includeControleSample);
            }

            return result;
        }



        public static ParticipantResult GetDbModelByParticAndExp(TrailAirDbContext db, string participantId, int experimentId, bool includeControleSample)
        {
            var tmp = (IQueryable<ParticipantResult>)db.ParticipantResults.AsNoTracking()
                            .Include(_ => _.ControlSampleResult)
                            .Include(_ => _.BenchmarkResults).ThenInclude(_ => _.Attributes)
                            .Include(_ => _.BenchmarkResults).ThenInclude(_ => _.Benchmark)
                            .Include(_ => _.TestSampleResults).ThenInclude(_ => _.Attributes)
                            .Include(_ => _.TestSampleResults).ThenInclude(_ => _.TestSample);
            if (includeControleSample)
                tmp = (IQueryable<ParticipantResult>)tmp.Include(_ => _.ControlSampleResult).ThenInclude(_ => _.Attributes).Include(_ => _.ControlSampleResult).ThenInclude(_ => _.ControlSample).ThenInclude(_ => _.ControlSampleType);

            var elementDb = tmp
                            .FirstOrDefault(_ => _.ParticipantId == participantId && _.ExperimentId == experimentId);

            return elementDb;
        }

        public static SingleBOActionResponse Update(TrailAirDbContext db, ParticipantResultIM clientObject, string userEmail)
        {
            var response = new SingleBOActionResponse()
            {
                Success = true
            };
            try
            {
                ParticipantResult elementDB;
                if (clientObject.Id < 1)
                {
                    elementDB = new ParticipantResult();
                    db.ParticipantResults.Add(elementDB);
                }
                else
                {
                    elementDB = db.ParticipantResults
                        .Include(_ => _.BenchmarkResults).ThenInclude(_ => _.Benchmark)
                        .Include(_ => _.BenchmarkResults).ThenInclude(_ => _.Attributes)
                        .Include(_ => _.TestSampleResults).ThenInclude(_ => _.TestSample)
                        .Include(_ => _.TestSampleResults).ThenInclude(_ => _.Attributes)
                        .FirstOrDefault(x => x.Id == clientObject.Id);
                }

                //RemoveDeletedBenchmarkResults(db, clientObject.BenchmarkResults, elementDB);
                //RemoveDeletedTestSampleResults(db, clientObject.TestSampleResults, elementDB);

                ParticipantResultMapper.MapUIToDb(clientObject, elementDB);

                db.SaveChanges(userEmail);
                response.ObjectId = elementDB.Id;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessages.Add(ExceptionObject.GetErrorMessage(ex));
            }
            return response;
        }

        public static List<ExperimentResultIM> GetOrderedExperimentResultsByParticipantResult(TrailAirDbContext db, int experimentId, string participantId)
        {
            var participantResultUi = GetUIModelByParticAndExp(db, participantId, experimentId, true);

            participantResultUi.ControlSampleResult.Order = -1;

            var orderedExperimentResults = new List<ExperimentResultIM>();
            orderedExperimentResults.Add(participantResultUi.ControlSampleResult);
            orderedExperimentResults.AddRange(participantResultUi.TestSampleResults.OrderBy(er => er.Order));

            return orderedExperimentResults;
        }

        public static ParticipantResultIM Submit(DB.TrailAirDbContext db, ParticipantResultIM participantResult, string userEmail)
        {
            ParticipantResultIM result = new();

            var elementDB = db.ParticipantResults
                            .FirstOrDefault(_ => _.ParticipantId == participantResult.ParticipantId && _.ExperimentId == participantResult.ExperimentId);

            if (elementDB == null)
            {
                elementDB = new ParticipantResult();
            }

            db.ParticipantResults.Add(elementDB);
            ParticipantResultMapper.MapUIToDb(participantResult, elementDB);

            elementDB.IsSubmitted = true;
            elementDB.CompletionDate = DateTime.Now;

            db.SaveChanges(userEmail);

            var experimentParticipantResults = db.ParticipantResults.Where(pr => pr.ExperimentId == participantResult.ExperimentId);
            int experimentNumberOfParticpants = db.Experiments.SingleOrDefault(e => e.Id == participantResult.ExperimentId).NumberOfParticipants.Value;

            bool areAllResultsSubmitted = experimentParticipantResults.All(pr => pr.IsSubmitted)
                && experimentParticipantResults.Count() == experimentNumberOfParticpants;
            if (areAllResultsSubmitted)
            {
                var experiment = db.Experiments.SingleOrDefault(e => e.Id == participantResult.ExperimentId);
                experiment.DateFinished = DateTime.Now;
                experiment.ExperimentStatus = db.ExperimentStatuses.SingleOrDefault(es => es.ExperimentStatusCode == DB.Models.Enumerations.ExperimentStatusEnum.Finished);
                db.SaveChanges(userEmail);
            }

            return result;
        }

        public static bool CheckIfParticipantResultExists(TrailAirDbContext db, string participantId, int experimentId)
        {
            var participantResult = db.ParticipantResults.SingleOrDefault(pr => pr.ParticipantId == participantId && pr.ExperimentId == experimentId);
            return participantResult != null;
        }

        private static void RemoveDeletedBenchmarkResults(TrailAirDbContext db, List<ExperimentResultIM> source, ParticipantResult destination)
        {
            if (destination.BenchmarkResults == null)
                destination.BenchmarkResults = new List<BenchmarkResult>();

            if (destination.Id > 0)
            {
                var srcIds = source != null ?
                                    source
                                        .Where(_ => _.Id != 0)                  //skip new entries (Id = 0)
                                        .Select(_ => _.Id).ToArray() :
                                    new int[] { };

                var objectsToDelete = destination.BenchmarkResults
                                    .Where(_ => !srcIds.Contains(_.Id));

                if (objectsToDelete.Count() > 0)
                {
                    db.BenchmarkResults.RemoveRange(objectsToDelete);
                }
            }

            destination.BenchmarkResults = BenchmarkResultMapper.MapBenchmarkResultsUIToDB(source, destination.BenchmarkResults);
        }

        private static void RemoveDeletedTestSampleResults(TrailAirDbContext db, List<ExperimentResultIM> source, ParticipantResult destination)
        {
            if (destination.TestSampleResults == null)
                destination.TestSampleResults = new List<TestSampleResult>();

            if (destination.Id > 0)
            {
                var srcIds = source != null ?
                                    source
                                        .Where(_ => _.Id != 0)                  //skip new entries (Id = 0)
                                        .Select(_ => _.Id).ToArray() :
                                    new int[] { };

                var objectsToDelete = destination.TestSampleResults
                                    .Where(_ => !srcIds.Contains(_.Id));

                if (objectsToDelete.Count() > 0)
                {
                    db.TestSampleResults.RemoveRange(objectsToDelete);
                }
            }

            destination.TestSampleResults = TestSampleResultMapper.MapTestSampleResultsUIToDB(source, destination.TestSampleResults);
        }

        public static SingleBOActionResponse WizardSubmit(TrailAirDbContext db, int experimentId, string participantId, string userEmail)
        {
            var response = new SingleBOActionResponse()
            {
                Success = true
            };
            try
            {
                var participantResult = db.ParticipantResults
                    .SingleOrDefault(pr => pr.ExperimentId == experimentId && pr.ParticipantId == participantId);

                if (participantResult != null)
                {
                    participantResult.IsSubmitted = true;
                    participantResult.CompletionDate = DateTime.Now;

                    db.SaveChanges(userEmail);

                    var experimentParticipantResults = db.ParticipantResults.Where(pr => pr.ExperimentId == experimentId);
                    int experimentNumberOfParticpants = db.Experiments.SingleOrDefault(e => e.Id == experimentId).NumberOfParticipants.Value;

                    /// Set Experiment as FINISHED and set DateFinished
                    bool areAllResultsSubmitted = experimentParticipantResults.All(pr => pr.IsSubmitted)
                        && experimentParticipantResults.Count() == experimentNumberOfParticpants;
                    if (areAllResultsSubmitted)
                    {
                        var experiment = db.Experiments.SingleOrDefault(e => e.Id == participantResult.ExperimentId);
                        experiment.DateFinished = DateTime.Now;
                        experiment.ExperimentStatus = db.ExperimentStatuses.SingleOrDefault(es => es.ExperimentStatusCode == DB.Models.Enumerations.ExperimentStatusEnum.Finished);
                        db.SaveChanges(userEmail);
                    }
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessages.Add(ExceptionObject.GetErrorMessage(ex));
            }
            return response;
        }

        public static SingleBOActionResponse CreateCalibration(TrailAirDbContext db, WizardCalibrationIM calibration, string userEmail)
        {
            var response = new SingleBOActionResponse()
            {
                Success = true
            };
            ParticipantResult participantResult = new ParticipantResult()
            {
                ExperimentId = calibration.experimentId,
                ParticipantId = calibration.participantId
            };

            db.ParticipantResults.Add(participantResult);

            db.SaveChanges(userEmail);

            var participantResultId = db.ParticipantResults
                .SingleOrDefault(pr => pr.ParticipantId == calibration.participantId && pr.ExperimentId == calibration.experimentId).Id;

            var controlSampleResult = new ControlSampleResult()
            {
                ControlSampleId = calibration.objectUi.ExperimentBencTestSampId,
                Intensity = calibration.objectUi.Intensity,
                ParticipantResultId = participantResultId,
                FullDetectionTime = calibration.objectUi.FullDetectionTime,
                FirstDetectionTime = calibration.objectUi.FirstDetectionTime,
                BuildUpTime = calibration.objectUi.BuildUpTime,
                Sensation = calibration.objectUi.Sensation,
                Date = DateTime.Now.Date
            };
            controlSampleResult.Attributes = ExperimentResultMapper.MapAttributesUIToDb(calibration.objectUi.AttributeTexts, controlSampleResult.Attributes);


            db.ControlSampleResults.Add(controlSampleResult);

            db.SaveChanges(userEmail);

            return response;
        }

        public static ExperimentResultIM GetSingleExperimentResult(TrailAirDbContext db, int baseExperimentResultId, int baseExperimentResultType)
        {
            var experimentResult = new ExperimentResultIM();

            if (baseExperimentResultType == (int)BaseExperimentResultType.BenchmarkResult)
            {
                var benchmarkResult = db.BenchmarkResults
                    .Include(br => br.Attributes)
                    .SingleOrDefault(br => br.Id == baseExperimentResultId);
                ExperimentResultMapper.MapBenchResultDBToUI(benchmarkResult, experimentResult);
            }
            else if (baseExperimentResultType == (int)BaseExperimentResultType.TestSampleResult)
            {
                var testSampleResult = db.TestSampleResults
                    .Include(tsr => tsr.Attributes)
                    .Include(_ => _.TestSample)
                    .SingleOrDefault(tsr => tsr.Id == baseExperimentResultId);
                ExperimentResultMapper.MapTestSamResultDBToUI(testSampleResult, experimentResult);
            }
            else if (baseExperimentResultType == (int)BaseExperimentResultType.ControlSample)
            {
                var controlSampleResult = db.ControlSampleResults
                    .Include(_ => _.Attributes)
                    .Include(_ => _.ControlSample).ThenInclude(__ => __.ControlSampleType)
                    .SingleOrDefault(_ => _.Id == baseExperimentResultId);
                ExperimentResultMapper.MapControlSamResultDBToUI(controlSampleResult, experimentResult);
            }

            return experimentResult;
        }

        public static SingleBOActionResponse CancelWizardResults(TrailAirDbContext db, int experimentId, string participantId, string userEmail)
        {
            var response = new SingleBOActionResponse()
            {
                Success = true
            };
            try
            {
                var participantResult = db.ParticipantResults.AsNoTracking()
                    .Include(pr => pr.BenchmarkResults).ThenInclude(br => br.Attributes)
                    .Include(pr => pr.TestSampleResults).ThenInclude(tsr => tsr.Attributes)
                    .Include(pr => pr.ControlSampleResult).ThenInclude(tsr => tsr.Attributes)
                    .SingleOrDefault(pr => pr.ExperimentId == experimentId && pr.ParticipantId == participantId);

                if (participantResult != null)
                {
                    foreach (var benchmarkResult in participantResult.BenchmarkResults)
                    {
                        db.ExperimentResultAttributes.RemoveRange(benchmarkResult.Attributes);
                    }
                    db.BenchmarkResults.RemoveRange(participantResult.BenchmarkResults);

                    foreach (var testSampleResult in participantResult.TestSampleResults)
                    {
                        db.ExperimentResultAttributes.RemoveRange(testSampleResult.Attributes);
                    }
                    db.TestSampleResults.RemoveRange(participantResult.TestSampleResults);


                    db.ExperimentResultAttributes.RemoveRange(participantResult.ControlSampleResult.Attributes);
                    db.ControlSampleResults.RemoveRange(participantResult.ControlSampleResult);

                    db.ParticipantResults.Remove(participantResult);

                    db.SaveChanges(userEmail);
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessages.Add(ExceptionObject.GetErrorMessage(ex));
            }
            return response;
        }

        public static bool CheckIfResultIsSubmitted(TrailAirDbContext db, int experimentId, string participantId, int experimentBenchmarkTestSampleId, int expResutlType)
        {
            var participantResult = db.ParticipantResults.AsNoTracking()
                    .SingleOrDefault(pr => pr.ExperimentId == experimentId && pr.ParticipantId == participantId);

            if (participantResult != null)
            {
                var participantResultId = participantResult.Id;
                if (expResutlType == (int)BaseExperimentResultType.BenchmarkResult)
                {
                    var benchmarkResult = db.BenchmarkResults.SingleOrDefault(br => br.ParticipantResultId == participantResultId && br.BenchmarkId == experimentBenchmarkTestSampleId);
                    if (benchmarkResult != null)
                        return benchmarkResult.IsSubmitted;
                }
                else if (expResutlType == (int)BaseExperimentResultType.TestSampleResult)
                {
                    var testSampleResult = db.TestSampleResults.SingleOrDefault(ts => ts.ParticipantResultId == participantResultId && ts.TestSampleId == experimentBenchmarkTestSampleId);
                    if (testSampleResult != null)
                        return testSampleResult.IsSubmitted;
                }
            }
            return false;
        }
    }
}
