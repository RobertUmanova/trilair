﻿using Microsoft.Extensions.Configuration;

namespace TrailAir.UI.Modelling
{
    public class StartUp
    {
        public static void InitilizeModellingLayer(IConfiguration configuration)
        {
            UI.Managers.ConfigurationManager.InitializeConfiguration(configuration);
        }
    }
}
