﻿using Microsoft.Extensions.Configuration;
using TrailAir.UI.Modelling.BoMappers.MultiLanguage;
using TrailAir.UI.Modelling.Managers.Language;

namespace TrailAir.UI.Managers
{
    public class ConfigurationManager
    {
        private static List<LangSettingsModel>  SingletonLangList = null;

        public static List<LangSettingsModel> LangList {
            get {
                return SingletonLangList;
            }
        }

        public static void InitializeConfiguration(IConfiguration configuration)
        {
            LocalizationJson locJson = new LocalizationJson();
            configuration.GetSection("Localization").Bind(locJson);

            SingletonLangList = locJson.SupportedCultures;

            Dictionary<string, LangSettingsModel> langsIn = new Dictionary<string, LangSettingsModel>();

            foreach (var lang in SingletonLangList)
            {
                langsIn.Add(lang.Code, lang);
            }

            LangTextMapper.InitializeSingleton(langsIn);

        }
    }
}
