﻿
namespace TrailAir.UI.Modelling.Managers.Language
{
    public class LangSettingsModel
    {
        public string Code { get; set; }
        public string ShortCode { get; set; }
    }

    public class LocalizationJson
    {
        public string DefaultCulture { get; set; }
        public List<LangSettingsModel> SupportedCultures { get; set; }
    }
}
