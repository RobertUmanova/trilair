﻿
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TrailAir.DB.Helper;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.BoMappers.BasicUser;
using TrailAir.UI.Modelling.UIModels.User;
using TrailAir.Utility;

namespace TrailAir.UI.Modelling.Managers
{
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store, IOptions<IdentityOptions> optionsAccessor, IPasswordHasher<ApplicationUser> passwordHasher,
            IEnumerable<IUserValidator<ApplicationUser>> userValidators, IEnumerable<IPasswordValidator<ApplicationUser>> passwordValidators, ILookupNormalizer keyNormalizer,
            IdentityErrorDescriber errors, IServiceProvider services, ILogger<UserManager<ApplicationUser>> logger)
            : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {

        }

        public override Task<ApplicationUser> FindByNameAsync(string userName)
        {
            var model = base.FindByNameAsync(userName);
            return model;
        }

        public ApplicationUser FindFullUiUserByName(string userName)
        {
            ////Check cache if user was already loaded

            ////Load user from db if not in cache
            //var baseUser = FindByNameAsync(userName).Result;

            ////Fetch some more data if needed

            ////Write user to cache

            ApplicationUser applicationUser = new ApplicationUser();
            ////Do some DTO magic ??

            using (var dbContext = ConnectionHelper.GetContextToRelease())
            {
                applicationUser = dbContext.Users.AsNoTracking().FirstOrDefault(_ => _.UserName == userName || _.Email.ToLower() == userName.ToLower());
                if (applicationUser != null)
                {
                    var buId = applicationUser?.BusinessLogicDataId;
                    var businessUser = (from bu in dbContext.BusinessLogicUsers.AsNoTracking()
                                        where bu.Id == buId
                                        select bu)
                                        .Include(_ => _.Permission)
                                        .Include(_ => _.Team)
                                        .AsNoTracking().FirstOrDefault();
                    //if (businessUser == null)
                    //{
                    //    ///TODO: low priority. clear user cache here and send and email with the wornings to the IT 
                    //    throw new Exception("Umanova controlled error. Requested user was deleted in the DB. Please clear your cookies or contact IT.");
                    //}
                    applicationUser.BusinessLogicData = businessUser;
                }
            }
            return applicationUser;
        }

        public async Task<object> CreateUserAccountAsync(BusinessLogicUserDetailsIM userDetails)
        {
            var user = new ApplicationUser
            {
                UserName = userDetails.UserName,
                Email = userDetails.UserName,
                EmailConfirmed = true,
                PhoneNumber = userDetails.PhoneNumber,
                //PhoneNumberConfirmed = true,
                FirstName = userDetails.FirstName,
                LastName = userDetails.LastName
            };

            var result = await CreateAsync(user, userDetails.Password);

            if (!result.Succeeded) return JsonResponseOb.GetCustomError(result.Errors.Select(_ => _.Description).ToString());

            //update user
            BusinessLogicUserMapper.MapUIToDb(userDetails, user.BusinessLogicData);
            var status = await UpdateAsync(user);
            return JsonResponseOb.GetSuccess(user.BusinessLogicDataId);
        }
    }
}
