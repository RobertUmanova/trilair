﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.Models.Enumerations;

namespace TrailAir.UI.Modelling.UIModels.ExpParticipantResult
{
    /// <summary>
    /// Used for Wizard and also resued for Experiment resuolt but some of the properties have less sense
    /// </summary>
    public class ExperimentResultBaseIM
    {
        public int Id { get; set; }
        /// <summary>
        /// In case of control sample will be the control sample title 
        /// </summary>
        public string SampleCode { get; set; }
        public string SampleName { get; set; }
        public bool IsSubmitted { get; set; }
        public int? Order { get; set; }
        public int SampleIndex { get; set; }
        public BaseExperimentResultType ExpResultType { get; set; }
    }
    /// <summary>
    /// Used for Wizard and also resued for Experiment resuolt but some of the properties have less sense.
    /// Limited time requires limited solutions :) 
    /// </summary>
    public class ExperimentResultIM : ExperimentResultBaseIM 
    {
        public int? FirstDetectionTime { get; set; }
        public int? BuildUpTime { get; set; }
        public int? FullDetectionTime { get; set; }
        public decimal? Intensity { get; set; }
        public string Date { get; set; }
        public List<string> AttributeTexts { get; set; }
        public string Sensation { get; set; }
        public int ParticipantResultId { get; set; }
        public string ComputedName { get; set; }

        /// <summary>
        /// This can be test sample or benchmark depending on ExpResultType
        /// </summary>
        public int BencTestSampId { get; set; } // This is for example Benchmark.Id
        /// <summary>
        /// Would be to save the Id of Benchamrk or test sample it that we write result about.
        /// Now benhcmark is deprecated so could be test sample.
        /// In case used for control sample has the control sample id 
        /// </summary>
        public int ExperimentBencTestSampId { get; set; } // This is for example ExperimentBenchmark.Id
        //public BaseExperimentResultType ExpResultType { get; set; }
    }
}
