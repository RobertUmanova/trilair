﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrailAir.UI.Modelling.UIModels.ExpParticipantResult
{
    public class ParticipantResultIM
    {
        public int Id { get; set; }
        public int ExperimentId { get; set; }
        public string CompletionDate { get; set; }
        public string ParticipantId { get; set; }
        public bool IsSubmitted { get; set; }
        /// <summary>
        /// Benchmarks are not used anymore !!!!!
        /// </summary>
        public List<ExperimentResultIM> BenchmarkResults { get; set; }
        public List<ExperimentResultIM> TestSampleResults { get; set; }
        public ExperimentResultIM ControlSampleResult { get; set; }
    }
}
