﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.UI.Modelling.UIModels.User;

namespace TrailAir.UI.Modelling.UIModels.Team
{
    public class TeamListIM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsArchived { get; set; }
        public List<BusinessLogicUserListIM> AssignedUsers { get; set; }
    }
}
