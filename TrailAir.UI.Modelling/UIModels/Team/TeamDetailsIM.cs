﻿using TrailAir.UI.Modelling.UIModels.User;

namespace TrailAir.UI.Modelling.UIModels.Team
{
    public class TeamDetailsIM : TeamListIM
    {
        public string Description { get; set; }
    }
}
