﻿
namespace TrailAir.UI.Modelling.UIModels.MultiLanguage
{
    public class LangTextModel
    {
        public string Code { get; set; }
        public string ShortCode { get; set; }
        public string Text { get; set; }
        public int Id { get; set; }
    }
}
