﻿
namespace TrailAir.UI.Modelling.UIModels.MultiLanguage
{
    public class MultyLangTextModel
    {
        public List<LangTextModel> LanguageTexts { get; set; }
        public int Id { get; set; }
    }
}
