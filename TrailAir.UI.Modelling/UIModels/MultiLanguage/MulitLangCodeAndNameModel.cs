﻿using TrailAir.UI.Modelling.UIModels.MultiLanguage;

namespace TrailAir.UI.Modelling.UIModels.MultiLanguage
{
    public class MulitLangCodeAndNameModel
    {
        public int Id { get; set; }
        public MultyLangTextModel Code { get; set; }
        public MultyLangTextModel Name { get; set; }
    }
}
