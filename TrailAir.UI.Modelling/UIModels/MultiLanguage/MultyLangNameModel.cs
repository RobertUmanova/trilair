﻿using TrailAir.UI.Modelling.UIModels.MultiLanguage;

namespace TrailAir.UI.Modelling.UIModels.MultiLanguage
{
    public class MultyLangNameModel
    {
        public int Id { get; set; }
        public MultyLangTextModel Name { get; set; }
    }
}
