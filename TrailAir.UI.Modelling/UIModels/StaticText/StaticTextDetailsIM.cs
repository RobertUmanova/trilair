﻿using TrailAir.UI.Modelling.UIModels.MultiLanguage;

namespace TrailAir.UI.Modelling.UIModels.StaticText
{
    public class StaticTextDetailsIM : StaticTextListIM
    {
        public string Description { get; set; }
        public MultyLangTextModel MlText { get; set; }
    }
}
