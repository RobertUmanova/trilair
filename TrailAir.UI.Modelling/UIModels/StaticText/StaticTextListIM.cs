﻿
namespace TrailAir.UI.Modelling.UIModels.StaticText
{
    public class StaticTextListIM
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string Text { get; set; }
    }
}
