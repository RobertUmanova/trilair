﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrailAir.UI.Modelling.UIModels.User
{
    public class BusinessLogicUserDetailsIM : BusinessLogicUserListIM
    {
        public string Password { get; set; }

        public BusinessLogicUserDetailsIM()
        {

        }
    }
}
