﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.UI.Modelling.UIModels.Permission;
using TrailAir.UI.Modelling.UIModels.Team;

namespace TrailAir.UI.Modelling.UIModels.User
{
    public class BusinessLogicUserListIM
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        //public PermissionListIM Permission { get; set; }
        //public TeamListIM Team { get; set; }
        public int? PermissionId { get; set; }
        public int? TeamId { get; set; }
        public string PermissionName { get; set; }
        public string TeamName { get; set; }

    }
}
