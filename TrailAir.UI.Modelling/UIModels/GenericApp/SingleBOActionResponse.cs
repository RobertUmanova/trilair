﻿
namespace TrailAir.UI.Modelling.UIModels.GenericApp
{
    public class SingleBOActionResponse: ActionResponse
    {
        public int ObjectId { get; set; }
    }
}
