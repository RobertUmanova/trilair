﻿
namespace TrailAir.UI.Modelling.UIModels.GenericApp
{
    /// <summary>
    /// class used to represent data used in jstree view
    ///  example : 
    ///  { "id" : "1", "parent" : "#", "text" : "Simple root node" },
    ///  { "id" : "2", "parent" : "1", "text" : "Simple child node" },
    /// </summary>
    public class JstreeJsonData
    {
        public string Id { get; set; }
        public string Parent { get; set; }
        public string Text { get; set; }
        public int Quantity { get; set; }
        public bool isArchived { get; set; } = false;

        public JstreeNodeState State { get; set; }
    }

    public class JstreeNodeState
    {
        public bool Checked { get; set; }
        
    }
}
