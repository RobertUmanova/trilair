﻿
namespace TrailAir.UI.Modelling.UIModels.GenericApp
{
    /// <summary>
    /// This class is borrowed from Swissco project
    /// </summary>
    public class MultipleBOActionResponse : ActionResponse
    {
        public List<int> ObjectIds { get; set; }
    }
}
