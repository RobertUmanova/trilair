﻿
namespace TrailAir.UI.Modelling.UIModels.GenericApp
{
    public class ActionResponse
    {
        public ActionResponse()
        {
            ErrorMessages = new List<string>();
        }
        public bool Success { get; set; }
        public List<string> ErrorMessages { get; set; }

    }
}
