﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrailAir.UI.Modelling.UIModels.Participant
{
    public class ParticipantListIM
    {
        public string Id { get; set; }
        public string Fullname { get; set; }
    }
}
