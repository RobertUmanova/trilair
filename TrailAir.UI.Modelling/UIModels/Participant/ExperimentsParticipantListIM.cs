﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrailAir.UI.Modelling.UIModels.Participant
{
    public class ExperimentsParticipantListIM
    {
        public ExperimentsParticipantListIM()
        {
            Participant = new ParticipantListIM();
        }
        public int Id { get; set; }
        public int ExperimentIndex { get; set; }
        public string ParticipantId { get; set; }
        public ParticipantListIM Participant { get; set; }
        public bool HasSubmittedResult { get; set; }
    }
}
