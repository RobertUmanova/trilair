﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.UI.Modelling.UIModels.MultiLanguage;

namespace TrailAir.UI.Modelling.UIModels.Permission
{
    public class PermissionDetailsIM : PermissionListIM
    {
        public MultyLangTextModel MlName { get; set; }
    }
}
