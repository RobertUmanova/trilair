﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.Models.Enumerations;

namespace TrailAir.UI.Modelling.UIModels.Permission
{
    public class PermissionListIM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public PermissionCode PermissionCode { get; set; }
    }
}
