﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.UI.Modelling.UIModels.Experiment;
using TrailAir.UI.Modelling.UIModels.ExpParticipantResult;

namespace TrailAir.UI.Modelling.UIModels
{
    public class WizardCalibrationIM
    {
        public ExperimentResultIM objectUi { get; set; }
        public int experimentId { get; set; }
        public string participantId { get; set; }
    }

}
