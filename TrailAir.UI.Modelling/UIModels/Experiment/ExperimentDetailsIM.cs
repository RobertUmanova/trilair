﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.UI.Modelling.UIModels.Participant;

namespace TrailAir.UI.Modelling.UIModels.Experiment
{
    public class ExperimentDetailsIM : ExperimentListIM
    {
        public int? SamplePreparationId { get; set; }
        public SamplePreparationIM SamplePreparation { get; set; }
        public List<ExperimentBenchmarkIM> ExperimentBenchmarks { get; set; }
        public List<ExperimentTestSampleIM> ExperimentTestSamples { get; set; }

        /// <summary>
        /// Default constructor for creating Empty element
        /// </summary>
        public ExperimentDetailsIM()
        {

            SamplePreparation = new SamplePreparationIM();
            ControlSample = new ControlSampleListIM();
        }
    }
}
