﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrailAir.UI.Modelling.UIModels.Experiment
{
    public class ControlSampleListIM
    {
        public int Id { get; set; }
        public int ControlSampleTypeId { get; set; }
        public string ControlSampleTypeName { get; set; }
        public string SampleCode { get; set; }
        public string SampleName { get; set; }
        public string LOTCode { get; set; }
        public string ManufacturedDate { get; set; }
        public string Base { get; set; }
        public int ControlSampleTypeIntensityScore { get; set; }
    }
}
