﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrailAir.UI.Modelling.UIModels.Experiment
{
    public class ExperimentTestSampleIM
    {
        public ExperimentTestSampleIM()
        {
            TestSample = new TestSampleIM();
        }
        public int ExperimentIndex { get; set; }
        public int Id { get; set; }
        public int ExperimentId { get; set; }
        public int TestSampleId { get; set; }
        public TestSampleIM TestSample { get; set; }
    }
}
