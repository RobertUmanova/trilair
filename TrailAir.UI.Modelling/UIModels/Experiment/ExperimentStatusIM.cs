﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.Models.Enumerations;

namespace TrailAir.UI.Modelling.UIModels.Experiment
{
    public class ExperimentStatusIM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ExperimentStatusEnum ExperimentStatusCode { get; set; }
    }
}
