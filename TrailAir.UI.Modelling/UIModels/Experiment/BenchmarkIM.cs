﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrailAir.UI.Modelling.UIModels.Experiment
{
    public class BenchmarkIM
    {
        public int Id { get; set; }
        public string SampleCode { get; set; }
        public string SampleName { get; set; }
    }
}
