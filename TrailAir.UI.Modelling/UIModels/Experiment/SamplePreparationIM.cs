﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrailAir.UI.Modelling.UIModels.Experiment
{
    public class SamplePreparationIM
    {
        public int? PerfumeConcentration { get; set; }
        public int? EvaporingTime { get; set; }
        public int? EvaporingTemperature { get; set; }

        /// <summary>
        /// Default constructor with the predefined values: EvaporingTime = 4; EvaporingTemperature = 32;
        /// </summary>
        public SamplePreparationIM()
        {
            EvaporingTime = 4;
            EvaporingTemperature = 32;
        }
    }
}
