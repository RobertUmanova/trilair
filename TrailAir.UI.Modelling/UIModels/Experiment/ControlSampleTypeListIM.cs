﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.Models.Enumerations;

namespace TrailAir.UI.Modelling.UIModels.Experiment
{
    public class ControlSampleTypeListIM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ControlSampleTypeCode ControlSampleTypeCode { get; set; }
        public int IntensityScore { get; set; }
        public string FullText { get; set; }
    }
}
