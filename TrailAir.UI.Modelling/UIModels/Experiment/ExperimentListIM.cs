﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.UI.Modelling.UIModels.Participant;
using TrailAir.UI.Modelling.UIModels.Team;

namespace TrailAir.UI.Modelling.UIModels.Experiment
{
    public class ExperimentListIM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Instruction { get; set; }
        public int? NumberOfSamples { get; set; }
        public int? ControlSampleId { get; set; }
        public ControlSampleListIM ControlSample { get; set; }
        //public bool Randomization { get; set; }
        //public int? TimeBetweenSamples { get; set; }
        //public int? TestTypeId { get; set; }
        //public TestTypeListIM TestType { get; set; }
        //public int? ScoringScaleDefinitionId { get; set; }
        //public ScoringScaleDefinitionListIM ScoringScaleDefinition { get; set; }
        public int? ExperimentStatusId { get; set; }
        public string ExperimentStatusName { get; set; }
        public string DatePublished { get; set; }
        public int TeamId { get; set; }
        public string RequesterId { get; set; }
        public string RequesterFullName { get; set; }
        public string TeamName { get; set; }
        public string Consumer { get; set; }
        public List<ExperimentsParticipantListIM> Participants { get; set; }
        public string Comment { get; set; }
        public bool IsPublished { get; set; }
        public bool IsFinished { get; set; }
        public string DateFinished { get; set; }
        public bool HasAllResultsSubmitted { get; set; }
    }
}
