﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrailAir.UI.Modelling.UIModels.Experiment
{
    public class TestSampleIM
    {
        public int Id { get; set; }
        public string SampleCode { get; set; }
        public string SampleName { get; set; }
        public bool IsBenchmark { get; set; }
        public bool IsInternal { get; set; }
        public string ProductName { get; set; }
        public string InternalName { get; set; }
        public string FantasyName { get; set; }
        public string LOTCode { get; set; }
        public string ManufacturedDate { get; set; }
        public string ApplicationDate { get; set; }
        public string GroupNumber { get; set; }
        public string TrialNumber { get; set; }
        public string Technology { get; set; }
        public int? Concentration { get; set; }
        public string Base { get; set; }
        public string ComputedName { get; set; }
        public int SampleIndex { get; set; }
        public string Comment { get; set; }
        public string ConcentrationStr { get; set; }
    }
}
