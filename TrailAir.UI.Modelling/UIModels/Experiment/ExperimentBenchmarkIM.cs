﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrailAir.UI.Modelling.UIModels.Experiment
{
    public class ExperimentBenchmarkIM
    {
        /// <summary>
        /// Default contructor for creating empty element
        /// </summary>
        public ExperimentBenchmarkIM()
        {
            Benchmark = new BenchmarkIM();
        }

        public int ExperimentIndex { get; set; }
        public int Id { get; set; }
        public int ExperimentId { get; set; }
        public int BenchmarkId { get; set; }
        public BenchmarkIM Benchmark { get; set; }
    }
}
