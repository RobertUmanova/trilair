﻿using TrailAir.DB.Helper;
using TrailAir.DB.Models.Base;

namespace TrailAir.DB.DataManagersCache
{
    public class LanguageManInst
    {
        public string DefaultLanguageCode = "en-GB";
        public Dictionary<string, LanguagesDictionary> language = null;
        public Dictionary<string, LanguagesDictionary> Language
        {
            get
            {
                if (language == null)
                {
                    using (var dbContext = ConnectionHelper.GetContextToRelease())
                    {
                        var allLanguages = dbContext.MultyLanguageTexts.SelectMany(_ => _.Texts).ToList()
                            .GroupBy(p => p.Key)
                       .Select(g => new
                       {
                           langCode = g.Key,
                           elements = g.Select(_ => new
                           {
                               key = _.MultyLanguageTextId,
                               value = _.Name
                           })
                       }).ToArray();
                        var result = new Dictionary<string, LanguagesDictionary>();
                        foreach (var lang in allLanguages)
                        {
                            var langDic = new LanguagesDictionary();
                            foreach (var singlText in lang.elements)
                            {
                                langDic.Add(singlText.key, singlText.value);
                            }
                            result.Add(lang.langCode, langDic);
                        }

                        language = result;
                    }
                }
                return language;
            }
        }

        public void ClearCache()
        {
            language = null;
        }

        public string GetCurrentLangText(int multLangTextId)
        {
            LanguagesDictionary currentLanguage = GetCurrentLanguage();
            if (currentLanguage == null)
                return string.Empty;

            string result = null;
            if (currentLanguage.TryGetValue(multLangTextId, out result))
                if (!string.IsNullOrEmpty(result))
                    return result;

            return string.Empty;
        }

        public LanguagesDictionary GetCurrentLanguage()
        {
            var currentLanguage = new LanguagesDictionary();
            var currentLanguageCode = LanguageCache.GetCurrentLanguageCode();
            if (Language.ContainsKey(currentLanguageCode))
                currentLanguage = Language[currentLanguageCode];
            else if (Language.ContainsKey(DefaultLanguageCode))
                currentLanguage = Language[DefaultLanguageCode];
            else
                return null;

            return currentLanguage;
        }
    }

    public class LanguagesDictionary : Dictionary<int, string>
    {
    }

    public static class LanguageCache
    {
        private static LanguageManInst langManOb = null;
        private static LanguageManInst LangManOb
        {
            get
            {
                if (langManOb == null)
                {
                    langManOb = new LanguageManInst();
                }
                return langManOb;
            }
        }

        public static string GetCurrentLangugaeText(int multLangTextId)
        {
            return LangManOb.GetCurrentLangText(multLangTextId);
        }

        public static string GetCurrentLanguageCode()
        {
            return System.Threading.Thread.CurrentThread.CurrentUICulture.Name;
        }


        public static void ClearCache()
        {
            LanguageCache.LangManOb.ClearCache();
        }

        internal static string GetDescription(MultyLanguageText multyLangMessage)
        {
            var result = multyLangMessage.Texts.FirstOrDefault(_ => _.Key == GetCurrentLanguageCode());

            if (result != null)
                return result.Description;

            return null;
        }
    }
}
