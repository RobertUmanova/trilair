﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.Helper;

namespace TrailAir.DB.DataManagersCache
{
    public static class StaticTextCache
    {
        public static Dictionary<string, int> texts = null;
        public static Dictionary<string, int> Texts
        {
            get
            {
                if (texts == null || texts.Count < 1)
                {
                    using (var dbContext = ConnectionHelper.GetContextToRelease())
                    {
                        var allTexts = dbContext.StaticTexts.ToList().ToArray();
                        var result = new Dictionary<string, int>();
                        foreach (var lang in allTexts)
                        {
                            result.Add(lang.Key, lang.TextId);
                        }

                        texts = result;
                    }
                }
                return texts;
            }
        }

        public static void ClearCache()
        {
            texts = null;
        }

        public static string GetCurrentLangText(string key)
        {
            int multLangId;
            if (!Texts.TryGetValue(key, out multLangId))
            {
                return string.Empty;
            }

            return LanguageCache.GetCurrentLangugaeText(multLangId);
        }


    }
}
