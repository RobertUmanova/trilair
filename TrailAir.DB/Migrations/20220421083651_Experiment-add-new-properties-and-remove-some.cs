﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class Experimentaddnewpropertiesandremovesome : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Experiments_ScoringScaleDefinitions_ScoringScaleDefinitionId",
                table: "Experiments");

            migrationBuilder.DropForeignKey(
                name: "FK_Experiments_TestTypes_TestTypeId",
                table: "Experiments");

            migrationBuilder.DropIndex(
                name: "IX_Experiments_ScoringScaleDefinitionId",
                table: "Experiments");

            migrationBuilder.DropColumn(
                name: "NumberOfPanelists",
                table: "Experiments");

            migrationBuilder.DropColumn(
                name: "Randomization",
                table: "Experiments");

            migrationBuilder.DropColumn(
                name: "ScoringScaleDefinitionId",
                table: "Experiments");

            migrationBuilder.RenameColumn(
                name: "TimeBetweenSamples",
                table: "Experiments",
                newName: "NumberOfParticipants");

            migrationBuilder.RenameColumn(
                name: "TestTypeId",
                table: "Experiments",
                newName: "ExperimentStatusId");

            migrationBuilder.RenameIndex(
                name: "IX_Experiments_TestTypeId",
                table: "Experiments",
                newName: "IX_Experiments_ExperimentStatusId");

            migrationBuilder.AddColumn<string>(
                name: "Consumer",
                table: "Experiments",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DatePublished",
                table: "Experiments",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeamId",
                table: "Experiments",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "ExperimentStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NameId = table.Column<int>(type: "int", nullable: false),
                    ExperimentStatusCode = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    IsArchived = table.Column<bool>(type: "bit", nullable: false),
                    IsManuallyArchived = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExperimentStatuses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExperimentStatuses_MultyLanguageTexts_NameId",
                        column: x => x.NameId,
                        principalTable: "MultyLanguageTexts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Experiments_TeamId",
                table: "Experiments",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_ExperimentStatuses_NameId",
                table: "ExperimentStatuses",
                column: "NameId");

            migrationBuilder.AddForeignKey(
                name: "FK_Experiments_ExperimentStatuses_ExperimentStatusId",
                table: "Experiments",
                column: "ExperimentStatusId",
                principalTable: "ExperimentStatuses",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Experiments_Teams_TeamId",
                table: "Experiments",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Experiments_ExperimentStatuses_ExperimentStatusId",
                table: "Experiments");

            migrationBuilder.DropForeignKey(
                name: "FK_Experiments_Teams_TeamId",
                table: "Experiments");

            migrationBuilder.DropTable(
                name: "ExperimentStatuses");

            migrationBuilder.DropIndex(
                name: "IX_Experiments_TeamId",
                table: "Experiments");

            migrationBuilder.DropColumn(
                name: "Consumer",
                table: "Experiments");

            migrationBuilder.DropColumn(
                name: "DatePublished",
                table: "Experiments");

            migrationBuilder.DropColumn(
                name: "TeamId",
                table: "Experiments");

            migrationBuilder.RenameColumn(
                name: "NumberOfParticipants",
                table: "Experiments",
                newName: "TimeBetweenSamples");

            migrationBuilder.RenameColumn(
                name: "ExperimentStatusId",
                table: "Experiments",
                newName: "TestTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_Experiments_ExperimentStatusId",
                table: "Experiments",
                newName: "IX_Experiments_TestTypeId");

            migrationBuilder.AddColumn<int>(
                name: "NumberOfPanelists",
                table: "Experiments",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Randomization",
                table: "Experiments",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "ScoringScaleDefinitionId",
                table: "Experiments",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Experiments_ScoringScaleDefinitionId",
                table: "Experiments",
                column: "ScoringScaleDefinitionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Experiments_ScoringScaleDefinitions_ScoringScaleDefinitionId",
                table: "Experiments",
                column: "ScoringScaleDefinitionId",
                principalTable: "ScoringScaleDefinitions",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Experiments_TestTypes_TestTypeId",
                table: "Experiments",
                column: "TestTypeId",
                principalTable: "TestTypes",
                principalColumn: "Id");
        }
    }
}
