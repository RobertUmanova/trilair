﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class ExperimentParticipantAddonemorelayerforkeepingindex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExperimentParticipant");

            migrationBuilder.AddColumn<int>(
                name: "ParticipantId",
                table: "Experiments",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ExperimentsParticipants",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ExperimentIndex = table.Column<int>(type: "int", nullable: false),
                    ParticipantId = table.Column<int>(type: "int", nullable: false),
                    ExperimentId = table.Column<int>(type: "int", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    IsArchived = table.Column<bool>(type: "bit", nullable: false),
                    IsManuallyArchived = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExperimentsParticipants", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExperimentsParticipants_Experiments_ExperimentId",
                        column: x => x.ExperimentId,
                        principalTable: "Experiments",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ExperimentsParticipants_Participants_ParticipantId",
                        column: x => x.ParticipantId,
                        principalTable: "Participants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Experiments_ParticipantId",
                table: "Experiments",
                column: "ParticipantId");

            migrationBuilder.CreateIndex(
                name: "IX_ExperimentsParticipants_ExperimentId",
                table: "ExperimentsParticipants",
                column: "ExperimentId");

            migrationBuilder.CreateIndex(
                name: "IX_ExperimentsParticipants_ParticipantId",
                table: "ExperimentsParticipants",
                column: "ParticipantId");

            migrationBuilder.AddForeignKey(
                name: "FK_Experiments_Participants_ParticipantId",
                table: "Experiments",
                column: "ParticipantId",
                principalTable: "Participants",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Experiments_Participants_ParticipantId",
                table: "Experiments");

            migrationBuilder.DropTable(
                name: "ExperimentsParticipants");

            migrationBuilder.DropIndex(
                name: "IX_Experiments_ParticipantId",
                table: "Experiments");

            migrationBuilder.DropColumn(
                name: "ParticipantId",
                table: "Experiments");

            migrationBuilder.CreateTable(
                name: "ExperimentParticipant",
                columns: table => new
                {
                    ExperimentsId = table.Column<int>(type: "int", nullable: false),
                    ParticipantsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExperimentParticipant", x => new { x.ExperimentsId, x.ParticipantsId });
                    table.ForeignKey(
                        name: "FK_ExperimentParticipant_Experiments_ExperimentsId",
                        column: x => x.ExperimentsId,
                        principalTable: "Experiments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExperimentParticipant_Participants_ParticipantsId",
                        column: x => x.ParticipantsId,
                        principalTable: "Participants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExperimentParticipant_ParticipantsId",
                table: "ExperimentParticipant",
                column: "ParticipantsId");
        }
    }
}
