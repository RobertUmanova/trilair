﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class ExperimentMoveIntesityScoreToControlSample : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IntensityScore",
                table: "Experiments");

            migrationBuilder.AddColumn<int>(
                name: "IntensityScore",
                table: "ControlSamples",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IntensityScore",
                table: "ControlSamples");

            migrationBuilder.AddColumn<int>(
                name: "IntensityScore",
                table: "Experiments",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
