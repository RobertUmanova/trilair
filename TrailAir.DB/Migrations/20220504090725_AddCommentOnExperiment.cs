﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class AddCommentOnExperiment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "TestTypes",
                newName: "TestTypes",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "Teams",
                newName: "Teams",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "StaticTexts",
                newName: "StaticTexts",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "ScoringScaleDefinitions",
                newName: "ScoringScaleDefinitions",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "SamplePreparations",
                newName: "SamplePreparations",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "RequestInfos",
                newName: "RequestInfos",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "PersistedGrants",
                newName: "PersistedGrants",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "Permissions",
                newName: "Permissions",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "Participants",
                newName: "Participants",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "MultyLanguageTexts",
                newName: "MultyLanguageTexts",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "LogActions",
                newName: "LogActions",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "LanguageText",
                newName: "LanguageText",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "Keys",
                newName: "Keys",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "ExperimentStatuses",
                newName: "ExperimentStatuses",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "Experiments",
                newName: "Experiments",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "Documents",
                newName: "Documents",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "DeviceCodes",
                newName: "DeviceCodes",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "BusinessLogicUsers",
                newName: "BusinessLogicUsers",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "AuthorizationTokens",
                newName: "AuthorizationTokens",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "AspNetUserTokens",
                newName: "AspNetUserTokens",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "AspNetUsers",
                newName: "AspNetUsers",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "AspNetUserRoles",
                newName: "AspNetUserRoles",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "AspNetUserLogins",
                newName: "AspNetUserLogins",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "AspNetUserClaims",
                newName: "AspNetUserClaims",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "AspNetRoles",
                newName: "AspNetRoles",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "AspNetRoleClaims",
                newName: "AspNetRoleClaims",
                newSchema: "dbo");

            migrationBuilder.AddColumn<string>(
                name: "Comment",
                schema: "dbo",
                table: "Experiments",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Comment",
                schema: "dbo",
                table: "Experiments");

            migrationBuilder.RenameTable(
                name: "TestTypes",
                schema: "dbo",
                newName: "TestTypes");

            migrationBuilder.RenameTable(
                name: "Teams",
                schema: "dbo",
                newName: "Teams");

            migrationBuilder.RenameTable(
                name: "StaticTexts",
                schema: "dbo",
                newName: "StaticTexts");

            migrationBuilder.RenameTable(
                name: "ScoringScaleDefinitions",
                schema: "dbo",
                newName: "ScoringScaleDefinitions");

            migrationBuilder.RenameTable(
                name: "SamplePreparations",
                schema: "dbo",
                newName: "SamplePreparations");

            migrationBuilder.RenameTable(
                name: "RequestInfos",
                schema: "dbo",
                newName: "RequestInfos");

            migrationBuilder.RenameTable(
                name: "PersistedGrants",
                schema: "dbo",
                newName: "PersistedGrants");

            migrationBuilder.RenameTable(
                name: "Permissions",
                schema: "dbo",
                newName: "Permissions");

            migrationBuilder.RenameTable(
                name: "Participants",
                schema: "dbo",
                newName: "Participants");

            migrationBuilder.RenameTable(
                name: "MultyLanguageTexts",
                schema: "dbo",
                newName: "MultyLanguageTexts");

            migrationBuilder.RenameTable(
                name: "LogActions",
                schema: "dbo",
                newName: "LogActions");

            migrationBuilder.RenameTable(
                name: "LanguageText",
                schema: "dbo",
                newName: "LanguageText");

            migrationBuilder.RenameTable(
                name: "Keys",
                schema: "dbo",
                newName: "Keys");

            migrationBuilder.RenameTable(
                name: "ExperimentStatuses",
                schema: "dbo",
                newName: "ExperimentStatuses");

            migrationBuilder.RenameTable(
                name: "Experiments",
                schema: "dbo",
                newName: "Experiments");

            migrationBuilder.RenameTable(
                name: "Documents",
                schema: "dbo",
                newName: "Documents");

            migrationBuilder.RenameTable(
                name: "DeviceCodes",
                schema: "dbo",
                newName: "DeviceCodes");

            migrationBuilder.RenameTable(
                name: "BusinessLogicUsers",
                schema: "dbo",
                newName: "BusinessLogicUsers");

            migrationBuilder.RenameTable(
                name: "AuthorizationTokens",
                schema: "dbo",
                newName: "AuthorizationTokens");

            migrationBuilder.RenameTable(
                name: "AspNetUserTokens",
                schema: "dbo",
                newName: "AspNetUserTokens");

            migrationBuilder.RenameTable(
                name: "AspNetUsers",
                schema: "dbo",
                newName: "AspNetUsers");

            migrationBuilder.RenameTable(
                name: "AspNetUserRoles",
                schema: "dbo",
                newName: "AspNetUserRoles");

            migrationBuilder.RenameTable(
                name: "AspNetUserLogins",
                schema: "dbo",
                newName: "AspNetUserLogins");

            migrationBuilder.RenameTable(
                name: "AspNetUserClaims",
                schema: "dbo",
                newName: "AspNetUserClaims");

            migrationBuilder.RenameTable(
                name: "AspNetRoles",
                schema: "dbo",
                newName: "AspNetRoles");

            migrationBuilder.RenameTable(
                name: "AspNetRoleClaims",
                schema: "dbo",
                newName: "AspNetRoleClaims");
        }
    }
}
