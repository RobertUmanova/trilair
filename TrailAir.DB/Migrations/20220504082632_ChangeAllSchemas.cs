﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class ChangeAllSchemas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "TestSamples",
                schema: "Trailair_DB_us",
                newName: "TestSamples",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "ExperimentTestSamples",
                schema: "Trailair_DB_us",
                newName: "ExperimentTestSamples",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "ExperimentsParticipants",
                schema: "Trailair_DB_us",
                newName: "ExperimentsParticipants",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "ExperimentBenchmarks",
                schema: "Trailair_DB_us",
                newName: "ExperimentBenchmarks",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "ControlSampleTypes",
                schema: "Trailair_DB_us",
                newName: "ControlSampleTypes",
                newSchema: "dbo");

            migrationBuilder.RenameTable(
                name: "ControlSamples",
                schema: "Trailair_DB_us",
                newName: "ControlSamples",
                newSchema: "dbo");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "TestSamples",
                schema: "dbo",
                newName: "TestSamples");

            migrationBuilder.RenameTable(
                name: "ExperimentTestSamples",
                schema: "dbo",
                newName: "ExperimentTestSamples");

            migrationBuilder.RenameTable(
                name: "ExperimentsParticipants",
                schema: "dbo",
                newName: "ExperimentsParticipants");

            migrationBuilder.RenameTable(
                name: "ExperimentBenchmarks",
                schema: "dbo",
                newName: "ExperimentBenchmarks");

            migrationBuilder.RenameTable(
                name: "ControlSampleTypes",
                schema: "dbo",
                newName: "ControlSampleTypes");

            migrationBuilder.RenameTable(
                name: "ControlSamples",
                schema: "dbo",
                newName: "ControlSamples");
        }
    }
}
