﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class Benchmarkfixtypo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExperimentBenchmarks_Bechmarks_BechmarkId",
                table: "ExperimentBenchmarks");

            migrationBuilder.RenameColumn(
                name: "BechmarkId",
                table: "ExperimentBenchmarks",
                newName: "BenchmarkId");

            migrationBuilder.RenameIndex(
                name: "IX_ExperimentBenchmarks_BechmarkId",
                table: "ExperimentBenchmarks",
                newName: "IX_ExperimentBenchmarks_BenchmarkId");

            migrationBuilder.AddForeignKey(
                name: "FK_ExperimentBenchmarks_Bechmarks_BenchmarkId",
                table: "ExperimentBenchmarks",
                column: "BenchmarkId",
                principalTable: "Bechmarks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExperimentBenchmarks_Bechmarks_BenchmarkId",
                table: "ExperimentBenchmarks");

            migrationBuilder.RenameColumn(
                name: "BenchmarkId",
                table: "ExperimentBenchmarks",
                newName: "BechmarkId");

            migrationBuilder.RenameIndex(
                name: "IX_ExperimentBenchmarks_BenchmarkId",
                table: "ExperimentBenchmarks",
                newName: "IX_ExperimentBenchmarks_BechmarkId");

            migrationBuilder.AddForeignKey(
                name: "FK_ExperimentBenchmarks_Bechmarks_BechmarkId",
                table: "ExperimentBenchmarks",
                column: "BechmarkId",
                principalTable: "Bechmarks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
