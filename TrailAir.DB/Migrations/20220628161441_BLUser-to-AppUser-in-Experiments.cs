﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class BLUsertoAppUserinExperiments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExperimentsParticipants_Participants_ParticipantId",
                schema: "dbo",
                table: "ExperimentsParticipants");

            migrationBuilder.DropForeignKey(
                name: "FK_ParticipantResults_Participants_ParticipantId",
                schema: "dbo",
                table: "ParticipantResults");

            migrationBuilder.AlterColumn<string>(
                name: "ParticipantId",
                schema: "dbo",
                table: "ParticipantResults",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "ParticipantId",
                schema: "dbo",
                table: "ExperimentsParticipants",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_ExperimentsParticipants_AspNetUsers_ParticipantId",
                schema: "dbo",
                table: "ExperimentsParticipants",
                column: "ParticipantId",
                principalSchema: "dbo",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ParticipantResults_AspNetUsers_ParticipantId",
                schema: "dbo",
                table: "ParticipantResults",
                column: "ParticipantId",
                principalSchema: "dbo",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExperimentsParticipants_AspNetUsers_ParticipantId",
                schema: "dbo",
                table: "ExperimentsParticipants");

            migrationBuilder.DropForeignKey(
                name: "FK_ParticipantResults_AspNetUsers_ParticipantId",
                schema: "dbo",
                table: "ParticipantResults");

            migrationBuilder.AlterColumn<int>(
                name: "ParticipantId",
                schema: "dbo",
                table: "ParticipantResults",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ParticipantId",
                schema: "dbo",
                table: "ExperimentsParticipants",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ExperimentsParticipants_Participants_ParticipantId",
                schema: "dbo",
                table: "ExperimentsParticipants",
                column: "ParticipantId",
                principalSchema: "dbo",
                principalTable: "Participants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ParticipantResults_Participants_ParticipantId",
                schema: "dbo",
                table: "ParticipantResults",
                column: "ParticipantId",
                principalSchema: "dbo",
                principalTable: "Participants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
