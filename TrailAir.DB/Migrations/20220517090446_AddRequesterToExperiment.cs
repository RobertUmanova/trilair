﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class AddRequesterToExperiment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RequesterId",
                schema: "dbo",
                table: "Experiments",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Experiments_RequesterId",
                schema: "dbo",
                table: "Experiments",
                column: "RequesterId");

            migrationBuilder.AddForeignKey(
                name: "FK_Experiments_BusinessLogicUsers_RequesterId",
                schema: "dbo",
                table: "Experiments",
                column: "RequesterId",
                principalSchema: "dbo",
                principalTable: "BusinessLogicUsers",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Experiments_BusinessLogicUsers_RequesterId",
                schema: "dbo",
                table: "Experiments");

            migrationBuilder.DropIndex(
                name: "IX_Experiments_RequesterId",
                schema: "dbo",
                table: "Experiments");

            migrationBuilder.DropColumn(
                name: "RequesterId",
                schema: "dbo",
                table: "Experiments");
        }
    }
}
