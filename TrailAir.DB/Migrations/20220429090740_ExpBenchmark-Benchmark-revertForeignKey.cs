﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class ExpBenchmarkBenchmarkrevertForeignKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExperimentBenchmarks_Benchmarks_BenchmarkId",
                table: "ExperimentBenchmarks");

            migrationBuilder.DropIndex(
                name: "IX_ExperimentBenchmarks_BenchmarkId",
                table: "ExperimentBenchmarks");

            migrationBuilder.DropColumn(
                name: "BenchmarkId",
                table: "ExperimentBenchmarks");

            migrationBuilder.AddColumn<int>(
                name: "ExperimentBenchmarkId",
                table: "Benchmarks",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Benchmarks_ExperimentBenchmarkId",
                table: "Benchmarks",
                column: "ExperimentBenchmarkId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Benchmarks_ExperimentBenchmarks_ExperimentBenchmarkId",
                table: "Benchmarks",
                column: "ExperimentBenchmarkId",
                principalTable: "ExperimentBenchmarks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Benchmarks_ExperimentBenchmarks_ExperimentBenchmarkId",
                table: "Benchmarks");

            migrationBuilder.DropIndex(
                name: "IX_Benchmarks_ExperimentBenchmarkId",
                table: "Benchmarks");

            migrationBuilder.DropColumn(
                name: "ExperimentBenchmarkId",
                table: "Benchmarks");

            migrationBuilder.AddColumn<int>(
                name: "BenchmarkId",
                table: "ExperimentBenchmarks",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ExperimentBenchmarks_BenchmarkId",
                table: "ExperimentBenchmarks",
                column: "BenchmarkId");

            migrationBuilder.AddForeignKey(
                name: "FK_ExperimentBenchmarks_Benchmarks_BenchmarkId",
                table: "ExperimentBenchmarks",
                column: "BenchmarkId",
                principalTable: "Benchmarks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
