﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class controlSampleResoultExtension : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_BusinessLogicUsers_BusinessLogicDataId",
                schema: "dbo",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_BenchmarkResults_Benchmarks_BenchmarkId",
                schema: "dbo",
                table: "BenchmarkResults");

            migrationBuilder.DropForeignKey(
                name: "FK_BenchmarkResults_ParticipantResults_ParticipantResultId",
                schema: "dbo",
                table: "BenchmarkResults");

            migrationBuilder.DropForeignKey(
                name: "FK_Benchmarks_Experiments_ExperimentId",
                schema: "dbo",
                table: "Benchmarks");

            migrationBuilder.DropForeignKey(
                name: "FK_BusinessLogicUsers_Permissions_PermissionId",
                schema: "dbo",
                table: "BusinessLogicUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_BusinessLogicUsers_Teams_TeamId",
                schema: "dbo",
                table: "BusinessLogicUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_ControlSampleResults_ControlSamples_ControlSampleId",
                schema: "dbo",
                table: "ControlSampleResults");

            migrationBuilder.DropForeignKey(
                name: "FK_ControlSampleResults_ParticipantResults_ParticipantResultId",
                schema: "dbo",
                table: "ControlSampleResults");

            migrationBuilder.DropForeignKey(
                name: "FK_ControlSamples_ControlSampleTypes_ControlSampleTypeId",
                schema: "dbo",
                table: "ControlSamples");

            migrationBuilder.DropForeignKey(
                name: "FK_ExperimentResultAttributes_BenchmarkResults_BenchmarkResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes");

            migrationBuilder.DropForeignKey(
                name: "FK_ExperimentResultAttributes_TestSampleResults_TestSampleResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes");

            migrationBuilder.DropForeignKey(
                name: "FK_Experiments_BusinessLogicUsers_RequesterId",
                schema: "dbo",
                table: "Experiments");

            migrationBuilder.DropForeignKey(
                name: "FK_Experiments_ControlSamples_ControlSampleId",
                schema: "dbo",
                table: "Experiments");

            migrationBuilder.DropForeignKey(
                name: "FK_Experiments_ExperimentStatuses_ExperimentStatusId",
                schema: "dbo",
                table: "Experiments");

            migrationBuilder.DropForeignKey(
                name: "FK_Experiments_Participants_ParticipantId",
                schema: "dbo",
                table: "Experiments");

            migrationBuilder.DropForeignKey(
                name: "FK_Experiments_SamplePreparations_SamplePreparationId",
                schema: "dbo",
                table: "Experiments");

            migrationBuilder.DropForeignKey(
                name: "FK_Experiments_Teams_TeamId",
                schema: "dbo",
                table: "Experiments");

            migrationBuilder.DropForeignKey(
                name: "FK_ExperimentsParticipants_Experiments_ExperimentId",
                schema: "dbo",
                table: "ExperimentsParticipants");

            migrationBuilder.DropForeignKey(
                name: "FK_ExperimentsParticipants_Participants_ParticipantId",
                schema: "dbo",
                table: "ExperimentsParticipants");

            migrationBuilder.DropForeignKey(
                name: "FK_ExperimentStatuses_MultyLanguageTexts_NameId",
                schema: "dbo",
                table: "ExperimentStatuses");

            migrationBuilder.DropForeignKey(
                name: "FK_LanguageText_MultyLanguageTexts_MultyLanguageTextId",
                schema: "dbo",
                table: "LanguageText");

            migrationBuilder.DropForeignKey(
                name: "FK_ParticipantResults_Experiments_ExperimentId",
                schema: "dbo",
                table: "ParticipantResults");

            migrationBuilder.DropForeignKey(
                name: "FK_ParticipantResults_Participants_ParticipantId",
                schema: "dbo",
                table: "ParticipantResults");

            migrationBuilder.DropForeignKey(
                name: "FK_Permissions_MultyLanguageTexts_NameId",
                schema: "dbo",
                table: "Permissions");

            migrationBuilder.DropForeignKey(
                name: "FK_ScoringScaleDefinitions_MultyLanguageTexts_TextId",
                schema: "dbo",
                table: "ScoringScaleDefinitions");

            migrationBuilder.DropForeignKey(
                name: "FK_StaticTexts_MultyLanguageTexts_TextId",
                schema: "dbo",
                table: "StaticTexts");

            migrationBuilder.DropForeignKey(
                name: "FK_TestSampleResults_ParticipantResults_ParticipantResultId",
                schema: "dbo",
                table: "TestSampleResults");

            migrationBuilder.DropForeignKey(
                name: "FK_TestSampleResults_TestSamples_TestSampleId",
                schema: "dbo",
                table: "TestSampleResults");

            migrationBuilder.DropForeignKey(
                name: "FK_TestSamples_Experiments_ExperimentId",
                schema: "dbo",
                table: "TestSamples");

            migrationBuilder.DropForeignKey(
                name: "FK_TestTypes_MultyLanguageTexts_TextId",
                schema: "dbo",
                table: "TestTypes");

            migrationBuilder.RenameColumn(
                name: "NewIntensity",
                schema: "dbo",
                table: "ControlSampleResults",
                newName: "Intensity");

            migrationBuilder.AddColumn<int>(
                name: "ControlSampleResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BuildUpTime",
                schema: "dbo",
                table: "ControlSampleResults",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                schema: "dbo",
                table: "ControlSampleResults",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FirstDetectionTime",
                schema: "dbo",
                table: "ControlSampleResults",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FullDetectionTime",
                schema: "dbo",
                table: "ControlSampleResults",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsSubmitted",
                schema: "dbo",
                table: "ControlSampleResults",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                schema: "dbo",
                table: "ControlSampleResults",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SampleIndex",
                schema: "dbo",
                table: "ControlSampleResults",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Sensation",
                schema: "dbo",
                table: "ControlSampleResults",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ExperimentResultAttributes_ControlSampleResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes",
                column: "ControlSampleResultId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_BusinessLogicUsers_BusinessLogicDataId",
                schema: "dbo",
                table: "AspNetUsers",
                column: "BusinessLogicDataId",
                principalSchema: "dbo",
                principalTable: "BusinessLogicUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BenchmarkResults_Benchmarks_BenchmarkId",
                schema: "dbo",
                table: "BenchmarkResults",
                column: "BenchmarkId",
                principalSchema: "dbo",
                principalTable: "Benchmarks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BenchmarkResults_ParticipantResults_ParticipantResultId",
                schema: "dbo",
                table: "BenchmarkResults",
                column: "ParticipantResultId",
                principalSchema: "dbo",
                principalTable: "ParticipantResults",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Benchmarks_Experiments_ExperimentId",
                schema: "dbo",
                table: "Benchmarks",
                column: "ExperimentId",
                principalSchema: "dbo",
                principalTable: "Experiments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BusinessLogicUsers_Permissions_PermissionId",
                schema: "dbo",
                table: "BusinessLogicUsers",
                column: "PermissionId",
                principalSchema: "dbo",
                principalTable: "Permissions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BusinessLogicUsers_Teams_TeamId",
                schema: "dbo",
                table: "BusinessLogicUsers",
                column: "TeamId",
                principalSchema: "dbo",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ControlSampleResults_ControlSamples_ControlSampleId",
                schema: "dbo",
                table: "ControlSampleResults",
                column: "ControlSampleId",
                principalSchema: "dbo",
                principalTable: "ControlSamples",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ControlSampleResults_ParticipantResults_ParticipantResultId",
                schema: "dbo",
                table: "ControlSampleResults",
                column: "ParticipantResultId",
                principalSchema: "dbo",
                principalTable: "ParticipantResults",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ControlSamples_ControlSampleTypes_ControlSampleTypeId",
                schema: "dbo",
                table: "ControlSamples",
                column: "ControlSampleTypeId",
                principalSchema: "dbo",
                principalTable: "ControlSampleTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ExperimentResultAttributes_BenchmarkResults_BenchmarkResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes",
                column: "BenchmarkResultId",
                principalSchema: "dbo",
                principalTable: "BenchmarkResults",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ExperimentResultAttributes_ControlSampleResults_ControlSampleResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes",
                column: "ControlSampleResultId",
                principalSchema: "dbo",
                principalTable: "ControlSampleResults",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ExperimentResultAttributes_TestSampleResults_TestSampleResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes",
                column: "TestSampleResultId",
                principalSchema: "dbo",
                principalTable: "TestSampleResults",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Experiments_BusinessLogicUsers_RequesterId",
                schema: "dbo",
                table: "Experiments",
                column: "RequesterId",
                principalSchema: "dbo",
                principalTable: "BusinessLogicUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Experiments_ControlSamples_ControlSampleId",
                schema: "dbo",
                table: "Experiments",
                column: "ControlSampleId",
                principalSchema: "dbo",
                principalTable: "ControlSamples",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Experiments_ExperimentStatuses_ExperimentStatusId",
                schema: "dbo",
                table: "Experiments",
                column: "ExperimentStatusId",
                principalSchema: "dbo",
                principalTable: "ExperimentStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Experiments_Participants_ParticipantId",
                schema: "dbo",
                table: "Experiments",
                column: "ParticipantId",
                principalSchema: "dbo",
                principalTable: "Participants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Experiments_SamplePreparations_SamplePreparationId",
                schema: "dbo",
                table: "Experiments",
                column: "SamplePreparationId",
                principalSchema: "dbo",
                principalTable: "SamplePreparations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Experiments_Teams_TeamId",
                schema: "dbo",
                table: "Experiments",
                column: "TeamId",
                principalSchema: "dbo",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ExperimentsParticipants_Experiments_ExperimentId",
                schema: "dbo",
                table: "ExperimentsParticipants",
                column: "ExperimentId",
                principalSchema: "dbo",
                principalTable: "Experiments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ExperimentsParticipants_Participants_ParticipantId",
                schema: "dbo",
                table: "ExperimentsParticipants",
                column: "ParticipantId",
                principalSchema: "dbo",
                principalTable: "Participants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ExperimentStatuses_MultyLanguageTexts_NameId",
                schema: "dbo",
                table: "ExperimentStatuses",
                column: "NameId",
                principalSchema: "dbo",
                principalTable: "MultyLanguageTexts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_LanguageText_MultyLanguageTexts_MultyLanguageTextId",
                schema: "dbo",
                table: "LanguageText",
                column: "MultyLanguageTextId",
                principalSchema: "dbo",
                principalTable: "MultyLanguageTexts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ParticipantResults_Experiments_ExperimentId",
                schema: "dbo",
                table: "ParticipantResults",
                column: "ExperimentId",
                principalSchema: "dbo",
                principalTable: "Experiments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ParticipantResults_Participants_ParticipantId",
                schema: "dbo",
                table: "ParticipantResults",
                column: "ParticipantId",
                principalSchema: "dbo",
                principalTable: "Participants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Permissions_MultyLanguageTexts_NameId",
                schema: "dbo",
                table: "Permissions",
                column: "NameId",
                principalSchema: "dbo",
                principalTable: "MultyLanguageTexts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ScoringScaleDefinitions_MultyLanguageTexts_TextId",
                schema: "dbo",
                table: "ScoringScaleDefinitions",
                column: "TextId",
                principalSchema: "dbo",
                principalTable: "MultyLanguageTexts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_StaticTexts_MultyLanguageTexts_TextId",
                schema: "dbo",
                table: "StaticTexts",
                column: "TextId",
                principalSchema: "dbo",
                principalTable: "MultyLanguageTexts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TestSampleResults_ParticipantResults_ParticipantResultId",
                schema: "dbo",
                table: "TestSampleResults",
                column: "ParticipantResultId",
                principalSchema: "dbo",
                principalTable: "ParticipantResults",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TestSampleResults_TestSamples_TestSampleId",
                schema: "dbo",
                table: "TestSampleResults",
                column: "TestSampleId",
                principalSchema: "dbo",
                principalTable: "TestSamples",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TestSamples_Experiments_ExperimentId",
                schema: "dbo",
                table: "TestSamples",
                column: "ExperimentId",
                principalSchema: "dbo",
                principalTable: "Experiments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TestTypes_MultyLanguageTexts_TextId",
                schema: "dbo",
                table: "TestTypes",
                column: "TextId",
                principalSchema: "dbo",
                principalTable: "MultyLanguageTexts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_BusinessLogicUsers_BusinessLogicDataId",
                schema: "dbo",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_BenchmarkResults_Benchmarks_BenchmarkId",
                schema: "dbo",
                table: "BenchmarkResults");

            migrationBuilder.DropForeignKey(
                name: "FK_BenchmarkResults_ParticipantResults_ParticipantResultId",
                schema: "dbo",
                table: "BenchmarkResults");

            migrationBuilder.DropForeignKey(
                name: "FK_Benchmarks_Experiments_ExperimentId",
                schema: "dbo",
                table: "Benchmarks");

            migrationBuilder.DropForeignKey(
                name: "FK_BusinessLogicUsers_Permissions_PermissionId",
                schema: "dbo",
                table: "BusinessLogicUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_BusinessLogicUsers_Teams_TeamId",
                schema: "dbo",
                table: "BusinessLogicUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_ControlSampleResults_ControlSamples_ControlSampleId",
                schema: "dbo",
                table: "ControlSampleResults");

            migrationBuilder.DropForeignKey(
                name: "FK_ControlSampleResults_ParticipantResults_ParticipantResultId",
                schema: "dbo",
                table: "ControlSampleResults");

            migrationBuilder.DropForeignKey(
                name: "FK_ControlSamples_ControlSampleTypes_ControlSampleTypeId",
                schema: "dbo",
                table: "ControlSamples");

            migrationBuilder.DropForeignKey(
                name: "FK_ExperimentResultAttributes_BenchmarkResults_BenchmarkResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes");

            migrationBuilder.DropForeignKey(
                name: "FK_ExperimentResultAttributes_ControlSampleResults_ControlSampleResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes");

            migrationBuilder.DropForeignKey(
                name: "FK_ExperimentResultAttributes_TestSampleResults_TestSampleResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes");

            migrationBuilder.DropForeignKey(
                name: "FK_Experiments_BusinessLogicUsers_RequesterId",
                schema: "dbo",
                table: "Experiments");

            migrationBuilder.DropForeignKey(
                name: "FK_Experiments_ControlSamples_ControlSampleId",
                schema: "dbo",
                table: "Experiments");

            migrationBuilder.DropForeignKey(
                name: "FK_Experiments_ExperimentStatuses_ExperimentStatusId",
                schema: "dbo",
                table: "Experiments");

            migrationBuilder.DropForeignKey(
                name: "FK_Experiments_Participants_ParticipantId",
                schema: "dbo",
                table: "Experiments");

            migrationBuilder.DropForeignKey(
                name: "FK_Experiments_SamplePreparations_SamplePreparationId",
                schema: "dbo",
                table: "Experiments");

            migrationBuilder.DropForeignKey(
                name: "FK_Experiments_Teams_TeamId",
                schema: "dbo",
                table: "Experiments");

            migrationBuilder.DropForeignKey(
                name: "FK_ExperimentsParticipants_Experiments_ExperimentId",
                schema: "dbo",
                table: "ExperimentsParticipants");

            migrationBuilder.DropForeignKey(
                name: "FK_ExperimentsParticipants_Participants_ParticipantId",
                schema: "dbo",
                table: "ExperimentsParticipants");

            migrationBuilder.DropForeignKey(
                name: "FK_ExperimentStatuses_MultyLanguageTexts_NameId",
                schema: "dbo",
                table: "ExperimentStatuses");

            migrationBuilder.DropForeignKey(
                name: "FK_LanguageText_MultyLanguageTexts_MultyLanguageTextId",
                schema: "dbo",
                table: "LanguageText");

            migrationBuilder.DropForeignKey(
                name: "FK_ParticipantResults_Experiments_ExperimentId",
                schema: "dbo",
                table: "ParticipantResults");

            migrationBuilder.DropForeignKey(
                name: "FK_ParticipantResults_Participants_ParticipantId",
                schema: "dbo",
                table: "ParticipantResults");

            migrationBuilder.DropForeignKey(
                name: "FK_Permissions_MultyLanguageTexts_NameId",
                schema: "dbo",
                table: "Permissions");

            migrationBuilder.DropForeignKey(
                name: "FK_ScoringScaleDefinitions_MultyLanguageTexts_TextId",
                schema: "dbo",
                table: "ScoringScaleDefinitions");

            migrationBuilder.DropForeignKey(
                name: "FK_StaticTexts_MultyLanguageTexts_TextId",
                schema: "dbo",
                table: "StaticTexts");

            migrationBuilder.DropForeignKey(
                name: "FK_TestSampleResults_ParticipantResults_ParticipantResultId",
                schema: "dbo",
                table: "TestSampleResults");

            migrationBuilder.DropForeignKey(
                name: "FK_TestSampleResults_TestSamples_TestSampleId",
                schema: "dbo",
                table: "TestSampleResults");

            migrationBuilder.DropForeignKey(
                name: "FK_TestSamples_Experiments_ExperimentId",
                schema: "dbo",
                table: "TestSamples");

            migrationBuilder.DropForeignKey(
                name: "FK_TestTypes_MultyLanguageTexts_TextId",
                schema: "dbo",
                table: "TestTypes");

            migrationBuilder.DropIndex(
                name: "IX_ExperimentResultAttributes_ControlSampleResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes");

            migrationBuilder.DropColumn(
                name: "ControlSampleResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes");

            migrationBuilder.DropColumn(
                name: "BuildUpTime",
                schema: "dbo",
                table: "ControlSampleResults");

            migrationBuilder.DropColumn(
                name: "Date",
                schema: "dbo",
                table: "ControlSampleResults");

            migrationBuilder.DropColumn(
                name: "FirstDetectionTime",
                schema: "dbo",
                table: "ControlSampleResults");

            migrationBuilder.DropColumn(
                name: "FullDetectionTime",
                schema: "dbo",
                table: "ControlSampleResults");

            migrationBuilder.DropColumn(
                name: "IsSubmitted",
                schema: "dbo",
                table: "ControlSampleResults");

            migrationBuilder.DropColumn(
                name: "Order",
                schema: "dbo",
                table: "ControlSampleResults");

            migrationBuilder.DropColumn(
                name: "SampleIndex",
                schema: "dbo",
                table: "ControlSampleResults");

            migrationBuilder.DropColumn(
                name: "Sensation",
                schema: "dbo",
                table: "ControlSampleResults");

            migrationBuilder.RenameColumn(
                name: "Intensity",
                schema: "dbo",
                table: "ControlSampleResults",
                newName: "NewIntensity");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_BusinessLogicUsers_BusinessLogicDataId",
                schema: "dbo",
                table: "AspNetUsers",
                column: "BusinessLogicDataId",
                principalSchema: "dbo",
                principalTable: "BusinessLogicUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BenchmarkResults_Benchmarks_BenchmarkId",
                schema: "dbo",
                table: "BenchmarkResults",
                column: "BenchmarkId",
                principalSchema: "dbo",
                principalTable: "Benchmarks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BenchmarkResults_ParticipantResults_ParticipantResultId",
                schema: "dbo",
                table: "BenchmarkResults",
                column: "ParticipantResultId",
                principalSchema: "dbo",
                principalTable: "ParticipantResults",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Benchmarks_Experiments_ExperimentId",
                schema: "dbo",
                table: "Benchmarks",
                column: "ExperimentId",
                principalSchema: "dbo",
                principalTable: "Experiments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BusinessLogicUsers_Permissions_PermissionId",
                schema: "dbo",
                table: "BusinessLogicUsers",
                column: "PermissionId",
                principalSchema: "dbo",
                principalTable: "Permissions",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_BusinessLogicUsers_Teams_TeamId",
                schema: "dbo",
                table: "BusinessLogicUsers",
                column: "TeamId",
                principalSchema: "dbo",
                principalTable: "Teams",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ControlSampleResults_ControlSamples_ControlSampleId",
                schema: "dbo",
                table: "ControlSampleResults",
                column: "ControlSampleId",
                principalSchema: "dbo",
                principalTable: "ControlSamples",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ControlSampleResults_ParticipantResults_ParticipantResultId",
                schema: "dbo",
                table: "ControlSampleResults",
                column: "ParticipantResultId",
                principalSchema: "dbo",
                principalTable: "ParticipantResults",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ControlSamples_ControlSampleTypes_ControlSampleTypeId",
                schema: "dbo",
                table: "ControlSamples",
                column: "ControlSampleTypeId",
                principalSchema: "dbo",
                principalTable: "ControlSampleTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ExperimentResultAttributes_BenchmarkResults_BenchmarkResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes",
                column: "BenchmarkResultId",
                principalSchema: "dbo",
                principalTable: "BenchmarkResults",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ExperimentResultAttributes_TestSampleResults_TestSampleResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes",
                column: "TestSampleResultId",
                principalSchema: "dbo",
                principalTable: "TestSampleResults",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Experiments_BusinessLogicUsers_RequesterId",
                schema: "dbo",
                table: "Experiments",
                column: "RequesterId",
                principalSchema: "dbo",
                principalTable: "BusinessLogicUsers",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Experiments_ControlSamples_ControlSampleId",
                schema: "dbo",
                table: "Experiments",
                column: "ControlSampleId",
                principalSchema: "dbo",
                principalTable: "ControlSamples",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Experiments_ExperimentStatuses_ExperimentStatusId",
                schema: "dbo",
                table: "Experiments",
                column: "ExperimentStatusId",
                principalSchema: "dbo",
                principalTable: "ExperimentStatuses",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Experiments_Participants_ParticipantId",
                schema: "dbo",
                table: "Experiments",
                column: "ParticipantId",
                principalSchema: "dbo",
                principalTable: "Participants",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Experiments_SamplePreparations_SamplePreparationId",
                schema: "dbo",
                table: "Experiments",
                column: "SamplePreparationId",
                principalSchema: "dbo",
                principalTable: "SamplePreparations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Experiments_Teams_TeamId",
                schema: "dbo",
                table: "Experiments",
                column: "TeamId",
                principalSchema: "dbo",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ExperimentsParticipants_Experiments_ExperimentId",
                schema: "dbo",
                table: "ExperimentsParticipants",
                column: "ExperimentId",
                principalSchema: "dbo",
                principalTable: "Experiments",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ExperimentsParticipants_Participants_ParticipantId",
                schema: "dbo",
                table: "ExperimentsParticipants",
                column: "ParticipantId",
                principalSchema: "dbo",
                principalTable: "Participants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ExperimentStatuses_MultyLanguageTexts_NameId",
                schema: "dbo",
                table: "ExperimentStatuses",
                column: "NameId",
                principalSchema: "dbo",
                principalTable: "MultyLanguageTexts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_LanguageText_MultyLanguageTexts_MultyLanguageTextId",
                schema: "dbo",
                table: "LanguageText",
                column: "MultyLanguageTextId",
                principalSchema: "dbo",
                principalTable: "MultyLanguageTexts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ParticipantResults_Experiments_ExperimentId",
                schema: "dbo",
                table: "ParticipantResults",
                column: "ExperimentId",
                principalSchema: "dbo",
                principalTable: "Experiments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ParticipantResults_Participants_ParticipantId",
                schema: "dbo",
                table: "ParticipantResults",
                column: "ParticipantId",
                principalSchema: "dbo",
                principalTable: "Participants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Permissions_MultyLanguageTexts_NameId",
                schema: "dbo",
                table: "Permissions",
                column: "NameId",
                principalSchema: "dbo",
                principalTable: "MultyLanguageTexts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ScoringScaleDefinitions_MultyLanguageTexts_TextId",
                schema: "dbo",
                table: "ScoringScaleDefinitions",
                column: "TextId",
                principalSchema: "dbo",
                principalTable: "MultyLanguageTexts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StaticTexts_MultyLanguageTexts_TextId",
                schema: "dbo",
                table: "StaticTexts",
                column: "TextId",
                principalSchema: "dbo",
                principalTable: "MultyLanguageTexts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TestSampleResults_ParticipantResults_ParticipantResultId",
                schema: "dbo",
                table: "TestSampleResults",
                column: "ParticipantResultId",
                principalSchema: "dbo",
                principalTable: "ParticipantResults",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TestSampleResults_TestSamples_TestSampleId",
                schema: "dbo",
                table: "TestSampleResults",
                column: "TestSampleId",
                principalSchema: "dbo",
                principalTable: "TestSamples",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TestSamples_Experiments_ExperimentId",
                schema: "dbo",
                table: "TestSamples",
                column: "ExperimentId",
                principalSchema: "dbo",
                principalTable: "Experiments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TestTypes_MultyLanguageTexts_TextId",
                schema: "dbo",
                table: "TestTypes",
                column: "TextId",
                principalSchema: "dbo",
                principalTable: "MultyLanguageTexts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
