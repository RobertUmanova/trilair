﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class ExperimentParticipantntonrelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Participants_Experiments_ExperimentId",
                table: "Participants");

            migrationBuilder.DropIndex(
                name: "IX_Participants_ExperimentId",
                table: "Participants");

            migrationBuilder.DropColumn(
                name: "ExperimentId",
                table: "Participants");

            migrationBuilder.CreateTable(
                name: "ExperimentParticipant",
                columns: table => new
                {
                    ExperimentsId = table.Column<int>(type: "int", nullable: false),
                    ParticipantsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExperimentParticipant", x => new { x.ExperimentsId, x.ParticipantsId });
                    table.ForeignKey(
                        name: "FK_ExperimentParticipant_Experiments_ExperimentsId",
                        column: x => x.ExperimentsId,
                        principalTable: "Experiments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExperimentParticipant_Participants_ParticipantsId",
                        column: x => x.ParticipantsId,
                        principalTable: "Participants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExperimentParticipant_ParticipantsId",
                table: "ExperimentParticipant",
                column: "ParticipantsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExperimentParticipant");

            migrationBuilder.AddColumn<int>(
                name: "ExperimentId",
                table: "Participants",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Participants_ExperimentId",
                table: "Participants",
                column: "ExperimentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Participants_Experiments_ExperimentId",
                table: "Participants",
                column: "ExperimentId",
                principalTable: "Experiments",
                principalColumn: "Id");
        }
    }
}
