﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class ExperimentBenchmarkAndTestSampleRefactor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BenchmarkResults_ExperimentBenchmarks_BenchmarkId",
                schema: "dbo",
                table: "BenchmarkResults");

            migrationBuilder.DropForeignKey(
                name: "FK_Benchmarks_ExperimentBenchmarks_ExperimentBenchmarkId",
                schema: "dbo",
                table: "Benchmarks");

            migrationBuilder.DropForeignKey(
                name: "FK_TestSampleResults_ExperimentTestSamples_TestSampleId",
                schema: "dbo",
                table: "TestSampleResults");

            migrationBuilder.DropForeignKey(
                name: "FK_TestSamples_ExperimentTestSamples_ExperimentTestSampleId",
                schema: "dbo",
                table: "TestSamples");

            migrationBuilder.DropTable(
                name: "ExperimentBenchmarks",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "ExperimentTestSamples",
                schema: "dbo");

            migrationBuilder.DropIndex(
                name: "IX_TestSamples_ExperimentTestSampleId",
                schema: "dbo",
                table: "TestSamples");

            migrationBuilder.DropIndex(
                name: "IX_Benchmarks_ExperimentBenchmarkId",
                schema: "dbo",
                table: "Benchmarks");

            migrationBuilder.DropColumn(
                name: "SampleCode",
                schema: "dbo",
                table: "TestSampleResults");

            migrationBuilder.DropColumn(
                name: "SampleName",
                schema: "dbo",
                table: "TestSampleResults");

            migrationBuilder.DropColumn(
                name: "SampleCode",
                schema: "dbo",
                table: "BenchmarkResults");

            migrationBuilder.DropColumn(
                name: "SampleName",
                schema: "dbo",
                table: "BenchmarkResults");

            migrationBuilder.RenameColumn(
                name: "ExperimentTestSampleId",
                schema: "dbo",
                table: "TestSamples",
                newName: "ExperimentId");

            migrationBuilder.RenameColumn(
                name: "ExperimentBenchmarkId",
                schema: "dbo",
                table: "Benchmarks",
                newName: "ExperimentId");

            migrationBuilder.CreateIndex(
                name: "IX_TestSamples_ExperimentId",
                schema: "dbo",
                table: "TestSamples",
                column: "ExperimentId");

            migrationBuilder.CreateIndex(
                name: "IX_Benchmarks_ExperimentId",
                schema: "dbo",
                table: "Benchmarks",
                column: "ExperimentId");

            migrationBuilder.AddForeignKey(
                name: "FK_BenchmarkResults_Benchmarks_BenchmarkId",
                schema: "dbo",
                table: "BenchmarkResults",
                column: "BenchmarkId",
                principalSchema: "dbo",
                principalTable: "Benchmarks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Benchmarks_Experiments_ExperimentId",
                schema: "dbo",
                table: "Benchmarks",
                column: "ExperimentId",
                principalSchema: "dbo",
                principalTable: "Experiments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TestSampleResults_TestSamples_TestSampleId",
                schema: "dbo",
                table: "TestSampleResults",
                column: "TestSampleId",
                principalSchema: "dbo",
                principalTable: "TestSamples",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TestSamples_Experiments_ExperimentId",
                schema: "dbo",
                table: "TestSamples",
                column: "ExperimentId",
                principalSchema: "dbo",
                principalTable: "Experiments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BenchmarkResults_Benchmarks_BenchmarkId",
                schema: "dbo",
                table: "BenchmarkResults");

            migrationBuilder.DropForeignKey(
                name: "FK_Benchmarks_Experiments_ExperimentId",
                schema: "dbo",
                table: "Benchmarks");

            migrationBuilder.DropForeignKey(
                name: "FK_TestSampleResults_TestSamples_TestSampleId",
                schema: "dbo",
                table: "TestSampleResults");

            migrationBuilder.DropForeignKey(
                name: "FK_TestSamples_Experiments_ExperimentId",
                schema: "dbo",
                table: "TestSamples");

            migrationBuilder.DropIndex(
                name: "IX_TestSamples_ExperimentId",
                schema: "dbo",
                table: "TestSamples");

            migrationBuilder.DropIndex(
                name: "IX_Benchmarks_ExperimentId",
                schema: "dbo",
                table: "Benchmarks");

            migrationBuilder.RenameColumn(
                name: "ExperimentId",
                schema: "dbo",
                table: "TestSamples",
                newName: "ExperimentTestSampleId");

            migrationBuilder.RenameColumn(
                name: "ExperimentId",
                schema: "dbo",
                table: "Benchmarks",
                newName: "ExperimentBenchmarkId");

            migrationBuilder.AddColumn<string>(
                name: "SampleCode",
                schema: "dbo",
                table: "TestSampleResults",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SampleName",
                schema: "dbo",
                table: "TestSampleResults",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SampleCode",
                schema: "dbo",
                table: "BenchmarkResults",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SampleName",
                schema: "dbo",
                table: "BenchmarkResults",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ExperimentBenchmarks",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    ExperimentId = table.Column<int>(type: "int", nullable: false),
                    ExperimentIndex = table.Column<int>(type: "int", nullable: false),
                    IsArchived = table.Column<bool>(type: "bit", nullable: false),
                    IsManuallyArchived = table.Column<bool>(type: "bit", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExperimentBenchmarks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExperimentBenchmarks_Experiments_ExperimentId",
                        column: x => x.ExperimentId,
                        principalSchema: "dbo",
                        principalTable: "Experiments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExperimentTestSamples",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    ExperimentId = table.Column<int>(type: "int", nullable: false),
                    ExperimentIndex = table.Column<int>(type: "int", nullable: false),
                    IsArchived = table.Column<bool>(type: "bit", nullable: false),
                    IsManuallyArchived = table.Column<bool>(type: "bit", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExperimentTestSamples", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExperimentTestSamples_Experiments_ExperimentId",
                        column: x => x.ExperimentId,
                        principalSchema: "dbo",
                        principalTable: "Experiments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TestSamples_ExperimentTestSampleId",
                schema: "dbo",
                table: "TestSamples",
                column: "ExperimentTestSampleId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Benchmarks_ExperimentBenchmarkId",
                schema: "dbo",
                table: "Benchmarks",
                column: "ExperimentBenchmarkId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ExperimentBenchmarks_ExperimentId",
                schema: "dbo",
                table: "ExperimentBenchmarks",
                column: "ExperimentId");

            migrationBuilder.CreateIndex(
                name: "IX_ExperimentTestSamples_ExperimentId",
                schema: "dbo",
                table: "ExperimentTestSamples",
                column: "ExperimentId");

            migrationBuilder.AddForeignKey(
                name: "FK_BenchmarkResults_ExperimentBenchmarks_BenchmarkId",
                schema: "dbo",
                table: "BenchmarkResults",
                column: "BenchmarkId",
                principalSchema: "dbo",
                principalTable: "ExperimentBenchmarks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Benchmarks_ExperimentBenchmarks_ExperimentBenchmarkId",
                schema: "dbo",
                table: "Benchmarks",
                column: "ExperimentBenchmarkId",
                principalSchema: "dbo",
                principalTable: "ExperimentBenchmarks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TestSampleResults_ExperimentTestSamples_TestSampleId",
                schema: "dbo",
                table: "TestSampleResults",
                column: "TestSampleId",
                principalSchema: "dbo",
                principalTable: "ExperimentTestSamples",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TestSamples_ExperimentTestSamples_ExperimentTestSampleId",
                schema: "dbo",
                table: "TestSamples",
                column: "ExperimentTestSampleId",
                principalSchema: "dbo",
                principalTable: "ExperimentTestSamples",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
