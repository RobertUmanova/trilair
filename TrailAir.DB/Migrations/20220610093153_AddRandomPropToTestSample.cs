﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class AddRandomPropToTestSample : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //procedure that would allow you to disable all the cascade delete 
            migrationBuilder.Sql(
@"CREATE PROCEDURE AdminRemoveCascade       
AS   
if OBJECT_ID('tempdb..#foreignkeys') is null
BEGIN
	CREATE TABLE #foreignkeys -- feel free to use a permanent table
	(
	  drop_script NVARCHAR(MAX),
	  create_script NVARCHAR(MAX)
	);
END

  
DECLARE @drop   NVARCHAR(MAX) = N'',
        @create NVARCHAR(MAX) = N'';

-- drop is easy, just build a simple concatenated list from sys.foreign_keys:
SELECT @drop += N'
ALTER TABLE ' + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name) 
    + ' DROP CONSTRAINT ' + QUOTENAME(fk.name) + ';'
FROM sys.foreign_keys AS fk
INNER JOIN sys.tables AS ct
  ON fk.parent_object_id = ct.[object_id]
INNER JOIN sys.schemas AS cs 
  ON ct.[schema_id] = cs.[schema_id]
WHERE ct.name NOT LIKE 'AspNet%'
AND ct.name NOT IN ( '__EFMigrationsHistory','AuthorizationTokens');

INSERT #foreignkeys(drop_script) SELECT @drop;

-- create is a little more complex. We need to generate the list of 
-- columns on both sides of the constraint, even though in most cases
-- there is only one column.
SELECT @create += N'
ALTER TABLE ' 
   + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name) 
   + ' ADD CONSTRAINT ' + QUOTENAME(fk.name) 
   + ' FOREIGN KEY (' + STUFF((SELECT ',' + QUOTENAME(c.name)
   -- get all the columns in the constraint table
    FROM sys.columns AS c 
    INNER JOIN sys.foreign_key_columns AS fkc 
    ON fkc.parent_column_id = c.column_id
    AND fkc.parent_object_id = c.[object_id]
    WHERE fkc.constraint_object_id = fk.[object_id]
    ORDER BY fkc.constraint_column_id 
    FOR XML PATH(N''), TYPE).value(N'.[1]', N'nvarchar(max)'), 1, 1, N'')
  + ') REFERENCES ' + QUOTENAME(rs.name) + '.' + QUOTENAME(rt.name)
  + '(' + STUFF((SELECT ',' + QUOTENAME(c.name)
   -- get all the referenced columns
    FROM sys.columns AS c 
    INNER JOIN sys.foreign_key_columns AS fkc 
    ON fkc.referenced_column_id = c.column_id
    AND fkc.referenced_object_id = c.[object_id]
    WHERE fkc.constraint_object_id = fk.[object_id]
    ORDER BY fkc.constraint_column_id 
    FOR XML PATH(N''), TYPE).value(N'.[1]', N'nvarchar(max)'), 1, 1, N'') + ')  ;'--
FROM sys.foreign_keys AS fk
INNER JOIN sys.tables AS rt -- referenced table
  ON fk.referenced_object_id = rt.[object_id]
INNER JOIN sys.schemas AS rs 
  ON rt.[schema_id] = rs.[schema_id]
INNER JOIN sys.tables AS ct -- constraint table
  ON fk.parent_object_id = ct.[object_id]
INNER JOIN sys.schemas AS cs 
  ON ct.[schema_id] = cs.[schema_id]
WHERE rt.is_ms_shipped = 0 AND ct.is_ms_shipped = 0
AND ct.name NOT LIKE 'AspNet%'
AND ct.name NOT IN ('__EFMigrationsHistory','AuthorizationTokens');
/*
UPDATE #foreignkeys SET create_script = @create;

--PRINT @drop;
PRINT @create;

PRINT LEN(@drop);
PRINT LEN(@create);
*/

EXEC sp_executesql @drop
-- clear out data etc. here
EXEC sp_executesql @create;
GO");


            //procedure that would allow you to delete all data for the experiments  
            migrationBuilder.Sql(
@"CREATE PROC DeleteAllExperimentsAndAllRelations
AS
BEGIN


--delete experiments results 

DELETE FROM ExperimentResultAttributes

DELETE FROM BenchmarkResults
DELETE FROM TestSampleResults
DELETE FROM ControlSampleResults

DELETE FROM ParticipantResults 

--delete experiment
DELETE FROM Benchmarks
DELETE FROM TestSamples
DELETE FROM SamplePreparations
DELETE FROM ExperimentsParticipants

--delete experiments
DELETE FROM Experiments

--delete table the experiment si linking to 
DELETE FROM ControlSamples
END
GO");
            // migrationBuilder.Sql("EXECUTE  [dbo].[AdminRemoveCascade]");
            //          EXECUTE @RC = [dbo].[AdminRemoveCascade]
            // @LastName
            //,@FirstName
            migrationBuilder.AddColumn<int>(
                name: "Random",
                schema: "dbo",
                table: "TestSamples",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ParticipantResults_ExperimentId",
                schema: "dbo",
                table: "ParticipantResults",
                column: "ExperimentId");

            migrationBuilder.CreateIndex(
                name: "IX_ParticipantResults_ParticipantId",
                schema: "dbo",
                table: "ParticipantResults",
                column: "ParticipantId");

            migrationBuilder.AddForeignKey(
                name: "FK_ParticipantResults_Experiments_ExperimentId",
                schema: "dbo",
                table: "ParticipantResults",
                column: "ExperimentId",
                principalSchema: "dbo",
                principalTable: "Experiments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ParticipantResults_Participants_ParticipantId",
                schema: "dbo",
                table: "ParticipantResults",
                column: "ParticipantId",
                principalSchema: "dbo",
                principalTable: "Participants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ParticipantResults_Experiments_ExperimentId",
                schema: "dbo",
                table: "ParticipantResults");

            migrationBuilder.DropForeignKey(
                name: "FK_ParticipantResults_Participants_ParticipantId",
                schema: "dbo",
                table: "ParticipantResults");

            migrationBuilder.DropIndex(
                name: "IX_ParticipantResults_ExperimentId",
                schema: "dbo",
                table: "ParticipantResults");

            migrationBuilder.DropIndex(
                name: "IX_ParticipantResults_ParticipantId",
                schema: "dbo",
                table: "ParticipantResults");

            migrationBuilder.DropColumn(
                name: "Random",
                schema: "dbo",
                table: "TestSamples");
        }
    }
}
