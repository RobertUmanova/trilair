﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class AddControlSampleandControlSampleType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ControlSample",
                table: "Experiments");

            migrationBuilder.AddColumn<int>(
                name: "ControlSampleId",
                table: "Experiments",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ControlSampleTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NameId = table.Column<int>(type: "int", nullable: false),
                    ControlSampleTypeCode = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    IsArchived = table.Column<bool>(type: "bit", nullable: false),
                    IsManuallyArchived = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ControlSampleTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ControlSampleTypes_MultyLanguageTexts_NameId",
                        column: x => x.NameId,
                        principalTable: "MultyLanguageTexts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ControlSamples",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ControlSampleTypeId = table.Column<int>(type: "int", nullable: false),
                    SampleCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SampleName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    IsArchived = table.Column<bool>(type: "bit", nullable: false),
                    IsManuallyArchived = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ControlSamples", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ControlSamples_ControlSampleTypes_ControlSampleTypeId",
                        column: x => x.ControlSampleTypeId,
                        principalTable: "ControlSampleTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Experiments_ControlSampleId",
                table: "Experiments",
                column: "ControlSampleId");

            migrationBuilder.CreateIndex(
                name: "IX_ControlSamples_ControlSampleTypeId",
                table: "ControlSamples",
                column: "ControlSampleTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ControlSampleTypes_NameId",
                table: "ControlSampleTypes",
                column: "NameId");

            migrationBuilder.AddForeignKey(
                name: "FK_Experiments_ControlSamples_ControlSampleId",
                table: "Experiments",
                column: "ControlSampleId",
                principalTable: "ControlSamples",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Experiments_ControlSamples_ControlSampleId",
                table: "Experiments");

            migrationBuilder.DropTable(
                name: "ControlSamples");

            migrationBuilder.DropTable(
                name: "ControlSampleTypes");

            migrationBuilder.DropIndex(
                name: "IX_Experiments_ControlSampleId",
                table: "Experiments");

            migrationBuilder.DropColumn(
                name: "ControlSampleId",
                table: "Experiments");

            migrationBuilder.AddColumn<bool>(
                name: "ControlSample",
                table: "Experiments",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
