﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class AddControlSampleResult : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ControlSampleResults",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ControlSampleId = table.Column<int>(type: "int", nullable: false),
                    ParticipantResultId = table.Column<int>(type: "int", nullable: false),
                    NewIntensity = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    IsArchived = table.Column<bool>(type: "bit", nullable: false),
                    IsManuallyArchived = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ControlSampleResults", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ControlSampleResults_ControlSamples_ControlSampleId",
                        column: x => x.ControlSampleId,
                        principalSchema: "dbo",
                        principalTable: "ControlSamples",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ControlSampleResults_ParticipantResults_ParticipantResultId",
                        column: x => x.ParticipantResultId,
                        principalSchema: "dbo",
                        principalTable: "ParticipantResults",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ControlSampleResults_ControlSampleId",
                schema: "dbo",
                table: "ControlSampleResults",
                column: "ControlSampleId");

            migrationBuilder.CreateIndex(
                name: "IX_ControlSampleResults_ParticipantResultId",
                schema: "dbo",
                table: "ControlSampleResults",
                column: "ParticipantResultId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ControlSampleResults",
                schema: "dbo");
        }
    }
}
