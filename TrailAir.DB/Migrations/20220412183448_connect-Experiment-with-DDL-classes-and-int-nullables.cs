﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class connectExperimentwithDDLclassesandintnullables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "TimeBetweenSamples",
                table: "Experiments",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "NumberOfSamples",
                table: "Experiments",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "NumberOfPanelists",
                table: "Experiments",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "ScoringScaleDefinitionId",
                table: "Experiments",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TestTypeId",
                table: "Experiments",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Experiments_ScoringScaleDefinitionId",
                table: "Experiments",
                column: "ScoringScaleDefinitionId");

            migrationBuilder.CreateIndex(
                name: "IX_Experiments_TestTypeId",
                table: "Experiments",
                column: "TestTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Experiments_ScoringScaleDefinitions_ScoringScaleDefinitionId",
                table: "Experiments",
                column: "ScoringScaleDefinitionId",
                principalTable: "ScoringScaleDefinitions",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Experiments_TestTypes_TestTypeId",
                table: "Experiments",
                column: "TestTypeId",
                principalTable: "TestTypes",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Experiments_ScoringScaleDefinitions_ScoringScaleDefinitionId",
                table: "Experiments");

            migrationBuilder.DropForeignKey(
                name: "FK_Experiments_TestTypes_TestTypeId",
                table: "Experiments");

            migrationBuilder.DropIndex(
                name: "IX_Experiments_ScoringScaleDefinitionId",
                table: "Experiments");

            migrationBuilder.DropIndex(
                name: "IX_Experiments_TestTypeId",
                table: "Experiments");

            migrationBuilder.DropColumn(
                name: "ScoringScaleDefinitionId",
                table: "Experiments");

            migrationBuilder.DropColumn(
                name: "TestTypeId",
                table: "Experiments");

            migrationBuilder.AlterColumn<int>(
                name: "TimeBetweenSamples",
                table: "Experiments",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "NumberOfSamples",
                table: "Experiments",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "NumberOfPanelists",
                table: "Experiments",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);
        }
    }
}
