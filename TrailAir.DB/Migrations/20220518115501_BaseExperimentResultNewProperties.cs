﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class BaseExperimentResultNewProperties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsSubmitted",
                schema: "dbo",
                table: "TestSampleResults",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                schema: "dbo",
                table: "TestSampleResults",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsSubmitted",
                schema: "dbo",
                table: "BenchmarkResults",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                schema: "dbo",
                table: "BenchmarkResults",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsSubmitted",
                schema: "dbo",
                table: "TestSampleResults");

            migrationBuilder.DropColumn(
                name: "Order",
                schema: "dbo",
                table: "TestSampleResults");

            migrationBuilder.DropColumn(
                name: "IsSubmitted",
                schema: "dbo",
                table: "BenchmarkResults");

            migrationBuilder.DropColumn(
                name: "Order",
                schema: "dbo",
                table: "BenchmarkResults");
        }
    }
}
