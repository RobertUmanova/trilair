﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class AddExperimentResultandbelongingclasses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ParticipantResults",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ExperimentId = table.Column<int>(type: "int", nullable: false),
                    CompletionDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ParticipantId = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    IsArchived = table.Column<bool>(type: "bit", nullable: false),
                    IsManuallyArchived = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParticipantResults", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BenchmarkResults",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BenchmarkId = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    IsArchived = table.Column<bool>(type: "bit", nullable: false),
                    IsManuallyArchived = table.Column<bool>(type: "bit", nullable: false),
                    SampleCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SampleName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FirstDetectionTime = table.Column<int>(type: "int", nullable: true),
                    BuildUpTime = table.Column<int>(type: "int", nullable: true),
                    FullDetectionTime = table.Column<int>(type: "int", nullable: true),
                    Intensity = table.Column<int>(type: "int", nullable: true),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Sensation = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ParticipantResultId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BenchmarkResults", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BenchmarkResults_ExperimentBenchmarks_BenchmarkId",
                        column: x => x.BenchmarkId,
                        principalSchema: "dbo",
                        principalTable: "ExperimentBenchmarks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BenchmarkResults_ParticipantResults_ParticipantResultId",
                        column: x => x.ParticipantResultId,
                        principalSchema: "dbo",
                        principalTable: "ParticipantResults",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TestSampleResults",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TestSampleId = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    IsArchived = table.Column<bool>(type: "bit", nullable: false),
                    IsManuallyArchived = table.Column<bool>(type: "bit", nullable: false),
                    SampleCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SampleName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FirstDetectionTime = table.Column<int>(type: "int", nullable: true),
                    BuildUpTime = table.Column<int>(type: "int", nullable: true),
                    FullDetectionTime = table.Column<int>(type: "int", nullable: true),
                    Intensity = table.Column<int>(type: "int", nullable: true),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Sensation = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ParticipantResultId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TestSampleResults", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TestSampleResults_ExperimentTestSamples_TestSampleId",
                        column: x => x.TestSampleId,
                        principalSchema: "dbo",
                        principalTable: "ExperimentTestSamples",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TestSampleResults_ParticipantResults_ParticipantResultId",
                        column: x => x.ParticipantResultId,
                        principalSchema: "dbo",
                        principalTable: "ParticipantResults",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExperimentResultAttributes",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AttributeText = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BenchmarkResultId = table.Column<int>(type: "int", nullable: true),
                    TestSampleResultId = table.Column<int>(type: "int", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    IsArchived = table.Column<bool>(type: "bit", nullable: false),
                    IsManuallyArchived = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExperimentResultAttributes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExperimentResultAttributes_BenchmarkResults_BenchmarkResultId",
                        column: x => x.BenchmarkResultId,
                        principalSchema: "dbo",
                        principalTable: "BenchmarkResults",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ExperimentResultAttributes_TestSampleResults_TestSampleResultId",
                        column: x => x.TestSampleResultId,
                        principalSchema: "dbo",
                        principalTable: "TestSampleResults",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_BenchmarkResults_BenchmarkId",
                schema: "dbo",
                table: "BenchmarkResults",
                column: "BenchmarkId");

            migrationBuilder.CreateIndex(
                name: "IX_BenchmarkResults_ParticipantResultId",
                schema: "dbo",
                table: "BenchmarkResults",
                column: "ParticipantResultId");

            migrationBuilder.CreateIndex(
                name: "IX_ExperimentResultAttributes_BenchmarkResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes",
                column: "BenchmarkResultId");

            migrationBuilder.CreateIndex(
                name: "IX_ExperimentResultAttributes_TestSampleResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes",
                column: "TestSampleResultId");

            migrationBuilder.CreateIndex(
                name: "IX_TestSampleResults_ParticipantResultId",
                schema: "dbo",
                table: "TestSampleResults",
                column: "ParticipantResultId");

            migrationBuilder.CreateIndex(
                name: "IX_TestSampleResults_TestSampleId",
                schema: "dbo",
                table: "TestSampleResults",
                column: "TestSampleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExperimentResultAttributes",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "BenchmarkResults",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "TestSampleResults",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "ParticipantResults",
                schema: "dbo");
        }
    }
}
