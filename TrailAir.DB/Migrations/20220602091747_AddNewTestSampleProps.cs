﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class AddNewTestSampleProps : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ApplicationDate",
                schema: "dbo",
                table: "TestSamples",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Base",
                schema: "dbo",
                table: "TestSamples",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Concentration",
                schema: "dbo",
                table: "TestSamples",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "FantasyName",
                schema: "dbo",
                table: "TestSamples",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GroupNumber",
                schema: "dbo",
                table: "TestSamples",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InternalName",
                schema: "dbo",
                table: "TestSamples",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsBenchmark",
                schema: "dbo",
                table: "TestSamples",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsInternal",
                schema: "dbo",
                table: "TestSamples",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "LOTCode",
                schema: "dbo",
                table: "TestSamples",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ManufacturedDate",
                schema: "dbo",
                table: "TestSamples",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "ProductName",
                schema: "dbo",
                table: "TestSamples",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Technology",
                schema: "dbo",
                table: "TestSamples",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TrialNumber",
                schema: "dbo",
                table: "TestSamples",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApplicationDate",
                schema: "dbo",
                table: "TestSamples");

            migrationBuilder.DropColumn(
                name: "Base",
                schema: "dbo",
                table: "TestSamples");

            migrationBuilder.DropColumn(
                name: "Concentration",
                schema: "dbo",
                table: "TestSamples");

            migrationBuilder.DropColumn(
                name: "FantasyName",
                schema: "dbo",
                table: "TestSamples");

            migrationBuilder.DropColumn(
                name: "GroupNumber",
                schema: "dbo",
                table: "TestSamples");

            migrationBuilder.DropColumn(
                name: "InternalName",
                schema: "dbo",
                table: "TestSamples");

            migrationBuilder.DropColumn(
                name: "IsBenchmark",
                schema: "dbo",
                table: "TestSamples");

            migrationBuilder.DropColumn(
                name: "IsInternal",
                schema: "dbo",
                table: "TestSamples");

            migrationBuilder.DropColumn(
                name: "LOTCode",
                schema: "dbo",
                table: "TestSamples");

            migrationBuilder.DropColumn(
                name: "ManufacturedDate",
                schema: "dbo",
                table: "TestSamples");

            migrationBuilder.DropColumn(
                name: "ProductName",
                schema: "dbo",
                table: "TestSamples");

            migrationBuilder.DropColumn(
                name: "Technology",
                schema: "dbo",
                table: "TestSamples");

            migrationBuilder.DropColumn(
                name: "TrialNumber",
                schema: "dbo",
                table: "TestSamples");
        }
    }
}
