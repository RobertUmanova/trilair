﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class Benchmarksfixtypo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExperimentBenchmarks_Bechmarks_BenchmarkId",
                table: "ExperimentBenchmarks");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Bechmarks",
                table: "Bechmarks");

            migrationBuilder.RenameTable(
                name: "Bechmarks",
                newName: "Benchmarks");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Benchmarks",
                table: "Benchmarks",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ExperimentBenchmarks_Benchmarks_BenchmarkId",
                table: "ExperimentBenchmarks",
                column: "BenchmarkId",
                principalTable: "Benchmarks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExperimentBenchmarks_Benchmarks_BenchmarkId",
                table: "ExperimentBenchmarks");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Benchmarks",
                table: "Benchmarks");

            migrationBuilder.RenameTable(
                name: "Benchmarks",
                newName: "Bechmarks");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Bechmarks",
                table: "Bechmarks",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ExperimentBenchmarks_Bechmarks_BenchmarkId",
                table: "ExperimentBenchmarks",
                column: "BenchmarkId",
                principalTable: "Bechmarks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
