﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class AttributesNullableForeignKeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExperimentResultAttributes_BenchmarkResults_BenchmarkResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes");

            migrationBuilder.DropForeignKey(
                name: "FK_ExperimentResultAttributes_TestSampleResults_TestSampleResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes");

            migrationBuilder.AlterColumn<int>(
                name: "TestSampleResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "BenchmarkResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_ExperimentResultAttributes_BenchmarkResults_BenchmarkResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes",
                column: "BenchmarkResultId",
                principalSchema: "dbo",
                principalTable: "BenchmarkResults",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ExperimentResultAttributes_TestSampleResults_TestSampleResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes",
                column: "TestSampleResultId",
                principalSchema: "dbo",
                principalTable: "TestSampleResults",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExperimentResultAttributes_BenchmarkResults_BenchmarkResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes");

            migrationBuilder.DropForeignKey(
                name: "FK_ExperimentResultAttributes_TestSampleResults_TestSampleResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes");

            migrationBuilder.AlterColumn<int>(
                name: "TestSampleResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "BenchmarkResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ExperimentResultAttributes_BenchmarkResults_BenchmarkResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes",
                column: "BenchmarkResultId",
                principalSchema: "dbo",
                principalTable: "BenchmarkResults",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ExperimentResultAttributes_TestSampleResults_TestSampleResultId",
                schema: "dbo",
                table: "ExperimentResultAttributes",
                column: "TestSampleResultId",
                principalSchema: "dbo",
                principalTable: "TestSampleResults",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
