﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class ControlSampleAddExperiment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Experiments_ControlSampleId",
                table: "Experiments");

            migrationBuilder.CreateIndex(
                name: "IX_Experiments_ControlSampleId",
                table: "Experiments",
                column: "ControlSampleId",
                unique: true,
                filter: "[ControlSampleId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Experiments_ControlSampleId",
                table: "Experiments");

            migrationBuilder.CreateIndex(
                name: "IX_Experiments_ControlSampleId",
                table: "Experiments",
                column: "ControlSampleId");
        }
    }
}
