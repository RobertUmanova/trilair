﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class ExperimentAddSamplePreparation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SamplePreparationId",
                table: "Experiments",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "SamplePreparations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PerfumeConcentration = table.Column<int>(type: "int", nullable: false),
                    EvaporingTime = table.Column<int>(type: "int", nullable: false),
                    EvaporingTemperature = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    IsArchived = table.Column<bool>(type: "bit", nullable: false),
                    IsManuallyArchived = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SamplePreparations", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Experiments_SamplePreparationId",
                table: "Experiments",
                column: "SamplePreparationId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Experiments_SamplePreparations_SamplePreparationId",
                table: "Experiments",
                column: "SamplePreparationId",
                principalTable: "SamplePreparations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Experiments_SamplePreparations_SamplePreparationId",
                table: "Experiments");

            migrationBuilder.DropTable(
                name: "SamplePreparations");

            migrationBuilder.DropIndex(
                name: "IX_Experiments_SamplePreparationId",
                table: "Experiments");

            migrationBuilder.DropColumn(
                name: "SamplePreparationId",
                table: "Experiments");
        }
    }
}
