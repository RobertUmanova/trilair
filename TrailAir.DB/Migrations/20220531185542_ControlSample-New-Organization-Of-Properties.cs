﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class ControlSampleNewOrganizationOfProperties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ControlSampleTypes_MultyLanguageTexts_NameId",
                schema: "dbo",
                table: "ControlSampleTypes");

            migrationBuilder.DropIndex(
                name: "IX_ControlSampleTypes_NameId",
                schema: "dbo",
                table: "ControlSampleTypes");

            migrationBuilder.DropColumn(
                name: "IntensityScore",
                schema: "dbo",
                table: "ControlSamples");

            migrationBuilder.RenameColumn(
                name: "NameId",
                schema: "dbo",
                table: "ControlSampleTypes",
                newName: "IntensityScore");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                schema: "dbo",
                table: "ControlSampleTypes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Base",
                schema: "dbo",
                table: "ControlSamples",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LOTCode",
                schema: "dbo",
                table: "ControlSamples",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ManufacturedDate",
                schema: "dbo",
                table: "ControlSamples",
                type: "datetime2",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                schema: "dbo",
                table: "ControlSampleTypes");

            migrationBuilder.DropColumn(
                name: "Base",
                schema: "dbo",
                table: "ControlSamples");

            migrationBuilder.DropColumn(
                name: "LOTCode",
                schema: "dbo",
                table: "ControlSamples");

            migrationBuilder.DropColumn(
                name: "ManufacturedDate",
                schema: "dbo",
                table: "ControlSamples");

            migrationBuilder.RenameColumn(
                name: "IntensityScore",
                schema: "dbo",
                table: "ControlSampleTypes",
                newName: "NameId");

            migrationBuilder.AddColumn<int>(
                name: "IntensityScore",
                schema: "dbo",
                table: "ControlSamples",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ControlSampleTypes_NameId",
                schema: "dbo",
                table: "ControlSampleTypes",
                column: "NameId");

            migrationBuilder.AddForeignKey(
                name: "FK_ControlSampleTypes_MultyLanguageTexts_NameId",
                schema: "dbo",
                table: "ControlSampleTypes",
                column: "NameId",
                principalSchema: "dbo",
                principalTable: "MultyLanguageTexts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
