﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class ExperimentRequesterisnowAppUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Experiments_BusinessLogicUsers_RequesterId",
                schema: "dbo",
                table: "Experiments");

            migrationBuilder.AlterColumn<string>(
                name: "RequesterId",
                schema: "dbo",
                table: "Experiments",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Experiments_AspNetUsers_RequesterId",
                schema: "dbo",
                table: "Experiments",
                column: "RequesterId",
                principalSchema: "dbo",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Experiments_AspNetUsers_RequesterId",
                schema: "dbo",
                table: "Experiments");

            migrationBuilder.AlterColumn<int>(
                name: "RequesterId",
                schema: "dbo",
                table: "Experiments",
                type: "int",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Experiments_BusinessLogicUsers_RequesterId",
                schema: "dbo",
                table: "Experiments",
                column: "RequesterId",
                principalSchema: "dbo",
                principalTable: "BusinessLogicUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
