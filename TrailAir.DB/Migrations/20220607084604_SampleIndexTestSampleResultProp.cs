﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TrailAir.DB.Migrations
{
    public partial class SampleIndexTestSampleResultProp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SampleIndex",
                schema: "dbo",
                table: "TestSampleResults",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SampleIndex",
                schema: "dbo",
                table: "BenchmarkResults",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SampleIndex",
                schema: "dbo",
                table: "TestSampleResults");

            migrationBuilder.DropColumn(
                name: "SampleIndex",
                schema: "dbo",
                table: "BenchmarkResults");
        }
    }
}
