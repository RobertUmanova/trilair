﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrailAir.DB.Models.Base
{
    [Serializable]
    public abstract class BaseBoObject
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Display(Name = "Created at")]
        public DateTime? CreatedAt { get; set; }

        [Display(Name = "Updated at")]
        public DateTime? UpdatedAt { get; set; }

        [StringLength(50)]
        [Display(Name = "Created by")]
        public string? CreatedBy { get; set; }

        [StringLength(50)]
        [Display(Name = "Updated by")]
        public string? UpdatedBy { get; set; }

        /// <summary>
        /// Property IsArchived is used to set object to archived,  --see Swissco project usecase
        /// </summary>
        public bool IsArchived { get; set; }

        /// <summary>
        /// Property IsManuallyArchived is used to keep the object archived in case: <br/>
        /// if parent object contains 3 children and we archive one child, if we archive and later unarchive the parent we need to keep that child still archived! --see Swissco project usecase
        /// </summary>
        public bool IsManuallyArchived { get; set; }

    }
}
