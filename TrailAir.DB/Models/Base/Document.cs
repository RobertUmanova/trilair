﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrailAir.DB.Models.Base
{
    [Serializable]
    public class Document : BaseBoObject
    {
        [StringLength(255)]
        public string Name { get; set; }
        public byte[] Data { get; set; }
        [StringLength(255)]
        public string Type { get; set; }
        public int DataLength { get; set; }
        [NotMapped]
        public string GetSize
        {
            get
            {
                if (Data.Length > 0)
                {
                    var size = (Data.Length / 1024f) / 1024f;
                    return (Math.Round((double)size, 3)).ToString() + "(MB)";
                }

                return "";

            }
        }
        [NotMapped]
        public string GetDataSrc
        {
            get
            {
                if (Data.Length > 0)
                {

                    return string.Format("data:{0};base64,{1}", Type, Convert.ToBase64String(Data));
                }

                return "";

            }
        }
        [NotMapped]
        public float GetSizeDropzone
        {
            get
            {
                if (Data.Length > 0)
                {
                    var size = (Data.Length / 1024f) / 1024f;
                    return size * 1000;
                }

                return 0;

            }
        }
        public bool? IsCompressed { get; set; }
        public byte[] Thumbnail { get; set; }
        public string VideoFileName { get; set; }
    }
}
