﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrailAir.DB.Models.Base
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
            if (BusinessLogicDataId == 0)
                this.BusinessLogicData = new BusinessLogicUser();
        }
        /// <summary>
        /// We will use this as Id because ApplicationUser has a GUID for Id and we don't like it.
        /// This is a safe thing to do because every ApplicationUsers has only one BusinessLogicUser and 
        /// they are always connected ONE-TO-ONE
        /// </summary>
        public int BusinessLogicDataId { get; set; }
        public BusinessLogicUser BusinessLogicData { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [NotMapped]
        public string Fullname
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
    }
}
