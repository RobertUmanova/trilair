﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrailAir.DB.Models.Base
{
    public class RequestInfo : BaseBoObject
    {
        [Column(TypeName = "nvarchar")]
        [StringLength(50)]
        public string Controller { get; set; }
        [Column(TypeName = "nvarchar")]
        [StringLength(50)]
        public string Action { get; set; }
        [Column(TypeName = "nvarchar")]
        [StringLength(250)]
        public string User { get; set; }
        [Column(TypeName = "nvarchar")]
        [StringLength(400)]
        public string QueryString { get; set; }
        [Column(TypeName = "nvarchar")]
        [StringLength(4000)]
        public string Form { get; set; }
        /// <summary>
        /// We use it to write huge text inside in case if you want to limit it go around the code to limit information that is written inside
        /// </summary>
        [Column(TypeName = "nvarchar(MAX)")]
        public string AngularData { get; set; }
        [Column(TypeName = "nvarchar")]
        [StringLength(250)]
        public string Browser { get; set; }
        [Column(TypeName = "nvarchar(MAX)")]
        public string RequestUrl { get; set; }
    }
}
