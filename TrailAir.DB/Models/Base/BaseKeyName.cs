﻿using System.ComponentModel.DataAnnotations;

namespace TrailAir.DB.Models.Base
{
    [Serializable]
    public class BaseKeyName : BaseBoObject
    {
        [StringLength(255)]
        public string Name { get; set; }
        [StringLength(50)]
        public virtual string Key { get; set; }

    }
}
