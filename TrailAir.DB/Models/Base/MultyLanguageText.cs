﻿
using TrailAir.DB.DataManagersCache;

namespace TrailAir.DB.Models.Base
{
    [Serializable]
    public class MultyLanguageText : BaseBoObject
    {
        public virtual ICollection<LanguageText> Texts { get; set; }

        public override string ToString()
        {
            if (Id == 0)
                return null;

            return LanguageCache.GetCurrentLangugaeText(Id);
        }

        public static implicit operator MultyLanguageText(string d)
        {
            return d.ToString();
        }

        public static implicit operator string(MultyLanguageText d)
        {
            throw new Exception("Wrong usage of the multilanguage text. Should never be created for single string. For Now :)");
            //return new MultyLanguageText();
        }
    }
}
