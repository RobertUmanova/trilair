﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TrailAir.DB.DataManagersCache;

namespace TrailAir.DB.Models.Base
{
    [Serializable]
    public class BaseMultiLangNameObject : BaseBoObject
    {
        [Required]
        public int MultyLanguageTextId { get; set; }
        [ForeignKey("MultyLanguageTextId")]
        public MultyLanguageText MultyLangName { get; set; }

        [NotMapped]
        public string Name
        {
            get
            {
                return LanguageCache.GetCurrentLangugaeText(MultyLanguageTextId);
            }
        }
    }
}
