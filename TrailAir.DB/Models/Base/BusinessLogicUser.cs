﻿
using System.ComponentModel.DataAnnotations.Schema;

namespace TrailAir.DB.Models.Base
{
    [Serializable]
    public class BusinessLogicUser : BaseBoObject
    {
        public int? PermissionId { get; set; }
        public Permission Permission { get; set; }
        public int? TeamId { get; set; }
        public Team Team { get; set; }

        [NotMapped]
        public string Fullname { get; set; }

        // in case a user can be part of 1 or more teams
        //public ICollection<TeamBusinessLogicUser>? TeamsBusinessLogicUsers { get; set; }
    }
}
