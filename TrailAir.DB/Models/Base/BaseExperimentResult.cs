﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrailAir.DB.Models.Base
{
    public class BaseExperimentResult : BaseBoObject
    {
        public int? FirstDetectionTime { get; set; }
        public int? BuildUpTime { get; set; }
        public int? FullDetectionTime { get; set; }
        public decimal? Intensity { get; set; }
        public DateTime? Date { get; set; }
        public List<ExperimentResultAttribute> Attributes { get; set; }
        /// <summary>
        /// The description
        /// </summary>
        public string Sensation { get; set; }
        public int ParticipantResultId { get; set; }
        public bool IsSubmitted { get; set; }
        public int? Order { get; set; }
        public int SampleIndex { get; set; }
    }
}
