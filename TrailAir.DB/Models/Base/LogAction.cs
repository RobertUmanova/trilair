﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrailAir.DB.Models.Base
{
    public class LogAction : BaseBoObject
    {
        [Column(TypeName = "nvarchar")]
        [StringLength(50)]
        public string ActionGroup { get; set; }
        [Column(TypeName = "nvarchar")]
        [StringLength(50)]
        public string ActionSubgroup { get; set; }
        [Column(TypeName = "nvarchar")]
        [StringLength(50)]
        public string Action { get; set; }
        [Column(TypeName = "nvarchar")]
        [StringLength(250)]
        public string User { get; set; }
        [Column(TypeName = "nvarchar")]
        [StringLength(250)]
        public string QueryString { get; set; }
        [Column(TypeName = "nvarchar")]
        [StringLength(4000)]
        public string Form { get; set; }
        [Column(TypeName = "nvarchar(MAX)")]
        public string JsonData { get; set; }
        public int? ObjectId { get; set; }
        [Column(TypeName = "nvarchar")]
        [StringLength(250)]
        public string Browser { get; set; }
        [Column(TypeName = "nvarchar(MAX)")]
        public string RequestUrl { get; set; }
    }
}
