﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrailAir.DB.Models.Base
{
    [Index(nameof(Key))]
    [Serializable]
    public class LanguageText : BaseKeyName
    {
        public string Description { get; set; }

        [StringLength(50)]
        [Column("LanguageKey")]
        public override string Key { get; set; }
        [Required]
        public int MultyLanguageTextId { get; set; }
        [ForeignKey("MultyLanguageTextId")]
        public MultyLanguageText MultyLanguageText { get; set; }
    }
}
