﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.Models.Base;

namespace TrailAir.DB.Models
{
    /// <summary>
    ///  DEPRECATED PROBABLY TO DELETE SOON (date of sentence 2022-06-01)
    ///  Don't delete code for now just leave it there 
    /// </summary>
    public class BenchmarkResult : BaseExperimentResult
    {
        public int BenchmarkId { get; set; }
        public Benchmark Benchmark { get; set; }
    }
}
