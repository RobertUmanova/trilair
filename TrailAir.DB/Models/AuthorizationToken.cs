﻿using System.ComponentModel.DataAnnotations;
using TrailAir.DB.Models.Base;

namespace TrailAir.DB.Models
{
    public class AuthorizationToken : BaseBoObject
    {
        [Required]
        public string Token { get; set; }

        [Required]
        public DateTime ExpiryDate { get; set; }

        public bool IsUsed { get; set; }
        
        [Required]
        [EmailAddress]
        public string Email { get; set; }

    }
}
