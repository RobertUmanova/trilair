﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.Models.Base;

namespace TrailAir.DB.Models
{
    [Serializable]
    public class Participant : BaseBoObject
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public ICollection<Experiment> Experiments { get; set; }
        [NotMapped]
        public string Fullname
        {
            get
            {
                return Name + " " + Surname;
            }
        }
    }
}
