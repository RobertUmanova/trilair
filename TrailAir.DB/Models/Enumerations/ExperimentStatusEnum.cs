﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrailAir.DB.Models.Enumerations
{
    public enum ExperimentStatusEnum
    {
        Draft = 1,
        InProgress = 2,
        Finished = 3
    }
}
