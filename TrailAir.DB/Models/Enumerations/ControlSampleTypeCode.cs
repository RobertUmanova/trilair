﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrailAir.DB.Models.Enumerations
{
    public enum ControlSampleTypeCode
    {
        No = 1,
        CocoMademoiselle = 2,
        TerreDHermes = 3,
        Others = 4
    }
}
