﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrailAir.DB.Models.Enumerations
{
    public enum PermissionCode
    {
        ITAdministrator = 1,
        SuperUser = 2,
        Assistant = 3,
        Perfumer = 4,
        PanelAdmin = 5,
        Panelist = 6
    }
}
