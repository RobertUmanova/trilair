﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrailAir.DB.Models.Enumerations
{
    public enum BaseExperimentResultType
    {
        BenchmarkResult = 1,
        TestSampleResult = 2,
        ControlSample =3
    }
}
