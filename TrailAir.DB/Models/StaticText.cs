﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TrailAir.DB.DataManagersCache;
using TrailAir.DB.Models.Base;

namespace TrailAir.DB.Models
{
    [Index(propertyNames: "Key", IsUnique = true, Name = "StaticTxt_Index")]
    public class StaticText : BaseBoObject
    {
        public int TextId { get; set; }
        public MultyLanguageText Text { get; set; }

        [NotMapped]
        public string ActiveLangText
        {
            get
            {
                return LanguageCache.GetCurrentLangugaeText(TextId);
            }
        }

        [StringLength(2000)]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [StringLength(50)]
        [Display(Name = "Key")]
        public string Key { get; set; }
    }
}
