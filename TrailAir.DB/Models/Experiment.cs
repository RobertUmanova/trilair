﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.Models.Base;

namespace TrailAir.DB.Models
{
    [Serializable]
    public class Experiment : BaseBoObject
    {
        public string Name { get; set; }

        [StringLength(2000)]
        [Display(Name = "Instruction")]
        public string Instruction { get; set; }
        public int? NumberOfSamples { get; set; }

        /// <summary>
        /// This is unused and will be replaced with non-mapped property with getter only
        /// </summary>
        public int? NumberOfParticipants { get; set; } 
        public int? ControlSampleId { get; set; }
        public ControlSample ControlSample { get; set; }
        public int? ExperimentStatusId { get; set; }
        public ExperimentStatus ExperimentStatus { get; set; }
        public DateTime? DatePublished { get; set; }
        public int TeamId { get; set; }
        public Team Team { get; set; }
        public string RequesterId { get; set; }
        public ApplicationUser Requester { get; set; }
        /// <summary>
        /// Client
        /// </summary>
        public string Consumer { get; set; }
        public int SamplePreparationId { get; set; }
        public SamplePreparation SamplePreparation { get; set; }
        public List<ExperimentsParticipant> Participants { get; set; }

        [NotMapped]
        public string NumberOfParticipantsString { 
            get 
            {
                if (Participants != null && Participants.Count > 0)
                {

                }

                return "0/0";
            }
        }
        /// <summary>
        ///  DEPRECATED PROBABLY TO DELETE SOON (date of sentence 2022-06-01)
        ///  Don't delete code for now just leave it there 
        /// </summary>
        public List<Benchmark> ExperimentBenchmarks { get; set; }
        public List<TestSample> ExperimentTestSamples { get; set; }
        public string Comment { get; set; }
        public DateTime? DateFinished { get; set; }
    }
}
