﻿using System.ComponentModel.DataAnnotations.Schema;
using TrailAir.DB.Models.Base;

namespace TrailAir.DB.Models
{
    public class ParticipantResult : BaseBoObject
    {
        public int ExperimentId { get; set; }
        public Experiment Experiment { get; set; }
        public DateTime? CompletionDate { get; set; }
        public string ParticipantId { get; set; }
        public ApplicationUser Participant { get; set; }
        public ControlSampleResult ControlSampleResult { get; set; }
        public List<BenchmarkResult> BenchmarkResults { get; set; }
        public List<TestSampleResult> TestSampleResults { get; set; }
        public bool IsSubmitted { get; set; }
    }
}

