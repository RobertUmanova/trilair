﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.Models.Base;

namespace TrailAir.DB.Models
{
    [Serializable]
    public class SamplePreparation : BaseBoObject
    {
        public int? PerfumeConcentration { get; set; }
        public int? EvaporingTime { get; set; }
        public int? EvaporingTemperature { get; set; }

        public Experiment Experiment { get; set; }
    }
}
