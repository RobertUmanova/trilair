﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.Models.Base;

namespace TrailAir.DB.Models
{
    /// <summary>
    ///  DEPRECATED PROBABLY TO DELETE SOON (date of sentence 2022-06-01)
    ///  Don't delete code for now just leave it there 
    /// </summary>
    [Serializable]
    [Table("Benchmarks", Schema = "dbo")]
    public class Benchmark : BaseBoObject
    {
        public string SampleCode { get; set; }
        public string SampleName { get; set; }
        public int ExperimentId { get; set; }
    }
}
