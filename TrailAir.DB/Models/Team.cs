﻿using System.ComponentModel.DataAnnotations;
using TrailAir.DB.Models.Base;

namespace TrailAir.DB.Models
{
    [Serializable]
    public class Team : BaseBoObject
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<BusinessLogicUser> Users { get; set; }

        // in case a user can be part of 1 or more teams
        //public ICollection<TeamBusinessLogicUser>? TeamsBusinessLogicUsers { get; set; }
    }
}
