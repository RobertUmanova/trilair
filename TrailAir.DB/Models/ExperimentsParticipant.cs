﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.Models.Base;

namespace TrailAir.DB.Models
{
    [Serializable]
    [Table("ExperimentsParticipants", Schema = "dbo")]
    public class ExperimentsParticipant : BaseBoObject
    {
        public int ExperimentIndex { get; set; }
        public int? ExperimentId { get; set; }
        public string ParticipantId { get; set; }
        public ApplicationUser Participant { get; set; }
    }
}
