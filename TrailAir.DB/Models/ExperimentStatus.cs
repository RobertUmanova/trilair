﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.DataManagersCache;
using TrailAir.DB.Models.Base;
using TrailAir.DB.Models.Enumerations;

namespace TrailAir.DB.Models
{
    [Serializable]
    public class ExperimentStatus : BaseBoObject
    {
        public int NameId { get; set; }
        public MultyLanguageText Name { get; set; }
        public ExperimentStatusEnum ExperimentStatusCode { get; set; }

        [NotMapped]
        public string ActiveLangName
        {
            get
            {
                return LanguageCache.GetCurrentLangugaeText(NameId);
            }
        }
    }
}
