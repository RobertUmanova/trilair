﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.DataManagersCache;
using TrailAir.DB.Models.Base;

namespace TrailAir.DB.Models
{
    public class ScoringScaleDefinition : BaseBoObject
    {
        public int TextId { get; set; }
        public MultyLanguageText Text { get; set; }
        public string Description { get; set; }

        [NotMapped]
        public string ActiveLangText
        {
            get
            {
                return LanguageCache.GetCurrentLangugaeText(TextId);
            }
        }
    }
}
