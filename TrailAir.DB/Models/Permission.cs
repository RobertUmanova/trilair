﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.DataManagersCache;
using TrailAir.DB.Models.Base;
using TrailAir.DB.Models.Enumerations;

namespace TrailAir.DB.Models
{
    public class Permission : BaseBoObject
    {
        public int NameId { get; set; }
        public MultyLanguageText Name { get; set; }
        public PermissionCode PermissionCode { get; set; }

        [NotMapped]
        public string ActiveLangName
        {
            get
            {
                return LanguageCache.GetCurrentLangugaeText(NameId);
            }
        }
        //public MultyLanguageText Description { get; set; }
        public ICollection<BusinessLogicUser> BusinessLogicUsers { get; set; }

    }
}
