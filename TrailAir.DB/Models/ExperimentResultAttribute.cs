﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.Models.Base;

namespace TrailAir.DB.Models
{
    public class ExperimentResultAttribute : BaseBoObject
    {
        public string AttributeText { get; set; }
        public int? BenchmarkResultId { get; set; }
        public int? TestSampleResultId { get; set; }
    }
}
