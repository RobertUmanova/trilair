﻿using TrailAir.DB.Models.Base;

namespace TrailAir.DB.Models
{
    public class TeamBusinessLogicUser: BaseBoObject
    {
        public int BusinessLogicUserId { get; set; }
        public BusinessLogicUser BusinessLogicUser { get; set; }

        public int TeamId { get; set; }
        public Team Team { get; set; }
    }
}
