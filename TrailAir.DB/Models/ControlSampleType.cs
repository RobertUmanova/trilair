﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.DataManagersCache;
using TrailAir.DB.Models.Base;
using TrailAir.DB.Models.Enumerations;

namespace TrailAir.DB.Models
{
    [Serializable]
    [Table("ControlSampleTypes", Schema = "dbo")]
    public class ControlSampleType : BaseBoObject
    {
        public string Name { get; set; }
        public ControlSampleTypeCode ControlSampleTypeCode { get; set; }
        public int IntensityScore { get; set; }

        [NotMapped]
        public string FullText
        {
            get
            {
                return String.Concat(Name, ". Reference intensity: ", IntensityScore);
            }
        }
    }
}
