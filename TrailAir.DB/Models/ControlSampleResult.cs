﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.DataManagersCache;
using TrailAir.DB.Models.Base;
using TrailAir.DB.Models.Enumerations;

namespace TrailAir.DB.Models
{
    [Serializable]
    public class ControlSampleResult : BaseExperimentResult
    {
        public int ControlSampleId { get; set; }
        public ControlSample ControlSample { get; set; }
    }
}
