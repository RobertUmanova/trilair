﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.DataManagersCache;
using TrailAir.DB.Models.Base;
using TrailAir.DB.Models.Enumerations;

namespace TrailAir.DB.Models
{
    [Serializable]
    [Table("ControlSamples", Schema = "dbo")]
    public class ControlSample : BaseBoObject
    {

        public int ControlSampleTypeId { get; set; }
        public ControlSampleType ControlSampleType { get; set; }
        public string SampleCode { get; set; }
        public string SampleName { get; set; }
        public Experiment Experiment { get; set; }
        public string LOTCode { get; set; }
        public DateTime? ManufacturedDate { get; set; }
        public string Base { get; set; }
        [NotMapped]
        public string FullText {
            get 
            {
                if (ControlSampleType == null)
                    throw new Exception($"To load property FullText of  ControlSample id:{Id}. Property ControlSampleType not loaded");

                var lotCode = String.IsNullOrEmpty(LOTCode) ? string.Empty : $" (LOT: {LOTCode})";
                return $"{ControlSampleType.FullText}{lotCode}";
            } 
        }
    }
}
