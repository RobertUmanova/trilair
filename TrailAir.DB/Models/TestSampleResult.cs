﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.Models.Base;

namespace TrailAir.DB.Models
{
    public class TestSampleResult : BaseExperimentResult
    {
        public int TestSampleId { get; set; }
        public TestSample TestSample { get; set; }
    }
}
