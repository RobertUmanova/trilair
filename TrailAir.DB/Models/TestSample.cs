﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.Models.Base;
using TrailAir.Utility;

namespace TrailAir.DB.Models
{
    /// <summary>
    /// This is now test sample that should be two things(Submission and Benchmark). 
    /// The only difference is that if it's benchmark the bool isBenchmark is true 
    /// (date of sentence 2022-06-01)
    /// </summary>
    [Serializable]
    [Table("TestSamples", Schema = "dbo")]
    public class TestSample : BaseBoObject
    {
        public string SampleCode { get; set; }
        public string SampleName { get; set; }
        public int ExperimentId { get; set; }
        public bool IsBenchmark { get; set; }

        /// <summary>
        /// True if internal (made internaly by Givaudan)
        /// False if market(external from competition) 
        /// </summary>
        public bool IsInternal { get; set; }
        public string ProductName { get; set; }
        public string InternalName { get; set; }
        public string FantasyName { get; set; }
        public string LOTCode { get; set; }
        public DateTime? ManufacturedDate { get; set; }
        public DateTime? ApplicationDate { get; set; }
        public string GroupNumber { get; set; }
        public string TrialNumber { get; set; }
        public string Technology { get; set; }
        public int? Concentration { get; set; }
        public string Base { get; set; }
        public int SampleIndex { get; set; }
        public int Random { get; set; }
        public string Comment { get; set; }

        /// <summary>
        /// This is the name that is shown on the client side for TestSample.
        /// </summary>
        [NotMapped]
        public string ComputedName
        {
            get
            {
                if (!IsInternal) {
                    var lotCode = !string.IsNullOrEmpty(LOTCode) ? $" - {LOTCode}" : "";
                    return $"Market: {ProductName}{lotCode} - " + BenchOrSub;
                } 
                else {
                    string name = !string.IsNullOrEmpty(InternalName) ? InternalName : FantasyName;
                    string trial = !string.IsNullOrEmpty(TrialNumber) ? $"- {TrialNumber} " : "";
                    string group = !string.IsNullOrEmpty(GroupNumber) ? $"- {GroupNumber} " : "";
                    string comm = !string.IsNullOrEmpty(Comment) ? $"- {StringHelper.TrimString(Comment)} " : "";
                    return $"Internal: {name} " + trial + group + comm + "- " + BenchOrSub;
                }
            }
        }

        [NotMapped]
        public string BenchOrSub
        {
            get
            {
                return IsBenchmark ? "Benchmark" : "Submission";            
            }
        }


        [NotMapped]
        public string InterExt
        {
            get
            {
                return IsInternal ? "Internal" : "Market";
            }
        }


        [NotMapped]
        public string ConcentrationStr
        {
            get
            {
                return Concentration==null ? string.Empty : $"{Concentration}%";
            }
        }
    }
}
