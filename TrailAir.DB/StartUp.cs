﻿using TrailAir.DB.Helper;

namespace TrailAir.DB
{
    public class StartUp
    {
        public static void InitilizeContext(string connectionString)
        {
            ConnectionHelper.ConnectionString = connectionString;
        }
    }
}
