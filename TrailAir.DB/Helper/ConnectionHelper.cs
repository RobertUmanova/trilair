﻿using Duende.IdentityServer.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrailAir.DB.Helper
{
    public class ConnectionHelper
    {
        private static string _ConnectionString = null;
        internal static string ConnectionString
        {
            private get
            {
                if (_ConnectionString == null)
                    throw new Exception("Controlled Umanova error. The DB porject start up should be always initialized by calling InitilizeContext at the application loading phase.");

                return _ConnectionString;
            }
            set
            {
                _ConnectionString = value;
            }
        }

        /// <summary>
        /// To Rease after usage!!!!
        /// .finally{
        ///     .Dispose();
        /// }
        /// </summary>
        /// <returns>db context object to be disposed</returns>
        public static TrailAirDbContext GetContextToRelease()
        {
            var optionsBuilder = new DbContextOptionsBuilder<TrailAirDbContext>();
            optionsBuilder.UseSqlServer(ConnectionString);

            IOptions<OperationalStoreOptions> storeOpt = Options.Create(new OperationalStoreOptions());

            return new TrailAirDbContext(optionsBuilder.Options, storeOpt);
        }
    }
}
