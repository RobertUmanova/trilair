﻿using Duende.IdentityServer.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using TrailAir.DB.DataManagersCache;
using TrailAir.DB.Models;
using TrailAir.DB.Models.Base;

namespace TrailAir.DB
{
    public class TrailAirDbContext : ApiAuthorizationDbContext<ApplicationUser>
    {
        public TrailAirDbContext(
            DbContextOptions options,
            IOptions<OperationalStoreOptions> operationalStoreOptions) : base(options, operationalStoreOptions)
        {
        }
        public DbSet<AuthorizationToken> AuthorizationTokens { get; set; }
        public DbSet<BusinessLogicUser> BusinessLogicUsers { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<LogAction> LogActions { get; set; }
        public DbSet<LanguageText> LanguageText { get; set; }
        public DbSet<MultyLanguageText> MultyLanguageTexts { get; internal set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<RequestInfo> RequestInfos { get; set; }
        public DbSet<StaticText> StaticTexts { get; set; }
        public DbSet<Team> Teams { get; set; }
        //public DbSet<TeamBusinessLogicUser> TeamsBusinessLogicUsers { get; set; }
        public DbSet<Experiment> Experiments { get; set; }
        public DbSet<TestType> TestTypes { get; set; }
        public DbSet<ScoringScaleDefinition> ScoringScaleDefinitions { get; set; }
        public DbSet<ExperimentStatus> ExperimentStatuses { get; set; }
        public DbSet<SamplePreparation> SamplePreparations { get; set; }
        public DbSet<Participant> Participants { get; set; }
        public DbSet<ExperimentsParticipant> ExperimentsParticipants { get; set; }
        public DbSet<ControlSample> ControlSamples { get; set; }
        public DbSet<ControlSampleType> ControlSampleTypes { get; set; }
        //public DbSet<Benchmark> ExperimentBenchmarks { get; set; }
        public DbSet<Benchmark> Benchmarks { get; set; }
        //public DbSet<TestSample> ExperimentTestSamples { get; set; }
        public DbSet<TestSample> TestSamples { get; set; }
        public DbSet<ParticipantResult> ParticipantResults { get; set; }
        public DbSet<ExperimentResultAttribute> ExperimentResultAttributes { get; set; }
        public DbSet<BenchmarkResult> BenchmarkResults { get; set; }
        public DbSet<TestSampleResult> TestSampleResults { get; set; }
        public DbSet<ControlSampleResult> ControlSampleResults { get; set; }


        public override int SaveChanges()
        {
            //throw new NotImplementedException("Call save change with user overload ");
            return SaveChanges("Automatic call");
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasDefaultSchema("dbo");

            var cascadeFKs = builder.Model.GetEntityTypes()
                .SelectMany(t => t.GetForeignKeys());
                //.Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var fk in cascadeFKs)
                fk.DeleteBehavior = DeleteBehavior.Restrict;


            base.OnModelCreating(builder);
        }

        private static bool CheckIfItImpactsMulitLanguage(object objectChaged)
        {
            if (objectChaged is LanguageText || (objectChaged is MultyLanguageText) || (objectChaged is StaticText))
            {
                return true;
            }
            return false;
        }

        public int SaveChanges(string userName)
        {
            int returnValue = 0;
            bool clearMultyLangCache = false;
            // this.ChangeTracker.DetectChanges();
            try
            {
                var deletedObjects = ChangeTracker.Entries()
                 .Where(p => p.State == EntityState.Deleted).Select(_ => _.Entity);


                List<LogAction> deletedActions = new List<LogAction>();
                foreach (var obToDelete in deletedObjects)
                {
                    var baseBoObject = obToDelete as BaseBoObject;

                    var logAction = new LogAction
                    {
                        ObjectId = baseBoObject?.Id ?? 0,
                        ActionGroup = "ContextSave",
                        ActionSubgroup = "ServerSide-Delete",
                        Action = obToDelete.GetType().Name,
                        User = userName,
                        JsonData = JsonConvert.SerializeObject(obToDelete)
                    };
                    deletedActions.Add(logAction);
                    clearMultyLangCache = clearMultyLangCache || CheckIfItImpactsMulitLanguage(obToDelete);
                }
                if (deletedActions.Count > 0)
                    this.LogActions.AddRange(deletedActions);

                //var addedAuditedEntitiesd = ChangeTracker.Entries().ToList();
                var addedAuditedEntities = ChangeTracker.Entries()
                   .Where(p => p.State == EntityState.Added)
                   .Select(p => p.Entity);


                var modifiedAuditedEntities = GetModifiedObjects();


                foreach (var added in addedAuditedEntities)
                {
                    UpdateObject(added, userName, true);
                    clearMultyLangCache = clearMultyLangCache || CheckIfItImpactsMulitLanguage(added);
                }

                foreach (var modified in modifiedAuditedEntities)
                {
                    UpdateObject(modified, userName, false);
                    clearMultyLangCache = clearMultyLangCache || CheckIfItImpactsMulitLanguage(modified);
                }

                returnValue = base.SaveChanges();
                if (clearMultyLangCache)
                {
                    StaticTextCache.ClearCache();
                    LanguageCache.ClearCache();
                }

            }
            catch (Exception ex)
            {
                // If there is error that some entities are already tracked, check DbContext.SaveChanges() method in Base Controllers,
                // also put breakpoint on modifiedAuditedEntities in this class, you might unecessarely add tracked entities to ChangeTracker

                // https://github.com/dotnet/efcore/issues/4434
                //throw ex;
                //if (ex is DbEntityValidationException)
                //{
                //exception that is thrown when deleting with cascading constraint
                //}

                throw ex;
            }

            return returnValue;

        }



        public IEnumerable<object> GetModifiedObjects()
        {
            return ChangeTracker.Entries()
                 .Where(p => p.State == EntityState.Modified)
                 .Select(p => p.Entity);
        }



        private void UpdateObject(object boObject, string user, bool isInsert)
        {
            BaseBoObject boBaseObject = boObject as BaseBoObject;
            if (boBaseObject == null)
                return;
            var a = boBaseObject.GetType();
            boBaseObject.UpdatedAt = DateTime.Now;
            boBaseObject.UpdatedBy = user;

            if (isInsert)
            {
                boBaseObject.CreatedAt = DateTime.Now;
                boBaseObject.CreatedBy = user;
            }
        }
    }
}
