using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using NLog;
using Sustainsys.Saml2.WebSso;
using System.Configuration;
using System.Globalization;
using TrailAir.DB;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.Managers;
using TrailAir.UI.Modelling.Managers.Language;
using TrailAir.Web.UI.Helpers;
using TrailAir.Web.UI.Helpers.Base.Exceptions;
using TrailAir.Web.UI.Helpers.Base.SecurityUsers;

var builder = WebApplication.CreateBuilder(args);


LogManager.LoadConfiguration(String.Concat(Directory.GetCurrentDirectory(), "/nlog.config"));

// Add services to the container.
var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
builder.Services.AddDbContext<TrailAirDbContext>(options =>
    options.UseSqlServer(connectionString));
TrailAir.UI.Modelling.StartUp.InitilizeModellingLayer(builder.Configuration);
StartUp.InitilizeContext(connectionString);

builder.Services.AddDbContext<TrailAirDbContext>(options =>
{
    options.UseSqlServer(connectionString);
    options.EnableSensitiveDataLogging();
});

builder.Services.AddDatabaseDeveloperPageExceptionFilter();

builder.Services.AddDefaultIdentity<ApplicationUser>(options =>
{
    options.SignIn.RequireConfirmedAccount = true;
    options.Password.RequireDigit = false;
    options.Password.RequireUppercase = false;
})
.AddEntityFrameworkStores<TrailAirDbContext>()
.AddUserManager<ApplicationUserManager>();

builder.Services.AddScoped<IUserClaimsPrincipalFactory<ApplicationUser>, ApplicationUserClaimsPrincipalFactory>();

builder.Services.AddScoped<AuthorizationTokenHelper>();
builder.Services.AddLocalizationService(builder.Configuration);

builder.Services.AddAuthentication(sharedOptions =>
{
    sharedOptions.DefaultScheme = Microsoft.AspNetCore.Authentication.Cookies.CookieAuthenticationDefaults.AuthenticationScheme;
    sharedOptions.DefaultSignInScheme = Microsoft.AspNetCore.Authentication.Cookies.CookieAuthenticationDefaults.AuthenticationScheme;
    sharedOptions.DefaultChallengeScheme = Sustainsys.Saml2.AspNetCore2.Saml2Defaults.Scheme;
})
    .AddSaml2(options =>
    {
        try
        {
            options.SPOptions = new Sustainsys.Saml2.Configuration.SPOptions()
            {
                AuthenticateRequestSigningBehavior = Sustainsys.Saml2.Configuration.SigningBehavior.Never,
                EntityId = new Sustainsys.Saml2.Metadata.EntityId(builder.Configuration.GetValue<string>("Saml2:EntityId")),
                MinIncomingSigningAlgorithm = builder.Configuration.GetValue<string>("Saml2:MinIncomingSigningAlgorithm"),
            };

            var idp = new Sustainsys.Saml2.IdentityProvider(
            new Sustainsys.Saml2.Metadata.EntityId(builder.Configuration.GetValue<string>("Saml2:IdpEntityId")), options.SPOptions)
            {
                Binding = Saml2BindingType.HttpPost,
                MetadataLocation = builder.Configuration.GetValue<string>("Saml2:IdpMetadata"),
                LoadMetadata = true
            };

            options.IdentityProviders.Add(idp);
        }
        catch (Exception ex)
        {
            File.AppendAllText("tmp.txt", ex.Message);
        }


    })
    .AddCookie();

builder.Services.AddControllersWithViews()
    .AddViewLocalization(LanguageViewLocationExpanderFormat.SubFolder)
    .AddDataAnnotationsLocalization();

builder.Services.AddRazorPages().AddRazorRuntimeCompilation();


builder.Services.ConfigureApplicationCookie(options =>
{
    options.Cookie.Name = ".AspNetCore.Identity.Application";
    options.ExpireTimeSpan = TimeSpan.FromHours(1); // If user is not active for 1 hour, the session will end
    options.SlidingExpiration = true;
});





// all below is a new .net 6 logic that is usually done in StartUp.cs method Configure in .net 5

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    app.UseMigrationsEndPoint();
}
else
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseMiddleware<ExceptionMiddleware>();

// old way in .net 5 to automatically update a database
//dbContext.Database.Migrate();

// new way in .net 6 to automatically update a database
using (var scope = app.Services.CreateScope())
{
    var db = scope.ServiceProvider.GetRequiredService<TrailAirDbContext>();
    db.Database.Migrate();
}

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseStaticFiles(new StaticFileOptions
{
    FileProvider = new PhysicalFileProvider(
           Path.Combine(builder.Environment.ContentRootPath, "Metadata")),
    RequestPath = "/StaticFiles"
});
app.UseStatusCodePages(async context =>
{
    string currentCulture = "";
    if (context.HttpContext.Request.Path.Value.Split('/').Length < 2)
    {
        currentCulture = "en-GB";
    }
    else
    {
        string LanguageGet = context.HttpContext.Request.Path.Value.Split('/')[1].ToLower();
        LocalizationJson locJson = new LocalizationJson();
        builder.Configuration.GetSection("Localization").Bind(locJson);
        if (locJson?.SupportedCultures != null)
        {
            currentCulture = locJson.SupportedCultures.Find(X => X.Code.ToLower() == LanguageGet) == null ? "en-GB" : locJson.SupportedCultures.Find(X => X.Code.ToLower() == LanguageGet).Code;
        }
        else currentCulture = "en-GB";
    }

    var redirectPath = "/" + currentCulture + "/Home/Error" + "?statusCode=" + context.HttpContext.Response.StatusCode;
    context.HttpContext.Response.Redirect(redirectPath);
});

app.UseRouting();

app.UseRequestLocalization();

app.UseAuthentication();
app.UseAuthorization();

//app.MapControllerRoute(
//    name: "default",
//    pattern: "{controller=Home}/{action=Index}/{id?}");

app.MapControllerRoute(
    name: "culture-route",
    pattern: "{culture=en-GB}/{controller=Home}/{action=Index}/{id?}");

app.MapRazorPages();

app.Run();



public class RsaPkCs1Sha256SignatureDescription : System.Security.Cryptography.SignatureDescription
{
    public RsaPkCs1Sha256SignatureDescription()
    {
        KeyAlgorithm = "System.Security.Cryptography.RSACryptoServiceProvider";
        DigestAlgorithm = "System.Security.Cryptography.SHA256Managed";
        FormatterAlgorithm = "System.Security.Cryptography.RSAPKCS1SignatureFormatter";
        DeformatterAlgorithm = "System.Security.Cryptography.RSAPKCS1SignatureDeformatter";
    }
    public override System.Security.Cryptography.AsymmetricSignatureDeformatter CreateDeformatter(System.Security.Cryptography.AsymmetricAlgorithm key)
    {
        var asymmetricSignatureDeformatter = (System.Security.Cryptography.AsymmetricSignatureDeformatter)System.Security.Cryptography.CryptoConfig.CreateFromName(DeformatterAlgorithm);
        asymmetricSignatureDeformatter.SetKey(key);
        asymmetricSignatureDeformatter.SetHashAlgorithm("SHA256");
        return asymmetricSignatureDeformatter;
    }
}
public class RsaPkCs1Sha1SignatureDescription : System.Security.Cryptography.SignatureDescription
{
    public RsaPkCs1Sha1SignatureDescription()
    {
        KeyAlgorithm = "System.Security.Cryptography.RSACryptoServiceProvider";
        DigestAlgorithm = "System.Security.Cryptography.SHA1Managed";
        FormatterAlgorithm = "System.Security.Cryptography.RSAPKCS1SignatureFormatter";
        DeformatterAlgorithm = "System.Security.Cryptography.RSAPKCS1SignatureDeformatter";
    }
    public override System.Security.Cryptography.AsymmetricSignatureDeformatter CreateDeformatter(System.Security.Cryptography.AsymmetricAlgorithm key)
    {
        var asymmetricSignatureDeformatter = (System.Security.Cryptography.AsymmetricSignatureDeformatter)System.Security.Cryptography.CryptoConfig.CreateFromName(DeformatterAlgorithm);
        asymmetricSignatureDeformatter.SetKey(key);
        asymmetricSignatureDeformatter.SetHashAlgorithm("SHA1");
        return asymmetricSignatureDeformatter;
    }
}
