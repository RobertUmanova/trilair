﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using TrailAir.DB.Models.Base;

namespace TrailAir.Web.UI.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class LogoutModel : PageModel
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger<LogoutModel> _logger;

        public LogoutModel(SignInManager<ApplicationUser> signInManager, ILogger<LogoutModel> logger)
        {
            _signInManager = signInManager;
            _logger = logger;
        }

        public void OnGet()
        {
        }

        public IActionResult OnPost(string returnUrl = null)
        {
            _logger.LogInformation("User logging out...");
            var authProps = new AuthenticationProperties
            {
                RedirectUri = Url.Action(nameof(Index), "Home", values: null, protocol: Request.Scheme)
            };
            // you need these two in order for Sustainsys.Saml2 to successfully sign out
            AddAuthenticationPropertiesClaim(authProps, "/SessionIndex");
            AddAuthenticationPropertiesClaim(authProps, "/LogoutNameIdentifier");

            var result = SignOut(authProps, CookieAuthenticationDefaults.AuthenticationScheme, Sustainsys.Saml2.AspNetCore2.Saml2Defaults.Scheme);
            _logger.LogInformation("User logged out.");
            return result;
            ///
            //await _signInManager.SignOutAsync();
            
            //_logger.LogInformation("User logged out.");
            if (returnUrl != null)
            {
                return LocalRedirect(returnUrl);
            }
            else
            {
                return RedirectToPage();
            }
        }

        private void AddAuthenticationPropertiesClaim(AuthenticationProperties authProps, string name)
        {
            string claimValue = GetClaimValue(name, out string claimName);
            if (!string.IsNullOrEmpty(claimValue))
                authProps.Items[claimName] = claimValue;
        }
        private string GetClaimValue(string name, out string fullName)
        {
            fullName = null;
            name = name.ToLowerInvariant();
            foreach (Claim claim in User.Claims)
            {
                if (claim.Type.ToLowerInvariant().Contains(name))
                {
                    fullName = claim.Type;
                    return claim.Value;
                }
            }
            return null;
        }
    }
}
