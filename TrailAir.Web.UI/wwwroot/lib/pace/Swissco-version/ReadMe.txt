﻿THe pace.min.custom.js is a modification of pace.min.js and the only difference is that in the array of trackingMethods in ajax object it is added the "POST" as element.
Before: ajax:{trackMethods:["GET"],trackWebSockets:!0,ignoreURLs:[]}}
After : ajax:{trackMethods:["GET","POST"],trackWebSockets:!0,ignoreURLs:[]}}