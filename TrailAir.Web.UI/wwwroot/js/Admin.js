﻿var app = new Vue({
    el: '#vue-app',
    data() {
        return {
            postManager: new PostManager(),
        }
    },
    mounted() { // We make API calls here
        var thisOb = this;

        thisOb.getData();
    },
    // So, for now you don't fetch any real data.
    // You just call GetNull to get the pace.js working!
    methods: {
        getData() {
            var thisOb = this;

            thisOb.postManager.get("/ApiUser/" + "GetNull", function (response) {
                if (response.data.success) {
                } else {
                }
            });
        }
    }
});