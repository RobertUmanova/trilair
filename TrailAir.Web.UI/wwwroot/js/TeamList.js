﻿var app = new Vue({
    el: '#vue-app',
    data() {
        return {
            searchString: '',
            conf : {
                defaultApi: "/ApiTeam/"
            },
            
            postManager : new PostManager(),
            teamList: [],

            //Pager code
            currentPage : 1,
            perPage : 10,

            //Search input & Filter code
            teamsFilter : "",

        }
    },
    mounted() { // We make API calls here
        var thisOb = this;

        thisOb.getData();
    },
    watch: { // Watches for changes in data

    },
    methods: {
        getData() {
            var thisOb = this;
            var url = thisOb.conf.defaultApi + "GetList?q=" + thisOb.searchString;

            // Get the data with the pager
            // You have to instance it every time you make a request because the "url" changes 
            // according to the filters!!
            var pager = new PagerHelper('teamsListPagerBottom', url,
                function (response) {

                    thisOb.teamList = response.data.data.items;
                },
                100, null, 'teamsListPagerTop');
            // 100 rows per page

            pager.getCurrentPage();

        },
        openEditPopup(teamId) {
            this.$refs.teamEditCmp.openModalPopUp(teamId);
        }
    }
});