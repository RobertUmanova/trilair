﻿var dateRegex = /^\d{4}-\d{2}-\d{2}(T\d{2}:\d{2}:\d{2})?.{0,5}$/;
function recurseObject(object) {
    var result = object;
    if (object != null) {
        result = angular.copy(object);
        for (var key in result) {
            var property = result[key];
            if (typeof property === 'object') {
                result[key] = recurseObject(property);
            } else if (typeof property === 'string' && dateRegex.test(property)) {
                result[key] = new Date(property);
            }
        }
    }
    return result;
}



function config($httpProvider) {
    $httpProvider.defaults.transformResponse = function (data) {
        try {
            var object;
            if (typeof data === 'object') {
                object = data;
            } else {
                object = JSON.parse(data);
            }
            return recurseObject(object);
        } catch (e) {
            return data;
        }
    };
}


