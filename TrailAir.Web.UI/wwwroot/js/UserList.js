﻿var patientId = patientId;
var clear = [];

var app = new Vue({
    el: '#vue-app',
    data() {
        return {
            searchString: '',
            conf: {
                defaultApi: "/ApiUser/"
            },
            postManager: new PostManager(),
            userList: [],
            IsUsernameTaken: false,
            //sendInviteModalHelper: new NgUserSendInviteModalHelper($scope, postManager),

            //Pager code
            currentPage: 1,
            perPage: 10,
            filteredItems: [],

            //Search input & Filter code
            usersFilter: "",

            //new NgUserEditHelperModal($scope, postManager, $scope.getData);
            //new NgUserCreateHelperModal($scope, postManager, $scope.getData);

            //create new company tooltip logic
            //new NgUserCreateCompanyCreateTooltipHelper($scope, postManager, null);

            // instantiate a multi select2  for companies of users 
            // data get and set in NgUserDetailModal 
            //slctFilterUserCompany: new Select2Helper("slctFilterUserCompany", onFilterChange.bind($scope)), // we do not want window binded per default
            //slctFilterUserCompany.SetMultipleValues(clear, slctFilterUserCompany),

            // instantiate a multi select2  for permissions of users 
            // data get and set in NgUserDetailModal
            //slctFilterUserPermission: new Select2Helper("slctFilterUserPermission", onFilterChange.bind($scope)), // we do not want window binded per default
            //slctFilterUserPermission.SetMultipleValues(clear, slctFilterUserPermission),

            // instantiate a multi select2  for work positions of users 
            // data get and set in NgUserDetailModal
            //slctFilterUserWorkPosition: new Select2Helper("slctFilterWorkPosition", onFilterChange.bind($scope)), // we do not want window binded per default
            //slctFilterUserWorkPosition.SetMultipleValues(clear, slctFilterUserWorkPosition),
        }
    },
    mounted() { // We make API calls here
        var thisOb = this;

        thisOb.getData();
    },
    watch: { // Watches for changes in data
       

    },
    methods: {
        getData () {
            var thisOb = this;

            //var url = thisOb.conf.defaultApi + "GetPatientsList?q=" + thisOb.searchString + "&patientStatusIds=" + thisOb.patientStatusIds + "&referentNurse=" + thisOb.referentNurseId;
            var url = thisOb.conf.defaultApi + "GetList?q=" + thisOb.searchString;

            // Get the data with the pager
            // You have to instance it every time you make a request because the "url" changes 
            // according to the filters!!
            var pager = new PagerHelper('usersListPagerBottom', url,
                function (response) {

                    thisOb.userList = response.data.data.items;
                },
                100, null, 'usersListPagerTop');
            // 100 rows per page

            pager.getCurrentPage();
        },
        submit ($event) {
            $event.preventDefault();
            var thisOb = this;

            if (IsUsernameTaken) return false;

            thisOb.userCreateHelperModal.saveBoDetails(thisOb.userCreateHelperModal, thisOb.ngUserDetWorkPositions);
        },
        IsUsernameAvailable () {
            var thisOb = thisOb;
            IsUsernameTaken = thisOb.userList.some(_ => _.email === thisOb.userCreateHelperModal.currentBo.username)
        },
        resetUsernameValidation () {
            angular.copy(false, IsUsernameTaken);
        },
        onFilterChange (event) {
            var thisOb = this;
            currentPage = 1;

            var isFilterForCompanySet = slctFilterUserCompany.GetSelectValue().length > 0;
            var filteringValuesForCompany = slctFilterUserCompany.GetSelectValue();

            var isFilterForPermissionSet = slctFilterUserPermission.GetSelectValue().length > 0;
            var filteringValuesForPermission = slctFilterUserPermission.GetSelectValue();

            var isFilterForWorkPositionSet = slctFilterUserWorkPosition.GetSelectValue().length > 0;
            var filteringValuesForWorkPosition = slctFilterUserWorkPosition.GetSelectValue();

            filteredItems = isFilterForCompanySet ? userList.filter((item) => {
                if (item.company) {
                    var companyId = item.company.id.toString();
                    if (filteringValuesForCompany.includes(companyId)) {
                        return item;
                    }
                }
            }) : userList;

            filteredItems = isFilterForPermissionSet ? filteredItems.filter((item) => {
                if (item.permission) {
                    var permissionId = item.permission.id.toString();
                    if (filteringValuesForPermission.includes(permissionId)) {
                        return item;
                    }
                }
            }) : filteredItems;

            filteredItems = isFilterForWorkPositionSet ? filteredItems.filter((item) => {
                var filteredItemWorkPositionsIds = item.workPositions.map(workPosition => (workPosition.id.toString()));
                let success = filteringValuesForWorkPosition.every((val) => filteredItemWorkPositionsIds.includes(val));
                if (success) {
                    return item;
                }
            }) : filteredItems;

            thisOb.CheckSearchFilter();
        },
        CheckSearchFilter (event) {
            var thisOb = this;

            filteredItems = filteredItems.filter((item) => {
                var filterTextLowerCase = usersFilter.toLowerCase();
                var itemWorkPositionsNamesLowerCase = item.workPositions.map(_ => _.name.toLowerCase());


                if (item.firstName?.toLowerCase().includes(filterTextLowerCase)) {
                    return item;
                }

                if (item.lastName?.toLowerCase().includes(filterTextLowerCase)) {
                    return item;
                }

                if (item.company?.name.toLowerCase().includes(filterTextLowerCase)) {
                    return item;
                }

                if (item.permission?.name.toLowerCase().includes(filterTextLowerCase)) {
                    return item;
                }

                if (itemWorkPositionsNamesLowerCase?.includes(filterTextLowerCase)) {
                    return item;
                }

            })
        },
        clearFilter (filter) {

            if (filter == "company") {

                // set select2 ng-model to [] is the must, otherwise clear filter is buggy 
                // and select2 is not cleared and we get errors like 'Error: [$rootScope:inprog] $apply already in progress'  !!!
                slctFilterUserCompanyModel = [];
                slctFilterUserCompany.SetValue([], slctFilterUserCompany);

            }
            if (filter == "permission") {
                // set select2 ng-model to [] is the must, otherwise clear filter is buggy
                // and select2 is not cleared and we get errors like 'Error: [$rootScope:inprog] $apply already in progress'  !!!
                slctFilterUserPermissionModel = [];
                slctFilterUserPermission.SetValue(null, slctFilterUserPermission);
            }
            if (filter == "workPosition") {
                // set select2 ng-model to [] is the must, otherwise clear filter is buggy
                // and select2 is not cleared and we get errors like 'Error: [$rootScope:inprog] $apply already in progress'  !!!
                slctFilterUserWorkPositionModel = [];
                slctFilterUserWorkPosition.SetValue(null, slctFilterUserWorkPosition);
            }
            onFilterChange();

        },
        openEditPopup(userId) {
            this.$refs.userEditCmp.openModalPopUp(userId);
        }

    }
});
