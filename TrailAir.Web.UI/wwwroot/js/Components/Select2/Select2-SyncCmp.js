﻿
// TODO: Check for LogActionHelper.js

// select2-sync is of course async but we call it lkike that because
// we first load the data outside the component and then create a select2 instance in watch/loadSelect2WithData

Vue.component('select2-sync', {
    template: '#Select2-SyncTemp',
    props: {
        elementId: {
            type: String,
            required: true, // Mandatory!
            value: ''
        },
        required: false,
        labelElementId: '',
        title: null,
        multiple: false,
        onChange: null,
        // CASE 1 (for multiple preselected data): So in the parent, first you make API call fetching the data and 
        // after the call is done you make API call fetching the preselected defaults.

        // TO CHECK FOR ASYNC SELECT!
        //preselectedData: []


        // CASE 2 (for the selected data from some object's property): In the parent you get the data and when the call is done, 
        // you call preselectValues function and inside it your Select 2 $ref and function setValue()
    },
    watch: { // Watches for changes in data
        //ddlData(newData) {
        //    this.loadSelect2WithData();
        //},
        // TO CHECK FOR ASYNC SELECT!

        //preselectedData(newVals, oldVals) {
        //    this.setMultipleValues(newVals);
        //}
    },
    data() {
        return {
            ddlData: [],
            jqueryDdlElement: null,
            select2Instance: null,
            listLoaded: false,
        };
    },
    mounted() {
        var thisOb = this;

        thisOb.jqueryDdlElement = $("#" + this.elementId);
        thisOb.defineMultipleSelect();
        thisOb.onChangeSelect2(this.jqueryDdlElement, this.onChange);
        thisOb.preventDefaultDeselectBehaviour('#' + this.elementId);

        thisOb.loadSelect2WithData();
    },
    methods: {
        defineMultipleSelect() {
            if (this.multiple) {
                $(this.jqueryDdlElement).attr('multiple', true);
            }
            else {
                $(this.jqueryDdlElement).removeAttr('multiple');
            }
        },
        onChangeSelect2(selectId, onSelect) {
            var thisOb = this;

            $(selectId).change(function () {
                if (onSelect)
                    onSelect(thisOb.getSelectValue());
            });
        },
        preventDefaultDeselectBehaviour(selectId) {
            // When you press 'x' to deselect selected object in select2 you open Select2 - this function prevents this
            if (selectId) {
                $(selectId).on('select2:unselecting', function (e) {
                    $(selectId).on('select2:opening', function (e) {
                        e.preventDefault();
                        $(selectId).off('select2:opening');
                    });
                });
            }
        },
        loadSelect2WithData() {
            var thisOb = this;
            var selectedValue = thisOb.getSelectValue();

            if (thisOb.select2Instance != null) {
                thisOb.jqueryDdlElement.select2('destroy');
                thisOb.jqueryDdlElement.children('option').remove();
            }
            thisOb.select2Instance = thisOb.jqueryDdlElement.select2({
                width: '100%',
                data: thisOb.ddlData,
                // dropdownParent part of code missing because it is used just in 
                // NgTourCalendarHelper. If you need it look in Select2Helper_S5.js
            });

            thisOb.listLoaded = true;
            if (selectedValue != null && selectedValue.length > 0) {
                thisOb.setValue(selectedValue);
            }
        },
        getSelectValue() {
            return this.jqueryDdlElement.val();
        },
        setValue(selectedValue, counter, reloadList) {
            var numberTimesCalled = counter || 0; numberTimesCalled += 1;
            var thisOb = this;
            var selVal = selectedValue;// selectedValue == null ? null : selectedValue + '';

            if (reloadList == true) {
                thisOb.listLoaded = false;
            }

            if ($("#" + thisOb.elementId + " option[value='" + selVal + "']").length != 0) {
                // the value already exists so can be selected
            }
            else if (selectedValue != null && (selectedValue + '').length > 0) {
                if (thisOb.listLoaded != true) {

                    if (numberTimesCalled > 50) {
                        alert("Error loading the dropdown list");
                        //LogActionHelper.saveLogAction(null, "SetValue", "UI-ValueNotFoundInDDL", thisOb.jqueryDdlElement.selector, selectedValue);
                        this.jqueryDdlElement.append($('<option>', { value: selVal, text: 'NA' }));
                    }
                    else {
                        setTimeout(function () {
                            thisOb.setValue(selectedValue, numberTimesCalled);
                        }, 200);
                        return;
                    }

                }
            }
            thisOb.jqueryDdlElement.val(selVal);
            thisOb.jqueryDdlElement.trigger('change.select2');
        },
        setDisabledSelect(trueOrFalse) {
            var thisOb = this;

            thisOb.jqueryDdlElement.prop("disabled", trueOrFalse).select2();
        },
        setMultipleValues(selectedValues, selectHlpObject) {
            var thisOb = selectHlpObject || this;

            var valsToSelect = selectedValues;
            if (!(selectedValues)) {
                return;
            }

            if (!Array.isArray(selectedValues)) {
                valsToSelect = valsToSelect.split(',');
            }

            thisOb.jqueryDdlElement.val(valsToSelect);
            thisOb.jqueryDdlElement.trigger('change.select2');
        }
    }
});


