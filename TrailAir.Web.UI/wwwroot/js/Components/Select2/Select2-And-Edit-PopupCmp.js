﻿
// TODO: Check for LogActionHelper.js

// select2-sync is of course async but we call it lkike that because
// we first load the data outside the component and then create a select2 instance in watch/loadSelect2WithData

Vue.component('select2-and-edit-popup', {
    template: '#Select2AndEditPopupTemp',
    props: {
        elementId: {
            type: String,
            required: true, // Mandatory!
            value: ''
        },
        labelElementId: {
            type: String,
            required: true, // Mandatory!
            value: ''
        },
        apiController: {
            type: String,
            required: true, // Mandatory!
            value: ''
        },
        ddlData: [],
        title: null,
        multiple: false,
        onChange: null,
        modalId: {
            type: String,
            required: true, // Mandatory!
            value: ''
        },
        disableEdit: false,
        onUpdateSuccessFunc: null
    },
    watch: { // Watches for changes in data

    },
    data() {
        return {
            postManager: new PostManager(),
            jqueryDdlLabelElement: null,
            jqueryDdlParentElement: null,
            tippy: null,
            modalElement: null
        };
    },
    mounted() {
        var thisOb = this;

        thisOb.jqueryDdlLabelElement = $("#" + this.labelElementId);
        thisOb.jqueryDdlParentElement = thisOb.jqueryDdlLabelElement.parent();
        var modEl = document.getElementById(thisOb.modalId);
        thisOb.modalElement = $(modEl);
        thisOb.addElements();
    },
    methods: {
        onEditEvent() {
            var thisOb = this;

            var selectedID = null;
            selectedID = thisOb.$refs.slctDDL.getSelectValue();
            if (!(selectedID) || selectedID == "" || selectedID == "0" || selectedID == 0) {
               
                alertOb.warningMessage(globalText.Warning, globalText.SelectElementEdit, null);
                return;
            }

            thisOb.openModalPopUp(selectedID);
        },
        onNewEvent() {
            var thisOb = this;

            thisOb.openModalPopUp();
        },
        openModalPopUp(selectedId) {
            var thisOb = this;

            thisOb.$emit('open-modal', selectedId);
            thisOb.focusFirstElement();
        },
        addElements() {
            var thisOb = this;

            var onNewClickEventName = "newBtn_click_" + thisOb.elementId;
            var onEditClickEventName = "editBtn_click_" + thisOb.elementId;
            var id = "label_id_" + thisOb.elementId;
            var templateId = "template_" + thisOb.elementId;
            thisOb.jqueryDdlLabelElement.attr('id', id);
            TooltipEditHelperWorkAround[onEditClickEventName] = thisOb.onEditEvent;
            TooltipEditHelperWorkAround[onNewClickEventName] = thisOb.onNewEvent;
            var editBtn = $('<div class="btn" id="editBtn_' + thisOb.elementId + '" onclick="TooltipEditHelperWorkAround.' + onEditClickEventName + '()"><i  style="font-size: 30px; color: gray;" class="fa fa-edit"></i></div>');
            var newBtn = $("<div class='btn' id='newtBtn_" + thisOb.elementId + "' onclick='TooltipEditHelperWorkAround." + onNewClickEventName + "()'><i style='font-size: 30px; color: gray;' class='fa fa-plus-circle'></i></div>");
            var popUpHtml = $('<div id="' + templateId + '" style="display:none;"></div>');
            thisOb.jqueryDdlLabelElement.css("cursor", "pointer");
            $('<span>&nbsp;</span>').appendTo(thisOb.jqueryDdlLabelElement);
            $('<i style="font-size: 16px;" class="fa fa-edit"></i>').appendTo(thisOb.jqueryDdlLabelElement);
            popUpHtml.append("<p>" + globalText.AddEditExistingList + "</p>");


            popUpHtml.append(newBtn);
            if (!thisOb.disableEdit) {
                popUpHtml.append(editBtn);
            }

            thisOb.jqueryDdlParentElement.append(popUpHtml);


            thisOb.tippy = new Tippy('#' + id, {
                //beforeShown: function () {
                //    thisInstance.OneBeforeShow();
                //},
                //shown: function () {
                //    thisInstance.OneBeforeShow();
                //    $("#editBtn_ddlCountryId").hide();
                //},
                position: 'right',
                animation: 'perspective',
                theme: 'light',
                trigger: 'click',
                interactive: true,
                html: templateId,
                offset: '10, 0'
            });
        },
        focusFirstElement() {
            var thisOb = this;

            setTimeout(function () {
                thisOb.modalElement.find('input').first().focus();
                thisOb.modalElement.find('input').first().focusin();
                thisOb.modalElement.find('input').first().click();
            }, 0);
        },
        setDisabledSelect(trueOrFalse) {
            var thisOb = this;

            thisOb.$refs.slctDDL.setDisabledSelect(trueOrFalse);
        }
    }
    
});


