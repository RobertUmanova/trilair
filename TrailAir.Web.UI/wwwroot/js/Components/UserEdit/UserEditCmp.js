﻿Vue.component('user-edit', {
    template: '#UserEditTemp',
    props: { // This is like contructor parameters
        onUpdateSuccessFunc: null
    },
    watch: { // Watches for changes in data

    },
    data() {
        return {
            postManager: new PostManager(),
            apiController: "/ApiUser/",
            modalId: "#UserEditCmp",
            userId: 0,
            currentUser: {},
            permissionList: [],
            teamList: [],
            phoneInputField: {},
            phoneInput: {},
            isNewObject: false
        };
    },
    mounted() {
        var thisOb = this;

        thisOb.phoneInputField = document.querySelector("#phoneUserDetailInput");
        thisOb.phoneInput = window.intlTelInput(this.phoneInputField, {
            preferredCountries: ["ch", "de", "at", "fr"],
            utilsScript:
                "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
        });

        thisOb.getPermissions();
        thisOb.getTeams();
    },
    methods: {
        openModalPopUp(userId) {
            var thisOb = this;

            if (!userId || userId == 0) {
                thisOb.currentUser = new User;
            }

            thisOb.userId = userId;


            if (!thisOb.userId) {

                thisOb.isNewObject = true;;
                thisOb.linkSpecialElementsFromObjectToUI();
            }
            else {
                thisOb.isNewObject = false;
                thisOb.getData();
            }

            $(thisOb.modalId).modal();
        },
        getData() {
            var thisOb = this;

            thisOb.postManager.get(thisOb.apiController + thisOb.userId,
                function (response) {
                    thisOb.currentUser = response.data.data;
                    thisOb.linkSpecialElementsFromObjectToUI();
                });

        },
        closeDetModal() {
            var thisOb = this;

            $(thisOb.modalId).modal('toggle');
        },
        saveUser() {
            var thisOb = this;

            thisOb.linkSpecialElementsFromUIToObject(thisOb);

            if (thisOb.currentUser.id == 0) {
                thisOb.saveNewUser(); // New user
                return;
            }

            thisOb.saveExistingUser(); // Existing user
        },
        saveExistingUser() {
            var thisOb = this;

            thisOb.postManager.post(thisOb.apiController + 'SaveUser/', thisOb.currentUser,
                function (response) {
                    if (!response.data.success) {
                        alertOb.errorMessage("Attention", "An error occured");
                        // TODO: Replace with global text!
                        event.preventDefault();
                    } else {
                        thisOb.closeDetModal();
                        ToastrHlp.updateSuccessMsg();
                        if (thisOb.onUpdateSuccessFunc)
                            thisOb.onUpdateSuccessFunc();
                    }
                });
        },
        saveNewUser() {
            var thisOb = this;

            thisOb.postManager.post(thisOb.apiController + 'CreateUser/', thisOb.currentUser,
                function (response) {
                    if (!response.data.success) {
                        alertOb.errorMessage("Attention", "An error occured");
                        // TODO: Replace with global text!
                        event.preventDefault();
                    } else {
                        thisOb.closeDetModal();
                        ToastrHlp.updateSuccessMsg();
                        if (thisOb.onUpdateSuccessFunc)
                            thisOb.onUpdateSuccessFunc();
                    }thisOb.onUpdateSuccessFunc();
                });
        },
        linkSpecialElementsFromUIToObject() {
            var thisOb = this;

            thisOb.currentUser.permissionId = thisOb.$refs.slctPermission.getSelectValue();
            thisOb.currentUser.teamId = thisOb.$refs.slctTeam.$refs.slctDDL.getSelectValue();
            thisOb.currentUser.phoneNumber = thisOb.phoneInput.getNumber();
        },
        linkSpecialElementsFromObjectToUI() {
            var thisOb = this;

            //if we are edditing the existing object display the phone number from DB
            if (!thisOb.isNewObject) {
                if (thisOb.currentUser.phoneNumber != null) {
                    thisOb.phoneInput.setNumber(thisOb.currentUser.phoneNumber);
                }

                thisOb.$refs.slctTeam.$refs.slctDDL.setValue(thisOb.currentUser.teamId);
                thisOb.$refs.slctPermission.setValue(thisOb.currentUser.permissionId);
            }
            else { // For the new User creation display Switzerland phone number
                thisOb.phoneInput.setCountry('ch');
            }

        },
        getPermissions() {
            var thisOb = this;

            thisOb.postManager.get('/ApiPermission/GetDDList',
                function (response) {
                    thisOb.permissionList = response.data.data;

                    // TODO: Check this how to automate Select2 Loading
                    thisOb.$refs.slctPermission.ddlData = thisOb.permissionList;
                    thisOb.$refs.slctPermission.loadSelect2WithData();
                });
        },
        getTeams(teamIdToSet) {
            var thisOb = this;

            thisOb.postManager.get('/ApiTeam/GetDDList',
                function (response) {
                    thisOb.teamList = response.data.data;

                    if (teamIdToSet && teamIdToSet != 0) {
                        thisOb.$refs.slctTeam.$refs.slctDDL.setValue(teamIdToSet, null, true);
                    }


                    // TODO: Check this how to automate Select2 Loading
                    thisOb.$refs.slctTeam.$refs.slctDDL.ddlData = thisOb.teamList;
                    thisOb.$refs.slctTeam.$refs.slctDDL.loadSelect2WithData();
                });
        },
        onTeamSaved(teamId) {
            var thisOb = this;

            thisOb.getTeams(teamId);
        },
        openTeamEditModal(teamId) {
            var thisOb = this;

            thisOb.$refs.teamEditCmp.openModalPopUp(teamId);
        }
    }
});