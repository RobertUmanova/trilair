﻿Vue.component('exp-partic-result', {
    template: '#ExpParticResTemp',
    props: { // This is like contructor parameters
        onUpdateSuccessFunc: null
    },
    watch: { // Watches for changes in data

    },
    data() {
        return {
            postManager: new PostManager(),
            apiController: "/ApiExperimentParticipantResult/",
            modalId: "#ExpParticResCmp",
            currentElement: new ParticipantResult(),
            parentGetData: null,
            validator: new ValidationHelper('#ExperimentEditCmp'),
            sliders: [],
            controlSampleResultSlider: {},
        };
    },
    mounted() {
        var thisOb = this;
    },
    methods: {
        openModalPopUp(participantId, experimentId) {
            var thisOb = this;

            thisOb.postManager.get("/ApiExperimentParticipantResult/GetExistingParticipantResult?participantId=" + participantId + "&experimentId=" + experimentId, function (response) {
                if (response.data.data)
                    alertOb.confirmMessage("There are active tests, would you like to delete data and start a new test?", function (isConfirmed) {
                        if (isConfirmed)
                            thisOb.postManager.get('/ApiExperimentParticipantResult/CancelWizardResults?experimentId=' + experimentId + '&participantId=' + participantId, function (response) {
                                if (!response.data.success) {
                                    alertOb.errorMessage("Attention", "An error occured");
                                    // TODO: Replace with global text!
                                    event.preventDefault();
                                    window.location = experimentListPage;
                                }
                                else {
                                    //if confirmed delete and proceed
                                    thisOb.getData(participantId, experimentId);
                                    ToastrHlp.updateSuccessMsg();
                                }
                            });
                        else {
                            window.location = experimentListPage;
                        }
                    });
                else
                    thisOb.getData(participantId, experimentId);
            });

            $(thisOb.modalId).modal();
        },
        getData(participantId, experimentId) {
            var thisOb = this;

            thisOb.currentElement.participantId = participantId;
            thisOb.currentElement.experimentId = experimentId;

            thisOb.postManager.get(thisOb.apiController + 'GetByParticAndExp?participantId=' + participantId +
                '&experimentId=' + experimentId,
                function (response) {
                    thisOb.currentElement = response.data.data;

                    Vue.nextTick(() => {
                        thisOb.currentElement.testSampleResults.forEach(tsr => {
                            thisOb.createSlider(tsr.experimentBencTestSampId, thisOb.currentElement.participantId, thisOb.currentElement.experimentId, tsr.intensity);
                        });

                        var controlSampleResultIntensity = 0;
                        if (thisOb.currentElement.controlSampleResult) {

                            if (thisOb.currentElement.controlSampleResult.intensity) {
                                controlSampleResultIntensity = thisOb.currentElement.controlSampleResult.intensity;
                            }

                            thisOb.createControlSampleResultSlider(controlSampleResultIntensity);
                        }

                    });

                    thisOb.linkSpecialElementsFromObjectToUI();
                });

        },
        createControlSampleResultSlider(intensity) {
            var thisOb = this;

            if (!intensity) {
                intensity = 0;
            }

            thisOb.controlSampleResultSlider = $("#ControlSampleResult-intensity");

            noUiSlider.create(thisOb.controlSampleResultSlider[0], {
                start: intensity,
                step: 0.5,
                behaviour: 'tap',
                connect: 'lower',
                range: {
                    'min': 0,
                    'max': 10
                },
                pips: {
                    mode: 'values',
                    values: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                    density: 5
                }
            });
        },
        createSlider(testSampleId, participantId, experimentId, intensity) {
            var thisOb = this;

            if (!intensity) {
                intensity = 0;
            }

            var slider = $(`#intensity-${testSampleId}-${participantId}-${experimentId}`)[0];

            thisOb.sliders.push({
                element: slider,
                testSampleId: testSampleId
            });

            noUiSlider.create(slider, {
                start: intensity,
                step: 0.5,
                behaviour: 'tap',
                connect: 'lower',
                range: {
                    'min': 0,
                    'max': 10
                },
                pips: {
                    mode: 'values',
                    values: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                    density: 5
                }
            });
        },
        closeDetModal() {
            var thisOb = this;

            thisOb.destroySliders();
            thisOb.destroyDatePickers();

            $(thisOb.modalId).modal('toggle');
        },
        saveResults() {
            var thisOb = this;

            thisOb.linkSpecialElementsFromUIToObject();

            if (!thisOb.validate()) {
                return;
            }

            thisOb.postManager.post(thisOb.apiController + 'SaveBo', thisOb.currentElement,
                function (response) {
                    if (!response.data.success) {
                        alertOb.errorMessage("Attention", "An error occured");
                        // TODO: Replace with global text!
                        event.preventDefault();
                    }
                    else {
                        ToastrHlp.updateSuccessMsg();
                    }
                    thisOb.closeDetModal();
                });
        },
        submit() {
            var thisOb = this;

            thisOb.linkSpecialElementsFromUIToObject();

            if (!thisOb.validate()) {
                return;
            }

            thisOb.postManager.post(thisOb.apiController + 'Submit', thisOb.currentElement,
                function (response) {
                    thisOb.currentElement = response.data.data;

                    //thisOb.linkSpecialElementsFromObjectToUI();

                    thisOb.closeDetModal();

                    thisOb.parentGetData();
                });
        },
        validate() {
            var thisOb = this;

            thisOb.validator.Validate(false);

            // --------- TEST SAMPLE VALIDATION ----------
            thisOb.currentElement.testSampleResults.forEach(tsr => {
                var TSfirstDetectionTimeId = '#firstDetectionTime-' + tsr.experimentBencTestSampId;
                var TSfullDetectionTimeId = '#fullDetectionTime-' + tsr.experimentBencTestSampId;
                thisOb.validator.ValidateSingleFull(true, TSfirstDetectionTimeId, thisOb.validator);
                thisOb.validator.ValidateSingleFull(true, TSfullDetectionTimeId, thisOb.validator);

                var TSintensityId = document.querySelectorAll(`[id^="intensity-${tsr.experimentBencTestSampId}"]`);
                thisOb.validator.ValidateSlider(tsr.intensity, TSintensityId[0], thisOb.validator);

                var TSdateId = '#date-' + tsr.experimentBencTestSampId;
                thisOb.validator.ValidateSingleFull(true, TSdateId, thisOb.validator);
            });
            // --------- TEST SAMPLE VALIDATION ----------


            // --------- CONTROL SAMPLE VALIDATION ----------
            var CSfirstDetectionTimeId = '#ControlSampleResult-firstDetectionTime';
            var CSfullDetectionTimeId = '#ControlSampleResult-fullDetectionTime';
            thisOb.validator.ValidateSingleFull(true, CSfirstDetectionTimeId, thisOb.validator);
            thisOb.validator.ValidateSingleFull(true, CSfullDetectionTimeId, thisOb.validator);

            var CSintensityId = document.getElementById("ControlSampleResult-intensity");
            thisOb.validator.ValidateSlider(thisOb.currentElement.controlSampleResult.intensity, CSintensityId, thisOb.validator);

            var CSdateId = '#ControlSampleResultDatePicker-date';
            thisOb.validator.ValidateSingleFull(true, CSdateId, thisOb.validator);
            // --------- CONTROL SAMPLE VALIDATION ----------

            if (thisOb.validator.isValid == false) {
                //thisOb.linkSpecialElementsFromObjectToUI();
                thisOb.validator.DisplayErrorMessages();
                return false;
            } else {
                return true;
            }

        },
        destroySliders() {
            var thisOb = this;

            // This is done this way because this bug is not important and will be handled 
            // with the loader, so you won't be able to close popup before it loads all the data,
            // so..... this try/catch is going to be unnecessary
            thisOb.sliders.forEach(s => {
                try {
                    s.element.noUiSlider.destroy();

                } catch (error) {

                }
            });

            thisOb.sliders.length = 0;

            if (thisOb.currentElement.controlSampleResult) {

                try {
                    thisOb.controlSampleResultSlider[0].noUiSlider.destroy();
                } catch (error) {

                }
            }

        },
        destroyDatePickers() {
            var testResDatePickers = document.querySelectorAll('[id^="TestDatePicker_res"]');
            testResDatePickers.forEach((dPicker, i) => {
                $('#' + dPicker.id).datepicker("destroy");
            });

            $('#ControlSampleResultDatePicker').datepicker("destroy");
        },
        linkSpecialElementsFromUIToObject() {
            var thisOb = this;

            thisOb.sliders.forEach(s => {
                var testSample = thisOb.currentElement.testSampleResults.find(tsr => tsr.experimentBencTestSampId == s.testSampleId);
                testSample.intensity = s.element.noUiSlider.get();
            });

            thisOb.currentElement.controlSampleResult.intensity = thisOb.controlSampleResultSlider[0].noUiSlider.get();
        },
        linkSpecialElementsFromObjectToUI() {
            var thisOb = this;


            this.$nextTick(function () { // This executes when the DOM is done with loading

                var testResDatePickers = document.querySelectorAll('[id^="TestDatePicker_res"]');

                var settings = {
                    todayBtn: "linked",
                    keyboardNavigation: true,
                    forceParse: false,
                    calendarWeeks: true,
                    autoclose: true,
                    language: lanUiLangShort,
                };

                testResDatePickers.forEach((dPicker, i) => {
                    // Set all date pickers default value to today
                    //$('#' + dPicker.id).datepicker("setDate", new Date);

                    //$('#' + dPicker.id).datepicker(settings).on('changeDate', function (selected) {
                    //    var date = new Date(selected.date.valueOf());

                    //    var dpInput = $('#' + dPicker.id)[0];

                    //    if (dpInput) {
                    //        var pickedDate = dpInput.value;
                    //        thisOb.currentElement.testSampleResults[i].date = pickedDate;
                    //    }
                    //});


                    // Automatically onDatePickerChange change the binded model, because there is a bug
                    // when working with model and Vue.js + datepicker
                    var someNewFunc = function (selectedValue) {

                        if (selectedValue) {
                            thisOb.currentElement.testSampleResults[i].date = selectedValue;
                        }
                    };

                    var datePicker = new DatePickerHelper(dPicker.id, null, someNewFunc, new Date);
                });


                // Automatically onDatePickerChange change the binded model, because there is a bug
                // when working with model and Vue.js + datepicker
                var onSelectDatePickerFunc = function (selectedValue) {

                    if (selectedValue) {
                        thisOb.currentElement.controlSampleResult.date = selectedValue;
                    }
                };

                var datePicker = new DatePickerHelper("ControlSampleResultDatePicker", null, onSelectDatePickerFunc, new Date);

                //$("#ControlSampleResultDatePicker").datepicker("setDate", new Date);

                //$("#ControlSampleResultDatePicker").datepicker(settings).on('changeDate', function (selected) {
                //    var date = new Date(selected.date.valueOf());

                //    var dpInput = $("#ControlSampleResultDatePicker")[0];

                //    if (dpInput) {
                //        var pickedDate = dpInput.value;
                //        thisOb.currentElement.controlSampleResult.date = pickedDate;
                //    }
                //});



                // WARNING: This collapse function is copy pasted from wwwroot/js/script.js
                // But, we need to call it again because we have multiple accordions which
                // are instanced via v-for loop and this function from script.js happens before v-for is finished

                // TODO: Put this in some class  so you can call it from anywhere!!

                // First you have to remove click event, because if you don't remove it you will get duplicated event
                // and you'll get bugs
                $('.collapse-link-for-dynamic').off("click");

                // Collapse ibox function
                $('.collapse-link-for-dynamic').on('click', function (e) {
                    e.preventDefault();
                    var ibox = $(this).closest('div.ibox');
                    var button = $(this).find('i');
                    var content = ibox.children('.ibox-content');
                    content.slideToggle(200);
                    button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                    ibox.toggleClass('').toggleClass('border-bottom');
                    setTimeout(function () {
                        ibox.resize();
                        ibox.find('[id^=map-]').resize();
                    }, 50);
                });

            });

        }
    }
});