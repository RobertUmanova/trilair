﻿Vue.component('single-exp-result', {
    template: '#SingleExpResTemp',
    props: { // This is like contructor parameters
        onUpdateSuccessFunc: null
    },
    watch: { // Watches for changes in data

    },
    data() {
        return {
            postManager: new PostManager(),
            apiController: "/ApiExperimentParticipantResult/",
            modalId: "#SingleExpResCmp",
            currentElement: new ExperimentResult(),
            parentGetData: null,
            validator: new ValidationHelper('#ExperimentEditCmp'),
            slider: null,
            testSampleName: ""
        };
    },
    mounted() {
        var thisOb = this;

        thisOb.slider = $('#singleIntensitySlider')[0];
    },
    methods: {
        openModalPopUp(baseExperimentResultId, baseExperimentResultType) {
            var thisOb = this;

            thisOb.getData(baseExperimentResultId, baseExperimentResultType);

            $(thisOb.modalId).modal();
        },
        getData(baseExperimentResultId, baseExperimentResultType) {
            var thisOb = this;

            thisOb.postManager.get(thisOb.apiController + 'GetSingleExperimentResult?baseExperimentResultId=' + baseExperimentResultId + '&baseExperimentResultType=' + baseExperimentResultType,
                function (response) {
                    thisOb.currentElement = response.data.data;

                    thisOb.createSlider(thisOb.currentElement.intensity);
                    thisOb.linkSpecialElementsFromObjectToUI();
                });

        },
        closeDetModal() {
            var thisOb = this;

            thisOb.slider.noUiSlider.destroy();

            $(thisOb.modalId).modal('toggle');
        },
        saveResult() {
            var thisOb = this;

            thisOb.linkSpecialElementsFromUIToObject();
            thisOb.validate();

            if (thisOb.validator.isValid == false) {
                thisOb.linkSpecialElementsFromObjectToUI();
                thisOb.validator.DisplayErrorMessages();
                return;
            }

            thisOb.postManager.post(thisOb.apiController + 'SaveSingleExperimentResult', thisOb.currentElement,
                function (response) {
                    if (!response.data.success) {
                        alertOb.errorMessage("Attention", "An error occured");
                        // TODO: Replace with global text!
                        event.preventDefault();
                    }
                    else {
                        ToastrHlp.updateSuccessMsg();
                        thisOb.parentGetData();
                    }
                    thisOb.closeDetModal();
                });
        },
        validate() {
            var thisOb = this;

            thisOb.validator.Validate(false);
            thisOb.validator.ValidateSingleFull(true, '#singleFirstDetectionTimeInput', thisOb.validator);
            thisOb.validator.ValidateSingleFull(true, '#singleFullDetectionTimeInput', thisOb.validator);
            thisOb.validator.ValidateSingleFull(true, '#singleDateInput', thisOb.validator);

            var intensityId = document.getElementById('singleIntensitySlider');
            thisOb.validator.ValidateSlider(thisOb.currentElement.intensity, intensityId, thisOb.validator);
        },
        createSlider(intensity) {
            var thisOb = this;

            noUiSlider.create(thisOb.slider, {
                start: intensity,
                step: 0.5,
                behaviour: 'tap',
                connect: 'lower',
                range: {
                    'min': 0,
                    'max': 10
                },
                pips: {
                    mode: 'values',
                    values: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                    density: 5
                }
            });
        },
        linkSpecialElementsFromUIToObject() {
            var thisOb = this;

            thisOb.currentElement.intensity = thisOb.slider.noUiSlider.get();
        },
        linkSpecialElementsFromObjectToUI() {
            var thisOb = this;


            this.$nextTick(function () { // This executes when the DOM is done with loading

                //var benchmarkResDatePickers = document.querySelectorAll('[id^="BenDatePicker"]');
                //benchmarkResDatePickers.forEach((dPicker) => {

                //    var datePicker = new DatePickerHelper(dPicker.id, null;
                //});

                var testResDatePickers = document.querySelectorAll('[id^="TestDatePicker_wiz"]');
                testResDatePickers.forEach((dPicker, i) => {

                    // Automatically onDatePickerChange change the binded model, because there is a bug
                    // when working with model and Vue.js + datepicker
                    var onSelectDatePickerFunc = function (selectedValue) {

                        if (selectedValue) {
                            thisOb.currentElement.date = selectedValue;
                        }
                    };

                    var datePicker = new DatePickerHelper(dPicker.id, null, onSelectDatePickerFunc);
                });

                // WARNING: This collapse function is copy pasted from wwwroot/js/script.js
                // But, we need to call it again because we have multiple accordions which
                // are instanced via v-for loop and this function from script.js happens before v-for is finished

                // TODO: Put this in some class  so you can call it from anywhere!!

                // First you have to remove click event, because if you don't remove it you will get duplicated event
                // and you'll get bugs
                $('.collapse-link-for-dynamic').off("click");

                // Collapse ibox function
                $('.collapse-link-for-dynamic').on('click', function (e) {
                    e.preventDefault();
                    var ibox = $(this).closest('div.ibox');
                    var button = $(this).find('i');
                    var content = ibox.children('.ibox-content');
                    content.slideToggle(200);
                    button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                    ibox.toggleClass('').toggleClass('border-bottom');
                    setTimeout(function () {
                        ibox.resize();
                        ibox.find('[id^=map-]').resize();
                    }, 50);
                });
            });
        }
    }
});