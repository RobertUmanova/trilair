﻿Vue.component('team-edit', {
    template: '#TeamEditTemp',
    props: { // This is like contructor parameters
        onUpdateSuccessFunc: null
    },
    watch: { // Watches for changes in data

    },
    data() {
        return {
            postManager: new PostManager(),
            apiController: "/ApiTeam/",
            modalId: "#TeamEditCmp",
            teamId: 0,
            currentElement: {},
            userList: [],
            isNewObject: false
        };
    },
    mounted() {
        var thisOb = this;
        thisOb.getUsers();
    },
    methods: {
        openModalPopUp(teamId) {
            var thisOb = this;

            if (!teamId || teamId == 0) {
                thisOb.currentElement = new Team();
            }

            thisOb.teamId = teamId;
            thisOb.getData();
            $(thisOb.modalId).modal();
        },
        getData() {
            var thisOb = this;

            if (!thisOb.teamId) return;

            thisOb.postManager.get(thisOb.apiController + thisOb.teamId,
                function (response) {
                    thisOb.currentElement = response.data.data;
                    thisOb.linkSpecialElementsFromObjectToUI();
                });

        },
        closeDetModal() {
            var thisOb = this;

            $(thisOb.modalId).modal('toggle');
        },
        saveTeam() {
            var thisOb = this;

            thisOb.linkSpecialElementsFromUIToObject(thisOb);

            //if (thisOb.currentBo.workPositions.length <= 0) {
            //    alertOb.errorMessage(globalText.ValidationError, globalText.AtLeastOneWorkPosition_User);
            //    return;
            //}
            if (!thisOb.currentElement.id || thisOb.currentElement.id == 0) {
                thisOb.isNewObject = true;
            } else thisOb.isNewObject = false;

            thisOb.postManager.post(thisOb.apiController + 'SaveBo/', thisOb.currentElement,
                function (response) {
                    if (!response.data.success) {
                        alertOb.errorMessage("Attention", "An error occured");
                        // TODO: Replace with global text!
                        event.preventDefault();
                    }
                    else {
                        thisOb.closeDetModal();
                        ToastrHlp.updateSuccessMsg();
                        if (thisOb.onUpdateSuccessFunc) {
                            thisOb.onUpdateSuccessFunc(response.data.data);
                        }
                        else {
                            thisOb.getData();
                        }
                    }
                });
        },
        linkSpecialElementsFromUIToObject() {
            var thisOb = this;

            //var assignedUsers = thisOb.$refs.slctUser.getSelectValue().map(value => {
            //    return {Id: value}
            //});

            //thisOb.currentElement.assignedUsers = assignedUsers;
        },
        linkSpecialElementsFromObjectToUI() {
            var thisOb = this;

            if (!thisOb.isNewObject) {

                //var preselectedAssignedUsersId = thisOb.userList.map(user => {
                //    return user.id;
                //});

                //thisOb.$refs.slctUser.setMultipleValues(preselectedAssignedUsersId);
            }

        },
        getUsers() {
            var thisOb = this;

            thisOb.postManager.get('/ApiUser/GetDDList',
                function (response) {
                    thisOb.userList = response.data.data;
                });
        }
    }
});