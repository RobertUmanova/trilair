﻿Vue.component('control-sample-type-edit', {
    template: '#ControlSampleTypeEditTemp',
    props: { // This is like contructor parameters
        onUpdateSuccessFunc: null
    },
    watch: { // Watches for changes in data

    },
    data() {
        return {
            postManager: new PostManager(),
            apiController: "/ApiControlSampleType/",
            modalId: "#ControlSampleTypeEditCmp",
            currentElement: {},
            controlSampleTypeDDList: [],
            validator: new ValidationHelper('#ExperimentEditCmp')
        };
    },
    mounted() {
        var thisOb = this;

    },
    methods: {
        openModalPopUp(controlSampleTypeId) {
            var thisOb = this;

            thisOb.getData(controlSampleTypeId);

            $(thisOb.modalId).modal();
        },
        closeDetModal() {
            var thisOb = this;

            $(thisOb.modalId).modal('toggle');
        },
        getData(controlSampleTypeId) {
            var thisOb = this;

            if (!controlSampleTypeId || controlSampleTypeId == 0) {
                thisOb.postManager.get(thisOb.apiController + 'GetEmptyControlSampleType',
                    function (response) {
                        thisOb.currentElement = response.data.data;
                    });

            }
            else {
                thisOb.postManager.get(thisOb.apiController + controlSampleTypeId,
                    function (response) {

                        thisOb.currentElement = response.data.data;
                    });
            }
        },
        saveControlSampleType() {
            var thisOb = this;

            thisOb.validator.Validate(false);

            thisOb.validator.ValidateSingleFull(true, '#controlSampleNameInput', thisOb.validator);
            thisOb.validator.ValidateSingleFull(true, '#controlSampleIntensityScoreInput', thisOb.validator);

            if (thisOb.validator.isValid == false) {
                thisOb.validator.DisplayErrorMessages();
                return;
            }

            thisOb.postManager.post(thisOb.apiController + 'SaveBo/', thisOb.currentElement,
                function (response) {
                    if (!response.data.success) {
                        alertOb.errorMessage("Attention", "An error occured");
                        // TODO: Replace with global text!
                        event.preventDefault();
                    }
                    else {
                        thisOb.closeDetModal();
                        ToastrHlp.updateSuccessMsg();
                        if (thisOb.onUpdateSuccessFunc) {
                            thisOb.onUpdateSuccessFunc(response.data.data);
                        }
                        else {
                            thisOb.getData();
                        }
                    }
                });
        },
    }
});