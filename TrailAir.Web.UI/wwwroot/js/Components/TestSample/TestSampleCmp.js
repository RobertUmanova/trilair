﻿Vue.component('test-sample-cmp', {
    template: '#TestSampleTemp',
    props: { // This is like contructor parameters
        onUpdateSuccessFunc: null,
        experimentId: 0,
        experimentInDraftStatus: false
    },
    watch: { // Watches for changes in data

    },
    data() {
        return {
            postManager: new PostManager(),
            apiController: "/ApiTestSample/",
            modalId: "#TestSampleModal",
            parentGetData: null,
            isInternal: false,
            testSamples: [],
            experimentTestSample: new ExperimentTestSample(),
            isNewObject: false,
            validator: new ValidationHelper('#ExperimentEditCmp'),
            // We use this variable to see if the experiment isPublished and isFinished
            // because this component is done the wrong way - it contains both list of Test Samples and modal
            // but it should contain modal only and then when you open modal you make an AJAX call
            experiment: {}
        };
    },
    mounted() {
        var thisOb = this;

    },
    methods: {
        getData() {
            var thisOb = this;

            // You don't have to have getData here because you already have getTestSampleList() which is called 
            // when the ExperimentEdit modal getData is called, so you just reuse the Id's from the list for 
            // opening the TestSampleEdit modal
        },
        getTestSampleList() {
            var thisOb = this;

            if (thisOb.experimentId && thisOb.experimentId !== 0) {
                thisOb.postManager.get(thisOb.apiController + 'GetTestSamplesForExperiment?experimentId=' + thisOb.experimentId, function (response) {
                    thisOb.testSamples = response.data.data;
                });
            }
        },
        openTestSamplePopup(isInternal, testSample) {
            var thisOb = this;

            // if an Experiment is published you cannot edit test samples
            if (thisOb.experiment.isPublished) {
                return;
            }

            thisOb.isInternal = isInternal;

            if (!testSample) {
                thisOb.isNewObject = true;
                thisOb.experimentTestSample = new ExperimentTestSample();
            } else {
                thisOb.isNewObject = false;
                thisOb.experimentTestSample = testSample;
            }

            thisOb.experimentTestSample.testSample.isInternal = isInternal;
            thisOb.linkSpecialElementsFromObjectToUI();

            $(thisOb.modalId).modal();
        },
        closeDetModal() {
            var thisOb = this;

            $(thisOb.modalId).modal('toggle');
        },
        linkSpecialElementsFromObjectToUI() {
            var thisOb = this;

            setTimeout(function () {

                // Automatically onDatePickerChange change the binded model, because there is a bug
                // when working with model and Vue.js + datepicker
                var onSelectAppDatePickerFunc = function (selectedValue) {

                    thisOb.experimentTestSample.testSample.applicationDate = selectedValue;
                };
                var onSelectManDatePickerFunc = function (selectedValue) {

                    thisOb.experimentTestSample.testSample.manufacturedDate = selectedValue;
                };

                var datePicker = new DatePickerHelper("ApplicationDateDatePicker", null, onSelectAppDatePickerFunc);
                var datePicker2 = new DatePickerHelper("TSManufacturedDateDatePicker", null, onSelectManDatePickerFunc);
                var appDtPckr = document.getElementById('ApplicationDateDatePicker');
                var manDtPckr = document.getElementById('TSManufacturedDateDatePicker');

                if (thisOb.isNewObject) { // You have to set Date picker to empty string again
                    appDtPckr.value = "";
                    manDtPckr.value = "";
                }


            }, 300);

        },
        saveTestSample(isUpdate) {
            var thisOb = this;

            thisOb.validator.Validate(false);

            thisOb.validator.ValidateSingleFull(true, '#TSManufacturedDateDatePicker', thisOb.validator);
            // thisOb.validator.ValidateSingleFull(true, '#baseInput', thisOb.validator);

            if (!thisOb.isInternal) {
                thisOb.validator.ValidateSingleFull(true, '#productNameInput', thisOb.validator);
            } else {
                if (!thisOb.validator.ValidateInput($('#fantasyNameInput'), true, null, thisOb.validator) &&
                    !thisOb.validator.ValidateInput($('#internalNameInput'), true, null, thisOb.validator)) {
                    thisOb.validator.AddCustomErrorMessage('Fantasy name or Internal name are mandatory', thisOb.validator);
                }
                // thisOb.validator.ValidateSingleFull(true, '#technologyInput', thisOb.validator);

                if (!thisOb.validator.ValidateInput($('#trialNumberInput'), true, null, thisOb.validator) &&
                    !thisOb.validator.ValidateInput($('#groupNumberInput'), true, null, thisOb.validator)) {
                    thisOb.validator.AddCustomErrorMessage('Trial number or group number are mandatory', thisOb.validator);
                }
                thisOb.validator.ValidateSingleFull(true, '#ApplicationDateDatePicker', thisOb.validator);
            }

            if (thisOb.validator.isValid == false) {
                thisOb.validator.DisplayErrorMessages();
                return;
            }

            thisOb.experimentTestSample.experimentId = thisOb.experimentId;


            // You send the TestSampleUI object to get the ComputedName property from backend
            thisOb.postManager.post(thisOb.apiController + 'GetComputedNameForTestSample', thisOb.experimentTestSample, function (response) {

                if (!response.data.success) {
                    alertOb.errorMessage("Attention", "An error occured");
                    event.preventDefault();
                }
                else {
                    thisOb.experimentTestSample.testSample.computedName = response.data.data;

                    if (thisOb.isNewObject) {
                        thisOb.testSamples.push(thisOb.experimentTestSample);
                    }
                    thisOb.reRender();
                    thisOb.closeDetModal();
                }
            });


            //if (thisOb.experimentId !== 0 && isUpdate) {
            //    thisOb.postManager.post(thisOb.apiController + 'SaveTestSample', thisOb.experimentTestSample, function (response) {
            //        if (!response.data.success) {
            //            alertOb.errorMessage("Attention", "An error occured");
            //            event.preventDefault();
            //        }
            //        else {
            //            thisOb.closeDetModal();
            //            ToastrHlp.updateSuccessMsg();
            //        }
            //    });
            //}
        },
        // This is the code that forces the VUE to Re-Render the component.
        // We need to do that sometimes e.g. when you are changing the variable from an array indirectly
        // like here in saveTestSample
        reRender() {
            this.$forceUpdate();
        },
        deleteTestSample(index) {
            var thisOb = this;

            thisOb.testSamples.splice(index, 1);

            //alertOb.confirmMessage("Are you sure you want to delete this test sample?", function () {
            //    thisOb.postManager.get(thisOb.apiController + 'Delete?testSampleId=' + id, function (response) {
            //        if (!response.data.success) {
            //            alertOb.errorMessage("Attention", "An error occured");
            //            // TODO: Replace with global text!
            //            event.preventDefault();
            //        }
            //        else {
            //            thisOb.getTestSampleList();
            //            ToastrHlp.updateSuccessMsg();
            //        }
            //    });
            //})
        }
    }
});