﻿Vue.component('experiment-edit', {
    template: '#ExperimentEditTemp',
    props: { // This is like contructor parameters
        onUpdateSuccessFunc: null
    },
    watch: { // Watches for changes in data

    },
    data() {
        return {
            postManager: new PostManager(),
            apiController: "/ApiExperiment/",
            modalId: "#ExperimentEditCmp",
            experimentId: 0,
            currentElement: {
                samplePreparation: {},
                controlSample: {}
            },
            isNewObject: false,
            teamsList: [],
            requestersList: [],
            participantsDDList: [],
            controlSampleTypeDDList: [],
            othersPicked: false,
            isNewObject: false,
            experimentDraftStatus: false,
            selectedParticipantIds: [],
            addingParticipantDisabled: false
        };
    },
    mounted() {
        var thisOb = this;
        //thisOb.configureCheckBoxes();
        //thisOb.getTestTypes();
        //thisOb.getScoringScaleDefinitions();
        thisOb.validator = new ValidationHelper('#ExperimentEditCmp');
        thisOb.getTeams();
        thisOb.getRequesters();
        thisOb.getParticipants();
        thisOb.getControlSampleTypes();
        //thisOb.getExperimentStatuses();
    },
    methods: {
        openModalPopUp(experimentId) {
            var thisOb = this;

            thisOb.experimentId = experimentId;
            thisOb.getData();

            $(thisOb.modalId).modal();
        },
        getData() {
            var thisOb = this;

            if (!thisOb.experimentId || thisOb.experimentId == 0) {

                thisOb.isNewObject = true;

                thisOb.postManager.get(thisOb.apiController + 'GetEmptyExperimentDetails',
                    function (response) {
                        thisOb.currentElement = response.data.data;
                        thisOb.linkSpecialElementsFromObjectToUI();
                        //thisOb.$refs.slctTeam.setValue(0);
                        //thisOb.$refs.slctRequester.setValue(0);
                        thisOb.$refs.slctControlSampleType.$refs.slctDDL.setValue(1);

                        thisOb.$refs.testSampleCmp.getTestSampleList();
                        // for explanation why we set this variable like this, see TestSampleCmp.js
                        thisOb.$refs.testSampleCmp.experiment = thisOb.currentElement;
                        thisOb.setDisabledSelects();
                    });

                return;
            }

            thisOb.isNewObject = false;

            thisOb.postManager.get(thisOb.apiController + thisOb.experimentId,
                function (response) {
                    thisOb.currentElement = response.data.data;

                    thisOb.$refs.testSampleCmp.getTestSampleList();
                    // for explanation why we set this variable like this, see TestSampleCmp.js
                    thisOb.$refs.testSampleCmp.experiment = thisOb.currentElement;
                    thisOb.linkSpecialElementsFromObjectToUI();
                    thisOb.setDisabledSelects();

                });

        },
        // You set 'disabled' on Select2's because this is the easiest way
        // to update 'disabled' to true. (It is false by default because there is no 
        // thisOb.currentElement yet)
        setDisabledSelects() {
            var thisOb = this;

            var trueOrFalse = true;
            if (!thisOb.isNotPublishable()) trueOrFalse = false;

            thisOb.$refs.slctRequester.setDisabledSelect(trueOrFalse);
            thisOb.$refs.slctTeam.setDisabledSelect(trueOrFalse);
            thisOb.$refs.slctControlSampleType.setDisabledSelect(trueOrFalse);
        },
        closeDetModal() {
            var thisOb = this;
            thisOb.$refs.testSampleCmp.testSamples = [];
            $(thisOb.modalId).modal('toggle');
        },
        saveExperiment() {
            var thisOb = this;

            if (!thisOb.validator.Validate(true))
                return;

            if (!thisOb.currentElement.id || thisOb.currentElement.id == 0) {
                thisOb.isNewObject = true;
            } else thisOb.isNewObject = false;

            thisOb.linkSpecialElementsFromUIToObject();

            thisOb.currentElement.experimentTestSamples = thisOb.$refs.testSampleCmp.testSamples;

            thisOb.postManager.post(thisOb.apiController + 'SaveBo/', thisOb.currentElement,
                function (response) {

                    if (!response.data.success) {
                        alertOb.errorMessage("Attention", "An error occured - " + response.data.errorMessage);
                        // TODO: Replace with global text!
                        event.preventDefault();
                    }
                    else {
                        thisOb.$refs.testSampleCmp.testSamples = [];
                        thisOb.closeDetModal();
                        ToastrHlp.updateSuccessMsg();
                        if (thisOb.onUpdateSuccessFunc) {
                            thisOb.onUpdateSuccessFunc(response.data.data);
                        }
                        else {
                            thisOb.getData();
                        }
                    }
                });
        },
        publishExperiment() {
            var thisOb = this;

            thisOb.validator.Validate(false);
            //thisOb.validator.ValidateSingleFull(true, '#experimentEditperfumeConcentrationInput', thisOb.validator);
            thisOb.validator.ValidateSingleFull(true, '#experimentEditevaporingTimeInput', thisOb.validator);
            thisOb.validator.ValidateSingleFull(true, '#experimentEditEvaporingTemperatureInput', thisOb.validator);

            if (!thisOb.validator.ValidateSelect($('#slctControlTypeSample'), true)) {
                thisOb.validator.AddCustomErrorMessage("Control Sample Type must be selected!", thisOb.validator);
            }

            thisOb.linkSpecialElementsFromUIToObject();

            if (thisOb.currentElement.participants.length == 0 || thisOb.currentElement.participants[0].participantId == 0) {
                thisOb.validator.AddCustomErrorMessage("At least one particpant must be selected!", thisOb.validator);
            }

            if (thisOb.$refs.testSampleCmp.testSamples.length < 1) {
                thisOb.validator.AddCustomErrorMessage("At least one test sample must be specified!", thisOb.validator);
            }

            thisOb.currentElement.experimentTestSamples = thisOb.$refs.testSampleCmp.testSamples;

            if (thisOb.validator.isValid == false) {
                thisOb.linkSpecialElementsFromObjectToUI();
                thisOb.validator.DisplayErrorMessages();
                return;
            } else {
                alertOb.confirmMessage('If you publish you will not be able to edit, are you sure?', function (isConfirmed) {
                    if (isConfirmed) {
                        thisOb.postManager.post(thisOb.apiController + 'Publish/', thisOb.currentElement, function (response) {
                            if (!response.data.success) {
                                alertOb.errorMessage("Attention", "An error occured");
                                // TODO: Replace with global text!
                                event.preventDefault();
                            }
                            else {
                                thisOb.closeDetModal();
                                ToastrHlp.updateSuccessMsg();
                                thisOb.$refs.testSampleCmp.testSamples = [];
                                if (thisOb.onUpdateSuccessFunc) {
                                    thisOb.onUpdateSuccessFunc(response.data.data);
                                }
                                else {
                                    thisOb.getData();
                                }
                            }
                        });
                    }
                });
            }
        },
        linkSpecialElementsFromUIToObject() {
            var thisOb = this;

            thisOb.currentElement.teamId = thisOb.$refs.slctTeam.getSelectValue();
            thisOb.currentElement.requesterId = thisOb.$refs.slctRequester.getSelectValue();
            if (thisOb.currentElement.requesterId == 0) {
                thisOb.currentElement.requesterId = null;
            };

            var ctrlSmplTypeId = thisOb.$refs.slctControlSampleType.$refs.slctDDL.getSelectValue() === null ? 0 : thisOb.$refs.slctControlSampleType.$refs.slctDDL.getSelectValue();
            thisOb.currentElement.controlSample.controlSampleTypeId = ctrlSmplTypeId;

            if (!thisOb.isNewObject) {
                //thisOb.currentElement.experimentBenchmarks.forEach((obj, i) => {

                //    obj.experimentId = thisOb.currentElement.id;
                //});

                thisOb.currentElement.experimentTestSamples.forEach((obj, i) => {

                    obj.experimentId = thisOb.currentElement.id;
                });
            }

            thisOb.getSelectedParticipants(true);
            //thisOb.disableSelectedParticipants();
        },
        linkSpecialElementsFromObjectToUI() {
            var thisOb = this;

            if (!thisOb.currentElement.participants || thisOb.currentElement.participants.length < 1) {
                thisOb.addEmptyParticipant();
            }

            //if (!thisOb.currentElement.experimentBenchmarks || thisOb.currentElement.experimentBenchmarks.length < 1) {
            //    thisOb.addEmptyBenchmark();
            //}


            if (!thisOb.currentElement.experimentTestSamples || thisOb.currentElement.experimentTestSamples.length < 1) {
                thisOb.addEmptyTestSample();
            }

            // TODO: Do this some other way.. but the logic is that the values needs to be set when 
            // the popup opens!
            setTimeout(function () {

                thisOb.setSelectedParticipants();
                thisOb.checkDisablingOfAddingNewParticipant();

                var ctrlSmplTypeId = thisOb.currentElement.controlSample.controlSampleTypeId === null ? 0 : thisOb.currentElement.controlSample.controlSampleTypeId;
                thisOb.$refs.slctControlSampleType.$refs.slctDDL.setValue(ctrlSmplTypeId);
                thisOb.onControlSampleTypeChange(thisOb.currentElement.controlSample.controlSampleTypeId);
                thisOb.$refs.slctTeam.setValue(thisOb.currentElement.teamId);

                var reqIdToSet = thisOb.currentElement.requesterId;
                if (reqIdToSet && reqIdToSet != 0) thisOb.$refs.slctRequester.setValue(reqIdToSet);
                else thisOb.$refs.slctRequester.setValue(0);


                // Automatically onDatePickerChange change the binded model, because there is a bug
                // when working with model and Vue.js + datepicker
                var onSelectDatePickerFunc = function (selectedValue) {

                    if (selectedValue) {
                        thisOb.currentElement.controlSample.manufacturedDate = selectedValue;
                    }
                };

                var datePicker = new DatePickerHelper("ManufacturedDateDatePicker", null, onSelectDatePickerFunc);

                var dtPckr = document.getElementById('ManufacturedDateDatePicker');
                if (thisOb.isNewObject) {
                    dtPckr.value = ""; // You have to set Date picker to empty string again
                }

            }, 300);
        },
        // Maps all selected participants from all Select2's to object AND 
        // disables all of the already selected options on new Select2 instances!
        // Parameters:
        // clearAllEmpty: Clears all empty
        getSelectedParticipants(clearAllEmpty) {
            var thisOb = this;

            var newExpParticList = [];
            thisOb.selectedParticipantIds = [];
            thisOb.$refs.slctParticipant.forEach((obj, i) => {
                var participantId = thisOb.$refs.slctParticipant[i].getSelectValue();

                var foundExpParticipant = thisOb.currentElement.participants.find(el => el.participantId == participantId);

                // If you didn't found, then you picked a new Participant
                if (!foundExpParticipant || foundExpParticipant.participantId == "0") {

                    var newExpPar = new ExperimentsParticipant;
                    if (newExpParticList.length > 0) {
                        newExpPar.experimentIndex = newExpParticList.length;
                    }
                    else {
                        newExpPar.experimentIndex = 0;
                    }
                    //newExpPar.participant.id = participantId;
                    //newExpPar.participant = null;
                    newExpPar.participantId = participantId;
                    //newExpParticList.participant = null;
                    newExpParticList.push(newExpPar);
                }
                // If you found then it's the current one
                else {

                    if (newExpParticList.length > 0) {
                        foundExpParticipant.experimentIndex = newExpParticList.length;
                    }
                    else foundExpParticipant.experimentIndex = "0";
                    newExpParticList.push(foundExpParticipant);
                }

            })



            // TODO: Negdje treba postaviti da su ovi koji su submittani disableani


            thisOb.currentElement.participants = newExpParticList;


            // AND!!! If on any experiment you add empty participant and save and then open the empty experiment,
            // or the experiment that doesn't have any participants, the participant list is empty!!

            // ------------------------------------------------------------------


            if (clearAllEmpty && thisOb.currentElement.participants.length > 1) {

                thisOb.currentElement.participants = newExpParticList.filter(x => x.participantId != "0");
            }
        },
        disableSelectedParticipants() {
            var thisOb = this;

            thisOb.selectedParticipantIds = thisOb.currentElement.participants.map(x => x.participantId.toString());

            // Waits for a newly added Select2 instance to appear in the DOM
            Vue.nextTick(() => {


                // Go through every Select2 Participant instance and disable already picked ones
                thisOb.$refs.slctParticipant.forEach((obj, i) => {

                    var selValue = obj.getSelectValue();

                    $("#slctParticipant_" + i + "> option").each(function () {
                        var option = this;

                        var found = thisOb.selectedParticipantIds.indexOf(option.value);
                        if (option.value != 0 && option.value != selValue && found != -1) {
                            option.setAttribute("disabled", "");
                        }
                        else option.removeAttribute("disabled", "");
                    });
                });

            });
        },
        setSelectedParticipants() {
            var thisOb = this;

            thisOb.$refs.slctParticipant.forEach((obj, i) => {

                // TODO: Check this how to automate Select2 Loading
                thisOb.$refs.slctParticipant[i].ddlData = thisOb.participantsDDList;
                thisOb.$refs.slctParticipant[i].loadSelect2WithData();

                var currParticipant = thisOb.currentElement.participants[i];
                thisOb.$refs.slctParticipant[i].setValue(currParticipant.participantId);

                if (currParticipant.hasSubmittedResult) {
                    thisOb.$refs.slctParticipant[i].setDisabledSelect(true);
                }
                else {
                    thisOb.$refs.slctParticipant[i].setDisabledSelect(false);
                }
            })

            thisOb.getSelectedParticipants();
            thisOb.disableSelectedParticipants();
        },
        getTeams(teamIdToSet) {
            var thisOb = this;

            thisOb.postManager.get('/ApiTeam/GetDDList',
                function (response) {

                    response.data.data.unshift({ id: 0, text: 'Select option' })
                    thisOb.teamsList = response.data.data;
                    // TODO: Replace with Select option with globalText


                    // TODO: Check this how to automate Select2 Loading
                    thisOb.$refs.slctTeam.ddlData = thisOb.teamsList;
                    thisOb.$refs.slctTeam.loadSelect2WithData();
                    if (thisOb.isNewExperiment()) {
                        thisOb.postManager.get('/ApiTeam/GetTeamIdByLoggedUser', function (response) {
                            var id = response.data.data;
                            thisOb.$refs.slctTeam.setValue(id);
                        });
                    }

                    //if (teamIdToSet && teamIdToSet != 0) {
                    //    thisOb.$refs.slctTeam.$refs.slctDDL.setValue(teamIdToSet, null, true);
                    //}
                });
        },
        getRequesters() {
            var thisOb = this;

            thisOb.postManager.get('/ApiUser/GetRequesterDDList',
                function (response) {

                    response.data.data.unshift({ id: 0, text: 'Select option' })
                    thisOb.requestersList = response.data.data;
                    // TODO: Replace with Select option with globalText

                    // TODO: Check this how to automate Select2 Loading
                    thisOb.$refs.slctRequester.ddlData = thisOb.requestersList;
                    thisOb.$refs.slctRequester.loadSelect2WithData();

                    //if (teamIdToSet && teamIdToSet != 0) {
                    //    thisOb.$refs.slctTeam.$refs.slctDDL.setValue(teamIdToSet, null, true);
                    //}
                });
        },
        onRequesterChange() {
            var thisOb = this;

            var selectedRequesterId = thisOb.$refs.slctRequester.getSelectValue();

            // Change the Location of the Experiment to Requester's team when you pick the Requester and 
            // the Requester picked has Team in the DB
            if (selectedRequesterId && selectedRequesterId != 0) {

                thisOb.postManager.get("/ApiUser/GetBLUserByAppUserId" + "?appUserId=" + selectedRequesterId,
                    function (response) {
                        var selectedRequester = response.data.data;

                        if (selectedRequester && selectedRequester.teamId && selectedRequester.teamId != 0) {

                            var requestersTeamFound = thisOb.teamsList.find(el => el.id == selectedRequester.teamId);

                            if (requestersTeamFound) {
                                thisOb.$refs.slctTeam.setValue(selectedRequester.teamId);
                            }

                        }
                    });
            }
        },
        getParticipants() {
            var thisOb = this;

            thisOb.postManager.get('/ApiUser/GetParticipantsDDList',
                function (response) {

                    response.data.data.unshift({ fullname: '', id: 0 });
                    thisOb.participantsDDList = response.data.data;

                    // Select2 participants are set in setSelectedParticipants for now !!
                });
        },
        getControlSampleTypes(controlSampleTypeIdToSet) {
            var thisOb = this;

            thisOb.postManager.get('/ApiControlSampleType/GetDDList',
                function (response) {

                    thisOb.controlSampleTypeDDList = response.data.data;

                    // "Others" Removed on behalf of the client, so I just removed it from here..
                    thisOb.controlSampleTypeDDList = thisOb.controlSampleTypeDDList.filter(function (c) {
                        return c.id != 4;
                    });

                    if (controlSampleTypeIdToSet && controlSampleTypeIdToSet != 0) {
                        thisOb.$refs.slctControlSampleType.$refs.slctDDL.setValue(controlSampleTypeIdToSet, null, true);
                    }


                    // TODO: Check this how to automate Select2 Loading
                    thisOb.$refs.slctControlSampleType.$refs.slctDDL.ddlData = thisOb.controlSampleTypeDDList;
                    thisOb.$refs.slctControlSampleType.$refs.slctDDL.loadSelect2WithData();
                });
        },
        onControlSampleTypeChange(selectedId) {
            var thisOb = this;

            if (selectedId == 4) { // If you pick "Others" you get new fields to enter
                thisOb.othersPicked = true;
            }
            else {
                thisOb.othersPicked = false;
                thisOb.currentElement.controlSample.sampleCode = null;
                thisOb.currentElement.controlSample.sampleName = null;
            }
        },
        onParticipantChange() {
            var thisOb = this;

            thisOb.checkDisablingOfAddingNewParticipant();
            thisOb.getSelectedParticipants();
            thisOb.disableSelectedParticipants();
        },
        // If the last Select2 hasn't any picked Participants, disable adding a new one until user picks it
        checkDisablingOfAddingNewParticipant() {
            var thisOb = this;

            if (thisOb.currentElement.participants && thisOb.currentElement.participants.length > 0) {
                var lastIndex = thisOb.$refs.slctParticipant.length - 1;
                var lastParticipantSelect2Value = thisOb.$refs.slctParticipant[lastIndex].getSelectValue();
                if (!lastParticipantSelect2Value || lastParticipantSelect2Value == 0) {

                    thisOb.addingParticipantDisabled = true;
                    return;
                }
                else thisOb.addingParticipantDisabled = false;
            }
        },
        addEmptyParticipant() {
            var thisOb = this;

            if (thisOb.addingParticipantDisabled == true && thisOb.currentElement.participants && thisOb.currentElement.participants.length > 0) return;

            thisOb.postManager.get('/ApiExperiment/GetEmptyExperimentParticipants',
                function (response) {
                    var emptyObj = response.data.data;

                    if (!(thisOb.currentElement.participants) || thisOb.currentElement.participants.length == 0) {
                        thisOb.currentElement.participants = [];
                    }

                    thisOb.currentElement.participants.push(emptyObj);

                    Vue.nextTick(() => { // Wait for the Participant Select2 to be instantiated in HTML before you give it the data!

                        // TODO: Check this how to automate Select2 Loading
                        var lastParticipantIndex = thisOb.$refs.slctParticipant.length - 1;
                        thisOb.$refs.slctParticipant[lastParticipantIndex].ddlData = thisOb.participantsDDList;
                        thisOb.$refs.slctParticipant[lastParticipantIndex].loadSelect2WithData();


                        // If you already picked some participants, remove them from Select2 list so you cannot pick them again
                        if (thisOb.currentElement.participants && thisOb.currentElement.participants.length > 0) {
                            thisOb.getSelectedParticipants();
                            thisOb.disableSelectedParticipants();
                        }

                    });
                });

            thisOb.addingParticipantDisabled = true;
        },
        participantAdd() {
            var thisOb = this;

            thisOb.addEmptyParticipant();
        },
        // A little bit complicated logic because you can remove the Participant that is not 
        // last in the list but the v-for removes last instance!
        // Continue reading comments...
        participantRemove(index, currExperiment) {
            var thisOb = this;

            thisOb.addingParticipantDisabled = false;

            // 1. So, First you have to update the model so you later can 
            // update the select2's with remaining selected models
            thisOb.getSelectedParticipants();


            // 2. Then you delete picked Participant from the list
            Vue.delete(currExperiment.participants, index);

            // 3. EXECUTES AFTER current DOM update cycle,
            // for this case, after v-for finishes new iteration because we 
            // removed one Participant
            Vue.nextTick(() => {

                // 4. So now, finally you have n-1 Select2 instances and you have 
                // to update every instance with the remaining values from the model!
                thisOb.$refs.slctParticipant.forEach((obj, i) => {

                    // You use this for use cases where you delete the Participant in the middle and then
                    // you set his value, but when you are trying to read that value it is null, so we again fill it with data
                    obj.loadSelect2WithData();
                    // HACK: Update Select2's after DOM updates
                    obj.setValue(currExperiment.participants[i].participantId);
                });

                thisOb.disableSelectedParticipants();
            });
        },
        getExperimentStatuses() { // This is not used like this anymore!!
            var thisOb = this;

            thisOb.postManager.get('/ApiExperimentStatus/GetDDList',
                function (response) {

                    thisOb.experimentStatusDDList = response.data.data;
                });
        },
        // UNUSED ANYMORE
        experimentBenchmarkAdd() {
            var thisOb = this;

            var newexpBench = new ExperimentBenchmarks;
            newexpBench.experimentIndex = thisOb.currentElement.experimentBenchmarks.length;

            thisOb.currentElement.experimentBenchmarks.push(newexpBench);
        },
        // UNUSED ANYMORE
        experimentBenchmarkRemove(index, currExperiment) {
            var thisOb = this;

            // 1. You delete picked ExperimentBenchmark from the list
            Vue.delete(currExperiment.experimentBenchmarks, index);

            // 2. EXECUTES AFTER current DOM update cycle,
            // for this case, after v-for finishes new iteration because we 
            // removed one ExperimentBenchmark
            Vue.nextTick(() => {

                // 3. Set all experimentIndex to right value
                currExperiment.experimentBenchmarks.forEach((obj, i) => {

                    obj.experimentIndex = i;
                })
            });
        },
        // UNUSED ANYMORE
        addEmptyBenchmark() {
            var thisOb = this;

            thisOb.postManager.get('/ApiExperiment/GetEmptyExperimentBenchmark',
                function (response) {
                    var emptyObj = response.data.data;
                    if (!(thisOb.currentElement.experimentBenchmarks) || thisOb.currentElement.experimentBenchmarks.length == 0) {
                        thisOb.currentElement.experimentBenchmarks = [];
                    }

                    thisOb.currentElement.experimentBenchmarks.push(emptyObj);
                });
        },
        experimentTestSampleAdd() {
            var thisOb = this;

            var newexpTestSample = new ExperimentTestSample;
            newexpTestSample.experimentIndex = thisOb.currentElement.experimentTestSamples.length;

            thisOb.currentElement.experimentTestSamples.push(newexpTestSample);
        },
        experimentTestSampleRemove(index, currExperiment) {
            var thisOb = this;

            // 1. You delete picked ExperimentTestSample from the list
            Vue.delete(currExperiment.experimentTestSamples, index);

            // 2. EXECUTES AFTER current DOM update cycle,
            // for this case, after v-for finishes new iteration because we 
            // removed one ExperimentTestSample
            Vue.nextTick(() => {

                // 3. Set all experimentIndex to right value
                currExperiment.experimentTestSamples.forEach((obj, i) => {

                    obj.experimentIndex = i;
                })
            });
        },
        addEmptyTestSample() {
            var thisOb = this;

            thisOb.postManager.get('/ApiExperiment/GetEmptyExperimentTestSample',
                function (response) {
                    var emptyObj = response.data.data;
                    if (!(thisOb.currentElement.experimentTestSamples) || thisOb.currentElement.experimentTestSamples.length == 0) {
                        thisOb.currentElement.experimentTestSamples = [];
                    }

                    thisOb.currentElement.experimentTestSamples.push(emptyObj);
                });
        },
        isPublished() {
            var thisOb = this;

            var isPublished = thisOb.currentElement.isPublished;

            // With this class you remove the ability to click on little Calendar icon if the Exp is published
            if (isPublished) {
                $('#ManufacturedDateDatePickerEventListener').addClass('disabled-click');
                $('#label_id_slctControlTypeSample').addClass('disabled-click')
            }
            else {
                $('#ManufacturedDateDatePickerEventListener').removeClass('disabled-click');
                $('#label_id_slctControlTypeSample').removeClass('disabled-click')
            }

            return isPublished;
        },
        isNotEditable() {
            var thisOb = this;
            return thisOb.currentElement.isFinished;
        },
        // This is used when you publish and then want to edit Participants,
        // so you cannot publish anymore, you can just save
        isNotPublishable() {
            var thisOb = this;

            return thisOb.currentElement.isPublished || thisOb.currentElement.isFinished;
        },
        isNewExperiment() {
            var thisOb = this;
            return thisOb.currentElement.id == 0;
        },
        openControlSampleTypeEditModal(slctControlSampleId) {
            var thisOb = this;

            thisOb.$refs.controlSampleTypeEditCmp.openModalPopUp(slctControlSampleId);
        },
        onControlSampleTypeSaved(id) {
            var thisOb = this;

            thisOb.getControlSampleTypes(id);
        },
        // --------------------------------------------------------------------------------------
        // ALL DOWN UNDER THIS IS LEGACY AND IT IS POSSIBLE THAT IT WILL NOT BE USED ANYMORE!
        // --------------------------------------------------------------------------------------







        configureCheckBoxes() {
            var thisOb = this;

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('#ckbControlSample').on('ifChecked', function (event) {
                thisOb.currentElement.controlSample = true;
            });
            $('#ckbControlSample').on('ifUnchecked', function (event) {
                thisOb.currentElement.controlSample = false;
            });

            $('#ckbRandomization').on('ifChecked', function (event) {
                thisOb.currentElement.randomization = true;
            });
            $('#ckbRandomization').on('ifUnchecked', function (event) {
                thisOb.currentElement.randomization = false;
            });
        },
        mapCheckboxesWithValues() {
            var thisOb = this;

            if (thisOb.currentElement.controlSample)
                $('#ckbControlSample').iCheck('check');
            else
                $('#ckbControlSample').iCheck('uncheck');

            if (thisOb.currentElement.randomization)
                $('#ckbRandomization').iCheck('check');
            else
                $('#ckbRandomization').iCheck('uncheck');
        },
        getTestTypes() {
            var thisOb = this;

            thisOb.postManager.get('/ApiExperiment/GetTestTypeDDList',
                function (response) {
                    thisOb.testTypeList = response.data.data;
                });
        },
        getScoringScaleDefinitions() {
            var thisOb = this;

            thisOb.postManager.get('/ApiExperiment/GetScoringScaleDefDDList',
                function (response) {
                    thisOb.scoringScaleDefList = response.data.data;
                });
        }
    }
});