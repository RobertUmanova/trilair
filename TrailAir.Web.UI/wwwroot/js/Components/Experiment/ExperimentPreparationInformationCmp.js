﻿Vue.component('experiment-preparation-information', {
    template: '#ExperimentPreparationInformationTemp',
    props: { // This is like contructor parameters
        onUpdateSuccessFunc: null
    },
    watch: { // Watches for changes in data

    },
    data() {
        return {
            postManager: new PostManager(),
            apiController: "/ApiExperiment/",
            modalId: "#ExperimentPreparationInformationCmp",
            experimentId: 0,
            currentElement: new Experiment(),
        };
    },
    mounted() {

    },
    methods: {
        openModalPopUp(experimentId) {
            var thisOb = this;

            thisOb.experimentId = experimentId;
            thisOb.getData();

            $(thisOb.modalId).modal();
        },
        getData() {
            var thisOb = this;

            thisOb.postManager.get(thisOb.apiController + thisOb.experimentId, function (response) {
                if (response.data.success) {
                    thisOb.currentElement = response.data.data;
                } else {
                    thisOb.closeDetModal();
                }
            });
        },
        closeDetModal() {
            var thisOb = this;

            $(thisOb.modalId).modal('toggle');
        },
        printPreparationInformation() {
            var thisOb = this;

            var printContainerId = 'printContainer';

            $('#' + printContainerId).css('font-size', '26px');
            $('#experimentName').css('font-size', '32px');

            var printHelper = new PrintHTMLElementHelper(printContainerId);
            printHelper.print();

            $('#' + printContainerId).css('font-size', 'inherit');
            $('#experimentName').css('font-size', 'inherit');
        },
        createTestSampleName(testSample) {
            var thisOb = this;

            var regularComputedName = "S" + testSample.testSample.sampleIndex + ": " + testSample.testSample.computedName

            if (testSample.testSample.isInternal && testSample.testSample.concentrationStr) {

                // TODO: Static text
                regularComputedName = regularComputedName + ' - Concentration ' + testSample.testSample.concentrationStr
            }
            return regularComputedName;
        }
    }
});