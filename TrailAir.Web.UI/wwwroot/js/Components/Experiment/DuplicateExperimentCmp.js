﻿Vue.component('experiment-duplicate', {
    template: '#ExperimentDuplicateTemp',
    props: { // This is like contructor parameters
        onUpdateSuccessFunc: null
    },
    watch: { // Watches for changes in data

    },
    data() {
        return {
            postManager: new PostManager(),
            apiController: "/ApiExperiment/",
            modalId: "#ExperimentDuplicateCmp",
            experimentId: 0,
            newExperimentName: '',
            parentGetData: null
        };
    },
    mounted() {

    },
    methods: {
        openModalPopUp(experimentId) {
            var thisOb = this;

            thisOb.experimentId = experimentId;

            $(thisOb.modalId).modal();
        },
        closeDetModal() {
            var thisOb = this;

            thisOb.newExperimentName = '';
            $(thisOb.modalId).modal('toggle');
        },
        duplicateExperiment() {
            var thisOb = this;
            thisOb.postManager.get(thisOb.apiController + 'DuplicateBo?id=' + thisOb.experimentId + '&newName=' + thisOb.newExperimentName, function (response) {
                if (!response.data.success) {
                    alertOb.errorMessage("Attention", "An error occured");
                    // TODO: Replace with global text!
                    event.preventDefault();
                }
                else {
                    thisOb.closeDetModal();
                    thisOb.parentGetData();
                    ToastrHlp.updateSuccessMsg();
                }
            });

        }
    }
});