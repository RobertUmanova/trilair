﻿/**
 * Depends on DateOb_S7.js, and datepicker js and css in layout.cshtml
 * @param {any} startDateId
 * @param {any} endDateId
 * @param {any} thisOb
 */
function DatePickerHelper(startDateId, endDateId, onDPChange, defaultValue) {
    var settings = {
        todayBtn: "linked",
        keyboardNavigation: true,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: DateOb.dateFormat,
        language: lanUiLangShort,
    };

    //var onDPChange = onDPChange;

    this.datepickerPlanningStart = $('#' + startDateId).datepicker(settings).on('changeDate', function (selected) {
        var thisOb = this;

        //we have to be sure End date cannot be lesser then Start date
        var startDate = new Date(selected.date.valueOf());
        $('#' + endDateId).datepicker('setStartDate', startDate);

        // 02.06.22. - This is the fix because VUEJS conflicts with the datePicker when using v-model input
        // So you just pass the function where you set your binded model to new value
        // I didn't go by this solution but here is another one:
        // https://stackoverflow.com/questions/66991453/vue-js-conflicts-with-datepicker-and-removes-date-value-after-changing-next-v-m

        var dpContainer = document.getElementById(startDateId);

        if (dpContainer) {
            var dpInput = dpContainer.getElementsByTagName('input')[0];

            if (dpInput) {
                var pickedDate = dpInput.value;
                if (onDPChange) onDPChange(pickedDate);
            }
        }
    });

    // 07.07.22. - If you clear the date from date picker the 'clearDate' is triggered, not 'changeDate'
    // So just set your variable to empty date
    this.datepickerPlanningStart = $('#' + startDateId).datepicker(settings).on('clearDate', function (selected) {

        if (onDPChange) {
            var pickedDate = null;
            onDPChange(pickedDate);
        }
    });

    // set default value
    if (defaultValue) {
        $('#' + startDateId).datepicker("setDate", defaultValue);
    }

    this.datepickerPlanningEnd = $('#' + endDateId).datepicker(settings).on('changeDate', function (selected) {

        //we have to be sure Start date cannot be greater then End date
        var fromEndDate = new Date(selected.date.valueOf());
        $('#' + startDateId).datepicker('setEndDate', fromEndDate);
    });

    //FIX the Date picker initial start and end date limits &&  fix dates when thet are changed but not saved in calendar when reopening modal
    //if (thisOb.planningStart && thisOb.planningEnd)
    //{
    //    var startDateLimit = new DateOb(thisOb.planningStart);
    //    var endDateLimit = new DateOb(thisOb.planningEnd);

    //    $('#' + endDateId).datepicker("setStartDate", startDateLimit.date);
    //    $('#' + endDateId).datepicker("setDate", endDateLimit.date);

    //    $('#' + startDateId).datepicker("setEndDate", endDateLimit.date);
    //    $('#' + startDateId).datepicker("setDate", startDateLimit.date);
    //}

    this.GetDurationOnIncrement = function (thisOb, incrementInDays) {
        var result = 0;
        var dateObjectStart = new DateOb(thisOb.planningStart);
        var dateObjectEnd = new DateOb(thisOb.planningEnd);
        dateObjectStart.date.toLocaleString("de-CH", { timeZone: "Europe/Zurich" });
        dateObjectEnd.date.toLocaleString("de-CH", { timeZone: "Europe/Zurich" });

        if (parseInt(dateObjectEnd.fullCmp) - parseInt(dateObjectStart.fullCmp) > 0 || incrementInDays > 0) {
            var endDate = new DateOb(thisOb.planningEnd).addDays(incrementInDays);
            thisOb.planningEnd = endDate.printDate;

            var dateStart = thisOb.planningStart;
            var dateEnd = thisOb.planningEnd;
            if (!StringOb.IsStringNullOrEmpty(dateStart) && !StringOb.IsStringNullOrEmpty(dateEnd)) {
                var dateObjectStart = new DateOb(dateStart);
                var dateObjectEnd = new DateOb(dateEnd);

                // select new value of end date on the calendar when incrementing days
                $('#' + endDateId).datepicker("setDate", dateObjectEnd.date);
                var diff = Math.floor((Date.UTC(dateObjectEnd.date.getFullYear(), dateObjectEnd.date.getMonth(), dateObjectEnd.date.getDate()) - Date.UTC(dateObjectStart.date.getFullYear(), dateObjectStart.date.getMonth(), dateObjectStart.date.getDate())) / (1000 * 60 * 60 * 24));
                var Difference_In_Time = parseInt((dateObjectEnd.date - dateObjectStart.date) / (1000 * 3600 * 24));

                result = diff;
            }
        }
        return result;
    }

    this.GetDurationOnInput = function (thisOb) {
        var result = 0;
        var dateStart = thisOb.planningStart;
        var dateEnd = thisOb.planningEnd;
        if (!StringOb.IsStringNullOrEmpty(dateStart) && !StringOb.IsStringNullOrEmpty(dateEnd)) {
            var dateObjectStart = new DateOb(dateStart);
            var dateObjectEnd = new DateOb(dateEnd);
            dateObjectStart.date.toLocaleString("de-CH", { timeZone: "Europe/Zurich" });
            dateObjectEnd.date.toLocaleString("de-CH", { timeZone: "Europe/Zurich" });

            // select new value of end date on the calendar when incrementing days
            $('#' + endDateId).datepicker("setDate", dateObjectEnd.date);
            var diff = Math.floor((Date.UTC(dateObjectEnd.date.getFullYear(), dateObjectEnd.date.getMonth(), dateObjectEnd.date.getDate()) - Date.UTC(dateObjectStart.date.getFullYear(), dateObjectStart.date.getMonth(), dateObjectStart.date.getDate())) / (1000 * 60 * 60 * 24));
            var Difference_In_Time = parseInt((dateObjectEnd.date - dateObjectStart.date) / (1000 * 3600 * 24));

            result = diff;
        }
        return result;
    }
}
