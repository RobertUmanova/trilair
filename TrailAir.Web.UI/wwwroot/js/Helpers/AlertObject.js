﻿function AlertObject() {
}

AlertObject.prototype.confirmMessage = function (message, onConfirm) {
    var title = message ? message : globalText.DeleteConfirmTitle;
    var text = message ? "" : globalText.DeleteConfirmText;

    swal({
        title: title,
        text: text,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: globalText.Yes + "!",
        cancelButtonText: globalText.Cancel,
        closeOnConfirm: true
    }, onConfirm);
    
};

AlertObject.prototype.confirmMessageFull = function (title, text, type, showCancelButton, confirmButtonColor, confirmButtonText, cancelButtonText, closeOnConfirm, onConfirm) {

    swal({
        title: title ? title : "",
        text: text ? text : "",
        type: type ? type : "warning",
        showCancelButton: (showCancelButton != null && showCancelButton == false) ? showCancelButton : true,
        confirmButtonColor: confirmButtonColor ? confirmButtonColor : "#DD6B55",
        confirmButtonText: confirmButtonText ? confirmButtonText : globalText.Yes + "!",
        cancelButtonText: cancelButtonText ? cancelButtonText : globalText.Cancel,
        closeOnConfirm: (closeOnConfirm != null && closeOnConfirm == false) ? closeOnConfirm : true
    }, onConfirm);
};


AlertObject.prototype.confirmMessageCustom = function (message, onConfirm) {
    var title = message ? message : title;
    var text = message ? "" : text;

    swal({
        title: title,
        text: text,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: globalText.Yes + "!",
        cancelButtonText: globalText.No,
        closeOnConfirm: true
    }, onConfirm);

};


AlertObjectchoice2Message = function (choice) {


    swal({
        title: "User input required.",
        text: choice.text,
        animation: "slide-from-top",
        showCancelButton: true,
        confirmButtonColor: "#18a689",
        cancelButtonColor: "#18a689",
        confirmButtonText: choice.option1Txt,
        cancelButtonText: choice.option2Txt,
        closeOnConfirm: true,
        closeOnCancel: true
    }, function (isConfirm) {
        if (isConfirm) {
            choice.onOption1();
        } else {
            choice.onOption2();
        }
    });
};

AlertObject.prototype.message = function (message) {
    swal({
        title: message + " " + globalText.HoursWithinLimit,
        text: globalText.HourRangeMinMax,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: true
    });

    
    
}

AlertObject.prototype.warningMessage = function (title, message, onConfirm) {
    var onConfirmFunc = onConfirm || null;

    swal({
        title: title,
        text: message,
        type: "warning",
        showCancelButton: false,
        confirmButtonColor: "#f8ac59",
        confirmButtonText: "Ok",
        closeOnConfirm: true
    }, onConfirmFunc);
}




AlertObject.prototype.errorMessage = function (title, message, onConfirm) {
    var onConfirmFunc = onConfirm || null;

    swal({
        title: title,
        text: message,
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: true
    }, onConfirmFunc);
}

var alertOb = new AlertObject();
