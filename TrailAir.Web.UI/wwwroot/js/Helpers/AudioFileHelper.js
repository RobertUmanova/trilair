﻿function AudioFileHelper(newAudioRecording, buttonId, audioElement, minutesId, secondsId, onUploadFinishedFunction, onUploadFinishCall, audioFileUrl)
{
    this.hasAudio = false;
    this.buttonId = buttonId;
    this.audioElement = $('#' + audioElement);
    this.newAudioRecording = $('#' + newAudioRecording);

    this.isRecording = false;
    this.onUploadFinishedFunc = onUploadFinishedFunction;
    this.onUploadFinishCaller = onUploadFinishCall;

    var thisOb = this;
    this.minutesLabel = $('#' + minutesId);
    this.secondsLabel = $('#' + secondsId);
    this.button = $('#' + buttonId);
    this.audioPlayer = document.querySelector('#' + audioElement);


    var totalSeconds = 0;
    this.secondsLabel.html("00");
    this.minutesLabel.html("00");
    this.audioChunks = []
    this.audioBlob;

    if (audioFileUrl == null) {
        $(thisOb.audioPlayer).hide();
        $(thisOb.newAudioRecording).show();

    } else
    {
        thisOb.audioPlayer.src = audioFileUrl;
        $(thisOb.audioPlayer).show();
        $(thisOb.newAudioRecording).hide();


    }

    navigator.mediaDevices.getUserMedia({ audio: true }).then(stream => {
        const mediaRecorder = new MediaRecorder(stream)
        // counterStop => with this we only allow for audio to be recorded only once!

        var counterStop = 0;
        thisOb.button.on('click', () => {
            if (thisOb.isRecording) {
                mediaRecorder.stop()
                this.isRecording = false;
                counterStop++;
            } else{
                if (counterStop == 0)
                {
                    mediaRecorder.start();
                    this.isRecording = true;
                }
            }



            //thisOb.secondsLabel.html("00");
            //thisOb.minutesLabel.html("00");

            //totalSeconds = 0;

            //function resetTimer () {

            //    thisOb.secondsLabel.html("00");
            //    thisOb.minutesLabel.html("00");
            //};

            function setTime() {
                if (thisOb.isRecording)
                {
                    setTimeout(function () { setTime() }, 1000);
                    thisOb.secondsLabel.html(pad(totalSeconds % 60));
                    thisOb.minutesLabel.html(pad(parseInt(totalSeconds / 60)));
                    ++totalSeconds;
                }
            }
            function pad(val) {
                var valString = val + "";
                if (valString.length < 2) {
                    return "0" + valString;
                } else {
                    return valString;
                }
            }
            if (thisOb.isRecording) {
                thisOb.button.removeClass("stoppedButton");
                thisOb.button.addClass("recordingButton");
                //resetTimer();
                thisOb.counter = setTime();
            } else
            {
                thisOb.button.removeClass("recordingButton");
                thisOb.button.addClass("stoppedButton");
                //clearInterval(setTime);
            }

            //if (button.hasClass('stoppedButton')) {
            //    button.removeClass("stoppedButton");
            //    button.addClass("recordingButton");
            //    this.resetTimer();
            //    this.counter = setInterval(setTime, 1000);
            //}
            //else {
            //    button.removeClass("recordingButton");
            //    button.addClass("stoppedButton");
            //    clearInterval(this.counter);
            //}

        })


        mediaRecorder.addEventListener('dataavailable', (e) => {
            thisOb.audioChunks.push(e.data);


        })

        mediaRecorder.addEventListener("stop", () => {
            thisOb.audioBlob = new Blob(thisOb.audioChunks, { 'type': 'audio/ogg; codecs=opus' });
            const audioUrl = URL.createObjectURL(thisOb.audioBlob)
            thisOb.audioPlayer.src = audioUrl;
            $(thisOb.audioPlayer).show();
        })

        //uploadButton.addEventListener("click", () => {
        //    thisOb.uploadFile();
        //})
        const toggleStatus = () => statusEl.classList.toggle("hide");

        /**
         * 
         * @param {Blob} audioBlob 
         */
        //const uploadFile = () => {
        //}
    })

    AudioFileHelper.prototype.uploadFile = function (thisOb) {
        
        var formData = new FormData()
        var url = "/" + lanUiLangFull + "/ApiDocument/UploadFile"
        formData.append('file', thisOb.audioBlob)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((data) => thisOb.onUploadFinishedFunc(data, thisOb.onUploadFinishCaller))
    }
}
