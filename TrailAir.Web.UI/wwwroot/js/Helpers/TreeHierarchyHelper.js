﻿
/**
 * TreeHierarchyHelper depends on
    <script src="~/lib/jstree/dist/jstree.js"></script>
    <script src="~/lib/jstree/src/misc.js"></script>
 * @param {any} elementId
 */
function TreeHierarchyHelper(elementId) {
    var elementId = '#' + elementId;

    $(elementId).jstree(
        {
            "types": {
                "default": {
                    "icon": "glyphicon glyphicon-flash"
                },
                "demo": {
                    "icon": "glyphicon glyphicon-ok"
                }
            },
            "plugins": ["wholerow", "types"]
        }
    );
    $(elementId).jstree(true).settings.core.data = [];
    $(elementId).jstree(true).refresh();


    TreeHierarchyHelper.prototype.Clean = function () {
        $(elementId).jstree(true).settings.core.data = [];
        $(elementId).jstree(true).refresh();
    }

    TreeHierarchyHelper.prototype.GetData = function () {
        return $(elementId).jstree(true).get_json('#', { flat: true });
    }

    TreeHierarchyHelper.prototype.SetData = function (data) {

        $(elementId).jstree('destroy');
        $(elementId).jstree({
            core: {
                data: data,
                themes: {
                    icons: false
                }
            },
            checkbox: {
                keep_selected_style: false,
                tie_selection: false,
                three_state: false,
                //cascade: 'up+down',
                cascade: 'down'
            },
            node_customize: {
                default: function (el, node) {

                    var input = (node.state.checked == true) ?
                        $(`<input type="number" min="0" class="uma-tree-item uma_anchor_input" id=${node.id + "_anchor_input"} />`) :
                        $(`<input type="number" min="0" class="uma-tree-item uma_anchor_input" style="display: none;" id=${node.id + "_anchor_input"} />`);

                    if (input.val() == "") {
                        input.val(node.original.quantity);
                        //node.quantity = node.original.quantity;

                        input.change(function (e) {
                            node.original.quantity = parseInt(input.val());
                            if (input.val() == 0) {
                                var targetId = e.target.id.split("_")[0];
                                console.log(targetId);
                                $("#jstree").jstree("uncheck_node", `${targetId}`);
                            }
                        });

                    }
                    /*$(el).find('a').after(input);*/
                    $(el).find(`#${node.id + "_anchor"}`).after(input);
                }
            },
            plugins: ['checkbox', 'node_customize']
        });
        this.SetJsTreeFunctions();
    }

    TreeHierarchyHelper.prototype.SetJsTreeFunctions = function ()
    {
         //// after the jstree has been rendered to following:
        $(elementId).on("ready.jstree", function (e, data) {

            //const elements = document.querySelectorAll('.uma_anchor_input');
            ////add event listeners to inputs
            //$('.uma_anchor_input').live('change', function () {
            //    var value = $(this).val();
            //    alert(value);
            //});
            // set all checkboxes to unchecked, comment this if  unwanted behaviour
            $("#jstree").jstree(true).uncheck_all();

            // Fix for: main parentLocation input is visible in the beginning and that is unwanted!
            $('.uma_anchor_input').hide();
        });

        // When Node is selected what to do
        $(elementId).on("select_node.jstree", function (e, data) {
        });

        // When Nodes checkbox is checked what to do
        $(elementId).on("check_node.jstree", function (e, data) {

            var treeData = $(elementId).jstree(true).get_json(`${data.node.id}`, { flat: false })
            var input = $(`#${data.node.id + "_anchor_input"}`);
            input.show();

            if (treeData.children.length > 0)
                showInput(treeData.children);

            var parentId = $(elementId).jstree(true).get_parent(data.node);
            if (parentId == false || parentId == '#')
                return;

            //if parent is unselected/unchecked then children are automatically unselected and cannot be selected, comment all below if it is undesired behaviour
            //var parentNode = $('#jstree').jstree(true).get_node(parentId);
            //if (parentNode.state.checked == false)
            //    $('#jstree').jstree(true).uncheck_node(data.node);
        });

        // When Nodes checkbox is unchecked what to do
        $(elementId).on("uncheck_node.jstree", function (e, data) {
            var treeData = $(elementId).jstree(true).get_json(`${data.node.id}`, { flat: false })

            var input = $(`#${data.node.id + "_anchor_input"}`);
            input.changeVal(1);
            input.hide();

            if (treeData.children.length > 0)
                hideInput(treeData.children);
        });


        // proxy to fire change event 
        $.fn.changeVal = function (v) {
            return this.val(v).trigger("change");
        }

        // recursive function to hide all input elements
        hideInput = function (treeData) {

            treeData.forEach(node => {
                var input = $(`#${node.id + "_anchor_input"}`);
                input.changeVal(1);
                input.hide();

                if (node.children.length > 0) {

                    hideInput(node.children);
                }

            });
        }

        // recursive function to show all input elements
        showInput = function (treeData) {

            treeData.forEach(node => {
                var input = $(`#${node.id + "_anchor_input"}`);
                input.show();

                if (node.children.length > 0) {

                    showInput(node.children);
                }
            });
        }
    }
}
