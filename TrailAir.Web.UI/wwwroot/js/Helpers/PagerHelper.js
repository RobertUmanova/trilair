﻿
// When you are using this Pager make sure to return JsonResponseOb from the backend 
// because the data returned is: response.data.data

function PagerHelper(pagerContainerId, url, onResponse, pageSize, displayedPageSpan, pagerContainerIdSecond) {

    //html element which will be the root of a generated pager (e.g. div id)
    this.pagerContainerId = pagerContainerId;

    //how many previous-next pages will pager show (e.g. if currentPage=5 and displayedPageSpan=3, pager will show:  << < 2 3 4  5  6 7 8 > >>)
    this.displayedPageSpan = displayedPageSpan || 3;

    //clientPager object which contains info about current page, will be returned from server
    this.pageInfo = {
        currentPage: 1,
        pageSize: pageSize,
        pageCount: 1
    };

    // if you have additional (usually TOP pager container)
    this.pagerContainerIdSecond = pagerContainerIdSecond;

    //server endpoint address for getting data
    this.url = url;

    //method to be called when pager receives response and updates itself
    this.onResponse = onResponse;

    //loader helper
    this.postManager = new PostManager();

    //redraws pager based on current page
    if (this.pagerContainerIdSecond) {
        this.refreshPager(this.pagerContainerIdSecond);
    }
    else {
        this.refreshPager();
    }
}


PagerHelper.prototype.refreshPager = function (localContainerId) {
    var thisOb = this;

    var container = localContainerId ? $('#' + localContainerId) : $('#' + thisOb.pagerContainerId);
    container.empty();
    container.append('<ul class="pagination">');

    var pageList = container.find('ul');

    if (thisOb.pageInfo.currentPage > 1) {
        //first page
        var first = $('<li class="paginate_button page-item previous"></li>');
        var firstLink = $('<a class="page-link">' + globalText.First + '</a>');
        //var firstLink = $('<a class="page-link">&lt;&lt;</a>');
        firstLink.click(function () {
            thisOb.getPage(1);
        });
        first.append(firstLink);
        pageList.append(first);

        //previous page
        var previous = $('<li class="paginate_button page-item previous"></li>');
        var previousLink = $('<a class="page-link">' + globalText.Previous + '</a>');
        //var previousLink = $('<a class="page-link">&lt;</a>');
        previousLink.click(function () {
            thisOb.getPage(thisOb.pageInfo.currentPage - 1);
        });
        previous.append(previousLink);
        pageList.append(previous);
    }

    for (var i = Math.max(1, thisOb.pageInfo.currentPage - thisOb.displayedPageSpan); i <= Math.min(thisOb.pageInfo.currentPage + thisOb.displayedPageSpan, thisOb.pageInfo.pageCount); i++) {
        var cssClass = (i == thisOb.pageInfo.currentPage) ? 'active' : '';
        var li = $('<li class="paginate_button page-item ' + cssClass + '">');
        var a = $('<a class="page-link">' + i + '</a>');
        a.click(function () {
            thisOb.getPage(parseInt(this.text));
        });
        li.append(a);
        pageList.append(li);
    }

    if (thisOb.pageInfo.currentPage < thisOb.pageInfo.pageCount) {
        //next page
        var next = $('<li class="paginate_button page-item next"></li>');
        var nextLink = $('<a class="page-link">' + globalText.Next + '</a>');
        //var nextLink = $('<a class="page-link">&gt;</a>');
        nextLink.click(function () {
            thisOb.getPage(thisOb.pageInfo.currentPage + 1);
        });
        next.append(nextLink);
        pageList.append(next);

        //last page
        var last = $('<li class="paginate_button page-item next"></li>');
        //var lastLink = $('<a class="page-link">&gt;&gt;</a>');
        var lastLink = $('<a class="page-link">' + globalText.Last + '</a>');
        lastLink.click(function () {
            thisOb.getPage(thisOb.pageInfo.pageCount);
        });
        last.append(lastLink);
        pageList.append(last);
    }
};

PagerHelper.prototype.getPage = function (page) {
    var thisOb = this;

    thisOb.pageInfo.currentPage = page;
    thisOb.getCurrentPage();
}

PagerHelper.prototype.getCurrentPage = function () {
    var thisOb = this;

    var suffix = (thisOb.url.includes("?")) ? '&' : '?';

    thisOb.postManager.get(thisOb.url + suffix + "page=" + thisOb.pageInfo.currentPage + "&pageSize=" + thisOb.pageInfo.pageSize,
        function (response) {
            thisOb.pageInfo.currentPage = response.data.data.currentPage;
            thisOb.pageInfo.pageCount = response.data.data.pageCount;
            thisOb.refreshPager();

            if (thisOb.pagerContainerIdSecond) {
                thisOb.refreshPager(thisOb.pagerContainerIdSecond);
            }

            thisOb.onResponse(response);
        });
};
