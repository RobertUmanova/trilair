﻿
function FileHelper() {

}

FileHelper.ImagePreviewIconName = 'iconPreview.png';

// not used
FileHelper.GetImageName = function (documentName) {
    var imageName = "";
    var length = documentName.split(".").length;
    var docType = documentName.split(".")[length - 1];
    switch (docType) {
        case "pdf":
            imageName = 'iconPDF.png';
            break;
        case "xlsx":
            imageName = 'iconExcel.png';
            break;
        case "xls":
            imageName = 'iconExcel.png';
            break;
        case "png":
            imageName = FileHelper.ImagePreviewIconName;
            break;
        case "jpg":
            imageName = FileHelper.ImagePreviewIconName;
            break;
        case "jpeg":
            imageName = FileHelper.ImagePreviewIconName;
            break;
        case "doc":
            imageName = 'iconWord.png';
            break;
        case "docx":
            imageName = 'iconWord.png';
            break;
        case "csv":
            imageName = 'iconCSV.png';
            break;
        case "txt":
            imageName = 'iconTxt.png';
            break; default:
        // code block
    }
    return imageName;
}

FileHelper.AddFileLink = function (document, td, onEmptyText) {
    if (document) {

        var imgName = FileHelper.GetImageName(document.name);

        var imgClass = '';
        var onClick = 'event.stopPropagation();';
        var href = "/" + lanUiLangFull + '/ApiDocument/DownloadFile?elementId=' + document.id + '&documentName=' + document.name;

        if (imgName == FileHelper.ImagePreviewIconName) {
            imgClass = 'imgPreview';
            onClick = "FileHelper.previewImage(" + document.id + ", '" + document.name + "');event.stopPropagation()";
            href = 'javascript:void(0);';
        }

        $(td).append('<a class="clickable text-secondary" onclick="' + onClick + '" href="' + href + '"><img src = "/Content/Pics/' + imgName + '" style = "height:25px;" class= ' + imgClass + ' /></a>');
    }
    else if (onEmptyText) {
        $(td).append(onEmptyText);
    }
}

FileHelper.FindAndAddFileLink = function (tableId, onEmptyText) {
    var elements = $('#' + tableId).find("[data-file-id]");
    if (elements) {
        for (var i = 0; i < elements.length; i++) {
            var document = null;
            var id = $(elements[i]).attr('data-file-id');
            var name = $(elements[i]).attr('data-file-name');
            if (id != '' && name != '')
                document = { id: id, name: name };
            FileHelper.AddFileLink(document, elements[i], onEmptyText)
        }
    }

    //Adding the hover option just for images
    //$(".imgPreview").hover(function (e) {
    //    $("#large")
    //        .css("top", (e.pageY - 55) + "px")
    //        .css("left", (e.pageX - 240) + "px")
    //        .html("<img style=' max-height: 200px; max-width: 400px;' src='" + $(this).closest('.tdImgPreview').attr("data-thumbnail-src") + "' />")
    //        .fadeIn("slow");
    //}, function () {
    //    $("#large").fadeOut("fast");
    //});


}

FileHelper.previewImage = function (imageId, imageName) {
    var modal = document.getElementById("myModal-picture");
    var modalImg = document.getElementById("imgPreview-picture");
    var captionText = document.getElementById("caption-picture");
    modal.style.display = "block";
    modalImg.src = "/" + lanUiLangFull + '/ApiDocument/DownloadFile?elementId=' + imageId;
    captionText.innerHTML = imageName;
}

FileHelper.previewImageDetails = function (image) {
    var modal = document.getElementById("myModal-picture");
    var modalImg = document.getElementById("imgPreview-picture");
    var pictureName = document.getElementById("caption-picture");


    modal.style.display = "block";
    modalImg.src = image.documentSrc;
    pictureName.innerHTML = image.document.name;
}

FileHelper.closePopUp = function () {
    var modal = document.getElementById("myModal-picture");
    modal.style.display = "none";
}
FileHelper.GetExtension = function (documentName) {
    var length = documentName.split(".").length;
    var docType = documentName.split(".")[length - 1];
    return docType;
}




