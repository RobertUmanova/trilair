﻿
/**
 * ImageHelper is used to create Dropzone for the image
 *
 * Parameters :
 * 1. div fileContainerId,
 * 2. div dropzoneId,
 * 3. url pointing to the ApiDocument controller and specific method
 * 4. method which will be called when we upload mage to the server
 * 5. Object to which we will add image Id from onComplete response
 * 
 * Dependencies:
 *  current language, variable defined as lanUiLangFull from Views/Shared/_Localization.cshtml,
 *  /lib/dropzone/dropzone.js,
 *  js/Helpers/FileHelper_S3.js,
 *  js/DropzoneHelper_SingleFile_S6.js
 */
function DocumentsMultipleFileHelper(fileContainerId, fileDropzoneId, onComplete, onCompleteCaller, wideImage, inputConfiguration) {
    this.fileDropzoneId = fileDropzoneId;
    this.fileContainerId = fileContainerId;
    this.url = "/" + lanUiLangFull + '/ApiDocument/UploadFile';
    this.hasFile = false;
    this.fileSrc = "";
    this.fileName = '';
    this.wideImage = wideImage;
    this.currentBo = onCompleteCaller;

    this.configuration = {
        onComplete: onComplete,
        onCompleteCaller: onCompleteCaller,
        acceptedFormats: '.pdf',
        displayType: 'standard',
        maxFiles: 3,
        maxFilesize: 50
    }

    if (inputConfiguration) {
        /*  
         *  jQuery.extend( [deep ], target, object1 [, objectN ] )
         *  deep
         *  Type: Boolean
         *  If true, the merge becomes recursive (aka. deep copy). Passing false for this argument is not supported.
         *  
         *  In our case Error happened : inputConfiguration had empty currentBo, and empty currentBo was sent as a parameter to thisObject.configuration.onComplete
        */

        //$.extend(true, this.configuration, inputConfiguration);
        $.extend(this.configuration, inputConfiguration);
    }

    this.dropzoneHelper = new DropzoneHelper(this.fileDropzoneId, this.url, this.configuration);
    //this.resizeFileContainer();
}

DocumentsMultipleFileHelper.prototype.processFile = function (file) {
    var thisOb = this;
    if (file) {
        thisOb.hasFile = true;
        thisOb.fileSrc = "/" + lanUiLangFull + '/ApiDocument/DownloadFile?elementId=' + file.id;

        //// hide file extension from the image name when opening an existing object(for edit)
        file.name = file.name.split(".")[0];

        thisOb.fileName = file.name;
        $(thisOb.fileContainerId).click(function () {
            FileHelper.previewImage(file.id, file.name);
        });
    }
    else {
        thisOb.resetProperties();
    }
};

DocumentsMultipleFileHelper.prototype.resizeFileContainer = function (image) {
    var thisOb = this;
    var container = $(thisOb.fileContainerId);
    if (thisOb.wideImage == true) {
        container.css("height", "150px");
        container.css("min-height", "150px");
        container.css("width", "100%");
    }
    else {
        container.css("height", "120px");
        container.css("min-height", "120px");
        container.css("width", "100px");
    }
};

DocumentsMultipleFileHelper.prototype.uploadNewFile = function () {
    var thisOb = this;

    thisOb.resetProperties();
};

DocumentsMultipleFileHelper.prototype.saveFile = function () {
    var thisOb = this;

    return thisOb.dropzoneHelper.processQueue();
};

DocumentsMultipleFileHelper.prototype.clearDropzone = function () {
    var thisOb = this;

    thisOb.dropzoneHelper.resetDropzoneWithSingleFile();
};



DocumentsMultipleFileHelper.prototype.resetProperties = function () {
    var thisOb = this;

    thisOb.hasFile = false;
    thisOb.fileSrc = null;
    thisOb.fileName = null;
};


