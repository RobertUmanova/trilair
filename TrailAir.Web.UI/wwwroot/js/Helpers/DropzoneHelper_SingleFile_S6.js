﻿///Helps handle Dropzone control creting it, geting value, setting value,...
function DropzoneHelper(dropzoneElementId, url, inputConfiguration, onComplete, onCompleteCaller) {
    this.configuration = {
        //still not implemented for multiple files 
        maxFiles: 1,
        uploadMultiple: false,
        parallelUploads: 10,
        //------------------
        timeout: 0,
        setFormData: null,
        onComplete: null,
        onCompleteCaller: null,
        acceptedFormats: '.png,.jpg,.jpeg,.pdf,.xlsx,.csv,.txt',
        maxFilesize: 100,
        maxHeight: null,
        displayType: 'standard'
    };
    if (inputConfiguration)
    {
        /*  
         *  jQuery.extend( [deep ], target, object1 [, objectN ] )
         *  deep
         *  Type: Boolean
         *  If true, the merge becomes recursive (aka. deep copy). Passing false for this argument is not supported.
         *  
         *  In our case Error happened : inputConfiguration had empty currentBo, and empty currentBo was sent as a parameter to thisObject.configuration.onComplete
        */

        //$.extend(true, this.configuration, inputConfiguration);
        $.extend(this.configuration, inputConfiguration);
    }



    this.dropzoneElementId = dropzoneElementId;
    this.addedFiles = [];
    this.addingExistingFiles = false;

    var thisObject = this;

    var deafultMessage = this.configuration.displayType != 'standard' ?
        "<strong style=\"font-size:16px;text-align:center;\"></strong><br/>" + "<i style=\"font-size:44px;padding-left:190px;padding-top:20px;\" class=\"fa fa-upload\"></i>" :
        "<strong style=\"font-size:16px;text-align:center;display:inline-block; margin-left:13px;\">" + globalText.DropFilesOrClick + "</strong><br/>" + "<i style=\"font-size:44px;padding-left:170px;padding-top:20px;\" class=\"fa fa-upload\"></i>";
    this.dropzoneOptions = {
        url: url,
        init: function () {

            //this.on("success", function (rep) {
            //    console.log(rep.UniqueId);
            //});

            //add other form data closure from NgHistoryList->setFormData
            this.on("sending", function (file, xhr, formData) {
                if (thisObject.configuration.setFormData)
                    thisObject.configuration.setFormData(formData);
            });
            // delete preview of first file and add the new one
            this.on("maxfilesexceeded", function (file) {
                this.removeAllFiles();
                this.addFile(file);
            });

            this.on("removedfile", function (file) {
                if (!this.parentHelper.addingExistingFiles) {
                    if (thisObject.configuration.onDelete) {
                        thisObject.configuration.onDelete(thisObject.configuration.onCompleteCaller);
                    }
                }
            });
           
            this.on("complete", function (response) {
                if (thisObject.configuration.onComplete){
                    if (response.status == 'success') {

                       var resp = JSON.parse(response.xhr.responseText);
                        thisObject.configuration.onComplete(resp, thisObject.configuration.onCompleteCaller);
                    }
                    else if (response.status == 'canceled') {
                    }
                    else {
                        var resp = { isSuccess: false, errorMessage: globalText.UploadError };
                        thisObject.configuration.onComplete(resp, thisObject.configuration.onCompleteCaller);
                    }

                }
            });
            this.on("addedfile", function (response) {
                if (this.parentHelper.dropzone.files.length > 3) {
                    this.removeFile(this.parentHelper.dropzone.files[0]);
                }
                if (!this.parentHelper.addingExistingFiles) {
                    this.parentHelper.addedFiles.push(response);
                    this.parentHelper.resizeDropzone(this.parentHelper);
                }
            });


            this.on("drop", function (response) {
                //alert('testDrop');
            });

            //this.on("error", function (file, message) {
            //    alert(message);
            //    this.removeFile(file);
            //});
            //this.on("queuecomplete", function (file) {
            //    alert("Le fichier a été téléchargé.");
            //});
            this.on("accept", function (file, done) {
                if (file.size > this.options.maxFilesize * 1024 * 1024) {
                    return done(this.options.dictFileTooBig.replace("{{filesize}}", Math.round(file.size / 1024 / 10.24) / 100).replace("{{maxFilesize}}", this.options.maxFilesize));
                }
            });
        },

        paramName: "file",
        maxFilesize: this.configuration.maxFilesize,
        timeout: this.configuration.timeout,
        parallelUploads: this.configuration.parallelUploads,
        uploadMultiple: this.configuration.uploadMultiple,
        maxFiles: this.configuration.maxFiles,
        addRemoveLinks: true,
        dictFileTooBig: globalText.FileTooBig + '(' + '{{filesize}}' + globalText.MegaByteShort + ').' + globalText.MaxFileSize + ':' + '10' + globalText.MegaByteShort + '.',
        dictRemoveFile: globalText.Remove,
        dictInvalidFileType: globalText.FileTypeNoMatch + ' (' + this.configuration.acceptedFormats + ')',
        dictMaxFilesExceeded: globalText.UploadFileLimit + ' ' + this.configuration.maxFiles + ' ' + globalText.Files,
        dictCancelUpload: globalText.CancelUpload,
        acceptedFiles: this.configuration.acceptedFormats, // types of files accepted
        dictDefaultMessage: deafultMessage,
        autoProcessQueue: false
    };

    this.dropzone = this.createDropzone(this.dropzoneElementId);
}

DropzoneHelper.prototype.createDropzone = function (dropzoneElementId) {
    var attachedDropzone = $(dropzoneElementId)[0].dropzone;
    if (attachedDropzone) {

        // it fires an event 
        attachedDropzone.destroy();
        $(dropzoneElementId)[0].dropzone = null;
        //this.clearDropzoneObject(attachedDropzone);
        //return attachedDropzone;
    }

    var dropzone = new Dropzone(dropzoneElementId, this.dropzoneOptions);
    dropzone.parentHelper = this;
    this.resizeDropzone(this);
    return dropzone;
};

DropzoneHelper.prototype.resetDropzoneWithMultipleFiles = function (fileList) {
    this.addingExistingFiles = true;
    this.dropzone.removeAllFiles();
    if (fileList) {
        var arrayLength = fileList.length;
        for (var i = 0; i < arrayLength; i++) {
            var fileObj = { name: fileList[i].Name, size: fileList[i].DropzoneSize, id: fileList[i].Id };
            this.dropzone.emit("addedfile", fileObj);
            this.dropzone.files.push(fileObj);
            fileObj.previewElement.addEventListener('click', function () {
                window.open("/ApiDocument/DownloadFile?elementId=" + fileObj.id);
            });
            var element = $(fileObj.previewElement).find(".dz-filename");
            element.addClass("clickable");
            element.children().addClass("clickable");
        }
    }
    this.addingExistingFiles = false;
};

DropzoneHelper.prototype.resetDropzoneWithSingleFile = function (file) {
    this.addedFiles = [];
    if (file) {
        this.resetDropzoneWithMultipleFiles([file]);
    }
    else {
        this.resetDropzoneWithMultipleFiles(null);
    }
};

DropzoneHelper.prototype.processQueue = function () {
    if (this.addedFiles.length > 0 && this.dropzone.files.length > 0) {
        this.dropzone.processQueue();
        return true;
    }
    return false;
};

DropzoneHelper.prototype.resizeDropzone = function (thisOb) {
    if (thisOb.configuration.displayType == 'standard')
        return;

    if (thisOb.configuration.displayType == 'smallImage') {
        var container = $(thisOb.dropzoneElementId);
        container.css("padding", "5px");
        container.css("height", "120px");
        container.css("min-height", "120px");
        container.css("width", "100px");

        var dzImagePrev = container.find(".dz-preview");
        dzImagePrev.css("padding", "0px");
        dzImagePrev.css("margin", "0px");
        var dzImage = container.find(".dz-image");
        dzImage.css("height", "82px");
        var dzImageTN = dzImage.find("[data-dz-thumbnail]");
        dzImageTN.css("height", "82px");
        var dzDetails = container.find(".dz-details");
        dzDetails.css("display", "none");
        var dzRemove = container.find(".dz-remove");
        dzRemove.css("margin-top", "0px");
        dzRemove.css("padding", "0px");
    }

    if (thisOb.configuration.displayType == 'wideImage') {
        var container = $(thisOb.dropzoneElementId);
        container.css("padding", "5px");
        container.css("height", "150px");
        container.css("min-height", "150px");
        container.css("width", "100%");

        var dzImagePrev = container.find(".dz-preview");
        dzImagePrev.css("padding", "0px");
        dzImagePrev.css("margin", "0px");
        var dzImage = container.find(".dz-image");
        dzImage.css("height", "100%");
        dzImage.css("width", "100%");
        var dzImageTN = dzImage.find("[data-dz-thumbnail]");
        dzImageTN.css("height", "100%");
        dzImageTN.css("width", "100%");
        var dzDetails = container.find(".dz-details");
        dzDetails.css("display", "none");
        var dzRemove = container.find(".dz-remove");
        dzRemove.css("margin-top", "0px");
        dzRemove.css("padding", "0px");
        var imgIcon = container.find(".fa-upload");
        imgIcon.css("padding", "0px");
    }


};


