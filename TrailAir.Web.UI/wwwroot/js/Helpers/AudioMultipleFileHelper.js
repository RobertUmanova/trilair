﻿function AudioMultipleFileHelper(newAudioRecording, buttonId, audioElement, minutesId, secondsId, onUploadFinishedFunction, onUploadFinishCall, audioFileUrl) {
    this.hasAudio = false;
    this.buttonId = buttonId;
    this.audioElement = $('#' + audioElement);
    this.newAudioRecording = $('#' + newAudioRecording);
    this.audioFilesContainer = document.querySelector('#audio-files');
    this.isRecording = false;
    this.onUploadFinishedFunc = onUploadFinishedFunction;
    this.onUploadFinishCaller = onUploadFinishCall;

    var thisOb = this;
    this.minutesLabel = $('#' + minutesId);
    this.secondsLabel = $('#' + secondsId);
    this.button = $('#' + buttonId);
    this.audioPlayer = document.querySelector('#' + audioElement);


    var totalSeconds = 0;
    this.secondsLabel.html("00");
    this.minutesLabel.html("00");
    this.audioChunks = []
    this.audioBlob;
    this.audioBlobs = [];

    //reset
    $(this.audioFilesContainer).find("li").remove();

    if (audioFileUrl == null) {
        $(thisOb.audioPlayer).hide();
        $(thisOb.newAudioRecording).show();
    } else {
        $(thisOb.newAudioRecording).hide();
        $(this.audioFilesContainer).append(`
           <li class="list-group-item d-flex justify-content-between align-items-start">
              <div class="ms-2 me-auto w-100">
                <audio src=${audioFileUrl} controls />
              </div>
           </li>`)
    }
    this.mediaRecorder = null;

    const handleStop = () => {
        var audioBlob = new Blob(thisOb.audioChunks, { 'type': 'audio/ogg; codecs=opus' });
        //push audio files
        this.addAudioBlob(audioBlob);
        this.audioChunks = [];
    }

    navigator.mediaDevices.getUserMedia({ audio: true }).then(stream => {
        this.mediaRecorder = new MediaRecorder(stream)
        // counterStop => with this we only allow for audio to be recorded only once!

        $('body').on('click', '.audio-action', function (e) {
            e.stopPropagation();
            e.preventDefault();
            const audioItem = e.target.closest("li");
            const dataId = audioItem.getAttribute("data-id");
            thisOb.removeAudioBlob(dataId);
            audioItem.remove();
        });

        thisOb.button.on('click', () => {
            if (thisOb.isRecording) {
                this.mediaRecorder.stop()
                this.isRecording = false;
            } else {
                this.mediaRecorder.start();
                this.isRecording = true;
            }
            //thisOb.secondsLabel.html("00");
            //thisOb.minutesLabel.html("00");

            //totalSeconds = 0;

            //function resetTimer () {

            //    thisOb.secondsLabel.html("00");
            //    thisOb.minutesLabel.html("00");
            //};

            function setTime() {
                if (thisOb.isRecording) {
                    setTimeout(function () { setTime() }, 1000);
                    thisOb.secondsLabel.html(pad(totalSeconds % 60));
                    thisOb.minutesLabel.html(pad(parseInt(totalSeconds / 60)));
                    ++totalSeconds;
                }
            }
            function pad(val) {
                var valString = val + "";
                if (valString.length < 2) {
                    return "0" + valString;
                } else {
                    return valString;
                }
            }
            if (thisOb.isRecording) {
                thisOb.button.removeClass("stoppedButton");
                thisOb.button.addClass("recordingButton");
                //resetTimer();
                thisOb.counter = setTime();
            } else {
                thisOb.button.removeClass("recordingButton");
                thisOb.button.addClass("stoppedButton");
                //clearInterval(setTime);
            }

            //if (button.hasClass('stoppedButton')) {
            //    button.removeClass("stoppedButton");
            //    button.addClass("recordingButton");
            //    this.resetTimer();
            //    this.counter = setInterval(setTime, 1000);
            //}
            //else {
            //    button.removeClass("recordingButton");
            //    button.addClass("stoppedButton");
            //    clearInterval(this.counter);
            //}

        })


        this.mediaRecorder.addEventListener('dataavailable', (e) => {
            thisOb.audioChunks.push(e.data);
        })
        this.mediaRecorder.addEventListener('stop', handleStop.bind(this));
        //uploadButton.addEventListener("click", () => {
        //    thisOb.uploadFile();
        //})
        const toggleStatus = () => statusEl.classList.toggle("hide");

        /**
         * 
         * @param {Blob} audioBlob 
         */
        //const uploadFile = () => {
        //}
    })
}

AudioMultipleFileHelper.prototype.uploadFile = function (thisOb) {
    var formData = new FormData()
    var url = "/" + lanUiLangFull + "/ApiDocument/UploadMultipleFiles"
    this.audioBlobs.forEach((audioBlob, i) => formData.append(`${i}`, audioBlob.file));
    fetch(url, {
        method: 'POST',
        body: formData
    }).then((response) => response.json())
        .then((data) => thisOb.onUploadFinishedFunc(data, thisOb.onUploadFinishCaller));
}

AudioMultipleFileHelper.prototype.addAudioBlob = function (blob) {
    var dataId = Math.random() * 100;
    this.audioBlobs.push({ id: dataId, file: blob });
    const audioUrl = URL.createObjectURL(blob);
    $(this.audioFilesContainer).append(`
           <li data-id=${dataId} class="list-group-item d-flex justify-content-between align-items-start">
              <div class="ms-2 me-auto w-100">
                <audio src=${audioUrl} controls />
              </div>
              <span class="badge bg-danger audio-action" style="cursor:pointer;">&times;</span>
           </li>`);
    //reset audio chunks
    this.audioChunks = [];
}

AudioMultipleFileHelper.prototype.removeAudioBlob = function (dataId) {
    this.audioBlobs = this.audioBlobs.filter(_ => _.id !== Number(dataId));
}