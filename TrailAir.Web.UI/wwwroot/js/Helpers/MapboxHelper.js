﻿var MapBoxHelper = (function () {
    'use strict'
    /**@class
     * @param{ string } accessToken generated access token
     * */
    function MapBoxHelper(accessToken) {
        this.accessToken = accessToken;
        mapboxgl.accessToken = this.accessToken;
        this.mapboxgl = mapboxgl;
    }

    MapBoxHelper.prototype.initMap = function (mapContainer) {
        this.map = new this.mapboxgl.Map({
            container: mapContainer || $('<div />')[0],
            style: 'mapbox://styles/mapbox/streets-v11', //'mapbox://styles/mapbox/streets-v11' 'mapbox://styles/mapbox/satellite-v9',
            zoom: 18,
            animate: false,
            center: [30.5, 50.5]
        });
        this.geocoder = this.initGeocoder();
        this.map.addControl(this.geocoder);
    }

    MapBoxHelper.prototype.initGeocoder = /** initialize geocoder plugin */function () {
        const geocoder = new MapboxGeocoder({
            accessToken: this.accessToken,
            mapboxgl: this.mapboxgl,
            flyTo: false
        });
        return geocoder;
    }

    MapBoxHelper.prototype.getCoordinates = async function (address) {
        return await this.geocoder.geocoderService.forwardGeocode({
            query: address,
            limit: 1
        }).send()
    }
    return MapBoxHelper;
}())