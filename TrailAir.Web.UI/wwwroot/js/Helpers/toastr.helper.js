﻿
toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

class ToastrHlp {
    static success(msg) {
        toastr.success(msg);
    }
    static error(msg) {
        toastr.error(msg);
    }
    static warning(msg) {
        toastr.warning(msg);
    }
    static updateSuccessMsg(addMsg) { // TODO: Replace all of this with the global text!!
        var standardText = globalText.DataUpdatedSuccess;
        standardText = ToastrHlp.AddMessageAndLimitIfNeeded(standardText, addMsg);
        ToastrHlp.success(standardText);
    }
    static updateErrorMsg(addMsg) {
        var standardText = globalText.ErrorUpdating;
        standardText = ToastrHlp.AddMessageAndLimitIfNeeded(standardText, addMsg);
        ToastrHlp.error(standardText);
    }

    static updateWarningMsg(addMsg) {
        var standardText = globalText.Warning;
        standardText = ToastrHlp.AddMessageAndLimitIfNeeded(standardText, addMsg, 130);
        ToastrHlp.warning(standardText);
    }

    static AddMessageAndLimitIfNeeded(msg1, msg2, totalCharacters) {
        var resultTxt = msg1;

        if (msg2) {
            if (resultTxt && resultTxt.length > 0)
                resultTxt = resultTxt + '. ' + msg2;
            else
                resultTxt = msg2;

            var characterCount = totalCharacters || 100;

            if (resultTxt.length > characterCount)
                resultTxt = resultTxt.slice(0, characterCount) + '...';
        }

        return resultTxt
    }
}