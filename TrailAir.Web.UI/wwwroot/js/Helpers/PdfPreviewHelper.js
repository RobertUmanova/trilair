var PDFPreviewerHelper = /** @class */ (function () {

    var previewer = document.querySelector('.pdf-viewer');
    var currentPage = document.querySelector('.currentPage');
    var totalPages = document.querySelector('.totalPages');
    var viewport = document.querySelector("#viewport");
    var currentPageIndex = 0;
    var pageMode = 1;
    var cursorIndex = 0;
    var pdfInstance = null;
    var totalPagesCount = 0;
    var pdfUrl = "";

    function PDFPreviewerHelper() {

        currentPageIndex = 0;
        pageMode = 1;
        cursorIndex = Math.floor(currentPageIndex / pageMode);
        pdfInstance = null;
        totalPagesCount = 0;
        pdfUrl = "";
    }
    PDFPreviewerHelper.prototype.previewPDF = function (pdfurl) {
        pdfUrl = pdfurl;
        window.PDFJS.getDocument(pdfurl).then(this.initViewer.bind(this));
    };
    PDFPreviewerHelper.prototype.initViewer = function (pdf) {
        pdfInstance = pdf;
        totalPagesCount = pdf.numPages;
        this.initPager();
        this.render();
        previewer.style.display = "flex";
    };
    PDFPreviewerHelper.prototype.onPagerButtonsClick = function (event) {
        var action = event.target.getAttribute("data-pager");
        if (action === "prev") {
            if (currentPageIndex === 0) {
                return;
            }
            currentPageIndex -= pageMode;
            if (currentPageIndex < 0) {
                currentPageIndex = 0;
            }
            this.render();
        }
        if (action === "next") {
            if (currentPageIndex === totalPagesCount - 1) {
                return;
            }
            currentPageIndex += pageMode;
            if (currentPageIndex > totalPagesCount - 1) {
                currentPageIndex = totalPagesCount - 1;
            }
            this.render();
        }
    };
    PDFPreviewerHelper.prototype.render = function () {
        var thisOb = this;
        cursorIndex = Math.floor(currentPageIndex / pageMode);
        var startPageIndex = cursorIndex * pageMode;
        var endPageIndex = startPageIndex + pageMode < totalPagesCount
            ? startPageIndex + pageMode - 1
            : totalPagesCount - 1;
        var renderPagesPromises = [];
        for (var i = startPageIndex; i <= endPageIndex; i++) {
            renderPagesPromises.push(pdfInstance.getPage(i + 1));
        }
        Promise.all(renderPagesPromises).then(function (pages) {
            var pagesHTML = ("<div style=\"width: " + (pageMode > 1 ? "50%" : "100%") + "\"><canvas></canvas></div>").repeat(pages.length);
            viewport.innerHTML = pagesHTML;
            pages.forEach(thisOb.renderPage.bind(thisOb));
        });
    };
    PDFPreviewerHelper.prototype.initPager = function () {
        var thisOb = this;
        var pager = document.querySelector("#pager");
        var previewClose = document.querySelector('#closePdfPreview');
        var downloadButton = document.querySelector('#downloadPdf');
        pager.addEventListener('click', this.onPagerButtonsClick.bind(this));
        previewClose.addEventListener('click', this.closePreviewer);
        downloadButton.addEventListener('click', this.downloadPDF);
        //leverage use of closure to perform cleanup of event handlers
        return function () {
            pager.removeEventListener("click", thisOb.onPagerButtonsClick.bind(thisOb));
            previewClose.removeEventListener('click', thisOb.closePreviewer.bind(thisOb));
            downloadButton.removeEventListener('click', thisOb.downloadPDF.bind(thisOb));
        };
    };
    PDFPreviewerHelper.prototype.renderPage = function (page) {
        var pdfViewport = page.getViewport(1);
        var container = viewport.children[page.pageIndex - cursorIndex * pageMode];
        pdfViewport = page.getViewport(container.offsetWidth / pdfViewport.width);
        var canvas = container.children[0];
        var context = canvas.getContext("2d");
        canvas.height = pdfViewport.height;
        canvas.width = pdfViewport.width;
        page.render({
            canvasContext: context,
            viewport: pdfViewport
        });
        currentPage.textContent = "" + (currentPageIndex + 1);
        totalPages.textContent = "" + totalPagesCount;
    };
    PDFPreviewerHelper.prototype.downloadPDF = function () {
        var anchor = document.createElement('a');
        anchor.href = pdfUrl;
        document.body.appendChild(anchor);
        anchor.click();
        document.body.removeChild(anchor);
    };
    PDFPreviewerHelper.prototype.closePreviewer = function () {
        //cleanup
        viewport.innerHTML = "";
        currentPageIndex = 0;
        pdfInstance = null;
        totalPagesCount = 0;
        cursorIndex = 0;
        //hide previewer
        previewer.style.display = 'none';
    };
    return PDFPreviewerHelper;
}());
