﻿///Helps handle Dropzone control creting it, geting value, setting value,...
function DropzoneHelper(dropzoneElementId, url, inputConfiguration) {

    this.configuration = {
        //still not implemented for multiple files 
        maxFiles: 1,
        uploadMultiple: false,
        setFormData: null,
        onComplete: null,
        onCompleteCaller: null,

    };

    this.dropzoneElementId = dropzoneElementId;

    if (inputConfiguration)
        $.extend(true, this.configuration, inputConfiguration);

    var thisObject = this;

    this.dropzoneOptions = {
        url: url,
        init: function () {
            this.on("sending", function (file, xhr, formData) {
                if (thisObject.configuration.setFormData)
                    thisObject.configuration.setFormData(formData);
            });
            this.on("maxfilesexceeded", function (file, message) {
                alert("Vous ne pouvez télécharger plus de " + maxFiles + " fichiers.");
                this.removeFile(file);
            });
            this.on("complete", function (response) {
                this.removeAllFiles();
                if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                    if (response.status == 'success') {
                        var resp = JSON.parse(response.xhr.response);
                        if (thisObject.configuration.onComplete)
                            thisObject.configuration.onComplete(resp, thisObject.configuration.onCompleteCaller);
                        else
                            location.reload();
                    }
                    else {
                        var resp = { isSuccess: false, errorMessage: globalText.UploadError };
                        thisObject.configuration.onComplete(resp, thisObject.configuration.onCompleteCaller);
                    }
                }
            });
            this.on("error", function (file, message) {
                alert(message);
                this.removeFile(file);
            });
            this.on("accept", function (file, done) {
                if (file.size > this.options.maxFilesize * 1024 * 1024) {
                    return done(this.options.dictFileTooBig.replace("{{filesize}}", Math.round(file.size / 1024 / 10.24) / 100).replace("{{maxFilesize}}", this.options.maxFilesize));
                }
            });
        },
        paramName: "file",
        maxFilesize: 100,
        timeout: 0,
        parallelUploads: 10,
        uploadMultiple: this.configuration.uploadMultiple,
        maxFiles: this.configuration.maxFiles,
        addRemoveLinks: true,
        dictFileTooBig: globalText.FileTooBig + '(' + '{{filesize}}' + globalText.MegaByteShort + ').' + globalText.MaxFileSize + ':' + '100' + globalText.MegaByteShort + '.',
        dictRemoveFile: globalText.Remove,
        dictInvalidFileType: globalText.FileTypeNoMatch + ' (.png,.jpg,.pdf,.xlsx,.csv,.txt)',
        dictMaxFilesExceeded: globalText.UploadFileLimit + ' ' + this.configuration.maxFiles + ' ' + globalText.Files,
        dictCancelUpload: globalText.CancelUpload,
        acceptedFiles: '.png,.jpg,.jpeg,.pdf,.xlsx,.xls,.doc,.docx,.csv,.txt', // types of files accepted, // types of files accepted
        dictDefaultMessage: "<strong style=\"font-size:16px;text-align:center;display:inline-block; margin-left:13px;\">" + globalText.DropFilesOrClick + "</strong><br/>" + "<i style=\"font-size:44px;padding-left:170px;padding-top:20px;\" class=\"fa fa-upload\"></i>",
        autoProcessQueue: false
    };

    //this.dropzone = this.createDropzone(this.dropzoneElementId);
}

DropzoneHelper.prototype.createDropzone = function (dropzoneElementId) {
    var dropzone = new Dropzone(dropzoneElementId, this.dropzoneOptions);
    return dropzone;
};

DropzoneHelper.prototype.processQueue = function () {
    if (this.dropzone.files.length > 0) {
        this.dropzone.processQueue();
        return true;
    }
    return false;
};

