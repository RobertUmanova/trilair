﻿//requires jquery and date helper 
function ValidationHelper(containerSelector) {
    this.jqContainerToValidate = $(containerSelector);
    this.containerSelector = containerSelector;
    this.elementsToValidate = this.jqContainerToValidate.find('input, select, textarea');
    this.stringMessages = null;
    this.isValid = true;
}

ValidationHelper.InvalidCss = "ng-invalid";
ValidationHelper.ValidCss = "ng-valid";

ValidationHelper.prototype.Validate = function (showErrorMessage) {
    showErrorMessage = showErrorMessage || false;
    var thisOb = this;
    thisOb.stringMessages = globalText.PleaseCheckFollowingValues + ":\r\n";
    thisOb.isValid = true;
    thisOb.elementsToValidate.each(function (index) {
        var controlIsValid = thisOb.ValidateSingleElement($(this), thisOb);
        thisOb.isValid = thisOb.isValid && controlIsValid;

        if (!controlIsValid) {
            thisOb.stringMessages = thisOb.AddErrorMessageForControle($(this), thisOb.stringMessages, thisOb)
        }
    });

    if (showErrorMessage)
        thisOb.DisplayErrorMessages();

    return thisOb.isValid;
}

ValidationHelper.prototype.DisplayErrorMessages = function () {
    var thisOb = this;
    if (!thisOb.isValid)
        alertOb.errorMessage(globalText.ValidationError, thisOb.stringMessages);
}

ValidationHelper.prototype.ValidateSingleFull = function (required, selector, thisOb) {
    var jqElement = $(selector)
    if (required == true) {
        jqElement.prop('required', true);
    }
    var controlIsValid = thisOb.ValidateSingleElement(jqElement, thisOb);
    if (required == true) {
        jqElement.removeAttr('required');
    }
    if (!controlIsValid) {
        thisOb.stringMessages = thisOb.AddErrorMessageForControle(jqElement, thisOb.stringMessages, thisOb)
        thisOb.isValid = false;
    }
}

ValidationHelper.prototype.ValidateSingleElement = function (jqEle, thisOb) {
    var isInput = jqEle.is("input") || jqEle.is("textarea");
    var isSelect = jqEle.is("select");
    var isRequired = jqEle.is(":required");
    var isMarkedInvalid = jqEle.hasClass(ValidationHelper.InvalidCss);
    var valid = true;
    if (isInput) {
        valid = thisOb.ValidateInput(jqEle, isRequired, isMarkedInvalid, thisOb);
    } else if (isSelect) {
        valid = thisOb.ValidateSelect(jqEle, isRequired, isMarkedInvalid, thisOb);
    }

    if (valid && isMarkedInvalid) {
        jqEle.removeClass(ValidationHelper.InvalidCss);
    }
    else if (!valid && !isMarkedInvalid) {
        jqEle.addClass(ValidationHelper.InvalidCss);
    }

    return valid;
}

ValidationHelper.prototype.AddCustomErrorMessage = function (errorMessage, thisOb) {
    thisOb.stringMessages = thisOb.stringMessages + "\r\n" + errorMessage;
    thisOb.isValid = false;
}


ValidationHelper.prototype.AddErrorMessageForControle = function (jqEle, existingMessage, thisOb) {
    var controleName = thisOb.GetUserFriendlyName(jqEle, thisOb);

    existingMessage = existingMessage + "\r\n" + controleName;
    return existingMessage;
}

ValidationHelper.prototype.GetUserFriendlyName = function (jqEle, thisOb) {
    var parent = jqEle.parent();
    var labelOb = parent.children('label');
    if (labelOb.length > 0)
        return thisOb.FormatFriendlyNameLabel(labelOb.html());
    labelOb = parent.children('.control-label');
    if (labelOb.length > 0)
        return thisOb.FormatFriendlyNameLabel(labelOb.html());

    return thisOb.GetUserFriendlyName(parent, thisOb);
}

ValidationHelper.prototype.FormatFriendlyNameLabel = function (text) {
    if (!text) {
        return "";
    }
    text = text.replace(/\n/g, ' ').trim();
    return text;
}



ValidationHelper.prototype.ValidateSelect = function (jqEle, isRequired, isMarkedInvalid, thisOb) {
    var selectedVal = jqEle.val();

    if ((selectedVal == "" || selectedVal == 0 || !selectedVal) && isRequired)
        return false;
    else //if (selectedVal == "")
        return true;

}

ValidationHelper.prototype.ValidateInput = function (jqEle, isRequired, isMarkedInvalid, thisOb) {
    var type = jqEle.attr("type");
    var inputVal = jqEle.val();
    
    if (inputVal == "" && isRequired)
        return false;
    else if (inputVal == "")
        return true;

    if (thisOb.IsDateInput(jqEle)) {
        var dtObj = new DateOb(inputVal);
        return dtObj.IsValidDate;
    } else if (thisOb.IsTimeInput(jqEle)) {
        var timeObj = new TimeOb(inputVal);
        return timeObj.isTimeValid();
    }
    else if (type == "number") {
        return thisOb.ValidateNumberInput(jqEle, inputVal, thisOb);
    }

    return true;
}

ValidationHelper.prototype.ValidateNumberInput = function (jqEle, val, thisOb) {
    var min = jqEle.attr("min");
    var max = jqEle.attr("max");
    var isInteger = jqEle.is("[cva-int]");

    if (!ValidationHelper.isValidNumber(val, isInteger))
        return false;

    var value = !isInteger ? parseFloat(val) : parseInt(val); // to be extended for decimal  
    if (min) {
        if (parseInt(min) > value)
            return false;
    }
    if (max) {
        if (parseInt(max) < value)
            return false;
    }

    return true;
}


ValidationHelper.isValidNumber = function (val, isInteger) {

    if (isInteger)
        return parseFloat(val) === parseInt(val);

    return !isNaN(val);/// this will be extended when adn if needed to include 
    ///isNaN(-1.23) //false
    ///isNaN(5 - 2) //false
    ///isNaN(null) //false
    ///isNaN('') //false
    //isNaN(true) //false
}


ValidationHelper.prototype.IsDateInput = function (jqEle) {
    return jqEle.parent().hasClass("date");
}

ValidationHelper.prototype.IsTimeInput = function (jqEle) {
    return jqEle.hasClass("time");
}

ValidationHelper.prototype.ValidateSlider = function (value, selector, thisOb) {
    var jqElement = $('#' + selector.id);

    if (value == '0.00' || value == 0 || !value) {
        thisOb.isValid = false;

        thisOb.stringMessages = thisOb.AddErrorMessageForControle(jqElement, thisOb.stringMessages, thisOb)
    }
}
