﻿///Helps handle select2 control creting it, geting value, setting value,...
///This helper is dependend on the LogActionHelper.js. If you are using the Select2Helper you must include the LogActionHelper.js also

function Select2Helper(elementId,) {
    var thisInstance = this;
    this.elementId = elementId;
    this.jqueryDdlElement = $("#" + elementId);
    this.parentContainerOb = this.jqueryDdlElement.parent();
    this.select2Instance = null;
    this.listLoaded = false;
}

Select2Helper.prototype.LoadSelect2WithData = function (ddlData, selectHlpObject) {
    var thisObject = selectHlpObject || this;
    var selectedValue = thisObject.GetSelectValue();

    if (thisObject.select2Instance != null) {
        thisObject.jqueryDdlElement.select2('destroy');
        thisObject.jqueryDdlElement.children('option').remove();
    }


    thisObject.select2Instance = thisObject.jqueryDdlElement.select2({
        width: '100%',
        data: ddlData,
        dropdownParent: thisObject.parentContainerOb
    });

    thisObject.listLoaded = true;

    if (selectedValue != null && selectedValue.length > 0) {
        thisObject.SetValue(selectedValue);
    }
}

Select2Helper.prototype.SetValue = function (selectedValue, selectHlpObject, counter) {
    var numberTimesCalled = counter || 0; numberTimesCalled += 1;
    var thisObject = selectHlpObject || this;
    var selVal = selectedValue;// selectedValue == null ? null : selectedValue + '';

    if ($("#" + thisObject.elementId + " option[value='" + selVal + "']").length != 0) {
        // the value already exists so can be selected
    }
    else if (selectedValue != null && (selectedValue + '').length > 0) {
        if (thisObject.listLoaded != true) {

            if (numberTimesCalled > 50) {
                alert("Error loading the dropdown list");
                LogActionHelper.saveLogAction(null, "SetValue", "UI-ValueNotFoundInDDL", thisObject.jqueryDdlElement.selector, selectedValue);
                this.jqueryDdlElement.append($('<option>', { value: selVal, text: 'NA' }));
            }
            else {
                setTimeout(function () {
                    thisObject.SetValue(selectedValue, selectHlpObject, numberTimesCalled);
                }, 200);
                return;
            }

        }
    }

    thisObject.jqueryDdlElement.val(selVal);
    //thisObject.jqueryDdlElement.trigger('change');
    //var selectedText = $("#" + thisObject.elementId +" option:selected").text();
    //thisObject.jqueryDdlElement.select2('data', { id: selectedValue, a_key: selectedText, text: selectedText });
    thisObject.jqueryDdlElement.trigger('change.select2');
}

Select2Helper.prototype.GetSelectValue = function () {
    return this.jqueryDdlElement.val();
}
