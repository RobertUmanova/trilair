﻿///THis is a newer version of 'Select2_Helper_S7' it is just the name changed to avoid confusion

function loadAsyncSelect2(selectId, apiPath, placeholder, onChange) {
    var longText = globalText.Filter3char;
    var noResText = globalText.NoResult;
    var tempNoResultText = noResText;
    this.jqueryDdlElement = $(selectId);
    //onChangeAsyncSelect2(selectId, onChange);

    if (!(placeholder))
        placeholder = globalText.PleaseSelect;

    this.selectJqId = selectId;
    this.onChange = onChange;
    this.select2Instance = null;
  
    loadAsyncSelect2.prototype.SetData = function (apiPathAdded) {
        onChangeAsyncSelect2(selectId, this.onChange);

        if (this.select2Instance != null) {
            this.jqueryDdlElement.select2('destroy');
            this.jqueryDdlElement.children('option').remove();

            // manually unbind the event after destroying the instance od select2
            this.jqueryDdlElement.off('change');
            // manually bind the event after unbinding it
            this.jqueryDdlElement.on('change', onChangeAsyncSelect2(selectId, this.onChange));
        }

        this.select2Instance = this.jqueryDdlElement.select2({
            width: '100%',
            placeholder: placeholder,
            allowClear: true,
            language: {
                noResults: function (params) {
                    return tempNoResultText;
                }
            },
            ajax: {
                url: "/" + lanUiLangFull + apiPathAdded,
                delay: 500,
                processResults: function (data) {
                    tempNoResultText = data.needMoreChar ? longText : noResText;

                    if (data.items) {
                        data.items.forEach(function (element) {
                            if (element.text)
                                if (element.text.length > 49)
                                    element.text = element.text.substring(0, 49) + '...';
                        });
                    }

                    return {
                        results: data.items
                    };
                }
            }
            ,
            templateResult: function (data) {
                //if (data.id === '') { // adjust for custom placeholder values
                //    return 'Custom styled placeholder text';
                //}

                return data ? data.text : "";
            }

        });

    }
    loadAsyncSelect2.prototype.Reset = function () {
       
        if (this.select2Instance != null) {
            this.jqueryDdlElement.select2('destroy');
            this.jqueryDdlElement.children('option').remove();
             // manually unbind the event after destroying the instance od select2
            this.jqueryDdlElement.off('change');
        }
    }

    loadAsyncSelect2.prototype.SetOptionsDisabled = function (selectedValues) {

        var valuesId = selectedValues.map(function (el, index) {
            return el.id;
        });
        valuesId.forEach(id => {
            $(this.selectJqId + " option[value='" + id + "']").attr("disabled", true);
            $(this.selectJqId + " option[value='" + id + "']").hide();

        });
    }
    // When you press 'x' to deselect selected object in select2 you open Select2 - this function prevents this
    if (selectId) {
        $(selectId).on('select2:unselecting', function (e) {
            $(selectId).on('select2:opening', function (e) {
                e.preventDefault();
                $(selectId).off('select2:opening');
            });
        });
    }
}



GetAsynSel2SelectedValues = function (selectId) {
    var selectedVals = $(selectId).val();

    var result = [];

    if (selectedVals != null && selectedVals.length > 0) {
        $(selectId + " > option").each(function () {
            if (selectedVals.includes(this.value)) {
                result.push({
                    Id: this.value,
                    FullName: this.text
                });
            }
        });
    }

    return result;
}

function onSelectAsyncSelect2(selectId, onSelect) {
    $(selectId).on("select2:selecting", function (event) {
        if (onSelect)
            onSelect(event);
        // what you would like to happen
    });

}

function onChangeAsyncSelect2(selectId, onSelect) {
    $(selectId).change(function (event) {
        if (onSelect) {
            // unselect fires change event and it will make a http request with id null -- this is a bad fix I did (Domagoj)
            if ($(selectId).val())
                onSelect($(selectId).val());
        }
    });
}