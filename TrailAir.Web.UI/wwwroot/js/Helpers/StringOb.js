﻿function StringOb(stringValue)
{
    this.value = this.IsStringNullOrEmpty(stringValue) ? stringValue: null;
}

StringOb.IsStringNullOrEmpty = function (value) {
    return (!value || value == undefined || value == "" ||value.length == 0);
}


