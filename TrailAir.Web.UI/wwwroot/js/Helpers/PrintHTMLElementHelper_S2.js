﻿function PrintHTMLElementHelper(containerId) {
    //html element which will be printeed
    this.containerId = containerId;
}


PrintHTMLElementHelper.prototype.print = function () {
    var thisOb = this;

    var headToPrint = document.getElementsByTagName("head")[0];
    var elementToPrint = document.getElementById(thisOb.containerId);
    var htmlResult = '<html><head>' + headToPrint.innerHTML + '</head><body><div><h2 style="text-align:center;padding:20px;"></h2></div><div>' + elementToPrint.outerHTML + '</div></body></html>';
    var domHTMLResult = (new DOMParser()).parseFromString(htmlResult, "text/html");
    thisOb.removeButtons(domHTMLResult);

    var WinPrint = window.open('', '', 'left=0,top=0,width=1000,height=900,toolbar=0,scrollbars=0,status=0');

    // This is HARD CORE HOTFIX but works for now
    // I didn't find any better solution
    setTimeout(function () {
        
        WinPrint.document.write(domHTMLResult.documentElement.innerHTML);
        WinPrint.document.close();

        setTimeout(function () {
            WinPrint.print();
            WinPrint.close();
        }, 100);
    }, 1000);
};

PrintHTMLElementHelper.prototype.removeButtons = function (elementToPrint) {
    var thisOb = this;
    var buttons = elementToPrint.getElementsByClassName("btn");

    if (buttons.length > 0) {
        for (var i = 0; i < buttons.length; i++) {
            buttons[i].style.display = 'none';
        }
    }
};

PrintHTMLElementHelper.prototype.revertButtons = function (elementToPrint) {
    var thisOb = this;
    var buttons = elementToPrint.getElementsByClassName("btn");

    if (buttons.length > 0) {
        for (var i = 0; i < buttons.length; i++) {
            buttons[i].style.display = 'block';
        }
    }
};