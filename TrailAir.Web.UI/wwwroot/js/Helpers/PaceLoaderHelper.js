﻿function PaceLoaderHelper() {
}

PaceLoaderHelper.CheckIsThereActiveHTTPRequests = function () {
    
    if (requestsActive == 0 && Pace.running) {
        // Some requests works funny so you have to put timeout.
        // e.g. Users/List - the white overlay remains active after Pace finishes
        setTimeout(function () {
            PaceLoaderHelper.StopPaceLoader();
        }, 200);
    }
}

PaceLoaderHelper.StopPaceLoader = function () {

    Pace.stop();
    // It works a little bit smoother if you do restart after stop
    // for cases where you have synchronous requests (other request starts after the first one finishes)
    Pace.restart();
}
