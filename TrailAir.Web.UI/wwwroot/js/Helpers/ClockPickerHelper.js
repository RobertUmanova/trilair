﻿/**
 * Helper function to display Two clocks, one for start and other for end 
 * Two clocks are dependant on each other
 * Format (hh:mm) is set in startClockPicker and endClockPicker
 * Depends on clockpicker/clockpicker.js and css in layout.cshtml
 * More info can be found at https://weareoutman.github.io/clockpicker/
 * @param {string} clockPickerStartDivId
 * @param {string} clockPickerEndDivId
 * @param {string} clockPickerStartInputId
 * @param {string} clockPickerEndInputId
 */
function ClockPickerHelper(clockPickerStartDivId, clockPickerEndDivId, clockPickerStartInputId, clockPickerEndInputId, valueToDisplay) {
    var thisOb = this;

    thisOb.clockPickStart = `#${clockPickerStartDivId}`;
    thisOb.clockPickEnd = `#${clockPickerEndDivId}`;

    thisOb.clockPickStartInput = $(`#${clockPickerStartInputId}`);
    thisOb.clockPickEndInput = $(`#${clockPickerEndInputId}`);

   
    var startClockPicker = $(thisOb.clockPickStart).clockpicker({
        donetext: 'Done',
        //twelvehour: true,
        autoclose: true,
        //leadingZeroHours: false,
        //upperCaseAmPm: true,
        //leadingSpaceAmPm: true,
        placement: 'bottom',
        init: function () {
            //should be used to show all hidden minutes nad hours 
            //unfortunatelly this is only fired on first popup, check end of this file for the fix
        },
        afterHourSelect: function () {  // To know if the hour is selected.

            if (thisOb.clockPickEndInput.val()) {

                var endHours = parseInt(thisOb.clockPickEndInput.val().split(":")[0]);
                var endMinutes = parseInt(thisOb.clockPickEndInput.val().split(":")[1]);
                var startHourCurrentSelected = $(thisOb.clockPickStart).siblings(".popover").first().find("span.clockpicker-span-hours")[0].innerText;

                if (endHours == startHourCurrentSelected) {

                    // Hide all start minutes that are greater then start minutes.
                    $(".clockpicker-minutes").find(".clockpicker-tick").filter(function (index, element) {
                        return parseInt($(element).text()) > endMinutes;
                    }).hide();

                    // Show all previously hidden start minutes.
                    $(".clockpicker-minutes").find(".clockpicker-tick").filter(function (index, element) {
                        return parseInt($(element).text()) <= endMinutes;
                    }).show();
                } else {

                    // Show all previously hidden start minutes.
                    $(".clockpicker-minutes").find(".clockpicker-tick").filter(function (index, element) {
                        return true;
                    }).show();
                }
            }
        },
        afterShow: function () {

            if (thisOb.clockPickEndInput.val()) {

                var endHours = parseInt(thisOb.clockPickEndInput.val().split(":")[0]);
                var endMinutes = parseInt(thisOb.clockPickEndInput.val().split(":")[1]);

                // Hide all start hours that are bigger then end hours.
                $(".clockpicker-hours").find(".clockpicker-tick").filter(function (index, element) {
                    return parseInt($(element).text()) > endHours;
                }).hide();

                // Show all previously hidden start hours.
                $(".clockpicker-hours").find(".clockpicker-tick").filter(function (index, element) {
                    return parseInt($(element).text()) <= endHours;
                }).show();
            }
        },
        afterDone: function () {

            // reopen  startClockPicker on minutes if we picked start minutes greater then start minutes
            if (thisOb.clockPickEndInput.val()) {
                var startHours = parseInt(thisOb.clockPickStartInput.val().split(":")[0]);
                var endHours = parseInt(thisOb.clockPickEndInput.val().split(":")[0]);
                var startMinutes = parseInt(thisOb.clockPickStartInput.val().split(":")[1]);
                var endMinutes = parseInt(thisOb.clockPickEndInput.val().split(":")[1]);

                if (startMinutes > endMinutes && startHours == endHours) {
                    $(thisOb.clockPickStart).clockpicker('show').clockpicker('toggleView', 'minutes');
                }
            }
        }

    });

    var endClockPicker = $(thisOb.clockPickEnd).clockpicker({
        donetext: 'Done',
        //twelvehour: true,
        autoclose: true,
        //leadingZeroHours: false,
        //upperCaseAmPm: true,
        //leadingSpaceAmPm: true,
        placement: 'bottom',
        default: 'now',
        init: function () {
            //should be used to show all hidden minutes nad hours 
            //unfortunatelly this is only fired on first popup, check end of this file for the fix
        },
        afterHourSelect: function () {  // To know if the hour is selected so we can hide or show minutes
            if (thisOb.clockPickStartInput.val()) {

                var startHours = parseInt(thisOb.clockPickStartInput.val().split(":")[0]);
                var startMinutes = parseInt(thisOb.clockPickStartInput.val().split(":")[1]);
                var endHourCurrentSelected = $(thisOb.clockPickEnd).siblings(".popover").first().find("span.clockpicker-span-hours")[0].innerText;

                if (startHours == endHourCurrentSelected) {

                    // Hide all end minutes that are less then start minutes.
                    $(".clockpicker-minutes").find(".clockpicker-tick").filter(function (index, element) {
                        return parseInt($(element).text()) < startMinutes;
                    }).hide();

                    // Show all previously hidden end minutes.
                    $(".clockpicker-minutes").find(".clockpicker-tick").filter(function (index, element) {
                        return parseInt($(element).text()) >= startMinutes;
                    }).show();
                } else {
                    // Show all previously hidden end minutes.
                    $(".clockpicker-minutes").find(".clockpicker-tick").filter(function (index, element) {
                        return true;
                    }).show();
                }
            }
        },
        afterShow: function () {
            if (thisOb.clockPickStartInput.val()) {

                var startHours = parseInt(thisOb.clockPickStartInput.val().split(":")[0]);
                var startMinutes = parseInt(thisOb.clockPickStartInput.val().split(":")[1]);

                // Hide all end hours that are less then start hours.
                $(".clockpicker-hours").find(".clockpicker-tick").filter(function (index, element) {
                    return parseInt($(element).text()) < startHours;
                }).hide();

                // how all previously hidden end hours.
                $(".clockpicker-hours").find(".clockpicker-tick").filter(function (index, element) {
                    return parseInt($(element).text()) >= startHours;
                }).show();

            }
        },
        afterDone: function () {
            // reopen  endClockPicker on minutes if we picked end minutes less then start minutes
            if (thisOb.clockPickStartInput.val()) {
                var startHours = parseInt(thisOb.clockPickStartInput.val().split(":")[0]);
                var endHours = parseInt(thisOb.clockPickEndInput.val().split(":")[0]);
                var startMinutes = parseInt(thisOb.clockPickStartInput.val().split(":")[1]);
                var endMinutes = parseInt(thisOb.clockPickEndInput.val().split(":")[1]);

                if (endMinutes < startMinutes && startHours == endHours) {
                    $(thisOb.clockPickEnd).clockpicker('show').clockpicker('toggleView', 'minutes');
                }
            }
        }

    });

    this.GetHoursAndMinutes = function () {
        if (thisOb.clockPickStartInput.val() && thisOb.clockPickEndInput.val()) {

            if (!is24HFormatLeadingZero(thisOb.clockPickStartInput.val()) || !is24HFormatLeadingZero(thisOb.clockPickEndInput.val())) {
                //thisOb.timeDuration = null;
                return null;
            }


            var startHours = parseInt(thisOb.clockPickStartInput.val().split(":")[0]);
            var startMinutes = parseInt(thisOb.clockPickStartInput.val().split(":")[1]);
            var endHours = parseInt(thisOb.clockPickEndInput.val().split(":")[0]);
            var endMinutes = parseInt(thisOb.clockPickEndInput.val().split(":")[1]);

            var dateStart = new Date(2000, 0, 1, startHours, startMinutes);
            var dateEnd = new Date(2000, 0, 1, endHours, endMinutes);
            var msec = dateEnd - dateStart;
            var hh = Math.floor(msec / 1000 / 60 / 60);
            msec -= hh * 1000 * 60 * 60;
            var mm = Math.floor(msec / 1000 / 60);
            msec -= mm * 1000 * 60;
            var ss = Math.floor(msec / 1000);
            msec -= ss * 1000;

            // should not happen!
            if (hh < 0 || mm < 0) {
                //thisOb.timeDuration = -1;
                return -1;
            }

            if (hh < 10) {
                hh = `0${hh}`;
            }
            if (mm < 10) {
                mm = `0${mm}`;
            }
            //thisOb.timeDuration = `${hh}:${mm}`;
            return `${hh}:${mm}`;

        } else {
            return null;
        }

    }

    function is24HFormatLeadingZero(value) {
        var isValidtwoDotsDelimiter = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/.test(value);
        if (isValidtwoDotsDelimiter) {
            return true;
        }
        return false;
    }
}

// FIX for ClockPicker, init functions of 2 clockpickers are only called for first modal popup
$(document).on('hidden.bs.modal', '.modal', function () {

    // On modal hide show all previously hidden end minutes.
    $(".clockpicker-minutes").find(".clockpicker-tick").filter(function (index, element) {
        return true;
    }).show();

    // On modal hide show all previously hidden end hours.
    $(".clockpicker-hours").find(".clockpicker-tick").filter(function (index, element) {
        return true;
    }).show();
});


