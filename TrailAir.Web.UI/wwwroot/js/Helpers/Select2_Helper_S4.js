﻿///Helps handle select2 control creting it, geting value, setting value,...
///This helper is dependend on the LogActionHelper.js. If you are using the Select2Helper you must include the LogActionHelper.js also

function Select2Helper(elementId, onChange) {
    var thisInstance = this;
    this.elementId = elementId;
    this.jqueryDdlElement = $("#" + elementId);
    this.parentContainerOb = this.jqueryDdlElement.parent();
    this.select2Instance = null;
    this.listLoaded = false;

    thisInstance.jqueryDdlElement.children('option').remove();
    thisInstance.onChangeSelect2(thisInstance.jqueryDdlElement, onChange);

    thisInstance.preventDefaultDeselectBehaviour('#' + elementId);
}

Select2Helper.prototype.LoadSelect2WithData = function (ddlData, selectHlpObject, preselectedValues) {
    var thisObject = selectHlpObject || this;
    var selectedValue = thisObject.GetSelectValue();

    if (thisObject.select2Instance != null) {
        thisObject.jqueryDdlElement.select2('destroy');
        thisObject.jqueryDdlElement.children('option').remove();
    }

    //thisObject.jqueryDdlElement.children('option').remove();
    


    thisObject.select2Instance = thisObject.jqueryDdlElement.select2({
        width: '100%',
        data: ddlData,
        dropdownParent: thisObject.parentContainerOb
    });

    thisObject.listLoaded = true;

    if (selectedValue != null && selectedValue.length > 0) {
        thisObject.SetValue(selectedValue);
    }
}

Select2Helper.prototype.SetOptionDisabled = function (selectedValue) {
    var selVal = selectedValue;// selectedValue == null ? null : selectedValue + '';
    $("#" + this.elementId + " option[value='" + selVal + "']").attr("disabled", true);
}

Select2Helper.prototype.SetOptionsDisabled = function (selectedValues) {

    var valuesId = selectedValues.map(function (el, index) {
        return el.id;
    });
    valuesId.forEach(id => {
        $("#" + this.elementId + " option[value='" + id + "']").attr("disabled", true);
    });
}

Select2Helper.prototype.SetOptionEnabled = function (selectedValue) {
    var selVal = selectedValue;// selectedValue == null ? null : selectedValue + '';
    $("#" + this.elementId + " option[value='" + selVal + "']").attr("disabled", false);
}

Select2Helper.prototype.SetValue = function (selectedValue, selectHlpObject, counter) {
    var numberTimesCalled = counter || 0; numberTimesCalled += 1;
    var thisObject = selectHlpObject || this;
    var selVal = selectedValue;// selectedValue == null ? null : selectedValue + '';

    if ($("#" + thisObject.elementId + " option[value='" + selVal + "']").length != 0) {
        // the value already exists so can be selected
    }
    else if (selectedValue != null && (selectedValue + '').length > 0) {
        if (thisObject.listLoaded != true) {

            if (numberTimesCalled > 50) {
                alert("Error loading the dropdown list");
                //LogActionHelper.saveLogAction(null, "SetValue", "UI-ValueNotFoundInDDL", thisObject.jqueryDdlElement.selector, selectedValue);
                this.jqueryDdlElement.append($('<option>', { value: selVal, text: 'NA' }));
            }
            else {
                setTimeout(function () {
                    thisObject.SetValue(selectedValue, selectHlpObject, numberTimesCalled);
                }, 200);
                return;
            }

        }
    }

    thisObject.jqueryDdlElement.val(selVal);
    //thisObject.jqueryDdlElement.trigger('change');
    //var selectedText = $("#" + thisObject.elementId +" option:selected").text();
    //thisObject.jqueryDdlElement.select2('data', { id: selectedValue, a_key: selectedText, text: selectedText });
    thisObject.jqueryDdlElement.trigger('change.select2');
}

///Selected value can be an array of values or a comma separated list ex:2,3,23
Select2Helper.prototype.SetMultipleValues = function (selectedValues, selectHlpObject) {
    var thisObject = selectHlpObject || this;

    var valsToSelect = selectedValues;
    if (!(selectedValues)) {
        return;
    }

    if (!Array.isArray(selectedValues)) {
        valsToSelect = valsToSelect.split(',');
    }

    thisObject.jqueryDdlElement.val(valsToSelect);
    //thisObject.jqueryDdlElement.trigger('change');
    //var selectedText = $("#" + thisObject.elementId +" option:selected").text();
    //thisObject.jqueryDdlElement.select2('data', { id: selectedValue, a_key: selectedText, text: selectedText });
    thisObject.jqueryDdlElement.trigger('change.select2');
};

Select2Helper.prototype.GetSelectValue = function () {
    return this.jqueryDdlElement.val();
}


Select2Helper.prototype.onChangeSelect2 = function (selectId, onSelect) {
    $(selectId).change(function () {
        if (onSelect)
            onSelect(event);
    });
}

Select2Helper.prototype.preventDefaultDeselectBehaviour = function (selectId) {

    // When you press 'x' to deselect selected object in select2 you open Select2 - this function prevents this
    if (selectId) {
        $(selectId).on('select2:unselecting', function (e) {
            $(selectId).on('select2:opening', function (e) {
                e.preventDefault();
                $(selectId).off('select2:opening');
            });
        });
    }
}
