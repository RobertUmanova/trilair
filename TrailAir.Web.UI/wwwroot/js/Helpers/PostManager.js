﻿var requestsActive = 0;

function PostManager() {

}

PostManager.prototype = {

    get(url, successFn) {
        var thisPMOb = this;

        var fullUrl = thisPMOb.addLangugageToUrl(url);
        requestsActive++;
        axios.get(fullUrl)
            .then(function (response) {

                requestsActive--;
                PaceLoaderHelper.CheckIsThereActiveHTTPRequests();
                successFn(response);

            }, function (error) {

                requestsActive--;
                PaceLoaderHelper.CheckIsThereActiveHTTPRequests();
                alertOb.errorMessage("Action failed to complete! ", "An email is sent to the support and will be handled as soon as possible.");
            })
    },


    post(url, data, successFn) {
        var thisPMOb = this;

        var fullUrl = thisPMOb.addLangugageToUrl(url);
        requestsActive++;
        var errorFunc = function (error) {
            alertOb.errorMessage("Action failed to complete! ", "An email is sent to the support and will be handled as soon as possible.");
        };

        axios.post(fullUrl, data)
            .then(function (response) {

                requestsActive--;
                PaceLoaderHelper.CheckIsThereActiveHTTPRequests();
                successFn(response);
            })
            .catch(function (error) {

                requestsActive--;
                PaceLoaderHelper.CheckIsThereActiveHTTPRequests();
                errorFunc(error);
            });
    }
}

PostManager.prototype.addLangugageToUrl = function (url) { // TODO: lanUiLangFull define in _Localization.cshtml --> Implement this file from Swisscon
    return "/" + 'en-GB' /*lanUiLangFull*/ + url;
}
