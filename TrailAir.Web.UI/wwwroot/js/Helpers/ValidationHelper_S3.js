﻿
//requires jquery and date helper 
function ValidationHelper(containerSelector) {
    this.jqContainerToValidate = $(containerSelector);
    this.containerSelector = containerSelector;
    this.elementsToValidate = this.jqContainerToValidate.find('input, select, textarea');
}

ValidationHelper.InvalidCss = "ng-invalid";
ValidationHelper.ValidCss = "ng-valid";

ValidationHelper.prototype.Validate = function (showErrorMessage) {
    showErrorMessage = showErrorMessage || false;
    var stringMessages = globalText.PleaseCheckFollowingValues + ":\r\n";
    var thisOb = this;
    var resultValid = true;
    this.elementsToValidate.each(function (index) {
        var controlIsValid = thisOb.ValidateSingleElement($(this), thisOb);
        resultValid = resultValid && controlIsValid;

        if (!controlIsValid && showErrorMessage) {
            stringMessages = thisOb.AddErrorMessageForControle($(this), stringMessages, thisOb)
        }
    });

    if (showErrorMessage && !resultValid)
        alertOb.errorMessage(globalText.ValidationError, stringMessages);
    return resultValid;
}


ValidationHelper.prototype.AllertErrors = function (thisOb) {
    var thisObj = thisOb || this;

    if (showErrorMessage && !resultValid)
        alertOb.errorMessage(globalText.ValidationError, stringMessages);
    return resultValid;
}



ValidationHelper.prototype.ValidateRequiredElement = function (elementSelector, thisOb) {
    var thisObj = thisOb || this;
    jqElementToValidate = thisObj.jqContainerToValidate.find(elementSelector);
    var valid = thisOb.ValidateSingleElement(jqElementToValidate, thisObj, true);
}

ValidationHelper.prototype.ValidateSingleElement = function (jqEle, thisOb, forceRequired) {
    var isInput = jqEle.is("input") || jqEle.is("textarea");
    var isSelect = jqEle.is("select");
    var isRequired = forceRequired || jqEle.is(":required") ;
    var isMarkedInvalid = jqEle.hasClass(ValidationHelper.InvalidCss);
    var valid = true;
    if (isInput) {
        valid = thisOb.ValidateInput(jqEle, isRequired, isMarkedInvalid, thisOb);
    } else if (isSelect) {
        valid = thisOb.ValidateSelect(jqEle, isRequired, isMarkedInvalid, thisOb);
    }

    if (valid && isMarkedInvalid) {
        jqEle.removeClass(ValidationHelper.InvalidCss);
    }
    else if (!valid && !isMarkedInvalid) {
        jqEle.addClass(ValidationHelper.InvalidCss);
    }

    return valid;
}



ValidationHelper.prototype.AddErrorMessageForControle = function (jqEle, existingMessage, thisOb) {
    var controleName = thisOb.GetUserFriendlyName(jqEle, thisOb);

    existingMessage = existingMessage + "\r\n" + controleName;
    return existingMessage;
}

ValidationHelper.prototype.GetUserFriendlyName = function (jqEle, thisOb) {
    var parent = jqEle.parent();
    var labelOb = parent.children('label');
    if (labelOb.length > 0)
        return labelOb.html();
    labelOb = parent.children('.control-label');
    if (labelOb.length > 0)
        return labelOb.html();

    return thisOb.GetUserFriendlyName(parent, thisOb);
}



ValidationHelper.prototype.ValidateSelect = function (jqEle, isRequired, isMarkedInvalid, thisOb) {
    var selectedVal = jqEle.val();

    if ((selectedVal == "" || selectedVal == 0 || !selectedVal) && isRequired)
        return false;
    else //if (selectedVal == "")
        return true;

}

ValidationHelper.prototype.ValidateInput = function (jqEle, isRequired, isMarkedInvalid, thisOb) {
    var type = jqEle.attr("type");
    var inputVal = jqEle.val();

    if (inputVal == "" && isRequired)
        return false;
    else if (inputVal == "")
        return true;

    if (thisOb.IsDateInput(jqEle)) {
        var dtObj = new DateOb(inputVal);
        return dtObj.IsValidDate;
    } else if (type == "number") {
        return thisOb.ValidateNumberInput(jqEle, inputVal, thisOb);
    }

    return true;
}

ValidationHelper.prototype.ValidateNumberInput = function (jqEle, val, thisOb) {
    var min = jqEle.attr("min");
    var max = jqEle.attr("max");
    var isInteger = jqEle.is("[cva-int]");

    if (!thisOb.isValidNumber(jqEle, val, isInteger))
        return false;

    var value = !isInteger ? parseFloat(val) : parseInt(val); // to be extended for decimal  
    if (min) {
        if (parseInt(min) > value)
            return false;
    }
    if (max) {
        if (parseInt(max) < value)
            return false;
    }

    return true;
}


ValidationHelper.prototype.isValidNumber = function (jqEle, val, isInteger) {

    if (isInteger)
        return parseFloat(val) === parseInt(val);

    return !isNaN(val);/// this will be extended when adn if needed to include 
    ///isNaN(-1.23) //false
    ///isNaN(5 - 2) //false
    ///isNaN(null) //false
    ///isNaN('') //false
    //isNaN(true) //false
}


ValidationHelper.prototype.IsDateInput = function (jqEle) {
    return jqEle.parent().hasClass("date");
}

