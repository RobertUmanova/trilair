﻿//ddlSelectId: id of the drop down list used to popoulate. When the component is used to opend a details based on drop down list
// currently the most advance partial ng for modals 
function NgPatientSelectListHelper(ngScope, httpPost, onSelectFunc, parentOb, lstPreselectedPatiens) {
    var thisOb = this;
    this.ngScope = ngScope;
    this.teamId = ngScope.TeamId;
    this.httpPost = httpPost;
    this.postManager = new PostManager(httpPost);
    this.patientStatSlcCmp = {};
    this.apiController = "ApiPatient";
    this.modalId = "#PatSelectListModalDet";
    this.tableId = '#tblMultipleSelectUsers';
    this.onSelectFunc = onSelectFunc;
    this.parentOb = parentOb;
    this.patientStatusTypes = [];
    this.selectedPatients = []; // List of patients we use so we know which patients are selected. // Object structure: Object: { Id: 99; FullName: 'Some Patient' }
    this.semaphoreHelper = new SemaphoreHelper();

    this.filter = [];
    this.filter.searchString = '';
    this.filter.patientStatusIds = '';
    this.filter.preselectedPatientStatuses = [];

    this.patients = []; // List of patients that you show on the page in the main table and filter them.

    // xxxxxxxxxxxxx   EVENT   xxxxxxxxxxxxx
    this.openPopup = function (thisObject, preselectedValues) {
        var thisOb = thisObject || this;

        thisOb.semaphoreHelper.AddCounter();
        //thisOb.linkSpecialElementsFromObjectToUI();
        thisOb.preselectValues(thisOb, preselectedValues);
        thisOb.preselectFilterDefaults(thisOb);
        thisOb.semaphoreHelper.RemoveCounter();
        thisOb.loadPageResult(thisOb);
        thisOb.openModal();
    };

    this.preloadDataAndInitSpecComp(this);
}

// START: On Init ------------------------------------------------------------------------------
NgPatientSelectListHelper.prototype.preloadDataAndInitSpecComp = function (thisObject) {
    var thisOb = thisObject || this;

    thisOb.getPatientStatuses();
}

NgPatientSelectListHelper.prototype.getPatientStatuses = function () {
    var thisOb = this;

    thisOb.postManager.get("/ApiPatientStatusTypes/GetPatientStatusTypes",
        function (response) {

            // angular.copy(response.data, thisOb.patientStatusTypes);

            var result = response.data.map(function (el, index) {
                return {
                    id: el.Id,
                    text: el.Name
                };
            });
            var functionOnChange = function () {
                thisOb.onFilterChange(thisOb);
            };
            thisOb.patientStatSlcCmp = new Select2Helper("slctPatientStatus", functionOnChange);

            thisOb.patientStatSlcCmp.LoadSelect2WithData(result, thisOb.patientStatSlcCmp);


            //get default active statuses that are preloaded when you open PopUp
            thisOb.postManager.get("/ApiPatientStatusTypes/GetDefaultActivePatientStatuses",
                function (response) {

                    angular.copy(response.data, thisOb.filter.preselectedPatientStatuses);

                    //thisOb.filter.patientStatusIds = response.data.join(",");
                });
        });
};
// END: On Init ------------------------------------------------------------------------------

NgPatientSelectListHelper.prototype.preselectValues = function (thisObj, preselectedValues) {
    var thisOb = thisObj || this;

    if (preselectedValues) {
        angular.copy(preselectedValues, thisOb.selectedPatients);

        thisOb.selectedPatients.forEach(function (pat) {

            thisOb.addAddressToFullName(pat);
        });
    }
};

NgPatientSelectListHelper.prototype.addAddressToFullName = function (patient) {

    var displayName = PatinentNamingHelper.getNameAndAddress(patient.FullName, patient.Address);

    patient.FullName = displayName;
};

NgPatientSelectListHelper.prototype.preselectFilterDefaults = function (thisObj) {
    var thisOb = thisObj || this;

    thisOb.patientStatSlcCmp.SetMultipleValues(thisOb.filter.preselectedPatientStatuses, thisOb.patientStatSlcCmp);
};


// xxxxxxxxxxxxx   EVENT   xxxxxxxxxxxxx
NgPatientSelectListHelper.prototype.onFilterChange = function (thisObject) {
    var thisOb = thisObject || this;

    thisOb.loadPageResult(thisOb);
}


NgPatientSelectListHelper.prototype.linkSpecialElementsFromObjectToUI = function (preselectedPatients) {
    var thisOb = this;

    // not implemented for now
};


NgPatientSelectListHelper.prototype.linkSpecialElementsFromUIToObject = function (thisObject) {
    var thisOb = thisObject || this;

    var selectedValues = $('#slctPatientStatus').val();
    if (selectedValues)
        thisOb.filter.patientStatusIds = selectedValues.toString();
    else
        thisOb.filter.patientStatusIds = '';
};

NgPatientSelectListHelper.prototype.openModal = function () {
    var thisOb = this;

    $(thisOb.modalId).modal();

    $(thisOb.modalId).on('hidden.bs.modal', function (e) {
        angular.copy([], thisOb.patients);
    });
}


NgPatientSelectListHelper.prototype.loadPageResult = function (thisObject) {
    var thisOb = thisObject || this;

    thisOb.linkSpecialElementsFromUIToObject(thisOb);

    thisOb.getData(thisObject);
};

NgPatientSelectListHelper.prototype.getData = function (thisObject) {
    var thisOb = thisObject || this;

    this.postManager.get("/" + this.apiController + "/GetListOfPatientsForTourPlanningUI?searchString=" + thisOb.filter.searchString + "&selectedStatuses=" + thisOb.filter.patientStatusIds,
        function (response) {

            thisOb.formatDates(response.data)
            angular.copy(response.data, thisOb.patients);
            thisOb.createCheckboxes(thisOb.selectedPatients);
        });
};


NgPatientSelectListHelper.prototype.createCheckboxes = function (preselectedPatients) {
    var thisOb = this;
    var tableId = this.tableId;
    var modalId = this.modalId;

    setTimeout(function () {

        CheckboxHelper.initialize(modalId);

        if (preselectedPatients) {
            for (var i = 0; i < preselectedPatients.length; i++) {
                thisOb.selectPatient(preselectedPatients[i].Id, true);
            }
        }

        $('#ckbSelectAll').show();

        $('#ckbSelectAll').on('ifChecked', function (event) {

            thisOb.selectAllPatients(true);
        });
        $('#ckbSelectAll').on('ifUnchecked', function (event) {

            thisOb.selectAllPatients(false);
        });

        $(tableId + ' input:checkbox').each(function (index, element) {
            $(element).parent().on('ifChecked', function (event) {

                thisOb.onPatientChecboxChange(event.target.id, true);
            });
            $(element).parent().on('ifUnchecked', function (event) {

                thisOb.onPatientChecboxChange(event.target.id, false);
            });
        });
    }, 200);
};

// xxxxxxxxxxxxx   EVENT   xxxxxxxxxxxxx
///Paremeter isChecking is true in case you select all and false in case deselect all
NgPatientSelectListHelper.prototype.selectAllPatients = function (isChecking) {
    var thisOb = this;

    thisOb.patients.forEach(function (pat) {
        var checked = CheckboxHelper.ischecked(pat.Id);

        if (checked == isChecking)
            return;

        thisOb.selectPatient(pat.Id);
    });
}


NgPatientSelectListHelper.prototype.selectPatient = function (id, justUpdateCheckbox) {
    var thisOb = this;
    var isChecked = CheckboxHelper.ischecked(id);

    CheckboxHelper.checkUncheck('#' + id, !isChecked);
    if (justUpdateCheckbox) {

    } else {
        thisOb.onPatientChecboxChange(id, !isChecked);
    }
};

// xxxxxxxxxxxxx   EVENT   xxxxxxxxxxxxx
NgPatientSelectListHelper.prototype.onPatientChecboxChange = function (patientId, isChecked) {
    var thisOb = this;

    var patient = thisOb.getPatientById(patientId);

    if (patient && patient.Id && patient.FullName) {

        if (isChecked) {
            var name = PatinentNamingHelper.getNameAndAddress(patient.FullName, patient.Address);

            var pat = { Id: patient.Id.toString(), FullName: name }
            thisOb.selectedPatients.push(pat);
        }
        else {
            thisOb.selectedPatients = thisOb.selectedPatients.filter(function (pat) {
                return pat.Id.toString() !== patient.Id.toString();
            });
        }
    }

    try {
        thisOb.ngScope.$digest();
    }
    catch (err) {
    }

};

// xxxxxxxxxxxxx   EVENT   xxxxxxxxxxxxx
NgPatientSelectListHelper.prototype.saveModal = function () {
    var thisOb = this;

    var patients = [];

    patients = thisOb.selectedPatients;

    if (thisOb.onSelectFunc)
        thisOb.onSelectFunc(patients, thisOb.parentOb);

    CheckboxHelper.checkUncheck('#ckbSelectAll', false);

    thisOb.closeDetModal();
};

NgPatientSelectListHelper.prototype.getPatientById = function (patientId) {
    var thisOb = this;

    if (!patientId) {
        return;
    }

    var patient = {};

    thisOb.patients.forEach(function (pat) {
        if (pat.Id.toString() == patientId.toString()) {
            patient = pat;
            return;
        }
    });

    return patient ? patient : null;
};

NgPatientSelectListHelper.prototype.formatDates = function (patients) {
    patients.forEach(function (patient) {
        patient.BirthDate = (new DateOb(patient.BirthDate)).printDate;
    });
};

NgPatientSelectListHelper.prototype.closeDetModal = function () {
    var thisOb = this;

    $(this.modalId).modal('toggle');
}

