﻿
function ResponseHelper() {

}

ResponseHelper.CheckResponse = function (response) {

    if (typeof response.data == "object")
        return true;

    if (response.data.indexOf("!DOCTYPE html") == -1)
        return true;

    if (response.data.indexOf("login") == -1)
        return true;

    window.location.href = "/LoggingUsers/Login";

    //The response will practically never be false,
    //so if you react on it like in document download 
    // do the code where you execute only if it's true instead of the standard if false return
    return false;
    //in case the response if long in page we should redirect the page. 
}


ResponseHelper.IsSuccessOrNotify = function (response) {

    //Review with luka - check already done in postManager
    //ResponseHelper.CheckResponse(response);

    if (response.success) {
        if (response.warningMessage)
            alertOb.warningMessage(globalText.Attention, response.warningMessage);

        return true;
    }

    alertOb.errorMessage(globalText.Attention, response.errorMessage);
    event.preventDefault();
    return false;
}

ResponseHelper.CheckResponseNotifyAndCallAction = function (response, onSuccess, onError) {
    var onSuccessFunc = onSuccess || null;
    var onErrorFunc = onError || null;

    ResponseHelper.CheckResponse(response);

    if (response.data.success) {
        if (response.data.warningMessage)
            alertOb.warningMessage(globalText.Attention, response.data.warningMessage, onSuccessFunc);
        else if (onSuccessFunc != null)
            onSuccessFunc(response);

        event.preventDefault();
        return;
    }

    alertOb.errorMessage(globalText.Attention, response.data.errorMessage, onErrorFunc);
    event.preventDefault();
    return;
}





