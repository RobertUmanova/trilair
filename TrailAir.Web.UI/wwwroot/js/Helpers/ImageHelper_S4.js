﻿
/**
 * ImageHelper is used to create Dropzone for the image
 *
 * Parameters :
 * 1. div imageContainerId,
 * 2. div dropzoneId,
 * 3. url pointing to the ApiDocument controller and specific method
 * 4. method which will be called when we upload mage to the server
 * 5. Object to which we will add image Id from onComplete response
 * 
 * Dependencies:
 *  current language, variable defined as lanUiLangFull from Views/Shared/_Localization.cshtml,
 *  /lib/dropzone/dropzone.js,
 *  js/Helpers/FileHelper_S3.js,
 *  js/DropzoneHelper_SingleFile_S6.js
 */
function ImageHelper(imageContainerId, imageDropzoneId, onComplete, onCompleteCaller, wideImage) {
    this.imageDropzoneId = imageDropzoneId;
    this.imageContainerId = imageContainerId;
    this.url = "/" + lanUiLangFull + '/ApiDocument/UploadFile';
    this.hasImage = false;
    this.imgSrc = "";
    this.imgName = '';
    this.wideImage = wideImage;
    this.currentBo = onCompleteCaller;

    this.dropzoneHelper = new DropzoneHelper(this.imageDropzoneId, this.url, {
        onComplete: onComplete,
        onCompleteCaller: onCompleteCaller,
        acceptedFormats: '.png,.jpg,.jpeg, .heif, .heic',
        displayType: wideImage ? 'wideImage' : 'smallImage',
        maxFiles: 3
    });
    //this.resizeImgContainer();
}

ImageHelper.prototype.processImage = function (image) {
    var thisOb = this;
    if (image) {
        thisOb.hasImage = true;
        //thisOb.imgSrc = 'data:image/png;base64,' + image.Data;
        thisOb.imgSrc = "/" + lanUiLangFull + '/ApiDocument/DownloadFile?elementId=' + image.id;

        //// hide file extension from the image name when opening an existing object(for edit)
        image.name = image.name.split(".")[0];

        thisOb.imgName = image.name;
        $(thisOb.imageContainerId).click(function () {
            FileHelper.previewImage(image.id, image.name);
        });
    }
    else {
        thisOb.resetProperties();
    }
};

ImageHelper.prototype.resizeImgContainer = function (image) {
    var thisOb = this;
    var container = $(thisOb.imageContainerId);
    if (thisOb.wideImage == true) {
        container.css("height", "150px");
        container.css("min-height", "150px");
        container.css("width", "100%");
    }
    else {
        container.css("height", "120px");
        container.css("min-height", "120px");
        container.css("width", "100px");
    }
};

ImageHelper.prototype.uploadNewImage = function () {
    var thisOb = this;

    thisOb.resetProperties();
};

ImageHelper.prototype.saveImage = function () {
    var thisOb = this;

    return thisOb.dropzoneHelper.processQueue();
};

ImageHelper.prototype.clearDropzone = function () {
    var thisOb = this;

    thisOb.dropzoneHelper.resetDropzoneWithSingleFile();
};



ImageHelper.prototype.resetProperties = function () {
    var thisOb = this;

    thisOb.hasImage = false;
    thisOb.imgSrc = null;
    thisOb.imgName = null;
};

