﻿
var dateObHelperStandardSplit = "/";
function StringToDate(dateStr) {
    var arrVal = dateStr.split(dateObHelperStandardSplit);
    arrVal = new Date(arrVal[2], arrVal[1] - 1, arrVal[0], 0, 0, 0, 0);
    return arrVal;
}


function DateOb(dateVal, hasTime, noEnd) {
    this.printDate;
    this.date;
    this.hasTime = hasTime || false;
    this.noEnd = noEnd || false;
    this.fullCmp;
    this.fullDateInt;
    this.IsValidDate = true;

    //  DateOb.dateFormat = "dd/mm/yyyy"; // if you change this you need to change also stringToDate() function 


    var date = dateVal ?
        ((typeof dateVal) == "string"
            ?
            this.StringsToDate(dateVal, DateOb.dateFormat)
            :
            dateVal
        ) : null /*new Date()*/;

    if (this.noEnd == true)
        date.setMonth(date.getMonth() + 1);

    this.updateValue(date);
}

DateOb.prototype.StringsToDate = function (dateVal, hasTime) {

    try {
        var dtLength = dateVal.length;
        if (dtLength >= 8 && dtLength <= 10 && dateVal.indexOf('-') > -1) {
            var arrDate = dateVal.split('-');
            var year = parseInt(arrDate[0]);
            var month = parseInt(arrDate[1]);
            var day = parseInt(arrDate[2]);
            this.hasTime = false;

            return new Date(year, month - 1, day);
        }
        else if (dtLength == 24 || dtLength == 19) {
            // 2017-11-03T23:00:00.000Z
            var year = parseInt(dateVal.substring(0, 4));
            var month = parseInt(dateVal.substring(5, 7));
            var day = parseInt(dateVal.substring(8, 10));
            var hour = parseInt(dateVal.substring(11, 13));
            var minute = parseInt(dateVal.substring(14, 16));
            var second = parseInt(dateVal.substring(17, 19));

            if (!year || !month || !day)
                throw "Invalid date";

            if (dtLength == 24) {
                this.hasTime = true;
            }

            return new Date(year, month - 1, day, hour, minute, second);
        }
        else if (dtLength == 10) {
            var year = parseInt(dateVal.substring(6, 10));
            var month = parseInt(dateVal.substring(3, 5));
            var day = parseInt(dateVal.substring(0, 2));

            return new Date(year, month - 1, day, 0, 0, 0);
        }
        else if (this.hasTime && dtLength >= 13 && dtLength <= 16 && dateVal.indexOf(' ') > -1) {
            //10/11/2019 3:00
            var dateTimeArr = dateVal.split(" ");
            var dateArr = dateTimeArr[0].split(dateObHelperStandardSplit);
            var timeArr = dateTimeArr[1].split(":");
            return new Date(dateArr[2], dateArr[1] - 1, dateArr[0], timeArr[0], timeArr[1], 0);
        }
        else if (dtLength == 8 || dtLength == 14) {
            var year = parseInt(dateVal.substring(0, 4));
            var month = parseInt(dateVal.substring(4, 6));
            var day = parseInt(dateVal.substring(6, 8));

            if (dtLength == 8) {
                this.hasTime = false;
                return new Date(year, month - 1, day);
            }

            this.hasTime = true;
            var hour = parseInt(dateVal.substring(8, 10));
            var minute = parseInt(dateVal.substring(10, 12));
            var second = parseInt(dateVal.substring(12, 14));

            return new Date(year, month - 1, day, hour, minute, second);
        }
        else if (dtLength == 7) {
            var year = parseInt(dateVal.substring(3, 7));
            var month = parseInt(dateVal.substring(0, 2));
            var day = 1;

            return new Date(year, month - 1, day, 0, 0, 0);
        } else {
            throw ('Date not valid Exception');
        }
    } catch (err) {
        this.IsValidDate = false;
        return null;
    }

    return this.updateValue(new Date(this.date.getTime() + (numDays * 24 * 60 * 60 * 1000)));
}

DateOb.prototype.addTimeToDate = function (time) {
    var timeFr = time;
    if (timeFr != null && (typeof timeFr.getHours === 'function')) {
        timeFr = timeFr.getHours() + ':' + timeFr.getMinutes();
    } else {

        if (timeFr == null || timeFr.length == 0 || (timeFr.length != 2 && timeFr.length != 5))
            timeFr = "00:00";
        if (timeFr.length == 2)
            timeFr = timeFr + ":00";
    }
    var timeEl = timeFr.split(":")
    var dateWithTimeAdded = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate(), parseInt(timeEl[0]), parseInt(timeEl[1]), 0, 0);
    this.hasTime = true;
    this.updateValue(dateWithTimeAdded);

}

DateOb.prototype.addDays = function (numDays) {
    return this.updateValue(new Date(this.date.getTime() + (numDays * 24 * 60 * 60 * 1000)));
}

DateOb.prototype.sliceTime = function () {
    return this.updateValue(new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate()));
}


DateOb.prototype.getTime = function () {
    return padLeft(this.date.getHours(), 2, '0') + ":" + padLeft(this.date.getMinutes(), 2, '0');
}


DateOb.dateFormat = "dd/mm/yyyy";
DateOb.prototype.updateValue = function (newDate) {

    if (newDate == null) {
        this.date = '';
        this.printDate = '';
        this.fullCmp = '';
        this.fullDateInt = '';
        return;
    }

    this.date = newDate;
    if (!(newDate))
        return;
    this.printDate = padLeft(this.date.getDate(), 2, '0') + "/" + padLeft(this.date.getMonth() + 1, 2, '0') + "/" + this.date.getFullYear();

    var strTime = !this.hasTime ? "" : padLeft(this.date.getHours(), 2, '0') + padLeft(this.date.getMinutes(), 2, '0') + padLeft(this.date.getSeconds(), 2, '0');

    if (this.hasTime)
        this.printDate = this.printDate + " " + strTime.substring(0, 2) + ":" + strTime.substring(2, 4) + ":" + strTime.substring(4, 6);

    this.fullCmp = this.date.getFullYear() + padLeft(this.date.getMonth() + 1, 2, '0') + padLeft(this.date.getDate(), 2, '0') + strTime;

    this.fullDateInt = parseInt(this.fullCmp);
    return this;
}

function padLeft(nr, n, str) {
    return Array(n - String(nr).length + 1).join(str || '0') + nr;
}