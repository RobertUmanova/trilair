/**
 * 
 * @param {string} elementId
 */
function SummernoteHelper(elementId, config={}) {
    this.elementId = elementId;
    this.config = config;
    this.instance = null;
}
SummernoteHelper.prototype.renderBasic = function () {
   this.instance = $(this.elementId).summernote({
        placeholder: "",
        height: 120,
        toolbar: [["style", ["bold", "underline", "clear"]]],
        codeviewFilter: false,
        codeviewIframeFilter: true
   });
};

SummernoteHelper.prototype.clearText = function () {
    this.instance.summernote('code','<p></p>');
}
SummernoteHelper.prototype.getText = function () {
    return this.instance.summernote('code')
}

SummernoteHelper.prototype.setText = /**@param {string} text @returns {string}*/ function (text) {
    this.instance.summernote('code', text)
}
