﻿//https://visjs.github.io/vis-timeline/examples/timeline/
// https://visjs.github.io/vis-timeline/docs/timeline/

function TimelineHelper(TimelineDivid, data, onClickElementFunction) {

    var thisOb = this;
    thisOb.onClickElementFunction = onClickElementFunction;
    thisOb.timelineDiv = document.getElementById(TimelineDivid);
    thisOb.options = {};

    // all items in thisOb.items, and temporary filtered items are kept  in thisOb.filteredItems
    thisOb.items = [];
    thisOb.filteredItems = [];

    // our timeline
    thisOb.timeline;

    // format: "dd/mm/yyyy" "hh:mm"
    thisOb.setDateAndTime = function (date, time) {
        var dateOb = new DateOb(date).date;
        if (time)
        {
            var hours = time.split(":")[0];
            var minutes = time.split(":")[1];
            dateOb.setHours(hours, minutes);
        }
        return dateOb;
    }
    thisOb.groups = [];
    thisOb.groupIndex = 0;
    thisOb.items = [];
    data.forEach(item => {
        var subgroup = `sub_group_${item.taskPhase}`;
        if (thisOb.groups.filter(_ => _.title === item.type).length > 0) {

        } else
        {
            thisOb.groups.push(
                {
                    id: thisOb.groupIndex,
                    treeLevel: 1,
                    title: item.type,
                    content: "",
                    /*subgroupVisibility: { subgroup: true }*/
                });
            thisOb.groupIndex++;
        }
        thisOb.items.push(
            {
                id: item.id,
                start: thisOb.setDateAndTime(item.planningStart, item.planningStartHour),
                end: thisOb.setDateAndTime(item.planningEnd, item.planningEndHour),
                title: item.title,
                type: 'point',
                className: `um-task-phase${item.taskPhase}`,
                startDateOb: new DateOb(item.planningStart),
                endDateOb: new DateOb(item.planningEnd),
                dependentOn: item.dependsOnTasksList,
                industry: item.industry,
                locationId: item.locationId,
                taskPhaseId: item.taskPhase,
                group: thisOb.groups.filter(_ => _.title == item.type).map(_ => _.id),
                subgroup: item.industry.name,
                assignedUsers: item.assignedUsers.map(assignedUser => {
                    return { id: assignedUser.id.toString() }
                }),
                companies: item.assignedUsers.map(assignedUser => {
                    return assignedUser.company;
                })
            }
        );
    });
    var start = thisOb.items[0].start;
    var end = thisOb.items[thisOb.items.length - 1].end;

    var min = new Date(start);
    min.setFullYear(start.getFullYear() - 1);

    var max = new Date(end);
    max.setFullYear(end.getFullYear() + 1);

    // Configuration for the Timeline
    thisOb.options = {
        locale: lanUiLangShort, /// see _Localization.cshtml
        template: function (item) {
            return `<div>
                        <div>
                            <span>${item.title}</span>
                        </div>
                    </div>`;
        },
        width: '100%',
        /*height: '400px',     <- this field is set in umaSoft.css */
        margin: {
            item: 20
        },
        min: min,
        max: max,
        zoomMin: 6000000, //  miliseconds: max zoom in is 5 min
        zoomMax: 315360000000, // miliseconds: max zoom out is 10 years
        //configure: function (option, path)
        //{
        //}
    };

    // TO TEST ALL OPTIONS, IT IS THE BEST TO ADD IN thisOb.options {} following:
    // configure: function(option, path){
    //}
    // it will display in html all option parameters available you can play with!!

    /*
     *  return `<div>
                        <div class="task">
                            <div class="taskTitle">
                                <div>
                                    <span>${item.title}</span>
                                </div>
                            </div>
                            <div>
                                <span>Start: ${item.startDateOb.printDate}</span>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span>End: ${item.endDateOb.printDate}</span>
                            </div>
                        </div>
                    </div>`;
     */

    // Create a Timeline
    thisOb.createNewTimeline(thisOb.items);
}

TimelineHelper.prototype.createNewTimeline = function (items)
{
    var thisOb = this;
    if (thisOb.timeline)
    {
        thisOb.timeline.destroy();
    }
    thisOb.timeline = new vis.Timeline(thisOb.timelineDiv, items, thisOb.groups, thisOb.options);
    //thisOb.moveTo();
    thisOb.generateArrows();
    //FIX: lets try to zoom out so all tasks are not in one line
    //thisOb.timeline.zoomOut(5);
    //console.log(items);
    //thisOb.timeline.zoomOut(1, { animation: true });

   
    //thisOb.timeline.on('select', function (properties) {
    //    // it fires select events on timeline when you have selected anything but not an item, if item is selected then properties.items.length will be 1 or greater then 0 
    //    if (properties.items.length > 0) {
    //        var clickedItemId = properties.items[0];
    //        //thisOb.onClickElementFunction(clickedItemId);
    //        var id = properties.items[0]
    //        console.log(items);
    //        thisOb.filteredItems = items.filter(function (item) {
    //            return (item.industry === "Sanitary");
    //        });
    //        console.log(thisOb.filteredItems);

    //        thisOb.timeline.destroy();
    //        thisOb.createNewTimeline(thisOb.filteredItems);
    //    }
    //});
    thisOb.timeline.on('click', function (properties) {
        console.log(properties);
        // if we have not clicked on empty field on timeline
        if (properties.item != null)
        {
            thisOb.onClickElementFunction(properties.item);
        }
    });
    //setTimeout(function () {
    //    thisOb.timeline.zoomOut(0, { animation: true });


    //}, 800); 

}
TimelineHelper.prototype.generateArrows = function ()
{
    var thisOb = this;
    var index = 0;
    var arrows_array = [];
    thisOb.items.forEach(item => {
        item.dependentOn.forEach(dependentItem => {
            arrows_array.push({ id: index, id_item_1: dependentItem.id, id_item_2: item.id })
        })
        index++;
    })

    const my_Arrow = new Arrow(thisOb.timeline, arrows_array);
}


// when we want to display timeline we want to nicely animate the timeline rendering
// the difference is when we are on TaskDetails or Index page
// we will focuns 
TimelineHelper.prototype.moveTo = function () {

    var timeOutMiliseconds = 1000;
    var thisOb = this;
    // get URL parameters 
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        value = value.replace('#', '');
        vars[key] = value;
    });

    var taskId = Number(vars["TaskId"]); // <- used if on taskDetails
    var task = thisOb.items.find(_ => _.id === taskId);

    //// if on task details
    //if (window.location.href.toString().includes("TaskDetails")) {

    //    setTimeout(function () {
    //        thisOb.timeline.focus(task.id, { animation: true });
    //    }, timeOutMiliseconds); 
    //}
    //// if on index page
    //else
    //{
    //}
    var taskIds = thisOb.items.map(_ => _.id);
    setTimeout(function () {
        thisOb.timeline.focus(taskIds, { animation: true });
    }, timeOutMiliseconds); 
}