﻿///Used to add the tooltip add update value
///Fpr now it works with select elements but can also with others
//You pass him the select element that need the popo up menu and what happens when you click add or edit existing 

// Expects Sweet Alert object to be globally initialized - now in AlertObject_S4.js -- Swisscon has a alertobject, check if this is right

function TooltipEditHelper(elementId, onNewClickEvent, onEditClickEvent, isDdlMode, labelElement, modalData) {
    var thisInstance = this;
    this.elementId = elementId;
    this.modalData = modalData;
    this.isDdlMode = isDdlMode || true;
    this.jqueryDdlElement = $("#" + elementId);
    this.jqueryDdlParentElement = this.jqueryDdlElement.parent();
    this.labelElement = '';
    if (labelElement && labelElement != '') {
        this.labelElement = document.getElementById(labelElement);
    }
    else {
        this.labelElement = this.jqueryDdlParentElement.children("label")[0];
    }
    this.jqueryDdlLabelElement = $(this.labelElement);
    this.onEditEvent = function () {
        var selectedID = null;
        if (thisInstance.isDdlMode) {
            var x = thisOb.slctUsersPerCompany.GetSelectValue();
            selectedID = thisInstance.jqueryDdlElement.val();
            if (!(selectedID) || selectedID == "" || selectedID == "0" || selectedID == 0) {
                //alert(globalText.SelectElementEdit);
                alertOb.warningMessage(globalText.Warning, globalText.SelectElementEdit, null);
                return;
            }
        }
        thisInstance.tippy.hide($("div.tippy-popper.html-template")[0]);
        onEditClickEvent(selectedID, thisInstance.modalData);
    };
    this.onNewEvent = function () {
        thisInstance.tippy.hide($("div.tippy-popper.html-template")[0]);
        //thisInstance.tippy.hide(thisInstance.tippy);
        onNewClickEvent(thisInstance.modalData);
    };
    this.tippy = null;
    this.AddElements();
}
var TooltipEditHelperWorkAround = {};

TooltipEditHelper.prototype.SelectDdlValue = function () {

}

TooltipEditHelper.prototype.AddElements = function () {
    var thisInstance = this;
    var onNewClickEventName = "newBtn_click_" + this.elementId;
    var onEditClickEventName = "editBtn_click_" + this.elementId;
    var id = "label_id_" + this.elementId;
    var templateId = "template_" + this.elementId;
    this.jqueryDdlLabelElement.attr('id', id);
    TooltipEditHelperWorkAround[onEditClickEventName] = this.onEditEvent;
    TooltipEditHelperWorkAround[onNewClickEventName] = this.onNewEvent;
    this.editBtn = $('<div class="btn" id="editBtn_' + this.elementId + '" onclick="TooltipEditHelperWorkAround.' + onEditClickEventName + '()"><i  style="font-size: 30px; color: gray;" class="fa fa-edit"></i></div>');
    this.newBtn = $("<div class='btn' id='newtBtn_" + this.elementId + "' onclick='TooltipEditHelperWorkAround." + onNewClickEventName + "()'><i style='font-size: 30px; color: gray;' class='fa fa-plus-circle'></i></div>");
    this.popUpHtml = $('<div id="' + templateId + '" style="display:none"></div>');
    this.jqueryDdlLabelElement.css("cursor", "pointer");
    $('<span>&nbsp;</span>').appendTo(this.jqueryDdlLabelElement);
    $('<button type="button" class="btn btn-primary"><i style="font-size: 16px;" class="fa fa-edit"></i></button>').appendTo(this.jqueryDdlLabelElement);
    this.popUpHtml.append("<p>" + globalText.AddEditExistingList + "</p>");


    this.popUpHtml.append(this.newBtn);
    //this.popUpHtml.append(this.editBtn);

    this.jqueryDdlParentElement.append(this.popUpHtml);


    this.tippy = new Tippy('#' + id, {
        //beforeShown: function () {
        //    thisInstance.OneBeforeShow();
        //},
        //shown: function () {
        //    thisInstance.OneBeforeShow();
        //    $("#editBtn_ddlCountryId").hide();
        //},
        position: 'right',
        animation: 'perspective',
        theme: 'light',
        trigger: 'click',
        interactive: true,
        html: templateId
    })


}
