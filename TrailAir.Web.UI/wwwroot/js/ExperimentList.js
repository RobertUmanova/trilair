﻿var app = new Vue({
    el: '#vue-app',
    data() {
        return {
            searchString: '',
            apiController: "/ApiExperiment/",
            postManager: new PostManager(),
            experimentList: [],
            teamsList: [],
            teamsIds: '',
            statusList: [],
            statusIds: '',
            defaultStatuses: [1, 2],
            //Pager code
            currentPage: 1,
            perPage: 10,
        }
    },
    mounted() { // We make API calls here
        var thisOb = this;

        thisOb.getFilters();
        thisOb.getData();
    },
    watch: { // Watches for changes in data

    },
    methods: {
        getData() {
            var thisOb = this;

            var workingStatuses = '';

            if (thisOb.statusIds === '') {
                workingStatuses = thisOb.defaultStatuses.join();
            } else {
                workingStatuses = thisOb.statusIds;
            }
            
            var url = thisOb.apiController + "GetFilteredList?q=" + thisOb.searchString
                + '&teamsIds=' + thisOb.teamsIds + '&statusIds=' + workingStatuses;

            // Get the data with the pager
            // You have to instance it every time you make a request because the "url" changes 
            // according to the filters!!
            var pager = new PagerHelper('experimentListPagerBottom', url,
                function (response) {

                    thisOb.experimentList = response.data.data.items;
                    thisOb.linkSpecialElementsFromObjectToUI();
                },
                100, null, 'experimentListPagerTop');
            // 100 rows per page

            pager.getCurrentPage();
        },
        openEditPopup(teamId, isPublished, isFinished) {
            if (!isFinished) {
                this.$refs.experimentEditCmp.openModalPopUp(teamId);
                this.$refs.experimentEditCmp.experimentDraftStatus = !(isPublished || isFinished);
            }
        },
        linkSpecialElementsFromObjectToUI() {
            var thisOb = this;
            Vue.nextTick(() => {
                $('.dropdown-toggle').dropdown();
            });
        },
        openManualResultEdit(participantId, experimentId) {
            var thisOb = this;

            this.$refs.expParticResCmp.openModalPopUp(participantId, experimentId);
            this.$refs.expParticResCmp.parentGetData = thisOb.getData;
        },
        configureCheckBoxes() {
            var thisOb = this;

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            var allControlSampleiChecks = $("input[id^='ckb_c_']");
            var allRandomizationiChecks = $("input[id^='ckb_r_']");

            allControlSampleiChecks.each(function (i, idString) {

                var idFull = '#' + idString.id;
                var id = idString.id.replace('ckb_c_', '');

                var exp = thisOb.experimentList.find(_ => _.id == id);
                if (exp) {
                    if (exp.controlSample) {

                        $(idFull).iCheck('check');
                    }
                    else $(idFull).iCheck('uncheck');

                    $(idFull).prop("disabled", true);
                }
            });

            allRandomizationiChecks.each(function (i, idString) {

                var idFull = '#' + idString.id;
                var id = idString.id.replace('ckb_r_', '');

                var exp = thisOb.experimentList.find(_ => _.id == id);
                if (exp) {
                    if (exp.randomization) {

                        $(idFull).iCheck('check');
                    }
                    else $(idFull).iCheck('uncheck');

                    $(idFull).prop("disabled", true);
                }
            });
        },
        deleteExperiment(id) {
            var thisOb = this;

            alertOb.confirmMessage("Are you sure you want to delete this experiment?", function () {
                thisOb.postManager.get(thisOb.apiController + 'DeleteBo?id=' + id, function (response) {
                    if (!response.data.success) {
                        alertOb.errorMessage("Attention", "An error occured");
                        // TODO: Replace with global text!
                        event.preventDefault();
                    }
                    else {
                        thisOb.getData();
                        ToastrHlp.updateSuccessMsg();
                    }
                });
            })
        },
        showDuplicateModal(id) {
            var thisOb = this;

            thisOb.$refs.experimentDuplicateCmp.parentGetData = thisOb.getData;
            thisOb.$refs.experimentDuplicateCmp.openModalPopUp(id);
        },
        getTeamsFilter(teamIdToSet) {
            var thisOb = this;

            thisOb.postManager.get('/ApiTeam/GetDDList',
                function (response) {

                    response.data.data.unshift({ id: 0, text: 'All' });
                    thisOb.teamsList = response.data.data;

                    // TODO: Check this how to automate Select2 Loading
                    thisOb.$refs.slctTeamFilter.ddlData = thisOb.teamsList;
                    thisOb.$refs.slctTeamFilter.loadSelect2WithData();

                    // TODO: Replace with Select option with globalText

                    //if (teamIdToSet && teamIdToSet != 0) {
                    //    thisOb.$refs.slctTeam.$refs.slctDDL.setValue(teamIdToSet, null, true);
                    //}
                });
        },
        getStatusFilter() {
            var thisOb = this;


            thisOb.postManager.get('/ApiExperimentStatus/GetDDList',
                function (response) {

                    // TODO: Replace with Select option with globalText
                    response.data.data.unshift({ id: 0, text: 'All' });
                    thisOb.statusList = response.data.data;

                    // TODO: Check this how to automate Select2 Loading
                    thisOb.$refs.slctStatusFilter.ddlData = thisOb.statusList;
                    thisOb.$refs.slctStatusFilter.loadSelect2WithData();
                    thisOb.$refs.slctStatusFilter.setMultipleValues(thisOb.defaultStatuses);
                });
        },
        getFilters() {
            var thisOb = this;

            thisOb.getTeamsFilter();
            thisOb.getStatusFilter();
        },
        onTeamFilterChange(values) {
            var thisOb = this;

            if (values) thisOb.teamsIds = values;
            if (!thisOb.teamsIds) thisOb.teamsIds = '';

            thisOb.onFilterChange();
        },
        onStatusFilterChange(values) {
            var thisOb = this;

            if (values) thisOb.statusIds = values;
            if (!thisOb.statusIds) tihsOb.statusIds = '';

            thisOb.onFilterChange();
        },
        onFilterChange() {
            var thisOb = this;

            thisOb.getData();
        },
        openExperimentWizard(experimentId, participantId) {
            var thisOb = this;

            window.location = experimentWizardPage + "/" + experimentId + "/" + participantId;
        },
        isDownloadDisabled(experiment) {
            var thisOb = this;

            var hasSubmittedResultFound = false;
            if (experiment && experiment.participants && experiment.participants.length > 0) {

                experiment.participants.forEach(function (par) {

                    if (par.hasSubmittedResult) {
                        hasSubmittedResultFound = true;
                    }
                });
                return !hasSubmittedResultFound;
            }
            return true;
        },
        downloadWizardResult(expId) {
            var thisOb = this;

            var downloadUrl = '/en-GB/ApiExperiment/GetExperimentResultExcel/?id=' + expId;

            window.open(downloadUrl, '_self');
        },
        showPreparationInformationModal(experimentId) {
            var thisOb = this;

            thisOb.$refs.experimentPreparationInformationCmp.openModalPopUp(experimentId);
        }
    }
});