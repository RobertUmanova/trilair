﻿var app = new Vue({
    el: '#vue-app',
    data() {
        return {
            apiController: "/ApiExperiment/",
            postManager: new PostManager(),
            currentElement: new Experiment,
            experimentId: experimentIdGlobal,
            participantId: participantIdGlobal,
            participantResult: {},
            isResumeButtonDisabled: false
        }
    },
    mounted() { // We make API calls here
        var thisOb = this;

        thisOb.getData();
    },
    watch: { // Watches for changes in data

    },
    methods: {
        getData() {
            var thisOb = this;
            thisOb.postManager.get('/ApiExperimentParticipantResult/' + 'GetByParticAndExpForWizard?participantId=' + thisOb.participantId +
                '&experimentId=' + thisOb.experimentId,
                function (response) {
                    thisOb.currentElement = response.data.data;

                    thisOb.checkDisablingOfResumeButton();
                });

        },
        openExperimentWizardTest(experimentTestSample, baseExperimentResultType) {
            var thisOb = this;

            if (experimentTestSample.isSubmitted) return;

            window.location = experimentWizardTestPage + "/" + thisOb.experimentId + "/"
                + thisOb.participantId + "/" + experimentTestSample.id + "/" + baseExperimentResultType;
        },
        goToResumePage() {
            var thisOb = this;

            window.location = experimentWizardResumePage + '/' + thisOb.experimentId + "/"
                + thisOb.participantId;
        },
        checkDisablingOfResumeButton() {
            var thisOb = this;

            if (thisOb.currentElement && thisOb.currentElement.bechTestSampleResults) {

                var submittedCount = 0;
                
                thisOb.currentElement.bechTestSampleResults.forEach(ts => {

                    if (ts.isSubmitted == false) {
                        thisOb.isResumeButtonDisabled = true;
                    } else submittedCount++;
                });

                if (submittedCount == thisOb.currentElement.bechTestSampleResults.length && submittedCount != 0) {
                    thisOb.isResumeButtonDisabled = false;
                }
            }
        },
        dontSave() {
            var thisOb = this;

            alertOb.confirmMessage("Are you sure you want to cancel this result?", function (isConfirmed) {
                if (isConfirmed)
                    thisOb.postManager.get('/ApiExperimentParticipantResult/CancelWizardResults?experimentId=' + thisOb.experimentId + '&participantId=' + thisOb.participantId, function (response) {
                        if (!response.data.success) {
                            alertOb.errorMessage("Attention", "An error occured");
                            // TODO: Replace with global text!
                            event.preventDefault();
                        }
                        else {
                            thisOb.getData();
                            ToastrHlp.updateSuccessMsg();
                        }

                        //FIXME redirect to experiment/list
                        window.location = experimentListPage;
                    });
                else
                    thisOb.shouldBeUnloaded = true;
            })
        }
    }
});