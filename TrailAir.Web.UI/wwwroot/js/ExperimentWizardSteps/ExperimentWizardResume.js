﻿var app = new Vue({
    el: '#vue-app',
    data() {
        return {
            apiController: "/ApiExperimentParticipantResult/",
            postManager: new PostManager(),
            experimentResultsList: null,
            experimentId: experimentIdGlobal,
            participantId: participantIdGlobal,
            shouldBeUnloaded: true
        }
    },
    mounted() { // We make API calls here
        var thisOb = this;

        window.addEventListener('beforeunload', (e) => {
            if (thisOb.shouldBeUnloaded) {
                console.log(e);
                return event.returnValue = "Are you sure you want to exit?";
            }
        });

        thisOb.getData();
    },
    watch: { // Watches for changes in data

    },
    methods: {
        getData() {
            var thisOb = this;

            thisOb.postManager.get(thisOb.apiController + 'GetExperimentResultsListForParticipantResult?experimentId=' + thisOb.experimentId + '&participantId=' + thisOb.participantId,
                function (response) {
                    thisOb.experimentResultsList = response.data.data;
                });

        },
        onCancel() {
            var thisOb = this;

            thisOb.shouldBeUnloaded = false;

            alertOb.confirmMessage("Are you sure you want to cancel this result?", function (isConfirmed) {
                if (isConfirmed)
                    thisOb.postManager.get(thisOb.apiController + 'CancelWizardResults?experimentId=' + thisOb.experimentId + '&participantId=' + thisOb.participantId, function (response) {
                        
                        if (!response.data.success) {
                            alertOb.errorMessage("Attention", "An error occured");
                            // TODO: Replace with global text!
                            event.preventDefault();
                        }
                        else {

                        }

                        //FIXME redirect to experiment/list
                        window.location = experimentListPage;
                    });
                else
                    thisOb.shouldBeUnloaded = true;
            })
        },
        submitResults() {
            var thisOb = this;

            thisOb.shouldBeUnloaded = false;

            alertOb.confirmMessage("Are you sure you want to submit results?", function (isConfirmed) {
                if (isConfirmed)
                    thisOb.postManager.get(thisOb.apiController + 'WizardSubmit?experimentId=' + thisOb.experimentId + '&participantId=' + thisOb.participantId, function (response) {
                        if (!response.data.success) {
                            alertOb.errorMessage("Attention", "An error occured");
                            // TODO: Replace with global text!
                            event.preventDefault();
                        }
                        else {
                            thisOb.getData();
                            ToastrHlp.updateSuccessMsg();
                        }

                        //FIXME redirect to experiment/list
                        window.location = experimentListPage;
                    });
                else
                    thisOb.shouldBeUnloaded = true;

            })
        },
        modify(baseExperimentResultId, baseExperimentResultType) {
            var thisOb = this;
            
            this.$refs.singleExpResult.openModalPopUp(baseExperimentResultId, baseExperimentResultType);
            this.$refs.singleExpResult.parentGetData = thisOb.getData;
        }
    }
});