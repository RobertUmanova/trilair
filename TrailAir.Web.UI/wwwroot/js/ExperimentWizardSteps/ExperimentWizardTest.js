﻿var app = new Vue({
    el: '#vue-app',
    data() {
        return {
            apiController: "/ApiExperimentParticipantResult/",
            postManager: new PostManager(),
            experimentId: experimentIdGlobal,
            participantId: participantIdGlobal,
            baseExpTestSmplId: expTestSamplIdGlobal,
            //baseExperimentResultType: baseExperimentResultTypeGlobal,
            currentElement: {},
            timerStarted: false,
            minutesLabel: '',
            secondsLabel: '',
            totalSeconds: 0,
            firstDetTimeFormatted: '',
            firstDetectionTimeSet: false,
            buildUpTimeFormatted: '',
            buildUpTimeSet: false,
            fullDetTimeFormatted: '',
            fullDetTimeSet: false,
            intensitySlider: {},
            participantResult: {},
            timerStopped: false,
            nextSampleClicked: false,
            validator: new ValidationHelper('#ExpWizardTestCmp')
        }
    },
    mounted() { // We make API calls here
        var thisOb = this;

        thisOb.minutesLabel = document.getElementById("minutes");
        thisOb.secondsLabel = document.getElementById("seconds");
        thisOb.slider = document.getElementById("Intensity_Slider");

        noUiSlider.create(thisOb.slider, {
                start: 0,
                step: 0.5,
                behaviour: 'tap',
                connect: 'lower',
                range: {
                    'min': 0,
                    'max': 10
                },
                pips: {
                    mode: 'values',
                    values: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                    density: 5
                }
            });

        thisOb.getData();
        thisOb.getParticipantResult();
    },
    watch: { // Watches for changes in data

    },
    methods: {
        getData() {
            var thisOb = this;

            thisOb.postManager.get(thisOb.apiController + 'GetEmptyOrExistingBaseExpResultByType'
                + '?experimentId=' + thisOb.experimentId
                + '&participantId=' + thisOb.participantId
                + '&expTestSamplId=' + thisOb.baseExpTestSmplId
                //+ '&baseExpResultType=' + thisOb.baseExperimentResultType
                ,function (response) {
                    thisOb.currentElement = response.data.data;
                    //thisOb.setSampleNumberTitle();
                });

        },
        getParticipantResult() {
            var thisOb = this;

            thisOb.postManager.get(thisOb.apiController + 'GetByParticAndExp?participantId=' + thisOb.participantId +
                '&experimentId=' + thisOb.experimentId,
                function (response) {
                    thisOb.participantResult = response.data.data;
                });
        },
        saveBaseExperimentResult() {
            var thisOb = this;

            thisOb.nextSampleClicked = true;

            thisOb.linkSpecialElementsFromUIToObject();
            thisOb.validate();

            if (thisOb.validator.isValid == false) {
                thisOb.validator.DisplayErrorMessages();
                thisOb.nextSampleClicked = false;
                return;
            }

            var fullUrl = thisOb.apiController + "SubmitExpParticipantResultFromWizard";
            var bodyObject = {
                objectUi: thisOb.currentElement,
                experimentId: thisOb.experimentId,
                participantId: thisOb.participantId,
                baseExpTestSmplId: thisOb.baseExperimentResultId,
                experimentResultTypeId: thisOb.baseExperimentResultType
            };
            
            thisOb.postManager.post(fullUrl, bodyObject,
                function (response) {

                    if (!response.data.success) {
                        alertOb.errorMessage("Attention", "An error occured");
                        event.preventDefault();
                    }
                    else {
                        ToastrHlp.updateSuccessMsg();

                        setTimeout(function () {
                            window.location = experimentWizardSamplePickerPage + "/" + thisOb.experimentId + "/" + thisOb.participantId;
                        },200);
                    }
                });

        },
        validate() {
            var thisOb = this;

            thisOb.validator.Validate(false);

            // We assume you can click "Fragrance detected" in less than 1 seconds, so it will be 0
            if (!thisOb.currentElement.firstDetectionTime && thisOb.currentElement.firstDetectionTime != 0) {
                thisOb.validator.AddCustomErrorMessage("Fragrance detected", thisOb.validator);
            }

            // We assume you can click "Full fragrance" in less than 1 seconds, so it will be 0
            if (!thisOb.currentElement.fullDetectionTime && thisOb.currentElement.fullDetectionTime != 0) {
                thisOb.validator.AddCustomErrorMessage("Full fragrance", thisOb.validator);
            }

            if (!thisOb.currentElement.intensity || thisOb.currentElement.intensity == '0.00' || thisOb.currentElement.intensity == 0) {
                thisOb.validator.AddCustomErrorMessage("Intensity", thisOb.validator);
            }
        },
        linkSpecialElementsFromUIToObject() {
            var thisOb = this;

            thisOb.currentElement.intensity = thisOb.slider.noUiSlider.get();
        },
        startTimer() {
            var thisOb = this;

            thisOb.timerStarted = true;
            thisOb.totalSeconds = 0;
            setInterval(setTime, 1000);

            function setTime() {

                if (thisOb.timerStopped) {
                    thisOb.totalSeconds = 0;
                    return;
                }
                thisOb.totalSeconds++;

                thisOb.secondsLabel.innerHTML = pad(thisOb.totalSeconds % 60);
                thisOb.minutesLabel.innerHTML = pad(parseInt(thisOb.totalSeconds / 60));
            }

            function pad(val) {
                var valString = val + "";
                if (valString.length < 2) {
                    return "0" + valString;
                } else {
                    return valString;
                }
            }
        },
        onFragranceDetected() {
            var thisOb = this;

            thisOb.firstDetectionTimeSet = true;
            thisOb.currentElement.firstDetectionTime = thisOb.totalSeconds;

            thisOb.firstDetTimeFormatted = thisOb.formatMinutesAndSeconds(thisOb.totalSeconds);
        },
        onBuildup() {
            var thisOb = this;

            thisOb.buildUpTimeSet = true;
            thisOb.currentElement.buildUpTime = thisOb.totalSeconds;
            thisOb.buildUpTimeFormatted = thisOb.formatMinutesAndSeconds(thisOb.totalSeconds);
        },
        onFullFragrance() {
            var thisOb = this;

            thisOb.timerStopped = true;

            thisOb.fullDetTimeSet = true;
            thisOb.currentElement.fullDetectionTime = thisOb.totalSeconds;
            thisOb.fullDetTimeFormatted = thisOb.formatMinutesAndSeconds(thisOb.totalSeconds);
        },
        formatMinutesAndSeconds(seconds) {

            var secs = (seconds % 60).toString();
            if (secs.length < 2) secs = '0' + secs;

            if (seconds < 60) {
                return '00:' + secs;
            }
            else if (seconds < 600) {

                var mins = (parseInt(seconds / 60)).toString();

                return '0' + mins + ':' + secs;
            }

            return mins + ':' + secs;
        },
        setSampleNumberTitle() {
            var thisOb = this;

        },
        backToSamplePicker() {
            var thisOb = this;
            window.location = experimentWizardSamplePickerPage + "/" + thisOb.experimentId + "/" + thisOb.participantId;
        },
        dontSave() {
            var thisOb = this;

            alertOb.confirmMessage("Are you sure you want to cancel this result?", function (isConfirmed) {
                if (isConfirmed)
                    thisOb.postManager.get(thisOb.apiController + 'CancelWizardResults?experimentId=' + thisOb.experimentId + '&participantId=' + thisOb.participantId, function (response) {
                        if (!response.data.success) {
                            alertOb.errorMessage("Attention", "An error occured");
                            // TODO: Replace with global text!
                            event.preventDefault();
                        }
                        else {
                            thisOb.getData();
                            ToastrHlp.updateSuccessMsg();
                        }

                        //FIXME redirect to experiment/list
                        window.location = experimentListPage;
                    });
                else
                    thisOb.shouldBeUnloaded = true;
            })
        },
    }
});