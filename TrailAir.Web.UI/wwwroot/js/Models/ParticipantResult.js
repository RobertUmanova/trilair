﻿class ParticipantResult {
    constructor() {
        this.id = 0;
        this.experimentId = 0;
        this.completionDate = null;
        this.participantId = 0;
        this.benchmarkResults = [];
        this.testSampleResults = [];
    }
}