﻿class Benchmark {

    constructor() {
        this.id = 0;
        this.sampleCode = '';
        this.sampleName = '';
    }
}

class ExperimentBenchmarks {
    constructor(_experimentId) {
        this.id = 0;
        this.experimentIndex = 0;
        this.experimentId = _experimentId ? _experimentId : 0;
        this.benchmarkId = 0;
        this.benchmark = new Benchmark();
    }
}