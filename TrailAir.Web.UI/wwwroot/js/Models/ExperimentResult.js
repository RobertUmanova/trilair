﻿
class ExperimentResult {

    constructor() {
        this.id = 0;
        this.sampleCode = '';
        this.sampleName = '';
        this.isSubmitted = false;
        this.order = null;
        this.expResultType = 0;
        this.firstDetectionTime = null;
        this.buildUpTime = null;
        this.fullDetectionTime = null;
        this.intensity = null;
        this.attributeTexts = [];
        this.sensation = '';
        this.participantResultId = 0;
        this.bencTestSampId = 0;
        this.experimentBencTestSampId = 0;
    }

}