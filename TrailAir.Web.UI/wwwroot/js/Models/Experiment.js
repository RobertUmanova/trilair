﻿
class Experiment {

    constructor() {
        this.id = 0;
        this.name = '';
        this.instruction = '';
        this.numberOfSamples = null;
        this.numberOfParticipants = null;
        this.experimentStatusId = null;
        this.experimentStatusName = null;
        this.datePublished = null;
        this.teamId = null;
        this.teamName = null;
        this.consumer = '';
        this.samplePreparationId = null;
        this.samplePreparation = {
                perfumeConcentration: null,
                evaporingTime: null,
                evaporingTemperature: null
        };
        this.participants = [/*new Participant()*/];
        this.controlSampleId = null;
        this.controlSample = {
            id: 0,
            controlSampleTypeId: 0,
            sampleCode: null,
            sampleName: null,
            controlSampleTypeName: '',
            intensityScore: 7
        };
        this.experimentBenchmarks = [];
        this.experimentTestSamples = [];
        this.comment = '';
        this.isPublished = false;
        this.isFinished = false;
        this.requsterId = 0;
    }
}