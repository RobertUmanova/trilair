﻿
class Team {

    constructor() {
        this.id = 0;
        this.description = '';
        this.name = '';
        this.isArchived = false;
        this.assignedUsers = [];
    }
}