﻿class TestSample {
    constructor() {
        this.id = 0;
        this.sampleCode = '';
        this.sampleName = '';
        this.isBenchmark = false;
        this.isInternal = false;
        this.productName = '';
        this.internalName = '';
        this.fantasyName = '';
        this.lotCode = '';
        this.manufacturedDate = null;
        this.applicationDate = null;
        this.groupNumber = '';
        this.trialNumber = '';
        this.technology = '';
        this.concentration = null;
        this.base = '';
    }
}

class ExperimentTestSample {
    constructor(_experimentId) {
        this.id = 0;
        this.experimentIndex = 0;
        this.experimentId = _experimentId ? _experimentId : 0;
        this.testSampleId = 0; //is needed?
        this.testSample = new TestSample();
    }
}