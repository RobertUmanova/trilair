﻿class Participant {

    constructor() {
        this.id = 0;
        this.fullname = '';
    }
}

class ExperimentsParticipant {
    constructor() {
        this.participant = null;
        this.experimentIndex = null;
        this.participantId = 0;
        this.id = 0;
    }
}