﻿
class User {

    constructor() {
        this.id = 0;
        this.userName = '';
        this.email = '';
        this.phoneNumber = '';
        this.firstName = '';
        this.lastName = '';
        this.permissionId = null;
        this.teamId = null;
        this.password;
        this.confirmPassword;
    }
}