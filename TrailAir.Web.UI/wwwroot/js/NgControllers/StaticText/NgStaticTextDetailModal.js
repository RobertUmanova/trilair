﻿function NgStaticTextEditHelperModal(ngScope, postManager, onUpdateSuccessFunc/*, mainPageDdlId, onUpdateFunc, hideModalForDll*/) {
    this.ngScope = ngScope;
    //To Cahnge configuration 1
    this.conf = {
        defaultApi: "/ApiStaticText/",
        modalJQSelector: "#StaticTextEditPartial"
    }
    //To Cahnge configuration 1 END

    this.postManager = postManager;
    // ngScope.

    this.emptyBo = {
        //name: "emptyName",
        //description: "emptydesc"
    };
    //this.currentBo = {};

    this.validator = {};
    var modalHelper = {
        currentBo: {},
        emptyBo: {},
        conf: this.conf,
        postManager: this.postManager,
        saveBoDetails: this.saveBoDetails,
        onUpdateSuccessFunc: onUpdateSuccessFunc,
        openPopup: this.OpenPopup,
        deleteBoObject: this.deleteBoObject,
        validator: this.validator,
        loadPageResult: this.LoadPageResult,
        linkSpecialElementsFromObjectToUI: this.LinkSpecialElementsFromObjectToUI,
        linkSpecialElementsFromUIToObject : this.LinkSpecialElementsFromUIToObject
    };

    //To Cahnge configuration 1
    ngScope.StaticTextEditHelperModal = {};
    angular.copy(modalHelper, ngScope.StaticTextEditHelperModal);

    this.PreloadDataAndInitSpecComp(this, postManager, ngScope);
    //To Cahnge configuration 1 END
}


NgStaticTextEditHelperModal.prototype.deleteBoObject = function (thisbObParam, id) {
    var thisOb = thisbObParam || this;

    alertOb.confirmMessage(globalText.DeleteConfirmTitle, () => {
        thisOb.postManager.delete(thisOb.conf.defaultApi + id,
            function (response) {
                if (thisOb.onUpdateSuccessFunc)
                    thisOb.onUpdateSuccessFunc();
            });
    });
};


NgStaticTextEditHelperModal.prototype.saveBoDetails = function (thisbObParam) {
    var thisOb = thisbObParam || this;
    thisOb.linkSpecialElementsFromUIToObject(thisOb);
    thisOb.validator = new ValidationHelper('form[name*="frmStaticTextValidator"]');
    if (!thisOb.validator.Validate(true))
        return;

    thisOb.postManager.post(thisOb.conf.defaultApi + "SaveBo/", thisOb.currentBo,
        function (response) {

            $(thisOb.conf.modalJQSelector).modal('toggle');
            if (thisOb.onUpdateSuccessFunc)
                thisOb.onUpdateSuccessFunc();
        });

};

NgStaticTextEditHelperModal.prototype.LinkSpecialElementsFromUIToObject = function (thisbObParam) {
    var thisOb = thisbObParam || this;

}

NgStaticTextEditHelperModal.prototype.LinkSpecialElementsFromObjectToUI = function (thisbObParam) {
    var thisOb = thisbObParam || this;

}

NgStaticTextEditHelperModal.prototype.LoadPageResult = function (thisbObParam, currentObject) {
    var thisOb = thisbObParam || this;

    angular.copy(currentObject, thisOb.currentBo);
    thisOb.linkSpecialElementsFromObjectToUI(thisOb);

    $(thisOb.conf.modalJQSelector).modal();
}

NgStaticTextEditHelperModal.prototype.OpenPopup = function (thisbObParam, id) {
    var thisOb = thisbObParam || this;

    if (!(id)) {
        id = 0;
    }

    thisOb.postManager.get(thisOb.conf.defaultApi + id,
        function (response) {

            //TODO check if response manager handle controller errors
            // var cleanedData = $scope.formatObservationDates(response.data.Items);

            thisOb.loadPageResult(thisOb, response.data.data);

            /* angular.copy(response.data.data, thisOb.currentBo);*/
            //if (!$scope.$$phase) {
            //    $scope.$digest();
            //}
            $(thisOb.conf.modalJQSelector).modal();
        });
}

NgStaticTextEditHelperModal.prototype.PreloadDataAndInitSpecComp = function (thisInstanceOb, postManager, ngScope) {
    var thisOb = thisInstanceOb || this;
};