﻿function NgWorkPositionEditHelperModal(ngScope, postManager, onUpdateSuccessFunc/*, mainPageDdlId, onUpdateFunc, hideModalForDll*/) {
    this.ngScope = ngScope;
    //To Cahnge configuration 1
    this.conf = {
        defaultApi: "/ApiWorkPosition/",
        modalJQSelector: "#WorkPositionEditPartial"
    }
    //To Cahnge configuration 1 END

    this.postManager = postManager;
    // ngScope.

    this.emptyBo = {
        //name: "emptyName",
        //description: "emptydesc"
    };

    this.validator = {};

    var modalHelper = {
        conf: this.conf,
        currentBo: {},
        deleteBoObject: this.DeleteBoObject,
        emptyBo: {},
        linkSpecialElementsFromObjectToUI: this.LinkSpecialElementsFromObjectToUI,
        linkSpecialElementsFromUIToObject: this.LinkSpecialElementsFromUIToObject,
        loadPageResult: this.LoadPageResult,
        onUpdateSuccessFunc: onUpdateSuccessFunc,
        openPopup: this.OpenPopup,
        postManager: this.postManager,
        saveBoDetails: this.SaveBoDetails,
        validator: this.validator
    };

    //To Cahnge configuration 1
    ngScope.workPositionEditHelperModal = {};
    ngScope.workPositionEditHelperModal = modalHelper;

    this.PreloadDataAndInitSpecComp(modalHelper, postManager, ngScope);
    //To Cahnge configuration 1 END
}

NgWorkPositionEditHelperModal.prototype.PreloadDataAndInitSpecComp = function (thisInstanceOb, postManager, ngScope) {
    var thisOb = thisInstanceOb || this;

    thisOb.validator = new ValidationHelper('form[name*="frmWorkPositionValidator"]');
};

NgWorkPositionEditHelperModal.prototype.DeleteBoObject = function (thisbObParam, id) {
    var thisOb = thisbObParam || this;

    alertOb.confirmMessage(globalText.DeleteConfirmTitle, () => {
        thisOb.postManager.delete(thisOb.conf.defaultApi + id,
            function (response) {
                if (thisOb.onUpdateSuccessFunc)
                    thisOb.onUpdateSuccessFunc();
            });
    });
};


NgWorkPositionEditHelperModal.prototype.SaveBoDetails = function (thisbObParam) {
    var thisOb = thisbObParam || this;
    thisOb.linkSpecialElementsFromUIToObject(thisOb);

    if (!thisOb.validator.Validate(true))
        return;

    thisOb.postManager.post(thisOb.conf.defaultApi + "SaveBo/", thisOb.currentBo,
        function (response) {

            $(thisOb.conf.modalJQSelector).modal('toggle');
            if (thisOb.onUpdateSuccessFunc)
                thisOb.onUpdateSuccessFunc();
        });

};


NgWorkPositionEditHelperModal.prototype.OpenPopup = function (thisbObParam, id) {
    var thisOb = thisbObParam || this;

    if (!(id)) {
        id = 0;
    }

    thisOb.postManager.get(thisOb.conf.defaultApi + id,
        function (response) {

            //TODO check if response manager handle controller errors
            // var cleanedData = $scope.formatObservationDates(response.data.Items);

            thisOb.loadPageResult(thisOb, response.data.data);

           /* angular.copy(response.data.data, thisOb.currentBo);*/
            //if (!$scope.$$phase) {
            //    $scope.$digest();
            //}
            $(thisOb.conf.modalJQSelector).modal();
        });
}

NgWorkPositionEditHelperModal.prototype.LinkSpecialElementsFromObjectToUI = function (thisbObParam) {
    var thisOb = thisbObParam || this;
}

NgWorkPositionEditHelperModal.prototype.LinkSpecialElementsFromUIToObject = function (thisbObParam) {
    var thisOb = thisbObParam || this;

}

NgWorkPositionEditHelperModal.prototype.LoadPageResult = function (thisbObParam, currentObject) {
    var thisOb = thisbObParam || this;

    angular.copy(currentObject, thisOb.currentBo);
    thisOb.linkSpecialElementsFromObjectToUI(thisOb);

    $(thisOb.conf.modalJQSelector).modal();

    //if (!$scope.$$phase) {
    //    $scope.$digest();
    //}
}