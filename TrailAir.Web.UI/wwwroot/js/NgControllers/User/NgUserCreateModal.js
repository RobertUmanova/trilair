﻿function NgUserCreateHelperModal(ngScope, postManager, onUpdateSuccessFunc/*, mainPageDdlId, onUpdateFunc, hideModalForDll*/) {
    this.ngScope = ngScope;

    this.conf = {
        defaultApi: "/ApiUser/",
        modalJQSelector: "#UserCreatePartial"
    }
    this.postManager = postManager;

    // https://www.twilio.com/blog/international-phone-number-input-html-javascript
    this.phoneInputField = document.querySelector("#phoneUserCreateInput");
    this.phoneInput = window.intlTelInput(this.phoneInputField, {
        preferredCountries: ["ch", "de", "at", "fr", "hr", "gh"],
        utilsScript:
            "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
    });
    var modalHelper = {
        currentBo: {
            confirmPassword: '',
            password: '',
            checkedPositions: [],
            username: ''
        },
        emptyBo: {},
        conf: this.conf,
        companySelect2Disabled: false, // this is by default false, but if we are on a companyDetails page or something similar set it to true!! We want admins to create a user for the company already selected, not another company
        postManager: this.postManager,
        saveBoDetails: this.saveBoDetails,
        onUpdateSuccessFunc: onUpdateSuccessFunc,
        openPopup: this.OpenPopup,
        deleteBoObject: this.deleteBoObject,
        phoneInput: this.phoneInput
    };

    ngScope.userCreateHelperModal = {};
    ngScope.userCreateHelperModal = modalHelper;

    //ngScope.userCreateHelperModal = {};
    //angular.copy(modalHelper, ngScope.userCreateHelperModal);
    this.PreloadDataAndInitSpecComp(modalHelper, postManager, ngScope);

    //cleanup after closing Create Modal has been closed
    $('#UserCreatePartial').on('hide.bs.modal', function () {
        $('#frmCreateUser')[0].reset();
        ngScope.IsUsernameTaken = false;
    })
}

NgUserCreateHelperModal.prototype.deleteBoObject = function (thisbObParam, id) {
    var thisOb = thisbObParam || this;


    thisOb.postManager.delete(thisOb.conf.defaultApi + id,
        function (response) {
            if (thisOb.onUpdateSuccessFunc)
                thisOb.onUpdateSuccessFunc();
        });

};


NgUserCreateHelperModal.prototype.saveBoDetails = function (thisbObParam, workPositions) {

    var thisOb = thisbObParam || this;

    var selectedValues = $('#slctUserCreateWorkPositions').val();

    var workPositions = selectedValues.map(function (el, index) {
        return {
            id: el,
            text: ""
        };
    });
    thisOb.currentBo.workPositions = workPositions;
    if (thisOb.currentBo.workPositions.length <= 0) {
        alertOb.errorMessage(globalText.ValidationError, globalText.AtLeastOneWorkPosition_User);
        return;
    }
    thisOb.currentBo.company = {};
    thisOb.currentBo.company.id = Number(thisOb.slctUserCreateCompany.GetSelectValue());

    thisOb.currentBo.permission = {};
    thisOb.currentBo.permission.id = Number(thisOb.slctUserCreatePermission.GetSelectValue());
    thisOb.currentBo.phoneNumber = thisOb.phoneInput.getNumber();


    thisOb.postManager.post(thisOb.conf.defaultApi.concat("create"), thisOb.currentBo,
        function (response) {

            $(thisOb.conf.modalJQSelector).modal('toggle');
            if (thisOb.onUpdateSuccessFunc)
                thisOb.onUpdateSuccessFunc();
        });

};
NgUserCreateHelperModal.prototype.OpenPopup = function (thisbObParam, id) {
    var thisOb = thisbObParam || this;

    if (id) {
        thisOb.postManager.get(thisOb.conf.defaultApi + id,
            function (response) {
                //TODO check if response manager handle controller errors
                angular.copy(response.data.data, thisOb.currentBo);
                
                //if (!$scope.$$phase) {
                //    $scope.$digest();
                //}
                $(thisOb.conf.modalJQSelector).modal();
                console.log(thisOb.currentBo);
            });
    }
    else {
        angular.copy(thisOb.emptyBo, thisOb.currentBo);
        //if (!$scope.$$phase) {
        //    $scope.$digest();
        //}

        var companyId = thisOb.currentBo.company == null ? {} : thisOb.currentBo.company.id;

        //NgUserCreateModal od CompanyDetails cshtml:
        //there we can create a user for a company already selected, so check the url and set select2 value to that company
        if (window.location.toString().includes("CompanyDetails")) {

            thisOb.companySelect2Disabled = true;

            // get URL parameters
            var vars = {};
            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
                value = value.replace('#', '');
                vars[key] = value;
            });

            companyId = vars["CompanyId"];
        }

        thisOb.slctUserCreateCompany.SetValue(companyId, thisOb.slctUserCreateCompany);

        var selectedValue = thisOb.currentBo.permission == null ? {} : thisOb.currentBo.permission.id;
        thisOb.slctUserCreatePermission.SetValue(selectedValue, thisOb.slctUserCreatePermission);
        $(thisOb.conf.modalJQSelector).modal();

    }
}

NgUserCreateHelperModal.prototype.PreloadDataAndInitSpecComp = function (thisInstanceOb, postManager, ngScope) {
    var thisOb = thisInstanceOb || this;

    thisOb.slctUserCreateWorkPositions = new Select2Helper("slctUserCreateWorkPositions");
    thisOb.slctUserCreateCompany = new Select2Helper("userCreateCompany");
    thisOb.slctUserCreatePermission = new Select2Helper("userCreatePermission");

    ngScope.ngUserDetWorkPositions = [];
    ngScope.ngUserDetCompanies = [];
    ngScope.ngUserDetPermissions = [];


    thisOb.postManager.get("/ApiCompany/",
        function (response) {
            angular.copy(response.data.data, ngScope.ngUserDetCompanies);
            var companies = response.data.data.map(function (el, index) {
                return {
                    id: el.id,
                    text: el.name
                };
            });
            thisOb.slctUserCreateCompany.LoadSelect2WithData(companies, thisOb.slctUserCreateCompany);

        });
    thisOb.postManager.get("/ApiPermission/",
        function (response) {
            angular.copy(response.data.data, ngScope.ngUserDetPermissions);
            var permissions = response.data.data.map(function (el, index) {
                return {
                    id: el.id,
                    text: el.name
                };
            });
            thisOb.slctUserCreatePermission.LoadSelect2WithData(permissions, thisOb.slctUserCreatePermission);

        });
    thisOb.postManager.get("/ApiWorkPosition/GetWorkPositions/",
        function (response) {
            angular.copy(response.data.data, ngScope.ngUserDetWorkPositions);

            var workPositions = response.data.data.map(function (el, index) {
                return {
                    id: el.id,
                    text: el.name
                };
            });
            thisOb.slctUserCreateWorkPositions.LoadSelect2WithData(workPositions, thisOb.slctUserCreateWorkPositions);
        });
};
