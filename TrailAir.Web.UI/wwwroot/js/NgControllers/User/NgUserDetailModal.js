﻿function NgUserEditHelperModal(ngScope, postManager, onUpdateSuccessFunc/*, mainPageDdlId, onUpdateFunc, hideModalForDll*/) {
    this.ngScope = ngScope;

    this.conf = {
        defaultApi: "/ApiUser/",
        modalJQSelector: "#UserEditPartial"
    }
    this.postManager = postManager;

    // https://www.twilio.com/blog/international-phone-number-input-html-javascript
    this.phoneInputField = document.querySelector("#phoneUserDetailInput");
    this.phoneInput = window.intlTelInput(this.phoneInputField, {
        preferredCountries: ["ch", "de", "at", "fr", "hr", "gh"],
        utilsScript:
            "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
    });
    var modalHelper = {
        conf: this.conf,
        currentBo: {},
        deleteBoObject: this.DeleteBoObject,
        emptyBo: {},
        linkSpecialElementsFromObjectToUI: this.LinkSpecialElementsFromObjectToUI,
        linkSpecialElementsFromUIToObject: this.LinkSpecialElementsFromUIToObject,
        loadPageResult: this.LoadPageResult,
        onUpdateSuccessFunc: onUpdateSuccessFunc,
        openPopup: this.OpenPopup,
        postManager: this.postManager,
        saveBoDetails: this.SaveBoDetails,
        workPositions: {},
        isNewObject: false,
        phoneInput: this.phoneInput,
        ngScope: this.ngScope
    };

    ngScope.userEditHelperModal = {};
    ngScope.userEditHelperModal = modalHelper;
    this.PreloadDataAndInitSpecComp(modalHelper, postManager, ngScope);
}


NgUserEditHelperModal.prototype.DeleteBoObject = function (thisbObParam, id) {
    var thisOb = thisbObParam || this;

    alertOb.confirmMessage(globalText.DeleteConfirmTitle, () => {
        thisOb.postManager.delete(thisOb.conf.defaultApi + id,
            function (response) {
                if (thisOb.onUpdateSuccessFunc)
                    thisOb.onUpdateSuccessFunc();
            });
    });
};


NgUserEditHelperModal.prototype.SaveBoDetails = function (thisbObParam, workPositions) {
    var thisOb = thisbObParam || this;
    thisOb.workPositions = workPositions;

    thisOb.linkSpecialElementsFromUIToObject(thisOb);

    if (thisOb.currentBo.workPositions.length <= 0)
    {
        alertOb.errorMessage(globalText.ValidationError, globalText.AtLeastOneWorkPosition_User);
        return;
    }
    thisOb.postManager.post(thisOb.conf.defaultApi, thisOb.currentBo,
        function (response) {

            $(thisOb.conf.modalJQSelector).modal('toggle');
            if (thisOb.onUpdateSuccessFunc)
                thisOb.onUpdateSuccessFunc();
        });

};

NgUserEditHelperModal.prototype.OpenPopup = function (thisbObParam, id) {
    var thisOb = thisbObParam || this;

    if (!(id)) {
        id = 0;
        thisOb.isNewObject = true;
    }

    thisOb.postManager.get(thisOb.conf.defaultApi + id,
        function (response) {

            //TODO check if response manager handle controller errors
            // var cleanedData = $scope.formatObservationDates(response.data.Items);

            thisOb.loadPageResult(thisOb, response.data.data);

            /* angular.copy(response.data.data, thisOb.currentBo);*/
            //if (!$scope.$$phase) {
            //    $scope.$digest();
            //}
            $(thisOb.conf.modalJQSelector).modal();
        });
}

NgUserEditHelperModal.prototype.LinkSpecialElementsFromObjectToUI = function (thisbObParam) {
    var thisOb = thisbObParam || this;

    if (thisOb.currentBo.workPositions.length > 0) {
        var workPositionsIds = thisOb.currentBo.workPositions.map(function (el, index) {
            return el.id;
        });

        thisOb.slctUserEditWorkPositions.SetMultipleValues(workPositionsIds, thisOb.slctUserEditWorkPositions);
    }
    //if we are edditing the existing object display the phone number from DB
    if (!thisOb.isNewObject)
    {
        if (thisOb.currentBo.phoneNumber != null)
        {
            thisOb.phoneInput.setNumber(thisOb.currentBo.phoneNumber);
        }
        //$("#phoneUserDetailInput").val(thisOb.currentBo.phoneNumber);
        //document.querySelector("#phoneUserDetailInput").value = thisOb.currentBo.phoneNumber;
        
    }


    var selectedValue = thisOb.currentBo.company == null ? {} : thisOb.currentBo.company.id;
    thisOb.slctUserEditCompany.SetValue(selectedValue, thisOb.slctUserEditCompany);

    selectedValue = thisOb.currentBo.permission == null ? {} : thisOb.currentBo.permission.id;
    thisOb.slctUserEditPermission.SetValue(selectedValue, thisOb.slctUserEditPermission);
}

NgUserEditHelperModal.prototype.LinkSpecialElementsFromUIToObject = function (thisbObParam) {
    var thisOb = thisbObParam || this;

    var selectedValues = $('#slctUserEditWorkPositions').val();

    var workPositions = selectedValues.map(function (el, index) {
        return {
            id: el,
            text: ""
        };
    });
    thisOb.currentBo.workPositions = workPositions;
    thisOb.currentBo.company = {};
    thisOb.currentBo.company.id = Number(thisOb.slctUserEditCompany.GetSelectValue());

    thisOb.currentBo.permission = {};
    thisOb.currentBo.permission.id = Number(thisOb.slctUserEditPermission.GetSelectValue());

    thisOb.currentBo.phoneNumber = thisOb.phoneInput.getNumber();

}

NgUserEditHelperModal.prototype.LoadPageResult = function (thisbObParam, currentObject) {
    var thisOb = thisbObParam || this;
    angular.copy(currentObject, thisOb.currentBo);
    thisOb.linkSpecialElementsFromObjectToUI(thisOb);

    $(thisOb.conf.modalJQSelector).modal();

    //if (!$scope.$$phase) {
    //    $scope.$digest();
    //}
}

NgUserEditHelperModal.prototype.PreloadDataAndInitSpecComp = function (thisInstanceOb, postManager, ngScope) {
    var thisOb = thisInstanceOb || this;

    ngScope.ngUserDetWorkPositions = [];
    ngScope.ngUserDetCompanies = [];
    ngScope.ngUserDetPermissions = [];

    thisOb.slctUserEditWorkPositions = new Select2Helper("slctUserEditWorkPositions");
    thisOb.slctUserEditCompany = new Select2Helper("userEditCompany");
    thisOb.slctUserEditPermission = new Select2Helper("userEditPermission");

    thisOb.postManager.get("/ApiCompany/",
        function (response) {
            angular.copy(response.data.data, ngScope.ngUserDetCompanies);
            var companies = response.data.data.map(function (el, index) {
                return {
                    id: el.id,
                    text: el.name
                };
            });
            thisOb.slctUserEditCompany.LoadSelect2WithData(companies, thisOb.slctUserEditCompany);
            ngScope.slctFilterUserCompany.LoadSelect2WithData(companies, ngScope.slctFilterUserCompany);

        });
    thisOb.postManager.get("/ApiPermission/",
        function (response) {
            angular.copy(response.data.data, ngScope.ngUserDetPermissions);
            var permissions = response.data.data.map(function (el, index) {
                return {
                    id: el.id,
                    text: el.name
                };
            });
            thisOb.slctUserEditPermission.LoadSelect2WithData(permissions, thisOb.slctUserEditPermission);
            ngScope.slctFilterUserPermission.LoadSelect2WithData(permissions, ngScope.slctFilterUserPermission);

        });
    thisOb.postManager.get("/ApiWorkPosition/GetWorkPositions/",
        function (response) {
            angular.copy(response.data.data, ngScope.ngUserDetWorkPositions);

            var workPositions = response.data.data.map(function (el, index) {
                return {
                    id: el.id,
                    text: el.name
                };
            });
            thisOb.slctUserEditWorkPositions.LoadSelect2WithData(workPositions, thisOb.slctUserEditWorkPositions);
            ngScope.slctFilterUserWorkPosition.LoadSelect2WithData(workPositions, ngScope.slctFilterUserWorkPosition);

        });

    //// https://www.twilio.com/blog/international-phone-number-input-html-javascript
    //const phoneInputField = document.querySelector("#phoneUserDetailInput");
    //var phoneInput = window.intlTelInput(phoneInputField, {
    //    preferredCountries: ["ch", "de", "at", "fr", "hr", "gh"],
    //    utilsScript:
    //        "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
    //});
};
