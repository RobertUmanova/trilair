﻿function NgUserSendInviteModalHelper(ngScope, postManager) {
    this.ngScope = ngScope;
    this.modalSelector = '#invitePartial';
    this.inviteEmail = '';
    this.companyId = 0;
    this.postManager = postManager;
    this.defaultApi = '/ApiUser/';
    this.Companies = [];

    var thisOb = this;

    $('#invitePartial').on('hide.bs.modal', function () {
        thisOb.resetFields();
    })

    this.PreloadDataAndInitSpecComp();
}

NgUserSendInviteModalHelper.prototype.openPopup = function () {
    var thisOb = this;
    $(thisOb.modalSelector).modal();
}

NgUserSendInviteModalHelper.prototype.sendInvite = function ($event) {
    $event.preventDefault();
    var thisOb = this;
    thisOb.postManager.post(thisOb.defaultApi.concat(`SendInvite/${thisOb.inviteEmail}/${thisOb.companyId}`), null, function (response) {
        $(thisOb.modalSelector).modal('toggle')
    })
}

NgUserSendInviteModalHelper.prototype.PreloadDataAndInitSpecComp = function () {
    var thisOb = this;

    thisOb.postManager.get("/ApiCompany/",
        function (response) {
            angular.copy(response.data.data, thisOb.Companies);
        });
};


NgUserSendInviteModalHelper.prototype.resetFields = function () {
    this.inviteEmail = '';
}
