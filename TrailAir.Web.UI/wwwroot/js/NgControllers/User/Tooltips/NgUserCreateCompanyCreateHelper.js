﻿function NgUserCreateCompanyCreateTooltipHelper(ngScope, postManager, onUpdateSuccessFunc/*, mainPageDdlId, onUpdateFunc, hideModalForDll*/) {
    this.ngScope = ngScope;
    //To Cahnge configuration 1
    this.conf = {
        defaultApi: "/ApiCompany/",
        modalJQSelector: "#CompanyEditPartial"
    }
    //To Cahnge configuration 1 END

    this.postManager = postManager;
    this.openItemDetById = this.OpenItemDetById.bind(this);
    this.functionOnNew = this.FunctionOnNew.bind(this);
    this.functionOnEdit = this.FunctionOnEdit.bind(this);
    this.focusFirstElement = this.FocusFirstElement.bind(this);
    this.openItemDet = this.OpenItemDet.bind(this);
    this.saveBoDetails = this.SaveBoDetails.bind(this);
    this.resetForm = this.ResetForm.bind(this);
    this.currentBo = {}
    ngScope.userCreateCompanyCreateTooltip = {
        saveBoDetails: this.saveBoDetails,
        currentBo: this.currentBo,
        ngScope: this.ngScope
    };
    ngScope.companyEditHelperModal = ngScope.userCreateCompanyCreateTooltip;

    // instantiate a tooltip
    this.tooltipUserCreateCompanyCreate = new TooltipEditHelper("userCreateCompany", this.functionOnNew, null, null, "UserCreateCompanyCreateTooltip", this.ngScope);

    this.PreloadDataAndInitSpecComp(this, postManager, ngScope);

}
NgUserCreateCompanyCreateTooltipHelper.prototype.PreloadDataAndInitSpecComp = function (thisOb, postManager, ngScope) {
    var thisOb = this;

    ngScope.ngCompanies = [];
    ngScope.ngCompanyDetStatuses = [];
    ngScope.ngCompanyDetCountries = [];

    thisOb.slctIndustries = new Select2Helper("slctCmpDetailIndustry");

    thisOb.postManager.get(thisOb.conf.defaultApi,
        function (response) {
            angular.copy(response.data.data, ngScope.ngCompanies);

        }
    );


    thisOb.postManager.get("/ApiStatus/GetSimpleList/",
        function (response) {
            angular.copy(response.data.data, ngScope.ngCompanyDetStatuses);

        });

    thisOb.postManager.get("/ApiCountry/GetSimpleList/",
        function (response) {
            angular.copy(response.data.data, ngScope.ngCompanyDetCountries);
            //ngScope.slctFilterCompanyCountry.LoadSelect2WithData(response.data.data, ngScope.slctFilterCompanyCountry);

        });

    thisOb.postManager.get("/ApiIndustry/GetSimpleList/",
        function (response) {
            thisOb.slctIndustries.LoadSelect2WithData(response.data.data, thisOb.slctIndustries);
            //ngScope.slctFilterCompanyBranch.LoadSelect2WithData(response.data.data, ngScope.slctFilterCompanyBranch);
        });


};

NgUserCreateCompanyCreateTooltipHelper.prototype.SaveBoDetails = function () {
    var thisOb = this;
    thisOb.validator = new ValidationHelper('form[name*="frmCompanyValidator"]');
    if (!thisOb.validator.Validate(true))
        return;

    var selectedValues = $('#slctCmpDetailIndustry').val();

    var industries = selectedValues.map(function (el, index) {
        return {
            id: el
        };
    });
    thisOb.currentBo.industries = industries;

    thisOb.postManager.post(thisOb.conf.defaultApi + "SaveBo", thisOb.currentBo,
        function (response) {

            $("#CompanyEditPartial").modal('toggle');
            //if (thisOb.onUpdateSuccessFunc)
            //    thisOb.onUpdateSuccessFunc();
            thisOb.resetForm();

            var newCompanyId = response.data.data;
            thisOb.postManager.get(thisOb.conf.defaultApi,
                function (response) {
                    angular.copy(response.data.data, thisOb.ngScope.ngCompanies);

                    var companies = response.data.data.map(function (el, index) {
                        return {
                            id: el.id,
                            text: el.name
                        };
                    });
                    var userCreateModal = thisOb.ngScope.userCreateHelperModal;
                    userCreateModal.slctUserCreateCompany.LoadSelect2WithData(companies, userCreateModal.slctUserCreateCompany);
                    userCreateModal.slctUserCreateCompany.SetValue(newCompanyId, userCreateModal.slctUserCreateCompany);

                }
            );
        }
    );
};

// tooltip function
NgUserCreateCompanyCreateTooltipHelper.prototype.FunctionOnNew = function () {
    var thisOb = this;
    thisOb.resetForm();
    thisOb.openItemDetById(0, thisOb);
    thisOb.focusFirstElement(thisOb);
};

// tooltip function
NgUserCreateCompanyCreateTooltipHelper.prototype.FunctionOnEdit = function (id, thisObParam) {
    var thisOb = thisObParam;
    //thisOb.openItemDetById(id, thisOb);
    thisOb.focusFirstElement(thisOb);
};

//tooltip function
NgUserCreateCompanyCreateTooltipHelper.prototype.OpenItemDetById = function (elementId, thisObject) {
    var thisOb = thisObject || this;
    thisOb.elementId = elementId;
    thisOb.openItemDet({}, thisOb);

    //this.postManager.get(thisOb.conf.defaultApi + elementId,
    //    function (response) {
    //        if (response.data.success) {
    //            thisOb.openItemDet(response.data.data, thisOb);
    //        }
    //    }
    //);
};

//tooltip function
NgUserCreateCompanyCreateTooltipHelper.prototype.FocusFirstElement = function (thisObject) {
    var thisOb = thisObject || this;
    setTimeout(function () {
        var input = $(thisOb.modalId).find('input').first();

        //input.focus();
        //input.focusin();
        //input.click();

    }, 500); // If timeout is 0, first time you click edit or new button on tooltip, $(thisOb.modalId).find('input').first() is not found as html element
};

// tooltip function
NgUserCreateCompanyCreateTooltipHelper.prototype.OpenItemDet = function (data, contextOb) {
    var thisOb = contextOb || this;
    angular.copy(data, thisOb.currentBo);
    $("#CompanyEditPartial").modal();
};

NgUserCreateCompanyCreateTooltipHelper.prototype.ResetForm = function () {
    var thisOb = this;

    $('#frmCreateCompany')[0].reset();
    // form reset will not reset select2 so we do it manually
    thisOb.slctIndustries.SetMultipleValues([], thisOb.slctIndustries);
};