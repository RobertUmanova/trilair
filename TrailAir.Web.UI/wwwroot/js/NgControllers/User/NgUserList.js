﻿var myApp = angular.module('UmaSoft', ['localytics.directives', 'ui.bootstrap']);
var ngScope;
myApp.config(config);
var patientId = patientId;
//var myController;

myApp.controller('ngUsers', function ($scope, $http, $timeout) {
    //ngControllerReady = true;
    this.ngScope = $scope;


    this.conf = {
        defaultApi: "/ApiUser/"
    }
    var me = this;


    var postManager = new PostManager($http);
    $scope.userList = [];
    $scope.IsUsernameTaken = false;
    $scope.sendInviteModalHelper = new NgUserSendInviteModalHelper($scope, postManager);

    //Pager code
    $scope.currentPage = 1;
    $scope.perPage = 10;
    $scope.filteredItems = [];

    //Search input & Filter code
    $scope.usersFilter = "";

    $scope.getData = function () {
        postManager.get(me.conf.defaultApi,
            function (response) {
                angular.copy(response.data.data, $scope.userList);
                angular.copy(response.data.data, $scope.filteredItems);
                console.log($scope.userList);
            });
    };

    new NgUserEditHelperModal($scope, postManager, $scope.getData);
    new NgUserCreateHelperModal($scope, postManager, $scope.getData);

    //create new company tooltip logic
    new NgUserCreateCompanyCreateTooltipHelper($scope, postManager, null);


    $scope.getDetails = function (id) {
        var thisOb = this;

    }
    $scope.getData();


    $scope.submit = function ($event) {
        $event.preventDefault();
        var thisOb = $scope;

        if ($scope.IsUsernameTaken) return false;

        thisOb.userCreateHelperModal.saveBoDetails(thisOb.userCreateHelperModal, thisOb.ngUserDetWorkPositions);
    }

    $scope.IsUsernameAvailable = function () {
        var thisOb = $scope;
        $scope.IsUsernameTaken = thisOb.userList.some(_ => _.email === thisOb.userCreateHelperModal.currentBo.username)
    }

    $scope.resetUsernameValidation = function () {
        angular.copy(false, $scope.IsUsernameTaken);
    }

    $scope.onFilterChange = function (event) {
        var thisOb = this;
        $scope.currentPage = 1;

        var isFilterForCompanySet = $scope.slctFilterUserCompany.GetSelectValue().length > 0;
        var filteringValuesForCompany = $scope.slctFilterUserCompany.GetSelectValue();

        var isFilterForPermissionSet = $scope.slctFilterUserPermission.GetSelectValue().length > 0;
        var filteringValuesForPermission = $scope.slctFilterUserPermission.GetSelectValue();

        var isFilterForWorkPositionSet = $scope.slctFilterUserWorkPosition.GetSelectValue().length > 0;
        var filteringValuesForWorkPosition = $scope.slctFilterUserWorkPosition.GetSelectValue();

        $scope.filteredItems = isFilterForCompanySet ? $scope.userList.filter((item) => {
            if (item.company) {
                var companyId = item.company.id.toString();
                if (filteringValuesForCompany.includes(companyId)) {
                    return item;
                }
            }
        }) : $scope.userList;

        $scope.filteredItems = isFilterForPermissionSet ? $scope.filteredItems.filter((item) => {
            if (item.permission) {
                var permissionId = item.permission.id.toString();
                if (filteringValuesForPermission.includes(permissionId)) {
                    return item;
                }
            }
        }) : $scope.filteredItems;

        $scope.filteredItems = isFilterForWorkPositionSet ? $scope.filteredItems.filter((item) => {
            var filteredItemWorkPositionsIds = item.workPositions.map(workPosition => (workPosition.id.toString()));
            let success = filteringValuesForWorkPosition.every((val) => filteredItemWorkPositionsIds.includes(val));
            if (success) {
                return item;
            }
        }) : $scope.filteredItems;

        thisOb.CheckSearchFilter();
    }

    $scope.CheckSearchFilter = function (event) {
        var thisOb = this;

        $scope.filteredItems = $scope.filteredItems.filter((item) => {
            var filterTextLowerCase = $scope.usersFilter.toLowerCase();
            var itemWorkPositionsNamesLowerCase = item.workPositions.map(_ => _.name.toLowerCase());


            if (item.firstName?.toLowerCase().includes(filterTextLowerCase)) {
                return item;
            }

            if (item.lastName?.toLowerCase().includes(filterTextLowerCase)) {
                return item;
            }

            if (item.company?.name.toLowerCase().includes(filterTextLowerCase)) {
                return item;
            }

            if (item.permission?.name.toLowerCase().includes(filterTextLowerCase)) {
                return item;
            }

            if (itemWorkPositionsNamesLowerCase?.includes(filterTextLowerCase)) {
                return item;
            }
           
        })
    }

    var clear = [];
    $scope.clearFilter = function (filter) {

        if (filter == "company") {

            // set select2 ng-model to [] is the must, otherwise clear filter is buggy 
            // and select2 is not cleared and we get errors like 'Error: [$rootScope:inprog] $apply already in progress'  !!!
            $scope.slctFilterUserCompanyModel = [];
            $scope.slctFilterUserCompany.SetValue([], $scope.slctFilterUserCompany);

        }
        if (filter == "permission") {
            // set select2 ng-model to [] is the must, otherwise clear filter is buggy
            // and select2 is not cleared and we get errors like 'Error: [$rootScope:inprog] $apply already in progress'  !!!
            $scope.slctFilterUserPermissionModel = [];
            $scope.slctFilterUserPermission.SetValue(null, $scope.slctFilterUserPermission);
        }
        if (filter == "workPosition") {
            // set select2 ng-model to [] is the must, otherwise clear filter is buggy
            // and select2 is not cleared and we get errors like 'Error: [$rootScope:inprog] $apply already in progress'  !!!
            $scope.slctFilterUserWorkPositionModel = [];
            $scope.slctFilterUserWorkPosition.SetValue(null, $scope.slctFilterUserWorkPosition);
        }
        $scope.onFilterChange();

    }

    // instantiate a multi select2  for companies of users 
    // data get and set in NgUserDetailModal 
    $scope.slctFilterUserCompany = new Select2Helper("slctFilterUserCompany", $scope.onFilterChange.bind($scope)); // we do not want window binded per default
    $scope.slctFilterUserCompany.SetMultipleValues(clear, $scope.slctFilterUserCompany);

    // instantiate a multi select2  for permissions of users 
    // data get and set in NgUserDetailModal
    $scope.slctFilterUserPermission = new Select2Helper("slctFilterUserPermission", $scope.onFilterChange.bind($scope)); // we do not want window binded per default
    $scope.slctFilterUserPermission.SetMultipleValues(clear, $scope.slctFilterUserPermission);

    // instantiate a multi select2  for work positions of users 
    // data get and set in NgUserDetailModal
    $scope.slctFilterUserWorkPosition = new Select2Helper("slctFilterWorkPosition", $scope.onFilterChange.bind($scope)); // we do not want window binded per default
    $scope.slctFilterUserWorkPosition.SetMultipleValues(clear, $scope.slctFilterUserWorkPosition);
});

myApp.directive("compareTo", function () {
    return {
        require: "ngModel",
        scope: {
            confirmPassword: "=compareTo"
        },
        link: function (scope, element, attributes, modelVal) {
            modelVal.$validators.compareTo = function (password) {
                return password === scope.confirmPassword;
            };
            scope.$watch("confirmPassword", function () {
                modelVal.$validate();
            });
        }
    }
})

//Pager code
myApp.filter('start', function () {
    return function (input, start) {
        if (!input || !input.length) { return; }
        start = +start;
        return input.slice(start);
    };
});
