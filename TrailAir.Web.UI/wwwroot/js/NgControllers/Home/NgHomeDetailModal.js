﻿function NgHomeEditHelperModal(ngScope, postManager, onUpdateSuccessFunc/*, mainPageDdlId, onUpdateFunc, hideModalForDll*/) {
    this.ngScope = ngScope;
    //To Cahnge configuration 1
    this.conf = {
        defaultApi: "/ApiUser/",
        modalJQSelector: "#HomeEditPartial"
    }
    //To Cahnge configuration 1 END

    this.postManager = postManager;
    // ngScope.

    this.emptyBo = {
        //name: "emptyName",
        //description: "emptydesc"
    };
    //this.currentBo = {};

    this.validator = {};
    this.saveBo = async function () {
        var thisOb = this;

        // for now here we allow to change only user image later rename the function here and in API 
        thisOb.postManager.post(thisOb.conf.defaultApi + "ChangeUserImage/", thisOb.currentBo,
            function (response) {

                $(thisOb.conf.modalJQSelector).modal('toggle');
                if (thisOb.onUpdateSuccessFunc) {

                    //thisOb.onUpdateSuccessFunc(projectId);

                    // temporary solution
                    window.location.reload();
                }

                // temporary solution
                window.location.reload();
            });
    }

    var modalHelper = {
        currentBo: {},
        emptyBo: {},
        saveBo: this.saveBo,
        conf: this.conf,
        postManager: this.postManager,
        saveBoDetails: this.SaveBoDetails,
        onUpdateSuccessFunc: onUpdateSuccessFunc,
        openPopup: this.OpenPopup,
        onUploadDocCompleted: this.OnUploadDocCompleted,
        uploadNewImage: this.UploadNewImage,
        validator: this.validator,
        previewFile: this.PreviewFile
    };

    ngScope.homeEditHelperModal = {};
    ngScope.homeEditHelperModal = modalHelper;
    this.PreloadDataAndInitSpecComp(this, postManager, ngScope);
}



NgHomeEditHelperModal.prototype.PreloadDataAndInitSpecComp = function (thisInstanceOb, postManager, ngScope) {
    var thisOb = thisInstanceOb || this;

    ngScope.ngUser = {};
    if (window.location.href.indexOf("HomeUserDetails") > -1) {
        thisOb.postManager.get("/ApiUser/GetCurrentLoggedInUser/",
            function (response) {
                angular.copy(response.data.data, ngScope.ngUser);

            }
        );
    }
  
};

NgHomeEditHelperModal.prototype.PreviewFile = function (thisbObParam, isFilePdf) {
    var thisOb = thisbObParam || this;
    //var imageExtensions = ['png', 'jpg', 'jpeg', 'heic', 'heif'];
    //var url = '/en-GB/ApiDocument/DownloadFile?elementId=' + thisOb.currentBo.document.id;
    //var extension = FileHelper.GetExtension(thisOb.documentName);

    if (isFilePdf) {
        var pdfPreviewHelper = new PDFPreviewerHelper();
        pdfPreviewHelper.previewPDF(thisOb.documentId);
        return false;
    }

    if (!isFilePdf) {
        FileHelper.previewImage(thisOb.document.id, thisOb.document.name);
    }
}

NgHomeEditHelperModal.prototype.SaveBoDetails = function (thisbObParam) {
    var thisOb = thisbObParam || this;

    // thisOb.imgHlp.saveImage() will return true on upload of the photo from the user and call saveBo
    if (!thisOb.imgHlp.saveImage()) {
        if (thisOb.removeImage == true) {
            thisOb.currentBo.document = {};
            thisOb.currentBo.documentId = null;

            // we want to remove the image so change the user
            thisOb.saveBo();
        }
        else
        {
            //we are keeping the same image so do not call server side code
            $(thisOb.conf.modalJQSelector).modal('toggle');
        }
    }
};

NgHomeEditHelperModal.prototype.OnUploadDocCompleted = function (response, thisOb) {
    var thisOb = thisOb || this;

    //thisOb.postManager.checkResponse(response);
    if (!response.success) {
        alertOb.errorMessage("Attention!", response.errorMessage);
        event.preventDefault();
    } else {
        var documentId = response.data;

        // Working with only one file/image 
        thisOb.currentBo.documentId = documentId;
        thisOb.saveBo();
    }
};

NgHomeEditHelperModal.prototype.OpenPopup = function (thisbObParam, id) {
    var thisOb = thisbObParam || this;

    thisOb.isNewObject = !(id) ? true : false;
    if (thisOb.isNewObject) {
        id = 0;
    }
    thisOb.postManager.get(thisOb.conf.defaultApi + id,
        function (response) {
            angular.copy(response.data.data, thisOb.currentBo);
            thisOb.imgHlp = new ImageHelper("#imgContainer", "#dropzoneForm", thisOb.onUploadDocCompleted, thisbObParam, "wideImage");
            thisOb.image = $('#image');
            thisOb.imgHlp.clearDropzone();

            //When opening an existing object(for edit) you need to call the ProcessImage method
            //from the image helper that will check if the object has image linked to it and if so it will display that image and hide the dropzone. 
            //Otherwise the dropzone will be shown which allows the user to do upload.
            thisOb.imgHlp.processImage(thisOb.currentBo.document);

            $(thisOb.conf.modalJQSelector).modal();
            console.log(thisOb);
        });
}

NgHomeEditHelperModal.prototype.UploadNewImage = function (thisbObParam) {
    var thisOb = thisbObParam || this;

    thisOb.currentBo.documentId = null;
    thisOb.currentBo.document = {};

    thisOb.removeImage = true;
    thisOb.imgHlp.uploadNewImage();

}