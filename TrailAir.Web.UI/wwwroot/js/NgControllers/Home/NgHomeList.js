﻿var myApp = angular.module('UmaSoft', ['localytics.directives']);
var ngScope;
myApp.config(config);
myApp.controller('ngHome', function ($scope, $http, $timeout) {
    this.conf = {
        defaultApi: "/ApiProject/",
        userApi: '/ApiUser/',
        taskPhaseApi: '/ApiTaskPhase/',
        taskApi: '/ApiTask/'
    }
    var me = this;
    const postManager = new PostManager($http);
    const noTaskEl = $('#no-task');


    $scope.taskPhase = 1;
    $scope.details = {};
    $scope.isWorker = true;

    $scope.taskPhases = [];

    $scope.filterTasks = function () {
        // we are simulating 'archived' or id=7 as a task phase but it is not, when the user clicks on archived he wil get all tasks that are archived no matter the task phase
        // check ApiTaskPhaseController method GetTaskPhases
        if ($scope.taskPhase != 7) {
            $scope.getUserDetails(false);
        } else
        {
            $scope.getUserDetails(true);
        }
       
    }
    $scope.getTaskPhases = function () {
        postManager.get(me.conf.taskPhaseApi + 'GetTaskPhases', function (response) {
            angular.copy(response.data.data, $scope.taskPhases);
        })
    }
    $scope.getUserDetails = function (areTasksArchived) {
        var urlArchived = "";
        var urlUnarchived = "";



        if ($scope.isWorker.toLowerCase() === 'true') {
            urlUnarchived = me.conf.userApi + `GetUserDetails?taskPhase=${$scope.taskPhase}`;
            urlArchived = me.conf.userApi + `GetUserDetailsWhereAllTasksArchived`;
        }
        else
        {
            urlUnarchived = me.conf.userApi + `GetUserDetails_NonWorker_TasksUnarchived?taskPhase=${$scope.taskPhase}`;
            urlArchived = me.conf.userApi + `GetUserDetails_NonWorker_TasksArchived`;
        }
        if (!areTasksArchived) {
            postManager.get(urlUnarchived, function (response) {
                angular.copy(response.data.data, $scope.details)
                if (!$scope.details.userTasks.length) {
                    if (noTaskEl.hasClass('d-none'))
                        noTaskEl.removeClass('d-none');
                }
                else {
                    if (!noTaskEl.hasClass('d-none'))
                        noTaskEl.addClass('d-none');
                }
            })
        } else
        {
            postManager.get(urlArchived, function (response) {
                angular.copy(response.data.data, $scope.details)
                if (!$scope.details.userTasks.length) {
                    if (noTaskEl.hasClass('d-none'))
                        noTaskEl.removeClass('d-none');
                }
                else {
                    if (!noTaskEl.hasClass('d-none'))
                        noTaskEl.addClass('d-none');
                }
            })
        }
    }
    $scope.gotoDetails = function (projectId, taskId) {
        location.href = `/${language}/Task/TaskDetails?ProjectId=${projectId}&TaskId=${taskId}`;
    }
    $scope.getDataAccordingToUserRole = function (isWorker) {
        $scope.isWorker = isWorker;
        $scope.getTaskPhases();
        $scope.getUserDetails();
    }
    $scope.NgShortcutsHelper = new NgTaskShortcutsHelper($scope, $http);
    new NgHomeEditHelperModal($scope, postManager, $scope.getUserDetails);

    $scope.testSendMail = function () {
        postManager.post(me.conf.userApi + "TestSendMail/",
                function (response) {
                }
            );
    }
});
