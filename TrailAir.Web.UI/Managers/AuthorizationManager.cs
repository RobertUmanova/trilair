﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrailAir.DB.Models.Base;
using TrailAir.DB.Models.Enumerations;
using TrailAir.Web.UI.Controllers.Base;
using TrailAir.Web.UI.Helpers.Base.Exceptions;

namespace TrailAir.Web.UI.Managers
{
    public class AuthorizationManager
    {
        // <summary>
        /// Admin
        /// - no limits
        /// </summary>
        /// <param name="currentUser"></param>
        /// <param name="throwException"></param>
        /// <param name="filterContext"></param>
        /// <returns></returns>
        public static bool IsITAdministrator(BusinessLogicUser currentUser, bool logOut = false)
        {
            if (currentUser.Permission.PermissionCode != PermissionCode.ITAdministrator)
            {
                ThrowExceptionAndLogOut(currentUser, logOut);
                return false;
            }
            return true;
        }

        #region Below some Permission logic

        /// <summary>
        /// If current user has less permissions or priviledges than IT Administrator then log him out
        /// </summary>
        /// <returns></returns>
        public static bool IsBelowITAdministrator(BusinessLogicUser currentUser, bool logOut = false, ActionExecutingContext filterContext = null)
        {
            if (currentUser.Permission.PermissionCode > PermissionCode.ITAdministrator)
            {
                ThrowExceptionAndLogOut(currentUser, logOut);
                return true;
            }
            return false;
        }

        /// <summary>
        /// If current user has less permissions or priviledges than Super User then log him out
        /// </summary>
        /// <returns></returns>
        public static bool IsBelowSuperUser(BusinessLogicUser currentUser, bool logOut = false, ActionExecutingContext filterContext = null)
        {
            if (currentUser.Permission.PermissionCode > PermissionCode.SuperUser)
            {
                ThrowExceptionAndLogOut(currentUser, logOut);
                return true;
            }
            return false;
        }

        #endregion


        #region Above some Permission logic

        /// <summary>
        /// If current user has more permissions or priviledges than Panellist show him the content
        /// </summary>
        /// <returns></returns>
        public static bool IsSuperUserOrAbove(BusinessLogicUser currentUser, bool logOut = false, ActionExecutingContext filterContext = null)
        {
            if (currentUser.Permission.PermissionCode > PermissionCode.SuperUser)
            {
                ThrowExceptionAndLogOut(currentUser, logOut);
                return false;
            }
            return true;
        }

        /// <summary>
        /// If current user has more permissions or priviledges than Panellist show him the content
        /// </summary>
        /// <returns></returns>
        public static bool IsAbovePanelist(BusinessLogicUser currentUser, bool logOut = false, ActionExecutingContext filterContext = null)
        {
            if (currentUser.Permission.PermissionCode < PermissionCode.Panelist)
            {
                return true;
            }
            ThrowExceptionAndLogOut(currentUser, logOut);
            return false;
        }


        #endregion






        private static void ThrowExceptionAndLogOut(BusinessLogicUser currentUser, bool logOut = false)
        {
            var context = BaseDBController.Current;
            if (logOut)
            {
                UnauthorizedAccessException exception = new UnauthorizedAccessException("Unauthorized!");
                
                ExceptionHelper.HandleControllerExceptionsFromContext(exception, context);
                BaseDBController.RedirectToLogOut();
            }
        }
    }
}
