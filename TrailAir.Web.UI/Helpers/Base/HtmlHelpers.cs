﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace TrailAir.Web.UI.Helpers
{
    public static class HtmlHelpers
    {
        public static string IsSelected(this IHtmlHelper html, string controller = null, string action = null, string cssClass = null, bool aditionalCheck = true)
        {
            if (aditionalCheck == false) 
            {
                return String.Empty;
            }

            if (String.IsNullOrEmpty(cssClass))
                cssClass = "active";

            string currentAction = (string)html.ViewContext.RouteData.Values["action"];
            string currentController = (string)html.ViewContext.RouteData.Values["controller"];

            if (String.IsNullOrEmpty(controller))
                controller = currentController;

            if (String.IsNullOrEmpty(action))
                action = currentAction;

            return controller == currentController && action == currentAction ?
                cssClass : String.Empty;
        }


        public static string IsSelected(this IHtmlHelper html, string[] controllers, string cssClass = null)
        {
            if (controllers == null || controllers.Length < 1)
            {
                return string.Empty;
            }

            foreach (var singleController in controllers)
            {
                var resultSingContr = html.IsSelected(controller: singleController, cssClass : cssClass);
                if (!string.IsNullOrEmpty(resultSingContr))
                    return resultSingContr;
            }

            return string.Empty;
        }

        public static string PageClass(this IHtmlHelper htmlHelper)
        {
            string currentAction = (string)htmlHelper.ViewContext.RouteData.Values["action"];
            return currentAction;
        }
    }
}
