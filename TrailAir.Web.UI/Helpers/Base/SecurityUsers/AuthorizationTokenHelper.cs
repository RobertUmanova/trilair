﻿using Microsoft.EntityFrameworkCore;
using System.Text;
using TrailAir.DB;
using TrailAir.DB.Models;

namespace TrailAir.Web.UI.Helpers.Base.SecurityUsers
{
    public class AuthorizationTokenHelper
    {
        private readonly TrailAirDbContext _ctx;
        public AuthorizationToken this[string token, string email] => _ctx.AuthorizationTokens.FirstOrDefault(_ => _.Token == token && _.Email == email && !_.IsUsed);
        public AuthorizationTokenHelper(TrailAirDbContext trailAirDbContext)
        {
            _ctx = trailAirDbContext;
        }
        /// <summary>
        /// A method to check the validity of an authorization token
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<bool> IsTokenValid(string token, string email)
        {
            var authorizationToken = await _ctx.AuthorizationTokens.FirstOrDefaultAsync(_ => _.Token == token && _.Email == email && !_.IsUsed);

            return authorizationToken is not null && (authorizationToken?.ExpiryDate) >= DateTime.UtcNow;
        }
        /// <summary>
        /// A method to generate random token to be used for the invite
        /// </summary>
        /// <returns></returns>
        public string GenerateToken() => Convert.ToBase64String(Encoding.UTF8.GetBytes(Guid.NewGuid().ToString()));
    }
}
