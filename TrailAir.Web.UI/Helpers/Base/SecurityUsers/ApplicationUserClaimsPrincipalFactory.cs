﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System.Security.Claims;
using TrailAir.DB;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.Managers;

namespace TrailAir.Web.UI.Helpers.Base.SecurityUsers
{
    public class ApplicationUserClaimsPrincipalFactory :
        UserClaimsPrincipalFactory<ApplicationUser>
    {
        private readonly TrailAirDbContext _context;
        private readonly ApplicationUserManager UserManager;

        public ApplicationUserClaimsPrincipalFactory(ApplicationUserManager userManager,
            IOptions<IdentityOptions> options, TrailAirDbContext context)
            : base(userManager, options)
        {
            _context = context;
            UserManager = userManager;
        }

        protected override async Task<ClaimsIdentity> GenerateClaimsAsync(ApplicationUser user)
        {
            

            var identity = await base.GenerateClaimsAsync(user);

           // UserManager.LoadUserForCurrentScope(user);
            //var businessUser = (from bu in _context.BusinessLogicUsers
            //                    where bu.Id == user.BusinessLogicDataId
            //                    select bu).FirstOrDefault();

            //if (businessUser != null )
            //{
            //    identity.AddClaim(new Claim("uid", user.BusinessLogicDataId.ToString()));
            //}

            return identity;
        }


        // Dogodi se prije GenerateClaimsAsync !
        public override Task<ClaimsPrincipal> CreateAsync(ApplicationUser user)
        {
            // Ovdje loadati BusinessUser iz baze ??

            return base.CreateAsync(user);
        }
    }
}
