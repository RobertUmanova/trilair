﻿using Microsoft.AspNetCore.Http.Features;
using System.Text;
using TrailAir.DB.Models.Base;
using TrailAir.Utility;
using TrailAir.Utility.Configuration;

namespace TrailAir.Web.UI.Helpers.Base.Exceptions
{
    public class ExceptionHelper
    {
        internal readonly static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        internal static void HandleControllerExceptionsFromContext(Exception ex, HttpContext httpContext)
        {
            string browserBrand;
            string browserVersion;
            string requestUrl;
            string controller = httpContext.Request.RouteValues["controller"].ToString();
            string action = httpContext.Request.RouteValues["action"].ToString();
            string queryString = httpContext.Request.QueryString.ToString();
            var httpRequestFeature = httpContext.Features.Get<IHttpRequestFeature>();
            string completeUrl = Convert.ToString(httpRequestFeature.RawTarget);
            string userName = httpContext.User?.Identity?.Name ?? "EMPTY";

            string fullExceptionMessage = GetMessageFromException(ex);

            ContextHelper.GetBrowserDataFromControllerContext(httpContext, out browserBrand, out browserVersion, out requestUrl);

            LogErrorAndSendEmail(completeUrl, queryString, userName, browserBrand, browserVersion, requestUrl, controller, action, ex, fullExceptionMessage);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpContext">System.Web.HttpContext.Current</param>
        /// <param name="error message"></param>
        /// <param name="currentUser"></param>
        //internal static void HandleControllerExceptionsFromContext(HttpContext httpContext, string errorMessage, UiUser currentUser)
        //{
        //    Exception customException = new Exception("errorMessage");
        //    HandleControllerExceptionsFromContext(httpContext, customException, currentUser);
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="httpContext">System.Web.HttpContext.Current</param>
        ///// <param name="exception">we take message, stacktrace, and inner messages</param>
        ///// <param name="currentUser"></param>
        //internal static void HandleControllerExceptionsFromContext(HttpContext httpContext, Exception exception, UiUser currentUser)
        //{
        //    string browserBrand;
        //    string browserVersion;
        //    string requestUrl;
        //    HttpRequest httpRequest = httpContext != null ? httpContext.Request : null;

        //    RouteData routeData = null;
        //    if (httpRequest != null && httpRequest.RequestContext != null)
        //        routeData = httpRequest.RequestContext.RouteData;
        //    string queryString = "Unable to get QueryString because request is null";
        //    if (httpRequest != null && httpRequest.QueryString != null)
        //        queryString = httpRequest.QueryString.ToString();
        //    string completeUrl = "Unable to get Url";
        //    if (httpRequest != null && httpRequest.Url != null)
        //        completeUrl = httpRequest.Url.ToString();

        //    string controller = ContextHelper.GetRouteValue(routeData, "controller");
        //    string action = ContextHelper.GetRouteValue(routeData, "action");
        //    string userName = currentUser != null ? currentUser.FullName : "User is null";
        //    string fullExceptionMessage = GetMessageFromException(exception);
        //    string userAgent = httpRequest != null ? httpRequest.UserAgent : "Unable to get User Agent because requesti is null";
        //    string rawUrl = httpRequest != null ? httpRequest.RawUrl : "Unable to get Url because requesti is null";

        //    ContextHelper.GetBrowserData(userAgent, rawUrl, out browserBrand, out browserVersion, out requestUrl);

        //    LogErrorAndSendEmail(completeUrl, queryString, userName, browserBrand, browserVersion, requestUrl, controller, action, exception, fullExceptionMessage);

        //}

        internal static void LogErrorAndSendEmail(string completeUrl, string queryString, string userName, string browserBrand, string browserVersion, string requestUrl, string controller, string action, Exception exception, string fullExceptionMessage)
        {
            try
            {
                LogError(controller, fullExceptionMessage, action, userName);
            }
            catch (Exception logEx)
            {
                try
                {
                    string fullErrorMessage = GetMessageFromException(logEx);
                    SendEmail("Internal Server Error", $"Log ex:{fullErrorMessage}");
                }
                catch (Exception exInner)
                {
                    _logger.Log(NLog.LogLevel.Error, exInner, $"We encountered an InternalServerError Exception!: {fullExceptionMessage}");
                    //not much we can do here except write a log on a disc or windows errors
                }

            }

            try
            {
                SendEmail(completeUrl, queryString, userName, browserBrand, browserVersion, requestUrl, controller, action, fullExceptionMessage, exception);
            }
            catch (Exception mailEx)
            {
                try
                {
                    string fullErrorMessage = GetMessageFromException(mailEx);
                    LogError("ExceptionHelper",
                        $"Unable to send email. " +
                        $"Check server email configuration. And the realted application error you should find it in another db log." +
                        $"Send email ex:{fullErrorMessage}",
                        "sendEmail() OnError",
                        "check other log");
                }
                catch (Exception exInner)
                {
                    _logger.Log(NLog.LogLevel.Error, exInner, $"We encountered an InternalServerError Exception!: {fullExceptionMessage}");
                    //not much we can do here except write a log on a disc or windows errors
                }

            }

        }

        //internal static void LogErrorAndSendEmailFromException(HttpRequestBase request, string userName, Exception e)
        //{
        //    var controller = request.RequestContext.RouteData.Values["controller"].ToString();
        //    var action = request.RequestContext.RouteData.Values["action"].ToString();
        //    try
        //    {
        //        LogError(controller, e.Message, action, userName);
        //    }
        //    catch (Exception logEx)
        //    {
        //        try
        //        {
        //            string fullErrorMessage = GetMessageFromException(logEx);
        //            SendEmail("Unable to log",
        //                $"Unable to log to the database. " +
        //                $"Check DB connection. And the realted application error you should recive it in aseccond email." +
        //                $"Log ex:{fullErrorMessage}");
        //        }
        //        catch (Exception exInner)
        //        {
        //            //not much we can do here except write a log on a disc or windows errors
        //        }

        //    }

        //    try
        //    {
        //        SendEmail(request.Url.ToString(), String.Empty, userName, request.Browser.Browser, request.Browser.Version, request.Url.ToString(), controller, action, e.Message, e);
        //    }
        //    catch (Exception mailEx)
        //    {
        //        try
        //        {
        //            string fullErrorMessage = GetMessageFromException(mailEx);
        //            LogError("ExceptionHelper",
        //                $"Unable to send email. " +
        //                $"Check server email configuration. And the realted application error you should find it in another db log." +
        //                $"Send email ex:{fullErrorMessage}",
        //                "sendEmail() OnError",
        //                "check other log");
        //        }
        //        catch (Exception exInner)
        //        {
        //            //not much we can do here except write a log on a disc or windows errors
        //        }

        //    }

        //}

        private static void LogError(string controller, string fullExceptionMessage, string action, string userName)
        {
            RequestInfo requestInfo = new RequestInfo
            {
                Controller = controller,
                Action = action,
                User = userName,
                QueryString = "",
                Form = "",
                AngularData = fullExceptionMessage
            };

            using (var con = DB.Helper.ConnectionHelper.GetContextToRelease())
            {
                con.RequestInfos.Add(requestInfo);
                con.SaveChanges();
            }

        }

        private static void SendEmail(string completeUrl, string queryString, string userName, string browserBrand, string browserVersion, string requestUrl,
            string controller, string action, string fullExceptionMessage, Exception exception)
        {
            string stackTrace = exception == null ? "Exception is null" : exception.StackTrace;
            string baseExMessage = exception == null ? "Exception is null" : exception.Message;
            //SUBJECT
            string txtSubject = "TrailAir - " + controller + " "
                + action + " " + "Error";
            //MESSAGE

            string txtMessage = "Complete url: " + completeUrl + "\n" +
                                "Controller: " + controller + "\n" +
                                "Action: " + action + "\n" +
                                "Exception message: " + baseExMessage + "\n" +
                                "RequestUrl: " + requestUrl + "\n" +
                                "User: " + userName + "\n" +
                                "Browser: " + $"{browserBrand} {browserVersion}" + Environment.NewLine +
                                "Query String: " + queryString + "\n" +
                                "Exception message: " + fullExceptionMessage + "\n" +
                                "Exception info: " + stackTrace;
            SendEmail(txtSubject, txtMessage);

        }

        public static void SendEmail(string txtSubject, string txtMessage)
        {
            // TODO: Uncomment this !

            MailHelper mail = new MailHelper(txtSubject, txtMessage, EmailSetup.WebSiteEmailSender, EmailSetup.WebSiteEmailReceiver);
            mail.Send();
        }

        internal static string GetMessageFromException(Exception exceptionOb, StringBuilder stringBuilder = null, int level = 0)
        {

            if (stringBuilder == null)
                stringBuilder = new StringBuilder();

            if (level == 3 || exceptionOb == null)
                return stringBuilder.ToString();


            //in case of EntityValidationErrors 
            //if (exceptionOb is DbEntityValidationException)
            //{
            //    string line = "";
            //    foreach (var ex in ((System.Data.Entity.Validation.DbEntityValidationException)exceptionOb).EntityValidationErrors)
            //    {
            //        line += "Entity of type " + ex.Entry.Entity.GetType().Name + " in state " + ex.Entry.State + " has the following validation errors: ";

            //        foreach (var ve in ex.ValidationErrors)
            //        {
            //            line += " - Property:" + ve.PropertyName + "Error: " + ve.ErrorMessage;
            //        }
            //    }
            //    return stringBuilder.AppendLine(line).ToString();
            //}

            stringBuilder.AppendLine(exceptionOb.Message);

            if (exceptionOb.InnerException == null)
                return stringBuilder.ToString();

            return GetMessageFromException(exceptionOb.InnerException, stringBuilder, level++);
        }
    }
}
