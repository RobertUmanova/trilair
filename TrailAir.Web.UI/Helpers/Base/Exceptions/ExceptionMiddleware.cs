﻿
namespace TrailAir.Web.UI.Helpers.Base.Exceptions
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                ExceptionHelper.HandleControllerExceptionsFromContext(ex, httpContext);
                await new ResponseExceptionHandling().HandleExceptionAsync(ex,httpContext);
            }
        }
    }
}
