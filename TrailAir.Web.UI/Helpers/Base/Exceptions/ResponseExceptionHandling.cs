﻿using System.Net;
using TrailAir.UI.Modelling.UIModels.GenericApp;
using TrailAir.Utility.ExceptionHelpers;

namespace TrailAir.Web.UI
{
    public class ResponseExceptionHandling
    {
        public Task HandleExceptionAsync(Exception exception, HttpContext context)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            return context.Response.WriteAsync(new ActionResponse()
            {
                Success = false,
                ErrorMessages = new List<string>{ ExceptionObject.GetErrorMessage(exception)}
            }.ToString());
        }
    }
}
