﻿using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc.Filters;
using UAParser;

namespace TrailAir.Web.UI.Helpers
{
    public class ContextHelper
    {
        internal static string GetRouteValueFromContext(ActionExecutingContext context, string valueName)
        {
            RouteData routeData = null;
            if (context != null)
                routeData = context.RouteData;

            return GetRouteValue(routeData, valueName);
        }

        private static string GetRouteValue(RouteData routeData, string valueName)
        {
            if (routeData == null)
                return $"Unable to retrive the {valueName} because routeData is null";
            object objectVal;
            if (routeData.Values.TryGetValue(valueName, out objectVal))
                return objectVal.ToString();

            return string.Empty;
        }

        internal static void GetBrowserDataFromControllerContext(ActionExecutingContext controllerContext, out string browserBrand, out string browserVersion, out string requestUrl)
        {
            string userAgent = "Unable to get User Agent because requesti is null";
            string rawUrl = "Unable to get Url because requesti is null";


            GetBrowserDataFromControllerContext(controllerContext.HttpContext, out browserBrand, out browserVersion, out requestUrl);
        }

        internal static void GetBrowserDataFromControllerContext(Microsoft.AspNetCore.Http.HttpContext context, out string browserBrand, out string browserVersion, out string requestUrl)
        {
            string userAgent = "Unable to get User Agent because requesti is null";
            string rawUrl = "Unable to get Url because requesti is null";

            if (context != null && context.Request != null)
            {
                userAgent = context.Request.Headers["User-Agent"];
                rawUrl = context.Features.Get<IHttpRequestFeature>().RawTarget;
            }
            GetBrowserData(userAgent, rawUrl, out browserBrand, out browserVersion, out requestUrl);
        }

        private static void GetBrowserData(string userAgent, string rawUrl, out string browserBrand, out string browserVersion, out string requestUrl)
        {
            try
            {

                ClientInfo broswerInfo = Parser.GetDefault().Parse(userAgent);

                //Set User browser Properties
                requestUrl = rawUrl;
                browserBrand = broswerInfo.UA.Family;
                browserVersion = broswerInfo.UA.Major;
            }
            catch (Exception)
            {
                browserBrand = "N/A";
                browserVersion = "N/A";
                requestUrl = "N/A";
            }
        }
    }
}
