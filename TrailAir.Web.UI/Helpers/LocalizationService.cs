﻿using Microsoft.AspNetCore.Localization.Routing;
using System.Globalization;

namespace TrailAir.Web.UI.Helpers
{
    public static class LocalizationService
    {
        public static IServiceCollection AddLocalizationService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddLocalization();
            services.Configure<RequestLocalizationOptions>(options =>
            {
                var langList = TrailAir.UI.Managers.ConfigurationManager.LangList;

                string defaultLanguage = configuration.GetSection("Localization").GetSection("DefaultCulture").Value;

                List<CultureInfo> supportedCultures = new List<CultureInfo>();
                foreach (var culture in langList)
                {
                    supportedCultures.Add(new CultureInfo(culture.Code));
                };

                options.DefaultRequestCulture = new Microsoft.AspNetCore.Localization.RequestCulture(new CultureInfo(defaultLanguage), new CultureInfo(defaultLanguage));
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
                options.RequestCultureProviders.Insert(0, new RouteDataRequestCultureProvider());
                options.ApplyCurrentCultureToResponseHeaders = true;
            });

            return services;
        }
    }
}
