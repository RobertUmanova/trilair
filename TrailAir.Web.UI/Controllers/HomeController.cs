﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using TrailAir.DB;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.Managers;
using TrailAir.Web.UI.Controllers.Base;
using TrailAir.Web.UI.Models;

namespace TrailAir.Web.UI.Controllers
{
    public class HomeController : BaseWebController
    {
        public HomeController(TrailAirDbContext context, ApplicationUserManager userManager, IConfiguration configuration, SignInManager<ApplicationUser> signInManager)
            : base(context, userManager, configuration, signInManager)
        {
        }

        public IActionResult Index()
        {
            return View();
        }


        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error(int? statusCode = null)
        {
            if (statusCode.HasValue)
            {
                if (statusCode == 404)
                {
                    var viewName = statusCode.ToString();
                    return View(viewName);
                }
            }

            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}