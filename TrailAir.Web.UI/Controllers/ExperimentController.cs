﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TrailAir.DB;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.Managers;
using TrailAir.Web.UI.Controllers.Base;

namespace TrailAir.Web.UI.Controllers
{
    public class ExperimentController : BaseWebController
    {
        public ExperimentController(TrailAirDbContext context, ApplicationUserManager userManager, IConfiguration configuration, SignInManager<ApplicationUser> signInManager)
            : base(context, userManager, configuration, signInManager)
        {
        }

        public IActionResult List()
        {
            return View();
        }

        // Unnecessary complex routing example in .Net
        [Route("{lang}/[controller]/[action]/{experimentId}/{participantId}")]
        public IActionResult ExperimentWizardCalibrate(int experimentId, string participantId)
        {
            ViewBag.experimentId = experimentId;
            ViewBag.participantId = participantId;

            return View("ExperimentWizardSteps/ExperimentWizardCalibrate");
        }
        
        [Route("{lang}/[controller]/[action]/{experimentId}/{participantId}")]
        public IActionResult ExperimentWizardSamplePicker(int experimentId, string participantId)
        {
            ViewBag.experimentId = experimentId;
            ViewBag.participantId = participantId;

            return View("ExperimentWizardSteps/ExperimentWizardSamplePicker");
        }
        
        [Route("{lang}/[controller]/[action]/{experimentId}/{participantId}/{expTestSamplId}/{baseExperimentResultType}")]
        public IActionResult ExperimentWizardTest(int experimentId, string participantId, int expTestSamplId, int expControlSamplId)
        {
            ViewBag.experimentId = experimentId;
            ViewBag.participantId = participantId;
            ViewBag.expTestSamplId = expTestSamplId;
           // ViewBag.baseExperimentResultType = baseExperimentResultType; DEPRECATED We are not using anymore benchmarks 

            return View("ExperimentWizardSteps/ExperimentWizardTest");
        }

        [Route("{lang}/[controller]/[action]/{experimentId}/{participantId}")]
        public IActionResult ExperimentWizardResume(int experimentId, string participantId, int experimentResultId)
        {
            ViewBag.experimentId = experimentId;
            ViewBag.participantId = participantId;
            ViewBag.experimentResultId = experimentResultId;

            return View("ExperimentWizardSteps/ExperimentWizardResume");
        }
    }
}
