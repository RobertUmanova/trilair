﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TrailAir.DB;
using TrailAir.DB.Models.Base;
using TrailAir.DB.Models.Enumerations;
using TrailAir.UI.Modelling.Managers;
using TrailAir.Utility;
using TrailAir.Web.UI.Controllers.Base;
using TrailAir.Web.UI.Helpers.Base.SecurityUsers;
using TrailAir.Web.UI.Managers;

namespace TrailAir.Web.UI.Controllers
{
    public class AdminController : BaseWebController
    {
        private readonly TrailAirDbContext dbContext;
        private readonly ApplicationUserManager appUserManager;
        public AdminController(TrailAirDbContext context, ApplicationUserManager userManager, IConfiguration configuration, SignInManager<ApplicationUser> signInManager)
            : base(context, userManager, configuration, signInManager)
        {
            this.dbContext = context;
            this.appUserManager = userManager;
        }

        // TODO: Implement this with checking if the user is admin

        //protected override void OnActionExecuting(ActionExecutingContext filterContext)
        //{
        //    base.OnActionExecuting(filterContext);
        //    //CheckIfUserIsAdmin(filterContext);
        //}


        public IActionResult List()
        {
            AuthorizationManager.IsITAdministrator(ViewBag.CurrentUser, true);
            return View("View");
        }

        // TOOD: Move this MAYBE to MultyLanguageText's constructor ??
        private static MultyLanguageText NewMultiLanguageText(string enText = "", string deText = "")
        {
            if (string.IsNullOrEmpty(deText))
                deText = enText;

            var stringMultiLangName = new MultyLanguageText();
            stringMultiLangName.Texts = new List<LanguageText>();
            stringMultiLangName.Texts.Add(new LanguageText
            {
                Key = "en-GB",
                Name = enText
            });
            stringMultiLangName.Texts.Add(new LanguageText
            {
                Key = "de-DE",
                Name = deText
            });


            return stringMultiLangName;
        }

        [HttpGet]
        [Authorize]
        public IActionResult AddExperimentStatuses()
        {
            if (dbContext.ExperimentStatuses.Count() > 0)
            {
                return View();
            }

            dbContext.ExperimentStatuses.Add(new DB.Models.ExperimentStatus
            {
                ExperimentStatusCode = ExperimentStatusEnum.Draft,
                Name = NewMultiLanguageText("Creation", "Creation")
            });
            dbContext.ExperimentStatuses.Add(new DB.Models.ExperimentStatus
            {
                ExperimentStatusCode = ExperimentStatusEnum.InProgress,
                Name = NewMultiLanguageText("In progress", "In progress")
            });
            dbContext.ExperimentStatuses.Add(new DB.Models.ExperimentStatus
            {
                ExperimentStatusCode = ExperimentStatusEnum.Finished,
                Name = NewMultiLanguageText("Completed", "Completed")
            });

            dbContext.SaveChanges("Config-Admin");

            return Json(JsonResponseOb.GetSuccess(null));
        }


        [HttpGet]
        [Authorize]
        public IActionResult AddAllPermissions()
        {

            // Just to fix to put ITAdmin on the first Permission code
            var itAdmin = dbContext.Permissions.First(_ => _.Id == 5);
            if (itAdmin != null)
            {
                itAdmin.PermissionCode = PermissionCode.ITAdministrator;
            }
            
            // Just to fix to put ITAdmin on the first Permission code
            var superUser = dbContext.Permissions.First(_ => _.Id == 4);
            if (superUser != null)
            {
                superUser.PermissionCode = PermissionCode.SuperUser;
            }

            // Rename Tester to Assistant
            var tester = dbContext.Permissions.First(_ => _.Id == 6);
            if (tester != null)
            {
                tester.Name = NewMultiLanguageText("Assistant", "Assistant");
            }

            dbContext.Permissions.Add(new DB.Models.Permission
            {
                PermissionCode = PermissionCode.Perfumer,
                Name = NewMultiLanguageText("Perfumer", "Perfumer")
            });
            dbContext.Permissions.Add(new DB.Models.Permission
            {
                PermissionCode = PermissionCode.PanelAdmin,
                Name = NewMultiLanguageText("Panel admin", "Panel admin")
            });
            dbContext.Permissions.Add(new DB.Models.Permission
            {
                PermissionCode = PermissionCode.Panelist,
                Name = NewMultiLanguageText("Panellist", "Panellist")
            });
           

            dbContext.SaveChanges("Config-Admin");

            return Json(JsonResponseOb.GetSuccess(null));
        }
    }
}
