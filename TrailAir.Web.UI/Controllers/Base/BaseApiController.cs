﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TrailAir.DB;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.Managers;

namespace TrailAir.Web.UI.Controllers.Base
{

    [ApiController]
    [Route("{culture}/[controller]")]
    public class BaseApiController : BaseDBController
    {

        public BaseApiController(TrailAirDbContext context, ApplicationUserManager userManager, IConfiguration configuration, 
            SignInManager<ApplicationUser> signInManager) 
            : base(context, userManager, configuration, signInManager)
        {
        }
    }
}
