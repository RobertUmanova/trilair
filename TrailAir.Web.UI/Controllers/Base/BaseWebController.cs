﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using TrailAir.DB;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.Managers;
using TrailAir.Web.UI.Helpers;

namespace TrailAir.Web.UI.Controllers.Base
{
    public class BaseWebController : BaseDBController
    {
        private readonly ApplicationUserManager appUserManager;

        public BaseWebController(TrailAirDbContext context, ApplicationUserManager userManager, IConfiguration configuration, SignInManager<ApplicationUser> signInManager) 
            : base(context, userManager, configuration, signInManager)
        {
            this.appUserManager = userManager;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string browserBrand;
            string browserVersion;
            string requestUrl;

            //ClaimsPrincipal currentUser = this.User;
            //var currentUserID = currentUser.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            //var user = userManager.GetUserId(User);
            //var testUser = userManager.GetUserId(HttpContext.User);
            ContextHelper.GetBrowserDataFromControllerContext(filterContext, out browserBrand, out browserVersion, out requestUrl);
            RequestInfo requestInfo = new RequestInfo()
            {
                Controller = ContextHelper.GetRouteValueFromContext(filterContext, "controller"),
                Action = ContextHelper.GetRouteValueFromContext(filterContext, "action"),
                RequestUrl = requestUrl,
                User = filterContext.HttpContext.User.Identity.Name,
                QueryString = filterContext.HttpContext.Request.QueryString.ToString(),
                //Form = FilterContextHelper.GetFormData(filterContext),
                //AngularData = FilterContextHelper.GetActionData(filterContext),
                Browser = $"{browserBrand} {browserVersion}"
            };
            DbContext.RequestInfos.Add(requestInfo);
            DbContext.SaveChanges();

            LoadInfoFromUrl(filterContext.HttpContext.User.Identity.Name);



            base.OnActionExecuting(filterContext);
        }
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
        }

        /// <summary>
        /// Get data likecurrent active project and location selection if it exists 
        /// 
        /// </summary>
        public void LoadInfoFromUrl(string userName) 
        { 

            //StringValues projectIdReq;
            //StringValues locationIdReq;
            //StringValues templateIdReq;
            //StringValues templatedLocReq;
            //StringValues teamIdReq;
            //StringValues taskIdReq;
            //StringValues companyIdReq;


            //string url = Request.Path.ToString();
            //Request.Query.TryGetValue("ProjectId", out projectIdReq);
            //Request.Query.TryGetValue("LocationId", out locationIdReq);
            //Request.Query.TryGetValue("TemplateId", out templateIdReq);
            //Request.Query.TryGetValue("TemplatedLoc", out templatedLocReq);
            //Request.Query.TryGetValue("TeamId", out teamIdReq);
            //Request.Query.TryGetValue("TaskId", out taskIdReq);
            //Request.Query.TryGetValue("CompanyId", out companyIdReq);

            //if (url.Contains("Panel")) 
            //{
            //    SetUserPanelSelected();
            //}

            //if (url.Contains("HomeUserDetails"))
            //{
            //    SetUserPanelSelected();
            //}
            //if (companyIdReq.Count() > 0)
            //{
            //    SelectActiveCompany(IntHelper.StringToInt(companyIdReq.First()));
            //}
            //if (taskIdReq.Count() > 0)
            //{
            //    SelectActiveTask(IntHelper.StringToInt(taskIdReq.First()));
            //    //var user = context.HttpContext.User.Identity.Name;
            //    ViewBag.isWorker = (int) appUserManager.FindFullUiUserByName(userName).BusinessLogicData.Permission.PermissionCode > 5;
            //}
            //if (teamIdReq.Count() > 0)
            //{
            //    SelectActiveTeam(IntHelper.StringToInt(teamIdReq.First()));
            //}
            //if (templateIdReq.Count() > 0) 
            //{
            //    SelectActiveTemplate(IntHelper.StringToInt(templateIdReq.First()));
            //}
            //if (templatedLocReq.Count() > 0)
            //{
            //    SelectActiveTemplatedLoc(IntHelper.StringToInt(templatedLocReq.First()));
            //}
            //if (projectIdReq.Count() == 0)
            //{
            //    return;
            //}
            //else 
            //{
            //    var requestedProject= IntHelper.StringToInt(projectIdReq.First());
            //    SelectActiveProject(requestedProject);
            //}


            //if (locationIdReq.Count() == 0)
            //{
            //    return;
            //}
            //else 
            //{
            //    var requestedLocation = IntHelper.StringToInt(locationIdReq.First());
            //    SelectActiveLocation(requestedLocation);
            //}
        }

        /*
        private void SetUserPanelSelected()
        {
            ViewBag.SelectedUserPanel = true;
        }

        public void SelectActiveProject(int? projectId)
        {
            if (projectId == null)
                return;

            ProjectDetailsIM project = ProjectLoader.GetItemDet(DbContext, (int) projectId);
            if (project.ProjectEngineer != null) 
            {
                var appUserEngineer = appUserManager.Users.FirstOrDefault(_ => _.BusinessLogicDataId == project.ProjectEngineer.Id);
                if (appUserEngineer != null) 
                {
                    project.ProjectEngineer.FirstName = appUserEngineer.FirstName;
                    project.ProjectEngineer.LastName = appUserEngineer.LastName;
                    project.ProjectEngineer.PhoneNumber = appUserEngineer.PhoneNumber;
                }
            }
            if (project.ProjectArchitect != null) 
            {
                var appUserArchitect = appUserManager.Users.FirstOrDefault(_ => _.BusinessLogicDataId == project.ProjectArchitect.Id);
                if (appUserArchitect != null) 
                {
                    project.ProjectArchitect.FirstName = appUserArchitect.FirstName;
                    project.ProjectArchitect.LastName = appUserArchitect.LastName;
                    project.ProjectArchitect.PhoneNumber = appUserArchitect.PhoneNumber;
                }
            }

            if (project.ProjectBuildingOwner!= null) 
            {
                var appUserBuildingOwner = appUserManager.Users.FirstOrDefault(_ => _.BusinessLogicDataId == project.ProjectBuildingOwner.Id);
                if (appUserBuildingOwner != null) 
                {
                    project.ProjectBuildingOwner.FirstName = appUserBuildingOwner.FirstName;
                    project.ProjectBuildingOwner.LastName = appUserBuildingOwner.LastName;
                    project.ProjectBuildingOwner.PhoneNumber = appUserBuildingOwner.PhoneNumber;
                }
            }

            ///Fill Viebag with project Id and name for now 
            ///go to db get the project nema, throw error if project don't exist 
            ///get data from a loader for now just basic project DAta like name and id 
            ///
            /// Json stringify clien side project model you got just a line above
            /// 
            ViewBag.SelectedProject = project;

        }
        public void SelectActiveTeam(int? teamId)
        {
            if (teamId == null)
                return;
            TeamListDetailsIM team = TeamLoader.GetItemDet(DbContext, (int)teamId, appUserManager);
            ViewBag.SelectedTeam = team;
        }
        public void SelectActiveCompany(int? companyId)
        {
            if (companyId == null)
                return;
            CompanyDetailsIM company = CompanyLoader.GetItemDet(DbContext, (int)companyId, appUserManager);
            ViewBag.SelectedCompany = company;
        }
        public void SelectActiveTask(int? taskId)
        {
            if (taskId == null)
                return;
            LocationDetailsIM location = new();
            TaskDetailsIM task = TaskLoader.GetItemDet(DbContext, (int)taskId, appUserManager);
            ViewBag.SelectedTask = task;
            if (task.LocationId != null) 
            {
                location = LocationLoader.GetItemDet(DbContext, (int)task.LocationId, appUserManager);
                ViewBag.LocationHierarchy = LocationLoader.GetItemParentHierarchy(DbContext, location);
            }
        }
        public void SelectActiveLocation(int? locationId) 
        {
            if (locationId == null)
                return;
            LocationDetailsIM location = LocationLoader.GetItemDet(DbContext, (int)locationId, appUserManager);
            if (location.Document == null) 
            {
                location.Document = new();
                location.Document.Data = Array.Empty<byte>();
            }
            ViewBag.SelectedLocation = location;
            ViewBag.SelectedParentLocation = location;
            ViewBag.ParentLocationArchived = false;
            ViewBag.LocationHierarchy = LocationLoader.GetItemParentHierarchy(DbContext, location);
            foreach (var item in ViewBag.LocationHierarchy)
            {
                if (item.IsDbArchived) 
                {
                    ViewBag.ParentLocationArchived = true;
                }
            }
        }

        public void SelectActiveTemplate(int? templateId) 
        {
            if (templateId == null)
                return;
            TemplateDetailsIM template = TemplateLoader.GetItemDet(DbContext, (int)templateId);
            ViewBag.SelectedTemplate = template;

        }
        public void SelectActiveTemplatedLoc(int? templatedLocationId) 
        {
            if (templatedLocationId == null)
                return;
            TemplatedLocationDetailsIM templatedLocation = TemplatedLocationLoader.GetItemDet(DbContext, (int)templatedLocationId);
            ViewBag.SelectedTemplatedLocation = templatedLocation;
            ViewBag.TemplatedLocationHierarchy = TemplatedLocationLoader.GetItemParentHierarchy(DbContext, templatedLocation);
        }

        */
    }
}
