﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Primitives;
using TrailAir.DB;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.Managers;
using TrailAir.Web.UI.Managers;

namespace TrailAir.Web.UI.Controllers.Base
{
    [Authorize]
    public class BaseDBController : Controller
    {
        public string language = null;
        public readonly ApplicationUserManager userManager;
        public ApplicationUser UiFullLoggedUser { get; private set; }
        public BusinessLogicUser currentUser = new BusinessLogicUser();
        public TrailAirDbContext DbContext { get; private set; }

        public static SignInManager<ApplicationUser> _signInManager { get; private set; }

        private static string _cookieLangName = "LangForMultiLanguage";
        private IConfiguration _configuration { get; }
        internal static HttpContext Current { get; private set; }

        public BaseDBController(TrailAirDbContext context, ApplicationUserManager appUserManager, IConfiguration configuration,
            SignInManager<ApplicationUser> signInManager)
        {
            DbContext = context;
            userManager = appUserManager;
            _configuration = configuration;
            //Current = HttpContext;
            _signInManager = signInManager;
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            //if (ViewBag.CurrentUser.Id == 0)
            //{
            //    RedirectToLogOut();
            //}

        }

        public static string LinePrefix
        {
            get
            {
                return DateTime.Now.ToShortTimeString() + ": ";
            }
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            SetupLocalization(context);

            //System.IO.File.AppendAllText("tmp.txt", LinePrefix + "starting..." + Environment.NewLine);

            /// Here we do SAML Auto-provisioning i.e. saving or updating the user in the DB
            /// more about Auto-provisioning: 
            /// https://help.zscaler.com/zia/about-saml
            if (context.HttpContext.User.Claims.Count() > 0) // SAML Request
            {
                var claims = context.HttpContext.User.Claims.ToList();

                var emailClaim = claims.FirstOrDefault(_ => _.Type.Contains("email"));
                if (emailClaim == null)
                    throw new Exception("We didn't find any Email Claim!");

                UiFullLoggedUser = userManager.FindFullUiUserByName(emailClaim.Value);

                // If you don't find the user, create a new one!
                if (UiFullLoggedUser == null)
                {
                    var nameClaim = claims.FirstOrDefault(_ => _.Type == "name");
                    var nameTxt = nameClaim?.Value ?? "";

                    var surnameClaim = claims.FirstOrDefault(_ => _.Type == "surname");
                    var surnameTxt = surnameClaim?.Value ?? "";

                    emailClaim = claims.FirstOrDefault(_ => _.Type == "email");
                    var emailTxt = emailClaim?.Value ?? "";

                    // TODO: Check should role also be MANDATORY to send via claims ??!
                    var roleClaim = claims.FirstOrDefault(_ => _.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role");
                    var roleTxt = roleClaim?.Value ?? "";

                    if (string.IsNullOrEmpty(emailTxt))
                    {
                        throw new Exception("We didn't find any Email Claim!");
                    }
                    else
                    {
                        var permission = DbContext.Permissions
                            .Include(_ => _.Name)
                            .ThenInclude(_ => _.Texts)
                            .FirstOrDefault(_ => _.Name.Texts.Any(t => t.Name.ToLower() == roleTxt.ToLower()));

                        // TODO: This is probably going to change
                        if (permission == null)
                        {
                            permission = DbContext.Permissions.FirstOrDefault(_ => _.PermissionCode == DB.Models.Enumerations.PermissionCode.SuperUser);
                        }

                        DbContext.Users.Add(new ApplicationUser
                        {
                            FirstName = nameTxt,
                            LastName = surnameTxt,
                            Email = emailTxt.ToLower(),
                            UserName = emailTxt.ToLower(),
                            NormalizedEmail = emailTxt.ToUpper(),
                            NormalizedUserName = emailTxt.ToUpper(),
                            LockoutEnabled = true,
                            EmailConfirmed = true,
                            BusinessLogicData = new BusinessLogicUser { Permission = permission }
                        });

                        DbContext.SaveChanges();

                        UiFullLoggedUser = userManager.FindFullUiUserByName(emailTxt);
                    }
                }
            }
            else // If it is ordinary HTTP Request
            {
                UiFullLoggedUser = userManager.FindFullUiUserByName(context.HttpContext.User.Identity.Name);
            }

            currentUser = GetAuthorizationUser(UiFullLoggedUser);

            ViewBag.CurrentUser = currentUser;
            ViewBag.CurrentUserEmail = UiFullLoggedUser != null ? UiFullLoggedUser.Email : "";
            ViewBag.CurrentUserFullName = UiFullLoggedUser != null ? $"{UiFullLoggedUser.Fullname}" : "";

            Current = context.HttpContext;

            base.OnActionExecuting(context);
        }
        internal static void RedirectToLogOut(ActionExecutingContext filterContext = null)
        {
            //if (filterContext != null)
            //{
            //    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
            //    {
            //        //controller = "LoggingUsers",
            //        controller = "Identity/Account",
            //        action = "Logout"
            //    }));
            //    return;
            //}

            var url = "/Identity/Account/Login";
            _signInManager.SignOutAsync();
            Current.Response.Redirect(url, true);
        }


        #region Localization
        private void SetupLocalization(ActionExecutingContext context)
        {

            StringValues languageRequested = new StringValues();
            Request.Query.TryGetValue("country_selector_code", out languageRequested);
            if (languageRequested.Count() > 0)
            {
                language = languageRequested.First();
            }
            else if (context.RouteData.Values.ContainsKey("culture"))
            {
                language = context.RouteData.Values["culture"].ToString();// Get language from HTML form (Request.Form in old implementation of .Net)
            }

            if (!string.IsNullOrEmpty(language))
            {
                var langList = TrailAir.UI.Managers.ConfigurationManager.LangList;

                var selCulture = langList.FirstOrDefault(cult => cult.Code.Equals(language, StringComparison.OrdinalIgnoreCase)
                || cult.ShortCode.Equals(language, StringComparison.OrdinalIgnoreCase));
                if (selCulture != null)
                {
                    language = selCulture.Code;
                    if (languageRequested.Count() > 0) // If user request the language change, for now comes by submitting the drop down 
                    {
                        RedirectToNewLanguage(context, language);
                    }
                    else
                    {
                        var cultureInfo = new System.Globalization.CultureInfo(language);
                        System.Threading.Thread.CurrentThread.CurrentUICulture = cultureInfo;
                        System.Threading.Thread.CurrentThread.CurrentCulture = cultureInfo;
                        SetCultureOnCookie(context.HttpContext.Response, language);
                    }
                }
                else
                {
                    SetDefaultLocalization(context);
                }
            }

            if (languageRequested.Count() > 0)
            {

                context.Result = new RedirectResult($"~/{language}/");
            }
        }

        private void SetCultureOnCookie(HttpResponse result, string currentLanguage)
        {
            result.Cookies.Delete(_cookieLangName);
            result.Cookies.Append(_cookieLangName, currentLanguage);
        }

        private void SetDefaultLocalization(ActionExecutingContext context)
        {
            language = _configuration?.GetSection("Localization")?.GetSection("DefaultCulture")?.Value ?? "";

            RedirectToNewLanguage(context, language);
        }

        private void RedirectToNewLanguage(ActionExecutingContext context, string language)
        {
            if (!string.IsNullOrEmpty(language))
            {
                var cultureInfo = new System.Globalization.CultureInfo(language);
                System.Threading.Thread.CurrentThread.CurrentUICulture = cultureInfo;
                System.Threading.Thread.CurrentThread.CurrentCulture = cultureInfo;
                SetCultureOnCookie(context.HttpContext.Response, language);

                context.RouteData.Values["culture"] = language;

                context.Result = new RedirectToRouteResult( // Redirect to default language if some unknown string comes to {culture}
                    new RouteValueDictionary(
                        new
                        {
                            culture = context.RouteData.Values["culture"],
                            controller = context.RouteData.Values["controller"],
                            action = context.RouteData.Values["action"],

                        }
                    )
                );
            }
        }
        #endregion

        private BusinessLogicUser GetAuthorizationUser(ApplicationUser applicationUser)
        {
            if (UiFullLoggedUser is null) return new BusinessLogicUser();

            return new BusinessLogicUser
            {
                Permission = applicationUser.BusinessLogicData.Permission,
                Team = applicationUser.BusinessLogicData.Team,
                Fullname = applicationUser.Fullname
            };
        }
    }
}
