﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TrailAir.DB;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.Managers;
using TrailAir.Web.UI.Controllers.Base;
using TrailAir.Web.UI.Managers;

namespace TrailAir.Web.UI.Controllers
{
    public class TeamController : BaseWebController
    {
        public TeamController(TrailAirDbContext context, ApplicationUserManager userManager, IConfiguration configuration, SignInManager<ApplicationUser> signInManager) 
            : base(context, userManager, configuration, signInManager)
        {
        }

        public IActionResult List()
        {
            AuthorizationManager.IsSuperUserOrAbove(ViewBag.CurrentUser, true);
            return View();
        }
    }
}
