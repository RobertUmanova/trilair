﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TrailAir.DB;
using TrailAir.DB.Models;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.BoLoaders;
using TrailAir.UI.Modelling.BoMappers.Experiment;
using TrailAir.UI.Modelling.Managers;
using TrailAir.UI.Modelling.UIModels.Experiment;
using TrailAir.UI.Modelling.UIModels.GenericApp;
using TrailAir.UI.Modelling.UIModels.Participant;
using TrailAir.Utility;
using TrailAir.Web.UI.Controllers.Base;
using TrailAir.Web.UI.Helpers.Base.SecurityUsers;

namespace TrailAir.Web.UI.Controllers.Api
{
    public class ApiTestSampleController : BaseApiController
    {
        private readonly TrailAirDbContext context;
        private readonly ApplicationUserManager appUserManager;
        private readonly AuthorizationTokenHelper _authorizationTokenHelper;

        public ApiTestSampleController(TrailAirDbContext context, ApplicationUserManager userManager,
            IConfiguration configuration, AuthorizationTokenHelper authorizationTokenHelper, SignInManager<ApplicationUser> signInManager)
            : base(context, userManager, configuration, signInManager)
        {
            appUserManager = userManager;
            this.context = context;
            this.appUserManager = userManager;
            _authorizationTokenHelper = authorizationTokenHelper;
        }

        [HttpGet("[action]")]
        public IActionResult GetTestSamplesForExperiment(int experimentId)
        {
            List<ExperimentTestSampleIM> testSamplesList = TestSampleLoader.GetTestSamplesUiForExperiment(context, experimentId);

            if (testSamplesList == null)
            {
                return Json(JsonResponseOb.GetCustomError("Test samples not found"));
            }
            else
            {
                return Json(JsonResponseOb.GetSuccess(testSamplesList, null));
            }
        }

        [HttpPost("[action]")]
        public IActionResult SaveTestSample(ExperimentTestSampleIM testSample)
        {
            SingleBOActionResponse response = TestSampleLoader.SaveTestSample(context, testSample, UiFullLoggedUser.Email);

            if (!response.Success)
            {
                return Json(JsonResponseOb.GetCustomError(response.ErrorMessages[0].ToString()));
            }
            else
            {
                return Json(JsonResponseOb.GetSuccess(response.ObjectId, null));
            }
        }

        [HttpGet("[action]")]
        public IActionResult Delete(int testSampleId)
        {
            SingleBOActionResponse response = TestSampleLoader.DeleteSingleTestSample(context, testSampleId, UiFullLoggedUser.Email);
            if (response.Success)
            {
                return Json(JsonResponseOb.GetSuccess(response.ObjectId, null));
            }
            else
            {
                return Json(JsonResponseOb.GetCustomError(response.ErrorMessages.ToString()));
            }
        }

        /// <summary>
        /// You send the new TestSample and you return it's Computed Name
        /// </summary>
        /// <param name="testSampleUI"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public IActionResult GetComputedNameForTestSample(ExperimentTestSampleIM testSampleUI)
        {
            TestSample testSampleDB = new TestSample();
            testSampleDB = ExperimentMapper.MapExperimentTestSampleUIToDB(testSampleDB, testSampleUI);

            return Json(JsonResponseOb.GetSuccess(testSampleDB.ComputedName, null));

        }
    }
}
