﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TrailAir.DB;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.BoLoaders;
using TrailAir.UI.Modelling.Managers;
using TrailAir.UI.Modelling.UIModels.User;
using TrailAir.Utility;
using TrailAir.Web.UI.Controllers.Base;
using TrailAir.Web.UI.Helpers.Base.SecurityUsers;
using TrailAir.DB.Models.Enumerations;

namespace TrailAir.Web.UI.Controllers.Api
{
    public class ApiUserController : BaseApiController
    {
        private readonly TrailAirDbContext context;
        private readonly ApplicationUserManager appUserManager;
        private readonly AuthorizationTokenHelper _authorizationTokenHelper;
        public ApiUserController(TrailAirDbContext context, ApplicationUserManager userManager,
            IConfiguration configuration, AuthorizationTokenHelper authorizationTokenHelper, SignInManager<ApplicationUser> signInManager)
            : base(context, userManager, configuration, signInManager)
        {
            this.context = context;
            this.appUserManager = userManager;
            _authorizationTokenHelper = authorizationTokenHelper;
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            BusinessLogicUserDetailsIM objectUiDet = BasicUserLoader.GetItemDet(context, id);
            if (objectUiDet.Id > 0)
            {
                var appUser = appUserManager.Users.AsNoTracking().FirstOrDefault(_ => _.BusinessLogicDataId == objectUiDet.Id);
                objectUiDet.Email = appUser.Email;
                objectUiDet.UserName = appUser.UserName;
                objectUiDet.PhoneNumber = appUser.PhoneNumber;
                objectUiDet.FirstName = appUser.FirstName;
                objectUiDet.LastName = appUser.LastName;
            }

            if (objectUiDet == null)
            {
                return Json(JsonResponseOb.GetCustomError("Country not found"));
            }
            else
            {
                return Json(JsonResponseOb.GetSuccess(objectUiDet, null));
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetBLUserByAppUserId(string appUserId)
        {
            var user = appUserManager.Users.FirstOrDefault(_ => _.Id == appUserId);

            if (user == null)
            {
                return Json(JsonResponseOb.GetCustomError("User not found"));
            }
            else
            {
                BusinessLogicUserDetailsIM objectUiDet = BasicUserLoader.GetItemDet(context, user.BusinessLogicDataId);

                if (objectUiDet == null)
                {
                    return Json(JsonResponseOb.GetCustomError("User not found"));
                }

                return Json(JsonResponseOb.GetSuccess(objectUiDet, null));
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetList(string q, int? page, int? pageSize)
        {
            var needMoreChar = string.IsNullOrEmpty(q) || q.Length < 3;

            /// This is a little bit clumsy where you first get BLUsers, map them to UI classes
            /// and then in foreach you access the DB for each BLUser to map the ApplicationUser
            /// It was done like that in the begginings of the Swisscon project and was not refactored
            List<BusinessLogicUserListIM> users = BasicUserLoader.GetList(context);
            foreach (var user in users)
            {
                var appUser = appUserManager.Users.AsNoTracking().FirstOrDefault(_ => _.BusinessLogicDataId == user.Id);
                user.Email = appUser.Email;
                user.UserName = appUser.UserName;
                user.PhoneNumber = appUser.PhoneNumber;
                user.FirstName = appUser.FirstName;
                user.LastName = appUser.LastName;
            }

            if (users == null)
            {
                return Json(JsonResponseOb.GetCustomError("Users not found"));
            }
            else
            {
                Pager<BusinessLogicUserListIM> pager = new Pager<BusinessLogicUserListIM>(users, page, pageSize);
                ClientPager<BusinessLogicUserListIM> clientPager = new ClientPager<BusinessLogicUserListIM>(
                    pager.Page.ToList(),
                    pager.Info
                    );

                var result = new
                {
                    NeedMoreChar = needMoreChar,
                    Items = clientPager.PageItems,
                    CurrentPage = clientPager.CurrentPage,
                    PageCount = clientPager.PageCount
                };

                return Json(JsonResponseOb.GetSuccess(result, null));
            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> SaveUser(BusinessLogicUserDetailsIM userDetails)
        {
            // current logged in user is already tracked 
            var response = BasicUserLoader.Update(context, userDetails, UiFullLoggedUser.Email);
            var user = appUserManager.Users.FirstOrDefault(_ => _.BusinessLogicDataId == userDetails.Id);
            user.PhoneNumber = userDetails.PhoneNumber;
            user.LastName = userDetails.LastName;
            user.FirstName = userDetails.FirstName;
            var result = await appUserManager.UpdateAsync(user);

            if (response.Success)
            {
                return Json(JsonResponseOb.GetSuccess(response.ObjectId, null));
            }
            else
            {
                return Json(JsonResponseOb.GetCustomError(response.ErrorMessages.ToString()));
            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> CreateUser(BusinessLogicUserDetailsIM userDetails)
        {
            JsonResponseOb result = (JsonResponseOb)await appUserManager.CreateUserAccountAsync(userDetails);
            if (result.success)
            {
                //EmailContentModel emailContentModel = EmailContentModel.GetNewUserAccountCreatedHtmlMessage(userDetails, Request);
                //emailContentModel.NotifyByEmailUseHtmlAsync(userDetails.UserName);
            }

            return Json(result);
        }

        [HttpGet("GetDDList")]
        public IActionResult GetDDList()
        {
            var appUsers = appUserManager.Users
                .Where(u => u.BusinessLogicData.Permission.PermissionCode != DB.Models.Enumerations.PermissionCode.ITAdministrator)
                .AsNoTracking().ToList();

            List<KeyValName> simpleList = new();
            foreach (var user in appUsers)
            {
                simpleList.Add(new KeyValName
                {
                    Id = user.BusinessLogicDataId,
                    Text = user.FirstName + " " + user.LastName,
                });
            }

            return Json(JsonResponseOb.GetSuccess(simpleList, null));
        }



        /// <summary>
        /// Endpoint used for loading ApplicationUsers in Experiment Edit / Requester DDList (IT Admin and Panelist excluded)
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetRequesterDDList")]
        public IActionResult GetRequesterDDList()
        {
            var appUsers = appUserManager.Users.Include(_ => _.BusinessLogicData.Permission)
                .Where(u => u.BusinessLogicData.Permission.PermissionCode != PermissionCode.ITAdministrator
                && u.BusinessLogicData.Permission.PermissionCode != PermissionCode.Panelist)
                .AsNoTracking().ToList();

            List<KeyGUIDValName> simpleList = new();
            foreach (var user in appUsers)
            {
                simpleList.Add(new KeyGUIDValName
                {
                    Id = user.Id,
                    Text = user.FirstName + " " + user.LastName,
                });
            }

            return Json(JsonResponseOb.GetSuccess(simpleList, null));
        }

        /// <summary>
        /// Endpoint used for loading ApplicationUsers in Experiment Edit / Participants DDList (IT Admin and Assistant excluded)
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetParticipantsDDList")]
        public IActionResult GetParticipantsDDList()
        {
            var appUsers = appUserManager.Users.Include(_ => _.BusinessLogicData.Permission)
                .Where(u => u.BusinessLogicData.Permission.PermissionCode != PermissionCode.ITAdministrator
                && u.BusinessLogicData.Permission.PermissionCode != PermissionCode.Assistant)
                .AsNoTracking().ToList();

            List<KeyGUIDValName> simpleList = new();
            foreach (var user in appUsers)
            {
                simpleList.Add(new KeyGUIDValName
                {
                    Id = user.Id,
                    Text = user.FirstName + " " + user.LastName,
                });
            }

            return Json(JsonResponseOb.GetSuccess(simpleList, null));
        }

        /// <summary>
        /// WARNING!
        /// This is not a real endpoint where you fetch some data because you really need it.
        /// This is just the endpoint so we can make ANY call from Home page, because the Pace jQuery loader
        /// is making some problems and stuck on 99% of loadng if you don't have any HTTP Requests
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetNull")]
        public IActionResult GetNull()
        {
            return Json(JsonResponseOb.GetSuccess(null, null));
        }
    }
}
