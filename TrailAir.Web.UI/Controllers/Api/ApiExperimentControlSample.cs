﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TrailAir.DB;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.BoLoaders;
using TrailAir.UI.Modelling.Managers;
using TrailAir.UI.Modelling.UIModels.Experiment;
using TrailAir.UI.Modelling.UIModels.Participant;
using TrailAir.Utility;
using TrailAir.Web.UI.Controllers.Base;
using TrailAir.Web.UI.Helpers.Base.SecurityUsers;

namespace TrailAir.Web.UI.Controllers.Api
{
    public class ApiExperimentControlSample : BaseApiController
    {
        private readonly TrailAirDbContext context;
        private readonly ApplicationUserManager appUserManager;
        private readonly AuthorizationTokenHelper _authorizationTokenHelper;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public ApiExperimentControlSample(TrailAirDbContext context, ApplicationUserManager userManager,
            IConfiguration configuration, AuthorizationTokenHelper authorizationTokenHelper, IWebHostEnvironment webHostEnvironment, SignInManager<ApplicationUser> signInManager) 
            : base(context, userManager, configuration, signInManager)
        {
            appUserManager = userManager;
            this.context = context;
            this.appUserManager = userManager;
            _authorizationTokenHelper = authorizationTokenHelper;
            _webHostEnvironment = webHostEnvironment;
        }

       
        [HttpGet("{id}")]
        public IActionResult GetByExperiment(int experimentId)
        {
            var objectUiDet = BaseExperimentResultLoader.GetEmptyUIElementByType(context, experimentId);

            if (objectUiDet == null)
            {
                return Json(JsonResponseOb.GetCustomError("Country not found"));
            }
            else
            {
                return Json(JsonResponseOb.GetSuccess(objectUiDet, null));
            }
        }
        //[HttpPost("[action]")]
        //public IActionResult SaveBo(ExperimentDetailsIM experiment)
        //{
        //    var response = ExperimentLoader.Update(context, experiment, UiFullLoggedUser.Email);
        //    if (response.Success)
        //    {
        //        return Json(JsonResponseOb.GetSuccess(response.ObjectId, null));
        //    }
        //    else
        //    {
        //        return Json(JsonResponseOb.GetCustomError(response.ErrorMessages[0].ToString()));
        //    }
        //}
        #region Empty Objects

        [HttpGet("{action}")]
        public IActionResult GetEmptyExperimentDetails()
        {
            return Json(JsonResponseOb.GetSuccess(new ExperimentDetailsIM (), null));
        }

        [HttpGet("{action}")]
        public IActionResult GetEmptyExperimentBenchmark()
        {
            return Json(JsonResponseOb.GetSuccess(new ExperimentBenchmarkIM(), null));
        }

        [HttpGet("{action}")]
        public IActionResult GetEmptyExperimentTestSample()
        {
            return Json(JsonResponseOb.GetSuccess(new ExperimentTestSampleIM(), null));
        }

        [HttpGet("{action}")]
        public IActionResult GetEmptyExperimentParticipants()
        {
            return Json(JsonResponseOb.GetSuccess(new ExperimentsParticipantListIM(), null));
        }
        #endregion


    }
}
