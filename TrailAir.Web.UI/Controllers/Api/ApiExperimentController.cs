﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TrailAir.DB;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.BoLoaders;
using TrailAir.UI.Modelling.Managers;
using TrailAir.UI.Modelling.UIModels.Experiment;
using TrailAir.UI.Modelling.UIModels.Participant;
using TrailAir.Utility;
using TrailAir.Web.UI.Controllers.Base;
using TrailAir.Web.UI.Helpers.Base.SecurityUsers;

namespace TrailAir.Web.UI.Controllers.Api
{
    public class ApiExperimentController : BaseApiController
    {
        private readonly TrailAirDbContext context;
        private readonly ApplicationUserManager appUserManager;
        private readonly AuthorizationTokenHelper _authorizationTokenHelper;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public ApiExperimentController(TrailAirDbContext context, ApplicationUserManager userManager,
            IConfiguration configuration, AuthorizationTokenHelper authorizationTokenHelper, IWebHostEnvironment webHostEnvironment, SignInManager<ApplicationUser> signInManager) 
            : base(context, userManager, configuration, signInManager)
        {
            appUserManager = userManager;
            this.context = context;
            this.appUserManager = userManager;
            _authorizationTokenHelper = authorizationTokenHelper;
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            ExperimentDetailsIM objectUiDet = ExperimentLoader.GetItemDetUI(context, id);

            if (objectUiDet == null)
            {
                return Json(JsonResponseOb.GetCustomError("Experiment not found"));
            }
            else
            {
                return Json(JsonResponseOb.GetSuccess(objectUiDet, null));
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetFilteredList(string q, string teamsIds, string statusIds, int? page, int? pageSize)
        {
            var needMoreChar = string.IsNullOrEmpty(q) || q.Length < 3;

            List<ExperimentListIM> experiments = ExperimentLoader.GetItemFilteredList(context, teamsIds, statusIds);
            if (experiments == null)
            {
                return Json(JsonResponseOb.GetCustomError("Experiments not found"));
            }
            else
            {
                Pager<ExperimentListIM> pager = new Pager<ExperimentListIM>(experiments, page, pageSize);
                ClientPager<ExperimentListIM> clientPager = new ClientPager<ExperimentListIM>(
                    pager.Page.ToList(),
                    pager.Info
                    );

                var result = new
                {
                    NeedMoreChar = needMoreChar,
                    Items = clientPager.PageItems,
                    CurrentPage = clientPager.CurrentPage,
                    PageCount = clientPager.PageCount
                };

                return Json(JsonResponseOb.GetSuccess(result, null));
            }
        }

        [HttpPost("[action]")]
        public IActionResult SaveBo(ExperimentDetailsIM experiment)
        {
            var response = ExperimentLoader.Update(context, experiment, UiFullLoggedUser.Email);
            if (response.Success)
            {
                return Json(JsonResponseOb.GetSuccess(response.ObjectId, null));
            }
            else
            {
                return Json(JsonResponseOb.GetCustomError(response.ErrorMessages[0].ToString()));
            }
        }

        [HttpGet("[action]")]
        public IActionResult DeleteBo(int id)
        {
            var response = ExperimentLoader.Delete(context, id, UiFullLoggedUser.Email);
            if (response.Success)
            {
                return Json(JsonResponseOb.GetSuccess(response.ObjectId, null));
            }
            else
            {
                return Json(JsonResponseOb.GetCustomError(response.ErrorMessages.ToString()));
            }
        }

        [HttpGet("[action]")]
        public IActionResult DuplicateBo(int id, string newName)
        {
            var response = ExperimentLoader.Duplicate(context, id, newName, UiFullLoggedUser.Email);
            if (response.Success)
            {
                return Json(JsonResponseOb.GetSuccess(response.ObjectId, null));
            }
            else
            {
                return Json(JsonResponseOb.GetCustomError(response.ErrorMessages.ToString()));
            }
        }

        [HttpPost("[action]")]
        public IActionResult Publish(ExperimentDetailsIM experimentDetails)
        {
            var response = ExperimentLoader.Publish(context, experimentDetails, UiFullLoggedUser.Email);
            if (response.Success)
            {
                return Json(JsonResponseOb.GetSuccess(response.ObjectId, null));
            }
            else
            {
                return Json(JsonResponseOb.GetCustomError(response.ErrorMessages.ToString()));
            }
        }


        [HttpGet("[action]")]
        public IActionResult GetExperimentResultExcel(int id)
        {
            var document = ExperimentLoader.GetExperimentResultExcelFile(context, id, _webHostEnvironment.WebRootPath, out string fileName);

            var outputFileName = !string.IsNullOrEmpty(fileName) ? fileName : "Exp_Result_" + DateTime.Now.ToString("dd_MM_yyyy") + ".xlsx";

            return File(document, System.Net.Mime.MediaTypeNames.Application.Octet, outputFileName);
        }

        #region Empty Objects

        [HttpGet("{action}")]
        public IActionResult GetEmptyExperimentDetails()
        {
            return Json(JsonResponseOb.GetSuccess(new ExperimentDetailsIM (), null));
        }

        [HttpGet("{action}")]
        public IActionResult GetEmptyExperimentBenchmark()
        {
            return Json(JsonResponseOb.GetSuccess(new ExperimentBenchmarkIM(), null));
        }

        [HttpGet("{action}")]
        public IActionResult GetEmptyExperimentTestSample()
        {
            return Json(JsonResponseOb.GetSuccess(new ExperimentTestSampleIM(), null));
        }

        [HttpGet("{action}")]
        public IActionResult GetEmptyExperimentParticipants()
        {
            return Json(JsonResponseOb.GetSuccess(new ExperimentsParticipantListIM(), null));
        }
        #endregion



        //[HttpGet("GetTestTypeDDList")]
        //public IActionResult GetTestTypeDDList()
        //{
        //    IEnumerable<KeyValName> simpleList = ExperimentLoader.GetTestTypeDDList(context);

        //    return Json(JsonResponseOb.GetSuccess(simpleList, null));
        //}

        //[HttpGet("GetScoringScaleDefDDList")]
        //public IActionResult GetScoringScaleDefDDList()
        //{
        //    IEnumerable<KeyValName> simpleList = ExperimentLoader.GetScoringScaleDefDDList(context);

        //    return Json(JsonResponseOb.GetSuccess(simpleList, null));
        //}
    }
}
