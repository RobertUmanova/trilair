﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TrailAir.DB;
using TrailAir.DB.Models.Base;
using TrailAir.DB.Models.Enumerations;
using TrailAir.UI.Modelling.BoLoaders;
using TrailAir.UI.Modelling.Managers;
using TrailAir.UI.Modelling.UIModels;
using TrailAir.UI.Modelling.UIModels.Experiment;
using TrailAir.UI.Modelling.UIModels.ExpParticipantResult;
using TrailAir.UI.Modelling.UIModels.GenericApp;
using TrailAir.Utility;
using TrailAir.Web.UI.Controllers.Base;
using TrailAir.Web.UI.Helpers.Base.SecurityUsers;

namespace TrailAir.Web.UI.Controllers.Api
{
    public class ApiExperimentParticipantResultController : BaseApiController
    {

        private readonly TrailAirDbContext context;
        private readonly ApplicationUserManager appUserManager;
        private readonly AuthorizationTokenHelper _authorizationTokenHelper;

        public ApiExperimentParticipantResultController(TrailAirDbContext context, ApplicationUserManager userManager,
            IConfiguration configuration, AuthorizationTokenHelper authorizationTokenHelper, SignInManager<ApplicationUser> signInManager) 
            : base(context, userManager, configuration, signInManager)
        {
            appUserManager = userManager;
            this.context = context;
            this.appUserManager = userManager;
            _authorizationTokenHelper = authorizationTokenHelper;
        }
        public class WizardSamplesList
        {
            public int ExperimentId { get; set; }
            public string ParticipantId { get; set; }
            public List<ExperimentResultBaseIM> BechTestSampleResults { get; set; }
        }

        private int GetRelatedBenchResult (int benchmarkexperimentBenchMarkId, ParticipantResultIM particResult, BaseExperimentResultType type) 
        {
            int result = 0;

            if(type == BaseExperimentResultType.BenchmarkResult)
            {
                if (particResult == null || particResult.BenchmarkResults == null )
                    return result;


                foreach (ExperimentResultIM benchRes in particResult.BenchmarkResults)
                { 
                    if(benchRes.ExperimentBencTestSampId == benchmarkexperimentBenchMarkId)
                    {
                        return benchRes.Id;
                    }
                }
            }

            if (type == BaseExperimentResultType.TestSampleResult)
            {
                if (particResult == null || particResult.TestSampleResults == null)
                    return result;


                foreach (ExperimentResultIM testSempRes in particResult.TestSampleResults)
                {
                    if (testSempRes.ExperimentBencTestSampId == benchmarkexperimentBenchMarkId)
                    {
                        return testSempRes.Id;
                    }
                }
            }
            return result;
        }
        
        [HttpGet("[action]")]
        public IActionResult GetByParticAndExpForWizard(string participantId, int experimentId)
        {
            var experimentUiDet = ExperimentLoader.GetItemDetUI(context, experimentId);//here we should change to give back the DB object no need to get the UI

            WizardSamplesList result = new WizardSamplesList();
            result.ExperimentId = experimentId;
            result.ParticipantId = participantId;

            result.BechTestSampleResults = new List<ExperimentResultBaseIM>();

            foreach (var testSample in experimentUiDet.ExperimentTestSamples)
            {
                result.BechTestSampleResults.Add(new ExperimentResultBaseIM()
                {
                    Id = testSample.Id,
                    SampleCode = testSample.TestSample.SampleCode,
                    SampleName = testSample.TestSample.SampleName,
                    ExpResultType = BaseExperimentResultType.TestSampleResult,
                    IsSubmitted = ParticipantResultLoader.CheckIfResultIsSubmitted(context, experimentId, participantId, testSample.Id, (int)BaseExperimentResultType.TestSampleResult),
                    SampleIndex = testSample.TestSample.SampleIndex
                });
            }

            return Json(JsonResponseOb.GetSuccess(result, null));
        }


        [HttpGet("[action]")]
        public IActionResult GetByParticAndExp(string participantId, int experimentId)
        {
            var particResult = ParticipantResultLoader.GetUIModelByParticAndExp(context, participantId, experimentId);
            if (particResult == null)
            {
                return Json(JsonResponseOb.GetCustomError("ParticipantResult not found"));
            }
            else
            {
                return Json(JsonResponseOb.GetSuccess(particResult, null));
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetExistingParticipantResult(string participantId, int experimentId)
        {
            var result = ParticipantResultLoader.CheckIfParticipantResultExists(context, participantId, experimentId);
            return Json(JsonResponseOb.GetSuccess(result, null));
        }

        [HttpPost("[action]")]
        public IActionResult SaveBo(ParticipantResultIM participantResult)
        {
            var response = ParticipantResultLoader.Update(context, participantResult, UiFullLoggedUser.Email);
            if (response.Success)
            {
                return Json(JsonResponseOb.GetSuccess(response.ObjectId, null));
            }
            else
            {
                return Json(JsonResponseOb.GetCustomError(response.ErrorMessages.ToString()));
            }
        }

        [HttpPost("[action]")]
        public IActionResult Submit(ParticipantResultIM participantResult)
        {
            var particResult = ParticipantResultLoader.Submit(context, participantResult, UiFullLoggedUser.Email);
            if (particResult == null)
            {
                return Json(JsonResponseOb.GetCustomError("ParticipantResult not found"));
            }
            else
            {
                return Json(JsonResponseOb.GetSuccess(particResult, null));
            }
        }

        [HttpGet("[action]")]
        public IActionResult WizardSubmit(int experimentId, string participantId)
        {
            var particResult = ParticipantResultLoader.WizardSubmit(context, experimentId, participantId, UiFullLoggedUser.Email);
            if (particResult == null)
            {
                return Json(JsonResponseOb.GetCustomError("ParticipantResult not found"));
            }
            else
            {
                return Json(JsonResponseOb.GetSuccess(particResult, null));
            }
        }


        [HttpGet("[action]")]
        public IActionResult GetEmptyOrExistingBaseExpResultByType(int experimentId, 
            int expTestSamplId)
        {
            var response = BaseExperimentResultLoader.GetEmptyUIElementByType(context, experimentId,
                expTestSamplId/*, baseExpResultType*/);

            return Json(JsonResponseOb.GetSuccess(response, null));
        }

        public class WizardSubmitResultUIBodyObject : WizardCalibrationIM
        {
            public int experimentResultTypeId { get; set; }
        }

        [HttpPost("[action]")]
        public IActionResult SubmitExpParticipantResultFromWizard(WizardSubmitResultUIBodyObject obj)
        {
            var response = BaseExperimentResultLoader.AddTestSampleResult(context, obj.experimentId, obj.participantId, obj.objectUi,
                obj.experimentResultTypeId, UiFullLoggedUser.Email);
            if (!response.Success)
            {
                return Json(JsonResponseOb.GetCustomError("Couldn't update experiment result"));
            }
            else
            {
                return Json(JsonResponseOb.GetSuccess(response, null));
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetExperimentResultsListForParticipantResult(int experimentId, string participantId)
        {
            var result = ParticipantResultLoader.GetOrderedExperimentResultsByParticipantResult(context, experimentId, participantId);
            if (result == null)
            {
                return Json(JsonResponseOb.GetCustomError("ParticipantResult not found"));
            }
            else
            {
                return Json(JsonResponseOb.GetSuccess(result, null));
            }
        }

        [HttpPost("[action]")]
        public IActionResult CreateWizardCalibrationResult(WizardCalibrationIM calibration)
        {
            SingleBOActionResponse response = ParticipantResultLoader.CreateCalibration(context, calibration, UiFullLoggedUser.Email);
            if (response.Success)
            {
                return Json(JsonResponseOb.GetSuccess(response.ObjectId, null));
            }
            else
            {
                return Json(JsonResponseOb.GetCustomError(response.ErrorMessages.ToString()));
            }
        }

        [HttpGet("[action]")]
        public IActionResult CancelWizardResults(int experimentId, string participantId)
        {
            var result = ParticipantResultLoader.CancelWizardResults(context, experimentId, participantId, UiFullLoggedUser.Email);
            if (result == null)
            {
                return Json(JsonResponseOb.GetCustomError("ParticipantResult not found"));
            }
            else
            {
                return Json(JsonResponseOb.GetSuccess(result, null));
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetSingleExperimentResult(int baseExperimentResultId, int baseExperimentResultType)
        {
            var result = ParticipantResultLoader.GetSingleExperimentResult(context, baseExperimentResultId, baseExperimentResultType);
            if (result == null)
            {
                return Json(JsonResponseOb.GetCustomError("ExperimentResult not found"));
            }
            else
            {
                return Json(JsonResponseOb.GetSuccess(result, null));
            }
        }

        [HttpPost("[action]")]
        public IActionResult SaveSingleExperimentResult(ExperimentResultIM obj)
        {
            var response = BaseExperimentResultLoader.UpdateSingle(context, obj, UiFullLoggedUser.Email);
            if (!response.Success)
            {
                return Json(JsonResponseOb.GetCustomError("Couldn't update experiment result"));
            }
            else
            {
                return Json(JsonResponseOb.GetSuccess(response, null));
            }
        }
    }
}
