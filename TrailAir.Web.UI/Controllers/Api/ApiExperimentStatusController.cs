﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TrailAir.DB;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.BoLoaders;
using TrailAir.UI.Modelling.Managers;
using TrailAir.Utility;
using TrailAir.Web.UI.Controllers.Base;
using TrailAir.Web.UI.Helpers.Base.SecurityUsers;

namespace TrailAir.Web.UI.Controllers.Api
{
    public class ApiExperimentStatusController : BaseApiController
    {
        private readonly TrailAirDbContext context;
        private readonly ApplicationUserManager appUserManager;
        private readonly AuthorizationTokenHelper _authorizationTokenHelper;
        public ApiExperimentStatusController(TrailAirDbContext context, ApplicationUserManager userManager, 
            IConfiguration configuration, AuthorizationTokenHelper authorizationTokenHelper, SignInManager<ApplicationUser> signInManager)
            : base(context, userManager, configuration, signInManager)
        {
            this.context = context;
            this.appUserManager = userManager;
            _authorizationTokenHelper = authorizationTokenHelper;
        }

        /// <summary>
        /// Fetches the data for the Drop Down List
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetDDList")]
        public IActionResult GetDDList()
        {
            IEnumerable<KeyValName> simpleList = ExperimentStatusLoader.GetDDList(context);

            return Json(JsonResponseOb.GetSuccess(simpleList, null));
        }

        /// <summary>
        /// Fetches the default Experiment Statuses. For now: Draft and InProgress
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetDefaultStatusIds")]
        public IActionResult GetDefaultStatusIds()
        {
            int[] simpleList = ExperimentStatusLoader.GetDefaultStatusIds();

            return Json(JsonResponseOb.GetSuccess(simpleList, null));
        }
    }
}
