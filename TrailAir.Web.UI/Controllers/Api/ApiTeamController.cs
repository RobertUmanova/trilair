﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TrailAir.DB;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.BoLoaders;
using TrailAir.UI.Modelling.Managers;
using TrailAir.UI.Modelling.UIModels.Team;
using TrailAir.Utility;
using TrailAir.Web.UI.Controllers.Base;
using TrailAir.Web.UI.Helpers.Base.SecurityUsers;

namespace TrailAir.Web.UI.Controllers.Api
{
    public class ApiTeamController : BaseApiController
    {
        private readonly TrailAirDbContext context;
        private readonly ApplicationUserManager appUserManager;
        private readonly AuthorizationTokenHelper _authorizationTokenHelper;

        public ApiTeamController(TrailAirDbContext context, ApplicationUserManager appUserManager,
            IConfiguration configuration, AuthorizationTokenHelper authorizationTokenHelper, SignInManager<ApplicationUser> signInManager)
            : base(context, appUserManager, configuration, signInManager)
        {
            appUserManager = userManager;
            this.context = context;
            this.appUserManager = userManager;
            _authorizationTokenHelper = authorizationTokenHelper;
        }

        [HttpGet("[action]")]
        public IActionResult GetList(string q, int? page, int? pageSize)
        {
            var needMoreChar = string.IsNullOrEmpty(q) || q.Length < 3;

            List<TeamListIM> teams = TeamLoader.GetItemList(context);
            if (teams == null)
            {
                return Json(JsonResponseOb.GetCustomError("Teams not found"));
            }
            else
            {
                Pager<TeamListIM> pager = new Pager<TeamListIM>(teams, page, pageSize);
                ClientPager<TeamListIM> clientPager = new ClientPager<TeamListIM>(
                    pager.Page.ToList(),
                    pager.Info
                    );

                var result = new
                {
                    NeedMoreChar = needMoreChar,
                    Items = clientPager.PageItems,
                    CurrentPage = clientPager.CurrentPage,
                    PageCount = clientPager.PageCount
                };

                return Json(JsonResponseOb.GetSuccess(result, null));
            }
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            TeamDetailsIM team = TeamLoader.GetItem(DbContext, id);
            if (team == null)
            {
                return Json(JsonResponseOb.GetCustomError("Team not found"));
            }
            else
            {
                return Json(JsonResponseOb.GetSuccess(team, null));
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetTeamIdByLoggedUser()
        {
            var currentUserId = appUserManager.GetUserId(this.User);

            int id = TeamLoader.GetTeamByLoggedUser(context, currentUserId);
            if (id == 0)
            {
                return Json(JsonResponseOb.GetCustomError("Team not found"));
            }
            else
            {
                return Json(JsonResponseOb.GetSuccess(id, null));
            }
        }

        [HttpPost("[action]")]
        public IActionResult SaveBo(TeamDetailsIM teamList)
        {
            var response = TeamLoader.Update(context, teamList, UiFullLoggedUser.Email);
            if (response.Success)
            {
                return Json(JsonResponseOb.GetSuccess(response.ObjectId, null));
            }
            else
            {
                return Json(JsonResponseOb.GetCustomError(response.ErrorMessages.ToString()));
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var response = TeamLoader.Delete(context, id, UiFullLoggedUser.Email);
            if (response.Success)
            {
                return Json(JsonResponseOb.GetSuccess(response.ObjectId, null));
            }
            else
            {
                return Json(JsonResponseOb.GetCustomError(response.ErrorMessages.ToString()));
            }
        }

        // which REST verb to use when archiving ?!
        [HttpDelete("[action]/{id}")]
        public IActionResult Archive(int id)
        {
            var response = TeamLoader.Archive(context, id, UiFullLoggedUser.Email);
            if (response.Success)
            {
                return Json(JsonResponseOb.GetSuccess(response.ObjectId, null));
            }
            else
            {
                return Json(JsonResponseOb.GetCustomError(string.Join("", response.ErrorMessages)));
            }
        }
        

        [HttpGet("GetDDList")]
        public IActionResult GetDDList()
        {
            IEnumerable<KeyValName> simpleList = TeamLoader.GetDDList(context);

            return Json(JsonResponseOb.GetSuccess(simpleList, null));
        }

        //private IEnumerable<KeyValName> FilterTeamListBySearchString(string q, IEnumerable<KeyValName> simpleList)
        //{
        //    if (!string.IsNullOrEmpty(q))
        //    {
        //        var tokens = q.Split(' ');
        //        foreach (var token in tokens)
        //        {
        //            if (!string.IsNullOrEmpty(token))
        //            {
        //                var stringToCompare = token.ToLower();
        //                simpleList = simpleList.Where(
        //                        p => p.Text != null && p.Text.ToLower().Contains(stringToCompare)
        //                );
        //            }
        //        }
        //    }

        //    return simpleList;
        //}

        ////[HttpGet("{id}")]
        ////public IActionResult Get(int id)
        ////{
        ////    TeamListIM team = TeamLoader.GetItem(context, id);
        ////    if (team == null)
        ////    {
        ////        return Json(JsonResponseOb.GetCustomError("Team not found"));
        ////    }
        ////    else
        ////    {
        ////        return Json(JsonResponseOb.GetSuccess(team, null));
        ////    }
        ////}

        //[HttpGet("{id}")]
        //public IActionResult Get(int id)
        //{
        //    TeamListDetailsIM team = TeamLoader.GetItemDet(DbContext, (int)id, appUserManager);
        //    if (team == null)
        //    {
        //        return Json(JsonResponseOb.GetCustomError("Team not found"));
        //    }
        //    else
        //    {
        //        return Json(JsonResponseOb.GetSuccess(team, null));
        //    }
        //}
        //[HttpPost("AddUserToTeam")]
        //public IActionResult AddUserToTeam(TeamListDetailsIM teamList)
        //{
        //    var response = TeamLoader.AddUserToTeam(context, teamList, UiFullLoggedUser.Email);
        //    if (response.Success)
        //    {
        //        return Json(JsonResponseOb.GetSuccess(response.ObjectId, null));
        //    }
        //    else
        //    {
        //        return Json(JsonResponseOb.GetCustomError(response.ErrorMessages.ToString()));
        //    }
        //}
        //[HttpPost("SaveBo")]
        //public IActionResult SaveBo(TeamListDetailsIM teamList)
        //{
        //    var response = TeamLoader.Update(context, teamList, UiFullLoggedUser.Email);
        //    if (response.Success)
        //    {
        //        return Json(JsonResponseOb.GetSuccess(response.ObjectId, null));
        //    }
        //    else
        //    {
        //        return Json(JsonResponseOb.GetCustomError(response.ErrorMessages.ToString()));
        //    }
        //}
        //[HttpDelete("{id}")]
        //public IActionResult Delete(int id)
        //{
        //    var response = TeamLoader.DeleteOb(context, id, UiFullLoggedUser.Email);
        //    if (response.Success)
        //    {
        //        return Json(JsonResponseOb.GetSuccess(response.ObjectId, null));
        //    }
        //    else
        //    {
        //        return Json(JsonResponseOb.GetCustomError(response.ErrorMessages.ToString()));
        //    }
        //}

        //[HttpDelete("[action]/{id}")]
        //public IActionResult Archive(int id)
        //{
        //    var response = TeamLoader.ArchiveOb(context, id, UiFullLoggedUser.Email);
        //    if (response.Success)
        //    {
        //        return Json(JsonResponseOb.GetSuccess(response.ObjectId, null));
        //    }
        //    else
        //    {
        //        return Json(JsonResponseOb.GetCustomError(string.Join("", response.ErrorMessages)));
        //    }
        //}
    }
}
