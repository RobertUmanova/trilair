﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TrailAir.DB;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.Managers;
using TrailAir.Utility;
using TrailAir.Web.UI.Controllers.Base;
using TrailAir.Web.UI.Helpers.Base.SecurityUsers;
using UmaSoft.UI.Modelling.BoLoaders;

namespace TrailAir.Web.UI.Controllers.Api
{
    public class ApiPermissionController : BaseApiController
    {
        private readonly TrailAirDbContext context;
        private readonly ApplicationUserManager appUserManager;
        private readonly AuthorizationTokenHelper _authorizationTokenHelper;
        public ApiPermissionController(TrailAirDbContext context, ApplicationUserManager userManager, 
            IConfiguration configuration, AuthorizationTokenHelper authorizationTokenHelper, SignInManager<ApplicationUser> signInManager) 
            : base(context, userManager, configuration, signInManager)
        {
            this.context = context;
            this.appUserManager = userManager;
            _authorizationTokenHelper = authorizationTokenHelper;
        }

        [HttpGet("[action]")]
        public IActionResult GetList()
        {



            return null;
        }

        /// <summary>
        /// Fetches the data for the Drop Down List
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetDDList")]
        public IActionResult GetDDList()
        {
            IEnumerable<KeyValName> simpleList = PermissionLoader.GetDDList(context);

            return Json(JsonResponseOb.GetSuccess(simpleList, null));
        }
    }
}
