﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TrailAir.DB;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.BoLoaders;
using TrailAir.UI.Modelling.Managers;
using TrailAir.UI.Modelling.UIModels.Experiment;
using TrailAir.Utility;
using TrailAir.Web.UI.Controllers.Base;
using TrailAir.Web.UI.Helpers.Base.SecurityUsers;

namespace TrailAir.Web.UI.Controllers.Api
{
    public class ApiControlSampleTypeController : BaseApiController
    {
        private readonly TrailAirDbContext context;
        private readonly ApplicationUserManager appUserManager;
        private readonly AuthorizationTokenHelper _authorizationTokenHelper;
        public ApiControlSampleTypeController(TrailAirDbContext context, ApplicationUserManager userManager,
            IConfiguration configuration, AuthorizationTokenHelper authorizationTokenHelper, SignInManager<ApplicationUser> signInManager) 
            : base(context, userManager, configuration, signInManager)
        {
            this.context = context;
            this.appUserManager = userManager;
            _authorizationTokenHelper = authorizationTokenHelper;
        }


        /// <summary>
        /// Fetches the data for the Drop Down List
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetDDList")]
        public IActionResult GetDDList()
        {
            IEnumerable<KeyValName> simpleList = ControlSampleTypeLoader.GetDDList(context);

            return Json(JsonResponseOb.GetSuccess(simpleList, null));
        }


        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            ControlSampleTypeListIM elementUI = ControlSampleTypeLoader.GetItem(DbContext, id);
            if (elementUI == null)
            {
                return Json(JsonResponseOb.GetCustomError("Control Sample Type not found"));
            }
            else
            {
                return Json(JsonResponseOb.GetSuccess(elementUI, null));
            }
        }

        [HttpGet("{action}")]
        public IActionResult GetEmptyControlSampleType()
        {
            return Json(JsonResponseOb.GetSuccess(new ControlSampleTypeListIM(), null));
        }

        [HttpPost("[action]")]
        public IActionResult SaveBo(ControlSampleTypeListIM currentElement)
        {
            var response = ControlSampleTypeLoader.Update(context, currentElement, UiFullLoggedUser.Email);
            if (response.Success)
            {
                return Json(JsonResponseOb.GetSuccess(response.ObjectId, null));
            }
            else
            {
                return Json(JsonResponseOb.GetCustomError(response.ErrorMessages.ToString()));
            }
        }
    }
}
