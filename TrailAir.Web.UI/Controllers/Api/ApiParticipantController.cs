﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TrailAir.DB;
using TrailAir.DB.Models.Base;
using TrailAir.UI.Modelling.BoLoaders;
using TrailAir.UI.Modelling.Managers;
using TrailAir.Utility;
using TrailAir.Web.UI.Controllers.Base;
using TrailAir.Web.UI.Helpers.Base.SecurityUsers;

namespace TrailAir.Web.UI.Controllers.Api
{
    public class ApiParticipantController : BaseApiController
    {
        private readonly TrailAirDbContext context;
        private readonly ApplicationUserManager appUserManager;
        private readonly AuthorizationTokenHelper _authorizationTokenHelper;

        public ApiParticipantController(TrailAirDbContext context, ApplicationUserManager userManager, 
            IConfiguration configuration, AuthorizationTokenHelper authorizationTokenHelper, SignInManager<ApplicationUser> signInManager) 
            : base(context, userManager, configuration, signInManager)
        {
            appUserManager = userManager;
            this.context = context;
            this.appUserManager = userManager;
            _authorizationTokenHelper = authorizationTokenHelper;
        }

        /// <summary>
        /// DEPRECATED. Not used anymore! We now use BusinessLogicUser insted of Participant!
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetDDList")]
        public IActionResult GetDDList()
        {
            //IEnumerable<KeyValName> simpleList = ParticipantLoader.GetDDList(context, appUserManager);

            //return Json(JsonResponseOb.GetSuccess(simpleList, null));
            throw new NotImplementedException("This is deprecated");
        }
    }
}
